function [INTP_PROPS] = StressUpdateAtIntegrationPoints(INTP_PROPS, MATERIALS)
% StressUpdateAtIntegrationPoints updates stresses and properties required 
% for the Stokes solver (such as mu_eff, rho, Tau_old, G) at integration
% points.
% The algorithm takes the fractions of each of the phases into account,
% computes a stress update for each of the fractions seperately and than
% performs an arithmetic average of stresses & other properties at the
% integration point.
%


% To be finished. What we have already is:
%   1) Fraction of each of the phases in every element
%   2) History variables (for example temperature, strainrate, plastic strain)


% Compute densities


% Compute elastic shear module


% Update stress based on the effective viscosity creeplaw for each of the
% phases


% Take plasticity into account


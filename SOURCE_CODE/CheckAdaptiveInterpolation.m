function NUMERICS = CheckAdaptiveInterpolation(NUMERICS,MATERIAL_PROPS, MESH, CHAR, dt);
% This checks if we need to do a full-field interpolation or only an
% inremental interpolatiomn during this timestep
%


if  NUMERICS.InterpolationMethod.AdaptiveIncrementInterpolation
    % If we actually do adaptive (smart) incremental interpolation
    
    if ~isfield(NUMERICS.InterpolationMethod,'TimeSinceLastFullnterpolation')
        NUMERICS.InterpolationMethod.TimeSinceLastFullnterpolation = 0;     % as it says
    end
    time_sinceLastFull = NUMERICS.InterpolationMethod.TimeSinceLastFullnterpolation;
    
    %---
    % Compute average thermal diffusivity
    if isfield(MATERIAL_PROPS(1).HeatCapacity,'Constant');
        Cp = MATERIAL_PROPS(1).HeatCapacity.Constant.Cp*CHAR.HeatCapacity;
    else
        Cp = 1000;
    end
    if isfield(MATERIAL_PROPS(1).Density,'Constant');
        Rho = MATERIAL_PROPS(1).Density.Constant.Rho*CHAR.Density;
    else
        Rho = 3000;
    end
    if isfield(MATERIAL_PROPS(1).Conductivity,'Constant');
        k = MATERIAL_PROPS(1).Conductivity.Constant.k*CHAR.Conductivity;
    else
        k = 3;
    end
    
    
    kappa = k/Rho/Cp;                                % typical for rocks [m2/s]
    kappa = kappa/( CHAR.Length^2/CHAR.Time);        % in ND units
    %---
    
    % Compute characteristic diffusion timescale of element
    if strcmp(MESH.element_type.velocity,'T2') | strcmp(MESH.element_type.velocity,'T1')
        % 1) compute area of triangles
        TRI =   MESH.ELEMS(1:3,:);
        x   =   MESH.NODES(1,:);
        z   =   MESH.NODES(2,:);
        MeanArea = mean(polyarea(x(TRI),z(TRI)));
        
        TimeDiffusion =MeanArea/kappa;
    else
        dx = (MESH.dx_min+MESH.dx_max)/2;   % average
        dz = (MESH.dz_min+MESH.dz_max)/2;   % average
        TimeDiffusion = min([dx dz])^2/kappa;
    end
    
    TimeDiffusionFactor = NUMERICS.InterpolationMethod.DiffusionFactor;
    if round(TimeDiffusion/dt)<15
        TimeDiffusionFactor = 15;       % to prevent full interpolation to be done too often
    end
    
    TimeDiffusion = TimeDiffusion*TimeDiffusionFactor; % multiply it by a factor
    time_sinceLastFull = time_sinceLastFull + dt;
    
    if time_sinceLastFull>TimeDiffusion
        disp(['Performing full interpolation step '])
        
        NUMERICS.InterpolationMethod.InterpolateOnlyIncrements=logical(0);
        time_sinceLastFull=0;
    else
        %         disp(['Performing partial interpolation step;  time_sinceLastFull=',num2str(time_sinceLastFull),' TimeDiffusion=',num2str(TimeDiffusion)])
        NUMERICS.InterpolationMethod.InterpolateOnlyIncrements=logical(1);
    end
    
    
    % Store time
    NUMERICS.InterpolationMethod.DiffusionTime                 = TimeDiffusion;   
    NUMERICS.InterpolationMethod.TimeSinceLastFullnterpolation = time_sinceLastFull;
    
    
end
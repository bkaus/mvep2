function [BC] =   SetBC_Thermal(MESH, BC, INTP_PROPS)
% Sets boundary conditions for the thermal FEM grid
%

% $Id$


GCOORD              =       MESH.NODES;
ELEM2NODE           =       MESH.ELEMS;
Point_id            =       MESH.Point_id;
BoundaryCondition   =       BC.Energy;

switch MESH.ndim %switch by ndim
    case 2 
    
    x_min               =       min(MESH.NODES(1,:));
    x_max               =       max(MESH.NODES(1,:));
    z_min               =       min(MESH.NODES(2,:));
    z_max               =       max(MESH.NODES(2,:));

    % Construct the four corner indices
    i_botleft           =   find(abs(GCOORD(1,:)-x_min)<1e-6 & abs(GCOORD(2,:)-z_min)<1e-6);

    i_botright           =   find(abs(GCOORD(1,:)-x_max)<1e-6 & abs(GCOORD(2,:)-z_min)<1e-6);


    % i_botright          =   find(GCOORD(1,:)==x_max & GCOORD(2,:)==z_min);
    i_topleft           =   find( abs(GCOORD(1,:)-x_min) < 1e-6);
    [dummy,imax]        =   max(GCOORD(2,i_topleft));
    i_topleft           =   i_topleft(imax);

    i_topright          =   find( abs(GCOORD(1,:)-x_max) < 1e-6);
    [dummy,imax]        =   max(GCOORD(2,i_topright));
    i_topright          =   i_topright(imax);


    % Boundary indices 
    Points_right        =   find(Point_id==2);
    Points_left         =   find(Point_id==4);
    Points_bot          =   find(Point_id==1);
    Points_top          =   find(Point_id==3);

    % Remove corner indices from boundary points (might cause problems in many
    % cases)
    IndexCorners        =   [i_botright i_topright i_botleft i_topleft];        % Corner indices
    if length(IndexCorners)==4
        for i=1:length(IndexCorners)
            id                  =   find(Points_right   ==  IndexCorners(i));    Points_right(id)   =[];
            id                  =   find(Points_left    ==  IndexCorners(i));    Points_left(id)    =[];
            id                  =   find(Points_top     ==  IndexCorners(i));    Points_top(id)     =[];
            id                  =   find(Points_bot     ==  IndexCorners(i));    Points_bot(id)     =[];
        end
        
        Points_right        =   [Points_right   ];
        Points_left         =   [Points_left      ];
        Points_bot          =   [Points_bot    i_botright     i_botleft                ];
        Points_top          =   [Points_top       i_topright      i_topleft           ];
        
    end
    
    %==========================================================================
    % LEFT BOUNDARY CONDITION
    %==========================================================================
    switch lower(BoundaryCondition.Left)
        case 'isothermal'
            BCleft       = [Points_left;
                            BoundaryCondition.Value.Left*ones(size(Points_left))];
        case 'zero flux'
            BCleft      =   [];
        case 'periodic'
            BCleft      =   [];     % is set later in the code

            [dummy,ind]       = sort(GCOORD(2,Points_left));
            Points_left       = Points_left(ind);
            BCleft            = [Points_left; 1*ones(size(Points_left))];


        otherwise
            error('left thermal boundary condition is not yet implemented')
    end

    %==========================================================================
    % RIGHT BOUNDARY CONDITION
    %==========================================================================
    switch lower(BoundaryCondition.Right)
        case 'isothermal'
            BCright      = [Points_right;
                            BoundaryCondition.Value.Right*ones(size(Points_right))];
        case 'zero flux'
            BCright      =   [];
        case 'periodic'
            [dummy,ind]       = sort(GCOORD(2,Points_right));
            Points_right       = Points_right(ind);
            BCright            = [Points_right; 1*ones(size(Points_left))];

        otherwise
            error('right thermal boundary condition is not yet implemented')
    end

    %==========================================================================
    % BOTTOM BOUNDARY CONDITION
    %==========================================================================
    switch lower(BoundaryCondition.Bottom)
        case 'isothermal'
            BCbottom    = [Points_bot;
                            BoundaryCondition.Value.Bottom*ones(size(Points_bot))];
        case 'zero flux'
            BCbottom    =   [];

        case 'gaussianshapedperturbation'
            x_bottom = GCOORD(1,Points_bot);
            lambda   = BoundaryCondition.Gaussian.Bottom.GaussianHalfwidth;
            dT       = BoundaryCondition.Gaussian.Bottom.GaussianAmplitude;
            T0       = BoundaryCondition.Value.Bottom;

            T_bottom = dT*exp(-x_bottom.^2./lambda^2) + T0;

            BCbottom    = [Points_bot;
                T_bottom];


        case 'stepliketemperature'
            x_bottom =  GCOORD(1,Points_bot);
            T_bottom =  ones(size(x_bottom))*BoundaryCondition.T.StepLike.Right;
            ind      =  find(x_bottom <= BoundaryCondition.StepLike.TransitionX);
            T_bottom(ind) = BoundaryCondition.T.StepLike.Left;

            BCbottom    = [Points_bot;
                T_bottom];

        case 'stepliketemperature_mid'
            x_bottom =  GCOORD(1,Points_bot);
            T_bottom =  ones(size(x_bottom))*BoundaryCondition.T.StepLike.Rest;
            ind      =  find(x_bottom >= BoundaryCondition.StepLike.LeftX & x_bottom <= BoundaryCondition.StepLike.RightX);
            T_bottom(ind) = BoundaryCondition.T.StepLike.Step;

            BCbottom    = [Points_bot;
                T_bottom];
            
        case 'temperaturefunction'
            x_bottom =  GCOORD(1,Points_bot);
            z_bottom =  GCOORD(2,Points_bot);
            T_bottom =  ones(size(x_bottom)).*BoundaryCondition.Function.Bottom(x_bottom,z_bottom);
            BCbottom    = [Points_bot;
                T_bottom];
        otherwise
            error('bottom thermal boundary condition is not yet implemented')
    end

    %==========================================================================
    % TOP BOUNDARY CONDITION
    %==========================================================================
    switch lower(BoundaryCondition.Top)
        case 'isothermal'
            BCtop        = [Points_top;
                            BoundaryCondition.Value.Top*ones(size(Points_top))];
        case 'zero flux'
            BCtop        =   [];
        otherwise
            error('bottom thermal boundary condition is not yet implemented')
    end

    
    % For compatibility with MILAMIN
    if strcmp(lower(BoundaryCondition.Left),'periodic') && strcmp(lower(BoundaryCondition.Right),'periodic')
        BC.Energy.PERIOD 	= [BCleft(1,:); BCright(1,:); BCleft(2,:);];
        BC.Energy.BC        = [BCbottom, BCtop];
    else
        BC.Energy.BC              = [BCleft, BCbottom, BCright, BCtop];
        BC.Energy.PERIOD          = [];
    end

    
        
    
case 3  
    %==========================================================================
    % 3D Boundary Conditions
    %==========================================================================

%     x_min               =       min(MESH.NODES(1,:));
%     x_max               =       max(MESH.NODES(1,:));  
%     y_min               =       min(MESH.NODES(2,:));
%     y_max               =       max(MESH.NODES(2,:));        
%     z_min               =       min(MESH.NODES(3,:));
%     z_max               =       max(MESH.NODES(3,:));
% 
%     Points_right        =   find(GCOORD(1,:)==x_max);
%     Points_left         =   find(GCOORD(1,:)==x_min);
%     Points_back         =   find(GCOORD(2,:)==y_max);
%     Points_front        =   find(GCOORD(2,:)==y_min);
%     Points_top          =   find(GCOORD(3,:)==z_max);
%     Points_bot          =   find(GCOORD(3,:)==z_min);
Points_back =find(MESH.Point_id==5);
Points_front=find(MESH.Point_id==6);
Points_left =find(MESH.Point_id==3);
Points_right=find(MESH.Point_id==4);
Points_bot  =find(MESH.Point_id==1) ;
Points_top  =find(MESH.Point_id==2);
    
    %==========================================================================
    % LEFT BOUNDARY CONDITION
    %==========================================================================
    switch lower(BoundaryCondition.Left)
        case 'isothermal'
            BCleft       = [Points_left;
                            BoundaryCondition.Value.Left*ones(size(Points_left))];
        case 'zero flux'
            BCleft      =   [];
        case 'periodic'
            BCleft      =   [];     % is set later in the code

            [dummy,ind]       = sort(GCOORD(2,Points_left));
            Points_left       = Points_left(ind);
            BCleft            = [Points_left; 1*ones(size(Points_left))];


        otherwise
            error('left thermal boundary condition is not yet implemented')
    end

    %==========================================================================
    % RIGHT BOUNDARY CONDITION
    %==========================================================================
    switch lower(BoundaryCondition.Right)
        case 'isothermal'
            BCright      = [Points_right;
                            BoundaryCondition.Value.Right*ones(size(Points_right))];
        case 'zero flux'
            BCright      =   [];
        case 'periodic'
            [dummy,ind]       = sort(GCOORD(2,Points_right));
            Points_right       = Points_right(ind);
            BCright            = [Points_right; 1*ones(size(Points_left))];

        otherwise
            error('right thermal boundary condition is not yet implemented')
    end

    %==========================================================================
    % BOTTOM BOUNDARY CONDITION
    %==========================================================================
    switch lower(BoundaryCondition.Bottom)
        case 'isothermal'
            BCbottom    = [Points_bot;
                            BoundaryCondition.Value.Bottom*ones(size(Points_bot))];
        case 'zero flux'
            BCbottom    =   [];

        case 'gaussianshapedperturbation'
            x_bottom = GCOORD(1,Points_bot);
            lambda   = BoundaryCondition.Gaussian.Bottom.GaussianHalfwidth;
            dT       = BoundaryCondition.Gaussian.Bottom.GaussianAmplitude;
            T0       = BoundaryCondition.Value.Bottom;

            T_bottom = dT*exp(-x_bottom.^2./lambda^2) + T0;

            BCbottom    = [Points_bot;
                T_bottom];


        case 'stepliketemperature'
            x_bottom =  GCOORD(1,Points_bot);
            T_bottom =  ones(size(x_bottom))*BoundaryCondition.T.StepLike.Right;
            ind      =  find(x_bottom <= BoundaryCondition.StepLike.TransitionX);
            T_bottom(ind) = BoundaryCondition.T.StepLike.Left;

            BCbottom    = [Points_bot;
                T_bottom];

        case 'stepliketemperature_mid'
            x_bottom =  GCOORD(1,Points_bot);
            T_bottom =  ones(size(x_bottom))*BoundaryCondition.T.StepLike.Rest;
            ind      =  find(x_bottom >= BoundaryCondition.StepLike.LeftX & x_bottom <= BoundaryCondition.StepLike.RightX);
            T_bottom(ind) = BoundaryCondition.T.StepLike.Step;

            BCbottom    = [Points_bot;
                T_bottom];

 
        otherwise
            error('bottom thermal boundary condition is not yet implemented')
    end

    %==========================================================================
    % TOP BOUNDARY CONDITION
    %==========================================================================
    switch lower(BoundaryCondition.Top)
        case 'isothermal'
            BCtop        = [Points_top;
                            BoundaryCondition.Value.Top*ones(size(Points_top))];
        case 'zero flux'
            BCtop        =   [];
        otherwise
            error('bottom thermal boundary condition is not yet implemented')
    end
    
    %==========================================================================
    % BACK BOUNDARY CONDITION
    %==========================================================================
    switch lower(BoundaryCondition.Back)
        case 'isothermal'
            BCback       = [Points_back;
                            BoundaryCondition.Value.Back*ones(size(Points_back))];
        case 'zero flux'
            BCback      =   [];
        case 'periodic'
            BCback      =   [];     % is set later in the code

            [dummy,ind]       = sort(GCOORD(2,Points_back));
            Points_back       = Points_back(ind);
            BCback            = [Points_back; 1*ones(size(Points_back))];


        otherwise
            error('left thermal boundary condition is not yet implemented')
    end
    
    
    %==========================================================================
    % FRONT BOUNDARY CONDITION
    %==========================================================================
    switch lower(BoundaryCondition.Front)
        case 'isothermal'
            BCfront       = [Points_front ;
                            BoundaryCondition.Value.Front *ones(size(Points_front ))];
        case 'zero flux'
            BCfront      =   [];
        case 'periodic'
            BCfront      =   [];     % is set later in the code

            [dummy,ind]       = sort(GCOORD(2,Points_front ));
            Points_front       = Points_front (ind);
            BCfront            = [Points_front ; 1*ones(size(Points_front ))];


        otherwise
            error('front thermal boundary condition is not yet implemented')
    end
    
    
    % For compatibility with MILAMIN
    if strcmp(lower(BoundaryCondition.Left),'periodic') && strcmp(lower(BoundaryCondition.Right),'periodic')
        BC.Energy.PERIOD          = [BCback(1,:); BCfront(1,:); BCback(2,:);];                
        BC.Energy.BC              = [BCleft, BCright, BCtop, BCbottom];
    else
        BC.Energy.BC              = [BCleft, BCbottom, BCtop, BCright, BCback, BCfront];
        BC.Energy.PERIOD          = [];
    end     
               
end

%==========================================================================
% Check the BC's and remove doubles
%==========================================================================
ndim            = 1;
nnod            = size(GCOORD,2);
sdof            = ndim*nnod;
LOC2GLOB        = [1:sdof];
LOC2GLOB        = reshape(LOC2GLOB, [ndim, nnod]);
Bc_ind          = zeros(1,size(BC.Energy.BC,2));
double          = [];
for i = 1:size(BC.Energy.BC,2)
    bc_nod  = BC.Energy.BC(1,i);
    Bc_ind(i)  = LOC2GLOB(1, bc_nod);
    if length(find(Bc_ind(1:i-1)==Bc_ind(i)))>0
        double = [double, i];
    end
end
BC.Energy.BC(:,double) = [];








function [MESH, INTP_PROPS, sdof, SUCCESS] = temp2d(MESH, INTP_PROPS, BC, dt);
% 2D temperature solver, using the finite element method and techniques
% based on the MILAMIN code by Dabrowski et al.
%
% 
% ELEM2NODE       =   MESH.ELEMS;
% GCOORD          =   MESH.NODES;
% BC              =   BC.
% PERIOD          =   
% VEL             =   MESH.VEL;
% 
% nelblo          =   400;    

% $Id$

remesh = logical(1);

persistent perm_temperature

   

method          =   {'std','vec'};

if MESH.ndim==2
   method  = method{2};
elseif MESH.ndim==3;
   method  = method{1};
end
 
PERIOD          =   BC.Energy.PERIOD;
BC              =   BC.Energy.BC;
GCOORD          =   MESH.NODES;
ELEM2NODE       =   MESH.ELEMS;
nelblo          =   400;

%AddAdvection    =   logical(1);             % does not work

DisplayInfo     = 0;    % 1-display info
if exist('lchol')>0
    SolverType      = 1;    % 1-standard milamin,  2-matlab
else
    SolverType      = 2;    % 1-standard milamin,  2-matlab
end
SolverType=2;


if isfield(INTP_PROPS,'ShearHeat');
    ShearHeating    = logical(1);
else
    ShearHeating   = logical(0);
end

%==========================================================================
% ???
%==========================================================================
nnod            = size(GCOORD,2);
nel             = size(ELEM2NODE,2);


%==========================================================================
% CONSTANTS
%==========================================================================
ndim            = 1;                    %       only 1 dof per element for temperature
nip             =   size(INTP_PROPS.X,2);                                    % Set's # of integration points
nnodel          =   size(MESH.ELEMS,1);
edof            = nnodel*ndim;

%==========================================================================
% ADD 7th NODE
%==========================================================================
if nnodel==7 & size(ELEM2NODE,1)==6
    if DisplayInfo==1
        tic; fprintf(1, '6 TO 7:                 ');
    end
    ELEM2NODE(7,:)  = nnod+1:nnod+nel;
    GCOORD          = [GCOORD, [...
        mean(reshape(GCOORD(1, ELEM2NODE(1:3,:)), 3, nel));...
        mean(reshape(GCOORD(2, ELEM2NODE(1:3,:)), 3, nel))]];
    nnod        = size(GCOORD,2);
    if DisplayInfo==1
        fprintf(1, [num2str(toc),'\n']);
    end
end
sdof        = ndim*nnod;

%==========================================================================
% PERIODIZE
%==========================================================================
LOC2GLOB        = [1:sdof];
if ~isempty(PERIOD)
    if DisplayInfo==1
        tic; fprintf(1, 'PERIODIZE DOFs:         ');
    end
    Keep            = ndim*(PERIOD(1,:)-1) + PERIOD(3,:);
    Elim            = ndim*(PERIOD(2,:)-1) + PERIOD(3,:);
    Dummy           = zeros(size(LOC2GLOB));
    Dummy(Elim)     = 1;
    LOC2GLOB        = LOC2GLOB-cumsum(Dummy);
    LOC2GLOB(Elim)  = LOC2GLOB(Keep);
    if DisplayInfo==1
        fprintf(1, [num2str(toc),'\n']);
    end
end
LOC2GLOB        = reshape(LOC2GLOB, [ndim, nnod]);
LOC2GLOB        = int32(LOC2GLOB);
sdof            = max(LOC2GLOB(:));   %REDUCE GLOBAL DOF COUNT

% %==========================================================================
% % PERIODIZE
% %==========================================================================
% LOC2GLOB        = [1:sdof];
% if ~isempty(PERIOD)
%     if DisplayInfo==1
%         tic; fprintf(1, 'PERIODIZE DOFs:         ');
%     end
%     Keep            = ndim*(PERIOD(1,:)-1) + PERIOD(3,:);
%     Elim            = ndim*(PERIOD(2,:)-1) + PERIOD(3,:);
%     Dummy           = zeros(size(LOC2GLOB));
%     Dummy(Elim)     = 1;
%     LOC2GLOB        = LOC2GLOB-cumsum(Dummy);
%     LOC2GLOB(Elim)  = LOC2GLOB(Keep);
%     if DisplayInfo==1
%         fprintf(1, [num2str(toc),'\n']);
%     end
% end
% LOC2GLOB        = reshape(LOC2GLOB, [ndim, nnod]);
% LOC2GLOB        = int32(LOC2GLOB);
% sdof            = max(LOC2GLOB(:));   %REDUCE GLOBAL DOF COUNT


%==========================================================================
% BOUNDARY CONDITIONS
%==========================================================================
Bc_ind  = zeros(1,size(BC,2));
Bc_val  = zeros(1,size(BC,2));
for i = 1:size(BC,2)
    bc_nod      = BC(1,i);
    Bc_ind(i)   = LOC2GLOB(1, bc_nod);
    Bc_val(i)   = BC(2,i);
end


%==========================================================================
% BLOCKING PARAMETERS (nelblo must be < nel)
%==========================================================================
nelblo      = min(nel, nelblo);
nblo        = ceil(nel/nelblo);

%==========================================================================
% PREPARE INTEGRATION POINTS & DERIVATIVES wrt LOCAL COORDINATES
%==========================================================================
if nnodel==3 || nnodel==7
    [IP_X IP_w] = ip_triangle(nip);
    [N dNdu]    = shp_deriv_triangles(IP_X, nnodel);
elseif nnodel==4 || nnodel==9
    [IP_X IP_w] = ip_quad(nip);
    [N dNdu]    = shp_deriv_quad(IP_X, nnodel);
elseif nnodel==8 || nnodel==27
    [IP_X IP_w] = ip_3d(nip);                   
    [   N dNdu] = shp_deriv_3d(IP_X, nnodel);   
end

minDetJ     = realmax;
switch method
    
    case 'std'
        indx_l       = tril(ones(nnodel)); indx_l = indx_l(:); indx_l = indx_l==1;

        %==================================================================
        % DECLARE VARIABLES (ALLOCATE MEMORY)
        %==================================================================
        K_elem      =   zeros(nnodel,nnodel);
        Rhs_elem    =   zeros(nnodel,1);
        KK_all      =   zeros(nel, edof*(edof+1)/2);
        RHS_all     =    zeros(nel, edof);
        RHS         =   zeros(nnodel,1);
        RHS_H       =   zeros(nnodel,1);
        
        %==================================================================
        % i) ELEMENT LOOP - MATRIX COMPUTATION
        %==================================================================
        fprintf(1, 'MATRIX ASSEMBLY:    '); tic;
        for iel = 1:nel
            %==============================================================
            % ii) FETCH DATA OF ELEMENT
            %==============================================================
            ECOORD_X        =   GCOORD(:,ELEM2NODE(:,iel));
         %   Temp_elem_old   =   TEMP_OLD(ELEM2NODE(:,iel));
                       
            
            %==============================================================
            % iii) INTEGRATION LOOP
            %==============================================================
            K_elem(:)       =   0;
            Rhs_elem(:)     =   0;
            RHS(:)          =   0;
            RHS_H(:)        =   0;
            for ip=1:nip
                RHO_CP   =  INTP_PROPS.RhoCp(iel,ip);                       % density*heat capacity
                K        =  INTP_PROPS.k(iel,ip);                           % thermal conductivity
                H        =  INTP_PROPS.H(iel,ip);                           % heat production
                Told     =  INTP_PROPS.T(iel,ip);                           % old temperature
                
                if ShearHeating
                
                       ShearHeat       = INTP_PROPS.ShearHeat(iel,ip);      % shear heating
                       AdiabaticHeat   = INTP_PROPS.AdiabaticHeat(iel,ip);  % adiabatic heating  (= T*alpha*rho*g*Vz, as computed in Stokes2D_ve_UpdatedStressesStrainrates.m)
                else
                    ShearHeat       = 0;                                    % no shear-heating computed
                    AdiabaticHeat   = 0;                                    % no adiabatic heating computed
                end
           
                %==========================================================
                % iv) LOAD SHAPE FUNCTIONS DERIVATIVES FOR INTEGRATION POINT
                %==========================================================
                dNdui       =   dNdu{ip};       % derivative
                Ni          =   N{ip};         % shape function

                %==========================================================
                % v) CALCULATE JACOBIAN, ITS DETERMINANT AND INVERSE
                %==========================================================
                J           = ECOORD_X*dNdui';
                detJ        = det(J);
                invJ        = inv(J);
                minDetJ     = min([minDetJ, min(detJ)]);

                %==========================================================
                % vi) DERIVATIVES wrt GLOBAL COORDINATES
                %==========================================================
                dNdX        = dNdui'*invJ;

                %==========================================================
                % vii) NUMERICAL INTEGRATION OF ELEMENT MATRICES
                %==========================================================
                K_elem      =   K_elem      +   IP_w(ip)*detJ*( K*(dNdX*dNdX')   + RHO_CP/dt*Ni*Ni' );
                
%                 if AddAdvection
%                     K_elem  =   K_elem         IP_w(ip)*detJ*( Vx*Ni*dNdX(:,1)' + Vy*Ni*dNdX(:,1)'     );
%                 end

                RHS_H       =   RHS_H       +   IP_w(ip)*detJ*(         (H + ShearHeat + AdiabaticHeat)*Ni);
                RHS         =   RHS         +   IP_w(ip)*detJ*(   RHO_CP/dt*Ni*Told);
                
            end

            %==============================================================
            % ix) WRITE DATA INTO GLOBAL STORAGE
            %==============================================================
          %  Rhs_elem        =   RHS*(  Temp_elem_old ) + RHS_H*ones(nnodel,1); 
            Rhs_elem        =   RHS + RHS_H;
          
            KK_all(iel,:)   =   K_elem(indx_l);
            RHS_all(iel,:)  =   Rhs_elem;
            
        end
        fprintf(1, [num2str(toc),'\n']);



    case 'vec'

        if isempty(INTP_PROPS.H)
            INTP_PROPS.H = zeros(size(INTP_PROPS.T));
        end

        %==========================================================================
        % DECLARE VARIABLES (ALLOCATE MEMORY)
        %==========================================================================
        KK_all      = zeros(nel, edof*(edof+1)/2);
        RHS_all     = zeros(nel, edof);
        K_block     = zeros(nelblo,edof*(edof+1)/2);
        RHS_block   = zeros(nelblo,edof);

        invJx       = zeros(nelblo, ndim);
        invJy       = zeros(nelblo, ndim);
        il          = 1;
        iu          = nelblo;

        %==========================================================================
        % i) BLOCK LOOP - MATRIX ASSEMBLY
        %==========================================================================
        if DisplayInfo==1
            fprintf(1, 'MATRIX ASSEMBLY: '); tic;
        end
        for ib = 1:nblo
            %======================================================================
            % ii) FETCH DATA OF ELEMENTS IN BLOCK
            %======================================================================
            ECOORD_x = reshape( GCOORD(1,ELEM2NODE(:,il:iu)), nnodel, nelblo);
            ECOORD_y = reshape( GCOORD(2,ELEM2NODE(:,il:iu)), nnodel, nelblo);

            %======================================================================
            % INTEGRATION LOOP
            %======================================================================
            K_block(:)  = 0;
            RHS_block(:)= 0;

            Area        = zeros(nelblo, 1);

            TEMP_nodes    = reshape( MESH.TEMP(ELEM2NODE(:,il:iu)),    nnodel, nelblo);  
            
            for ip=1:nip

                % Compute properties @ integration points
                RHO_CP   = INTP_PROPS.RhoCp(il:iu,ip);              % density*heat capacity
                K        = INTP_PROPS.k(il:iu,ip);                  % thermal conductivity
                H        = INTP_PROPS.H(il:iu,ip);                  % heat production
                if ShearHeating
                    ShearHeat       = INTP_PROPS.ShearHeat(il:iu,ip);   % shear heating
                    AdiabaticHeat   = INTP_PROPS.AdiabaticHeat(il:iu,ip);   % adiabatic heating
                  
                else
                    ShearHeat       = zeros(size(H));                   % no shear-heating computed
                    AdiabaticHeat   = zeros(size(H));                   % no adiabatic heating computed
                end

                %==================================================================
                % iii) LOAD SHAPE FUNCTIONS DERIVATIVES FOR INTEGRATION POINT
                %==================================================================
                dNdui       =   dNdu{ip}';      % Derivative at integration point
                Ni          =   N{ip};          % Shape function at integration point
                
                
                %T_old       = INTP_PROPS.T(il:iu,ip);                  % temperature from last timestep
                T_old  =   TEMP_nodes'*Ni;  % reconstruct T at INTP from nodal T
                

                %==================================================================
                % iv) CALCULATE JACOBIAN, ITS DETERMINANT AND INVERSE
                %==================================================================
                Jx          = ECOORD_x'*dNdui;
                Jy          = ECOORD_y'*dNdui;
                detJ        = Jx(:,1).*Jy(:,2) - Jx(:,2).*Jy(:,1);
                minDetJ     = min([minDetJ, min(detJ)]);

                invdetJ     = 1.0./detJ;
                invJx(:,1)  = +Jy(:,2).*invdetJ;
                invJx(:,2)  = -Jy(:,1).*invdetJ;
                invJy(:,1)  = -Jx(:,2).*invdetJ;
                invJy(:,2)  = +Jx(:,1).*invdetJ;

                %==================================================================
                % v) DERIVATIVES wrt GLOBAL COORDINATES
                %==================================================================
                dNdx        = invJx*dNdui';
                dNdy        = invJy*dNdui';

                Ni_vec      = ones(size(invJx,1),1)*Ni';


                %==================================================================
                % vi) NUMERICAL INTEGRATION OF ELEMENT MATRICES - ONLY LOWER TRIANGLE
                %==================================================================
                weight2     = IP_w(ip)*detJ;
                weight      = weight2.*K;
                Area        = Area + weight2;

                indx = 0;
                for i = 1:nnodel
                    for j = i:nnodel
                        indx = indx + 1;
                        K_block(:,indx)     =   K_block(:,indx) + ...
                            (dNdx(:,i).*dNdx(:,j)+ dNdy(:,i).*dNdy(:,j)).*weight;

                        K_block(:,indx)     =  K_block(:,indx)   + ...
                            (RHO_CP./dt.*Ni_vec(:,i).*Ni_vec(:,j)   ).*weight2;

                    end

                    % Note: only one multiplication with temp. shape fct. here since
                    % T_old is defined @ integration points
                    RHS_block(:,i) =  RHS_block(:,i) + (RHO_CP./dt.*T_old + H + ShearHeat + AdiabaticHeat).*Ni(i).*weight2;

                end

            end

            %======================================================================
            % vii) WRITE DATA INTO GLOBAL STORAGE
            %======================================================================
            KK_all(il:iu,:)     = K_block;
            RHS_all(il:iu,:)    = RHS_block;

            %======================================================================
            % viii) READJUST START, END AND SIZE OF BLOCK. REALLOCATE MEMORY
            %======================================================================
            il  = il+nelblo;
            if(ib==nblo-1)
                nelblo      = nel-iu;
                K_block     = zeros(nelblo,      edof*(edof+1)/2);
                RHS_block	= zeros(nelblo,      edof    );
                invJx       = zeros(nelblo,      ndim);
                invJy       = zeros(nelblo,      ndim);
            end
            iu  = iu+nelblo;
        end

end         %end of switch method



if DisplayInfo==1
    etime  = toc;
    fprintf(1, ['\t',num2str(etime),'\n']);
end

if minDetJ<0
    SUCCESS = 0;    % stop computation -> grid too deformed
else
    SUCCESS = 1;
end

if SUCCESS==1 % if grid is fine
    %==========================================================================
    % ix) CREATE TRIPLET FORMAT INDICES
    %==========================================================================
    ELEM2NODE = ELEM2NODE';
    if DisplayInfo==1
        tic; fprintf(1, 'TRIPLET INDICES:  ');
    end
    ELEM_DOF = zeros(nel, edof,'int32');
    ELEM_DOF(:,1:ndim:end) = reshape(LOC2GLOB(1,ELEM2NODE),nel, nnodel);
    tj = repmat(1:edof,edof,1); ti = tj';
    ti = tril(ti); ti = ti(:); ti = ti(ti>0);
    tj = tril(tj); tj = tj(:); tj = tj(tj>0);

    
    KK_i = ELEM_DOF(:,ti);
    KK_j = ELEM_DOF(:,tj);

    KK_i    = KK_i(:);
    KK_j    = KK_j(:);
    
%     for i =1:length(KK_i)
%         if(KK_i(i)<KK_j(i))
%             tmp     = KK_i(i);
%             KK_i(i) = KK_j(i);
%             KK_j(i) = tmp;
%         end
%     end
%     
    % vectorized version of the above loop
    ind = find(KK_i<KK_j);
    tmp = KK_i(ind);
    KK_i(ind) = KK_j(ind);
    KK_j(ind) = tmp;
    
    KK_all  = KK_all(:);

%     B_i = reshape(int32(1:nel*np),nel,np);
%     B_i = repmat(B_i,edof,1);
%     B_j = repmat(ELEM_DOF,1,np);
%     if DisplayInfo==1
%         fprintf(1, ['\t',num2str(toc),'\n']);
%     end

    %==========================================================================
    % x) CONVERT TRIPLET DATA TO SPARSE MATRIX
    %==========================================================================
    if DisplayInfo==1
        fprintf(1, 'SPARSIFICATION:    '); tic
    end
    try
        KK      = sparse2(KK_i(:), KK_j(:), KK_all(:));
        Rhs     = sparse2(ELEM_DOF(:), ones(size(ELEM_DOF(:))), RHS_all(:));
    catch
        KK      = sparse (double(KK_i(:)),      double(KK_j(:)), KK_all(:));
        Rhs     = sparse (double(ELEM_DOF(:)),  ones(size(ELEM_DOF(:))), RHS_all(:));
    end
    
    clear B_i B_j B_all KK_i KK_j KK_all RHS_all;
    if DisplayInfo==1
        fprintf(1, ['\t',num2str(toc),'\n']);
    end

    %==========================================================================
    % BOUNDARY CONDITIONS
    %==========================================================================
    if DisplayInfo==1
        fprintf(1, 'BC:    '); tic;
    end
    Free        = 1:sdof;
    Free(Bc_ind)= [];
    if SolverType==1
        TMP         = KK(:,Bc_ind) + cs_transpose(KK(Bc_ind,:));
    else
        TMP         = KK(:,Bc_ind) + (KK(Bc_ind,:)).';
    end
    Rhs         = Rhs -  TMP*Bc_val';
    KK          = KK(Free,Free);
    if DisplayInfo==1
        fprintf(1, ['\t\t\t',num2str(toc),'\n']);
    end

    %==========================================================================
    % REORDERING
    %==========================================================================
    if SolverType==1
        if DisplayInfo==1
            fprintf(1, 'AMD:  '); tic;
        end
        %perm = amd(KK);
        if(remesh)
            perm_temperature = metis(KK);
        end
        if DisplayInfo==1
            fprintf(1, ['\t\t\t',num2str(toc),'\n']);
        end
        %==========================================================================
        % FACTORIZATION
        %========================================================================
        if DisplayInfo==1
            fprintf(1, 'FACTORIZATION:    '); tic;
        end
        KK = cs_transpose(KK);
        KK = cs_symperm(KK,perm_temperature);
        KK = cs_transpose(KK);
        L = lchol(KK);
        if DisplayInfo==1
            time_solve = toc;
            fprintf(1, ['\t',num2str(time_solve,'%8.6f'),'\n']);
        end
    else
        % Standard matlab solver requires full matrix
        KK1=    KK+(KK-diag(diag(KK)))';
        %KK=KK';  % cholmod requires upper triangle
    end

    %======================================================================
    % SOLVE
    %======================================================================
    TEMP         = zeros(sdof,1);
    TEMP(Bc_ind) = Bc_val;

    if SolverType==1
        TEMP(Free(perm_temperature)) = cs_ltsolve(L,cs_lsolve(L,Rhs(Free(perm_temperature))));
    elseif SolverType==2
        % Standard matlab solver
        TEMP(Free) = KK1\Rhs(Free);
        
    end

    TEMP      = TEMP(LOC2GLOB);


    
    
    
    %======================================================================
    % POSTPROCESSING: compute new temperature @ integration points
    %======================================================================
    K_block     = zeros(nelblo,edof*(edof+1)/2);
    RHS_block   = zeros(nelblo,edof);

    Tnew_Intp   = zeros(nel,nip);
    il          = 1;
    iu          = nelblo;
    for ib = 1:nblo
        %======================================================================
        % ii) FETCH DATA OF ELEMENTS IN BLOCK
        %======================================================================
        TEMP_nodes    = reshape( TEMP(ELEM2NODE(il:iu,:)'),    nnodel, nelblo);

        %======================================================================
        % INTEGRATION LOOP
        %======================================================================
        K_block(:)  = 0;
        RHS_block(:)= 0;

        for ip=1:nip
            %==================================================================
            % LOAD SHAPE FUNCTIONS DERIVATIVES FOR INTEGRATION POINT
            %==================================================================
            Ni          =   N{ip};          % Shape function at integration point

            Tnew_Intp(il:iu,ip)  =   TEMP_nodes'*Ni;

        end

        il  = il+nelblo;
        if(ib==nblo-1)
            nelblo      = nel-iu;
        end
        iu  = iu+nelblo;

    end


    INTP_PROPS.T = Tnew_Intp;   % new temperature @ integration points



end % if grid is fine

MESH.TEMP = TEMP(:)';



    




function [nonLinear_Res, Res_vec, Sol_vec_trial,INTP_PROPS, NUMERICS, OUTPUT_MAT] = NonlinearLineSearch(omega, Sol_vec0, dSol, MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, CHAR, dt)  
% Computes 

Sol_vec_trial       =   Sol_vec0 + omega*dSol;
[Res_vec,INTP_PROPS, NUMERICS, OUTPUT_MAT]           =   Stokes2d_Residual(Sol_vec_trial, MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, CHAR, dt);
nonLinear_Res   	=   norm(Res_vec)/norm(Sol_vec_trial);



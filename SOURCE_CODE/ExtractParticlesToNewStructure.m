function Particles_new  =  ExtractParticlesToNewStructure(Particles, ind, Particles_new)
% ExtractParticles
% Extracts particles from a PARTICLES structure and copies them into a new
% structure
%
% Syntax
%   Particles_new   =       ExtractParticlesToNewStructure(Particles, ind, [Particles_new]);
%
%       Input:
%               Particles       -   Particles structure
%               ind             -   an index array that indicates which  particles to extract
%               Particles_new   -   new structure to which we add these  particles
%       Output:
%               Particles_new   -   new Particles structure with new Particles added to it

% Every field in Particles and in Particles_new should be a row vector when
% entering this function. If it is not, most probably this function is NOT 
% the right place to handle this!

Pfields     = fieldnames(Particles);

if isempty(Particles_new)
    new_empty       = true;
else
    new_empty       = false;
end

% loop over fields of Particles and recursively call the same function
% again on sub-structs
for ifield  = 1:length(Pfields);
    cName   = Pfields{ifield};
    cData   = Particles.(cName);
    
    if ~isstruct(cData)
        % fields directly given on the current struct
        data_new    = cData(ind);
        if ~new_empty
            data_new    = [Particles_new.(cName), data_new];
        end
        Particles_new.(cName)   = data_new;
        
    else
        % manage sub-structs recursively
        if ~isfield(Particles_new,cName)
            Particles_new.(cName)   = [];
        end
        Particles_new.(cName)       = ExtractParticlesToNewStructure( ...
            Particles.(cName), ind, Particles_new.(cName));
        
    end
end
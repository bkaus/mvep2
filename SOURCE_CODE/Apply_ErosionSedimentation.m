function [ MESH, varargout ] = Apply_ErosionSedimentation(varargin)
% APPLY_EROSIONSEDIMENTATION
%   applies erosion and sedimentation to either the upper boundary of the computational domain
%   or to the particles, depending which method is being employed
%
% SYNTAX:
%       [ MESH, [PARTICLES, NUMERICS] ] = Apply_ErosionSedimentation(MESH,NUMERICS,dt, [PARTICLES, time])
%
%        The erosion & sedimentation methods that are currently implemented
%        in the code are defined in the NUMERICS.Erosion structure:
%
%           1)  NUMERICS.Erosion.Method = 'none'  [default, nothing is done]
%
%           2)  NUMERICS.Erosion.Method = 'Constant_SedimentationErosionRates'
%
%                   Constant erosion rate above a certain elevation and constant sedimentation rate below a certain elevation.
%                   This method is applied at the upper boundary of the computational domain
%
%                   Parameters:
%                       NUMERICS.ErosionSedimentation.Erosion_Velocity            - erosion velocity          (in m/s - is non-dimensionalized in NonDimensionalizeParameters)
%                       NUMERICS.ErosionSedimentation.Sedimentation_Velocity      - sedimentation velocity    (in m/s - is non-dimensionalized in NonDimensionalizeParameters)
%                       NUMERICS.ErosionSedimentation.ElevationBelowWhichToApplySedimentation    - critical elevation for sedimentation (in m)
%                       NUMERICS.ErosionSedimentation.ElevationAboveWhichToApplyErosion          - guess ..
%
%           3)  NUMERICS.Erosion.Method = 'FastErosionWithSedimentation_Particles'
%
%                   Infinitely fast erosion and constant sedimentation rate applied to particles.
%
%                   Parameters:
%                       NUMERICS.ErosionSedimentation.ElevationAboveWhichToApplyErosion -   level above which erosion is being applied    ( in meters - is automatically updated with the sedimentatio rate)
%                       NUMERICS.ErosionSedimentation.Sedimentation_Velocity            -   sedimentation velocity                        (in m/s - is non-dimensionalized in NonDimensionalizeParameters)
%                       NUMERICS.ErosionSedimentation.AirPhase                          -   Phase number of air/water
%                       NUMERICS.ErosionSedimentation.SedimentPhases                    -   Phase number of sediments (can be several)
%                       NUMERICS.ErosionSedimentation.SedimentationTimeInterval_years   -   Period of time during which a given sediment phase is being sedimented (in years - is nondimensionalized in NonDimensionalizeParameters)
%                       NUMERICS.ErosionSedimentation.InfinitelyFastErosion             -   Flag to indicate if we apply (fast) erosion          (=logical(1)) or not (logical(0))


MESH        =   varargin{1};
NUMERICS    =   varargin{2};
dt          =   varargin{3};
if nargin==5
    PARTICLES   =   varargin{4};
    time        =   varargin{5};
end

% In case we have a time-dependent erosion velocity, compute the
% time and the phase that is sediment
if isfield( NUMERICS.ErosionSedimentation,'TimeInterval')
    
    %  check in which time interval we are currently
    TimeInterval = NUMERICS.ErosionSedimentation.TimeInterval;
    id           = find(TimeInterval<time);
    id           = id(end)
    
    % extract the sedimentation velocity and phase that belongs to this
    % time interval
    NUMERICS.ErosionSedimentation.Sedimentation_Velocity =   NUMERICS.ErosionSedimentation.SedimentationRateTime(id);
    SedimentationPhase                  =   NUMERICS.ErosionSedimentation.PhaseSedimented(id);
    
    TimeDependentSedimentationRate = logical(1);
else
    TimeDependentSedimentationRate = logical(0);
end


switch  NUMERICS.ErosionSedimentation.Method
    case 'none'
        % Nothing to be done
        
    case 'Constant_SedimentationErosionRates'
        ErosionModel = NUMERICS.ErosionSedimentation;
        
        % Catch errors
        switch MESH.element_type.velocity
            case {'Q2','Q1'}
            otherwise
                error ('This currently only implemented for quadrilateral elements')
        end
        
        % Extract upper surface of the computational domain
        Z           = MESH.NODES(2,:);
        Z2d         = Z(MESH.RegularGridNumber);
        Topo_Z      = Z2d(end,:);                                              % vertical coordinates of free surface
        
        % ----------------------------------------
        %
        Topo_Eroded_Z   =   Topo_Z;
        
        % Apply sedimentation
        ind                 =   find(Topo_Z<ErosionModel.ElevationBelowWhichToApplySedimentation);
        Topo_Eroded_Z(ind)  =   Topo_Eroded_Z(ind) + dt*ErosionModel.Sedimentation_Velocity;            % move surface upwards
        
        % Apply erosion
        ind                 =   find(Topo_Z>ErosionModel.ElevationAboveWhichToApplyErosion);
        Topo_Eroded_Z(ind)  =   Topo_Eroded_Z(ind) - dt*ErosionModel.Erosion_Velocity;                  % move surface downwards due tp erosion
        
        
        % Put erosion back to the mesh
        Z2d(end,:)          =   Topo_Eroded_Z;
        Znew                =   Z2d(MESH.RegularGridNumber(:));
        MESH.NODES(2,:)     =   Znew;
        
    case 'FastErosionWithSedimentation_Particles'
        
        % Applies a fast erosion method to particles
        ErosionModel                =   NUMERICS.ErosionSedimentation;
        
        % Update erosion level
        ErosionModel.ElevationAboveWhichToApplyErosion   =   ErosionModel.ElevationAboveWhichToApplyErosion + ErosionModel.Sedimentation_Velocity*dt;
        
        
        
        % Phase of the air
        AirPhase                        =       ErosionModel.AirPhase;
        
        % Phase of the sediment (can change every x-years)
        if ~TimeDependentSedimentationRate
            SedimentationPhases             =       ErosionModel.SedimentPhases;
            
            iPhase                          =       mod(ceil(time/NUMERICS.ErosionSedimentation.SedimentationTimeInterval_years),length(ErosionModel.SedimentPhases));
            if iPhase==0; iPhase=length(ErosionModel.SedimentPhases); end
            SedimentationPhase              =       SedimentationPhases(iPhase);
        end
        
        
        if NUMERICS.ErosionSedimentation.InfinitelyFastErosion
            % Convert rock particles above the erosion level to air/water (fast erosion)
            ind                             =       find(PARTICLES.phases ~= AirPhase & PARTICLES.z > ErosionModel.ElevationAboveWhichToApplyErosion);
            PARTICLES.phases(ind)           =       AirPhase;
            
        end
        
        % Convert water particles below the erosion level to sediment
        ind                             =       find(PARTICLES.phases == AirPhase & PARTICLES.z <= ErosionModel.ElevationAboveWhichToApplyErosion);
        PARTICLES.phases(ind)           =       SedimentationPhase;
        
        % 
        NUMERICS.ErosionSedimentation   =       ErosionModel;
        
        
        
        % Make the top surface of the computational domain flat
        Z                   =   MESH.NODES(2,:);
        Z2d                 =   Z(MESH.RegularGridNumber);
        Topo_Z              =   Z2d(end,:);                                              % vertical coordinates of free surface
        Topo_Z              =   ones(size(Topo_Z))*mean(Topo_Z);
        
        Z2d(end,:)          =   Topo_Z;
        Znew                =   Z2d(MESH.RegularGridNumber(:));
        MESH.NODES(2,:)     =   Znew;
        
        
        z_max               =   max(MESH.NODES(2,:));   % max elevation of mesh
        
        % ensure that erosion surface is not above top of model
        if  NUMERICS.ErosionSedimentation.ElevationAboveWhichToApplyErosion>z_max
             NUMERICS.ErosionSedimentation.ElevationAboveWhichToApplyErosion = z_max;
        end
        
        
    otherwise
        error ('This Erosion type is not implemented yet');
end


% Output depending on the input parameters
if nargin==5
    varargout{1}    =   PARTICLES;
    varargout{2}    =   NUMERICS;
else
    varargout       =   [];
end


end


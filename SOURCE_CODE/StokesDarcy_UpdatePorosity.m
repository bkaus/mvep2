function [ MESH, INTP_PROPS ] = StokesDarcy_UpdatePorosity(MESH, INTP_PROPS, NUMERICS, dt)
%STOKESDARCY_UPDATEPOROSITY updates the porosity on the MESH and in
%integration points.
%   Update equation:    Dphi/Dt - (1-phi) div(v_s) = 0
%   Equivalent to: dphi/dt + v_s * grad(phi) - (1-phi) div(v_s) = 0
%   In this function only dphi/dt = (1 - phi) div(v_s) is solved, the
%   advection v_s * grad(phi) is done outside this function by moving the
%   particles with updated porosity values using the solid velocity.

%   dphi/dt = (1 - phi) div(v_s) is numerically solved with the scheme
%   phi_n+1 = phi_n + dt (1 - phi_n) div(v_s), where phi_n, phi_n+1 is the
%   current and updated porosity value, respectively.

GCOORD          =   MESH.NODES;
ELEM2NODE       =   MESH.ELEMS;
nel             =   size(ELEM2NODE,2);
nip             =   size(MESH.DARCY.ELEMS,1);
nnodel          =   size(MESH.ELEMS,1);
ndim            =   MESH.ndim;
Method          =   NUMERICS.LinearSolver.Method;

phi             =   MESH.DARCY.PHI;
phi0            =   MESH.DARCY.PHI0;
VEL             =   MESH.VEL;
div_vs_all      =   zeros(size(MESH.DARCY.ELEMS));

%% Prepare integration points and derivatives wrt local coordinates
if nnodel==3 || nnodel==7
    [IP_X, ~   ]    = ip_triangle(nip);
    [   ~, dNdu]    = shp_deriv_triangles(IP_X, nnodel);
elseif nnodel==4 || nnodel==9
    nodes = [-1  1 1 -1;% 0 1 0 1 0;
             -1 -1 1  1]';% -1 0 1 0 0];
    % [IP_X, ~   ]    = ip_quad(nip);
    [   ~, dNdu]    = shp_deriv_quad(nodes, nnodel);
elseif nnodel==8 || nnodel==27
    error('function not validated for this element');
    [IP_X, ~   ]    = ip_3d(nip);
    [   ~, dNdu]    = shp_deriv_3d(IP_X, nnodel);
end



switch Method
    
    case 'std'
        %% Element loop to compute velocity divergence
        for iel = 1:nel
            % element coordinates
            ECOORD  = GCOORD(:,ELEM2NODE(:,iel));
            % coefficients to form velocity as lin. combination of shape functions
            vel_co  = VEL(:,ELEM2NODE(:,iel))';
            
            for ic = 1:length(dNdu)
                dNdui   = dNdu{ic};
                J       = ECOORD*dNdui';
                dNdX    = dNdui'/J;
                
                div_vs_all(ic,iel)  = sum(vel_co(:) .* dNdX(:));
            end
        end
        
        
    case 'opt'
        
        
        il          =   1;
        nelblo      =   400;
        nelblo      =   min(nel, nelblo);
        nblo    	=   ceil(nel/nelblo);
        iu          =   nelblo;
        div_vs_block=   zeros(length(dNdu), nelblo);  
        for ib = 1:nblo
            div_vs_block(:) = 0;
            
            %======================================================================
            % ii) FETCH DATA OF ELEMENTS IN BLOCK
            %======================================================================
            ECOORD_x = reshape( GCOORD(1,ELEM2NODE(:,il:iu)), nnodel, nelblo);
            ECOORD_y = reshape( GCOORD(2,ELEM2NODE(:,il:iu)), nnodel, nelblo);
            VEL_x    = reshape( VEL(1,ELEM2NODE(:,il:iu)),    nnodel, nelblo);
            VEL_y    = reshape( VEL(2,ELEM2NODE(:,il:iu)),    nnodel, nelblo);
            for ip=1:length(dNdu)
                 
                dNdui       =   dNdu{ip}';      % Derivative at integration point
                 
                
                if MESH.ndim==2;
                    Jx          =   ECOORD_x'*dNdui;
                    Jy          =   ECOORD_y'*dNdui;
                    detJ        =   Jx(:,1).*Jy(:,2) - Jx(:,2).*Jy(:,1);
                    
                    invdetJ     =   1.0./detJ;
                    invJx(:,1)  =   +Jy(:,2).*invdetJ;
                    invJx(:,2)  =   -Jy(:,1).*invdetJ;
                    invJy(:,1)  =   -Jx(:,2).*invdetJ;
                    invJy(:,2)  =   +Jx(:,1).*invdetJ;
                    
                    %==================================================================
                    % DERIVATIVES wrt GLOBAL COORDINATES
                    %==================================================================
                    dNdx        =   invJx*dNdui';
                    dNdy        =   invJy*dNdui';
                    
                    for j = 1:nnodel 
                        div_vs_block(ip,:) =   div_vs_block(ip,:)  +  dNdx(:,j)'.*VEL_x(j,:) +   dNdy(:,j)'.*VEL_y(j,:);
                    end
                    
                    
                end
                
            end
            div_vs_all(:,il:iu)  = div_vs_block;
            
            
            
            il  = il+nelblo;
            if(ib==nblo-1)
                nelblo          = nel-iu;
                div_vs_block 	= zeros(length(dNdu),nelblo);
                invJx       = zeros(nelblo,      ndim);
                invJy       = zeros(nelblo,      ndim);
                if MESH.ndim==3;
                    invJz       = zeros(nelblo,      ndim);
                end
                
            end
            iu  = iu+nelblo;
            
            
            
        end
        
        
        
    otherwise
        error('Unknown method')
end


% average
if exist('sparse2','file')
    div_vs  = sparse2(1,MESH.DARCY.ELEMS(:),div_vs_all(:));
    div_vs  = div_vs ./ sparse2(1, MESH.DARCY.ELEMS(:), 1);
else
    div_vs  = sparse(1, double(MESH.DARCY.ELEMS(:)), div_vs_all(:));
    div_vs  = div_vs ./ sparse(1, double(MESH.DARCY.ELEMS(:)), 1);
end

% div_vs(1:size(MESH.DARCY.RegularGridNumber,1):end) = div_vs(2:size(MESH.DARCY.RegularGridNumber,1):end);

% zero out if sufficiently far from porosity pulse
% z_max       = max(MESH.DARCY.NODES(2,:));
% z_min       = min(MESH.DARCY.NODES(2,:));
% dist_cnt    = abs(MESH.DARCY.NODES(2,:) - .5*(z_max+z_min));
% ind         = dist_cnt > .4*(z_max-z_min); % zeros 10 per cent at bottom and top of domain
% div_vs(ind) = 0;

% store velocity divergence if it is not available from preceding time-step
if  NUMERICS.Nonlinear.CurrentNonlinearIter==1 && ~isfield(MESH,'div_vs0')
    MESH.div_vs0        = div_vs;
end

% store current velocity divergence
MESH.div_vs         = full(div_vs);

div_vs0 = MESH.div_vs0;

% implicit 1th order method
% MESH.DARCY.PHI  = phi0 + dt * (1 - phi) .* div_vs;

% 2nd order method [Keller et al. 2013, eq. 64]
MESH.DARCY.PHI      = phi0 + .5 * dt * ( (1-phi0) .* div_vs0 + (1-phi) .* div_vs);
MESH.DARCY.PHI      = max(MESH.DARCY.PHI,0);
MESH.DARCY.PHI      = min(MESH.DARCY.PHI,1);
% ind                 = find(MESH.DARCY.PHI<-eps & abs(MESH.DARCY.PHI)<1e-9);
% MESH.DARCY.PHI(ind) = -MESH.DARCY.PHI(ind);

%MESH.DARCY.PHI(find(MESH.DARCY.PHI<-eps))  = eps;

% Interpolate node data to integration points for next iteration

% [V_MARKERS]     =   einterp_MVEP2(MESH, MESH.VEL, [PARTICLES.x; PARTICLES.z], PARTICLES.elem,opts);

%% FASTER VERSION:
T                   = uint32(repmat((1:nel)',[1 size(INTP_PROPS.X,2)]));
PHI_                = einterp_MVEP2(MESH.DARCY, MESH.DARCY.PHI, [INTP_PROPS.X(:)'; INTP_PROPS.Z(:)'], T(:)'); 
INTP_PROPS.PHI      = reshape(PHI_,nel,size(INTP_PROPS.X,2));
INTP_PROPS.PHI      = max(INTP_PROPS.PHI,0);
INTP_PROPS.PHI      = min(INTP_PROPS.PHI,1);

div_vs_             = einterp_MVEP2(MESH.DARCY, MESH.div_vs, [INTP_PROPS.X(:)'; INTP_PROPS.Z(:)'], T(:)'); 
INTP_PROPS.div_vs   = reshape(div_vs_,nel,size(INTP_PROPS.X,2));

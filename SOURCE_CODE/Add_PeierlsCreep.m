function [MATERIAL_PROPS] = Add_PeierlsCreep(MATERIAL_PROPS, PhaseNumber, RockFlowLawName)
% Add_PeierlsCreep adds predefined rheological Peierls creep
% parameters to a given phase. These parameters are usually taken from the
% literature.
%
% Syntax:
%   [MATERIAL_PROPS] = Add_PeierlsCreep(MATERIAL_PROPS, Phase, RockFlowLawName)
%
%       Input:      
%               MATERIAL_PROPS  -   Structure containing material properties
%                                   for the various phases.
%               Phase           -   The number of the phase to which you want 
%                                   to add these creep law parameters.
%               RockFlowLawName -   Name of the flowlaw. See the file
%                                   Add_DislocationCreep.m to have a look at 
%                                   what is available.        
%       Output:
%               MATERIAL_PROPS  -   Updated material properties structure             
%


% We assume that the creeplaw has the form:
% 
% Mu_Peierls  =   S0./2./eII/sqrt(3).*(1-sqrt(T./E0.*log(sqrt(3).*H/2./eII)));
         
%
%   eII     -   strain rate             [1/s        ]
%   Tau     -   stress                  [Pa         ]
%   P       -   Pressure                [Pa         ]
%   R       -   Universal gas constant  [=8.3145
%   A       -   prefactor, typically given in units of [MPa^(-1)s^(-1)] or [Pa^(-1)s^(-1)]
%   n       -   powerlaw exponent       [   ]
%   E       -   Activation Energy       [J/MPA/mol]
%   V       -   Activation volums       [m^3/mol]
%   d       -   Grainsize               [in mu_m (1e-6 meter)]
%   p       -   exponent of grainsize
%
%   In addition, we take into account that the creeplaws are typically
%   measured under uniaxial or triaxial compression, whereas we need them
%   in tensorial format (this gives some geometrical factors).
%
% See also: Add_DislocationCreep, ex_ViscousRheologyProfile

% $Id: Add_DiffusionCreep.m 4900 2013-10-26 18:41:38Z lkausb $

switch RockFlowLawName
    
     case 'Goetze and Evans (1979)'
        
         % Retrieve parameters
         S0              =   8.5e9;                                         % [Pa]
         H               =   5.7*1e11;                                      % [1/s]
         E0              =   525*1e3/8.314472;                              % [J/mol]/[J/mol/K] = [K]
         ValidityStress  =   200e6;                                         % [Pa]
         Mu_Top          =   1e30;
        

    otherwise
        error(['Unknown Peierls creep parameters:  ', RockFlowLawName])
        
    
end



%% Add the creeplaw parameters to the structure



% 
% % Convert from [MPa^(-n)s^(-1)] to [Pa^(-n)s^(-1)] if required
% if MPa
%     Astar = [1e6.*A.^(-1)].^(-1);        % transfer to Pa, apply correction, transfer back to Pa^(-n)s^(-1)
% else
%     Astar = A;
% end

% Peierls creep parameters:
MATERIAL_PROPS(PhaseNumber).Viscosity.PeierlsCreep.Name               =   RockFlowLawName; 
MATERIAL_PROPS(PhaseNumber).Viscosity.PeierlsCreep.S0                 =   S0;             % Peierls stress
MATERIAL_PROPS(PhaseNumber).Viscosity.PeierlsCreep.H                  =   H;              % 
MATERIAL_PROPS(PhaseNumber).Viscosity.PeierlsCreep.E0                 =   E0;             % 
MATERIAL_PROPS(PhaseNumber).Viscosity.PeierlsCreep.ValidityStress     =   ValidityStress; % stress above which peierls creep law is valid
MATERIAL_PROPS(PhaseNumber).Viscosity.PeierlsCreep.Mu_Top             =   Mu_Top;           % Cutoff viscosity for regions that do not have peierls


end



function [PARTICLES, MESH] =  UpdatePropertiesAt_PARTICLES(MESH, PARTICLES, INTP_PROPS, MESH_OLD, INTP_PROPS_OLD, NUMERICS, BC,dt)
%UpdatePropertiesAt_PARTICLES
%
% Computes:
%  	1) Velocity, [Temperature; Other History variables] on all particles.
%   	"History variables" are variables that have a time history.
%         Example are temperature, stresses (in case of viscoelasticity).
%         They should be defined within the oroginal PARTICLES.HistVar and
%         will than automatically be updated.
%
%   2) Socalled "computational variables" on particles. These ara
%       parameters that help the computation at the next timestep but are not
%       strictly speaking necessary. Examples are strainrate and stress
%       invariants which help non-linear iterations.
%
%

% $Id$

% Set MUTILS options
opts = NUMERICS.mutils;


%% Compute velocity @ PARTICLES
[V_MARKERS]     =   einterp_MVEP2(MESH, MESH.VEL, [PARTICLES.x; PARTICLES.z], PARTICLES.elem,opts);
PARTICLES.Vx    =   V_MARKERS(1,:);
PARTICLES.Vz    =   V_MARKERS(2,:);


%% (1) UPDATE HISTORY VARIABLES ON PARTICLES
if isfield(PARTICLES,'HistVar')
    
    % Loop over all history variables and update accordingly
    if ~isfield(MESH,'HistVar')
        MESH.HistVar = [];
    end
    
    HistVar_names = fieldnames(PARTICLES.HistVar);
    
    if isfield(INTP_PROPS,'HistVar')
        HistVar_names = [HistVar_names; fieldnames(INTP_PROPS.HistVar)];
    end
    HistVar_names = unique(HistVar_names);
    
    for ifield = 1:length(HistVar_names);
        CurrentField = HistVar_names{ifield};
        
        
        switch CurrentField
            case 'T'                    % Temperature is given on nodes
                
                if isfield(MESH,'TEMP')
                    
                    if NUMERICS.InterpolationMethod.InterpolateOnlyIncrements
                        % Only interpolate temperature increments from elements to particles
                        dT                      =   einterp_MVEP2(MESH, MESH.TEMP-MESH_OLD.TEMP,  [PARTICLES.x; PARTICLES.z], PARTICLES.elem,opts);
                        
                        if size(PARTICLES.HistVar.T,1)>1
                            PARTICLES.HistVar.T = PARTICLES.HistVar.T(:)';      % should be in collumn order
                        end
                        
                        
                        T_Total               	=   einterp_MVEP2(MESH, MESH.TEMP,  [PARTICLES.x; PARTICLES.z], PARTICLES.elem,opts);
                        
                        
                        %   fac  = 0  - full interpolation
                        %   fac  = 1  - only increments are added
                        
                        fac                     =   1;
                        PARTICLES.HistVar.T  	=   fac*(PARTICLES.HistVar.T + dT) + (1-fac).*T_Total;
                        PARTICLES.HistVar.dT    =   dT;
                        
                        
                        % Interpolating only increments might result in
                        % over/undershoot of T
                        
                        
                        
                    else
                        % Interpolate full temperature field:
                        PARTICLES.HistVar.T 	=   einterp_MVEP2(MESH, MESH.TEMP,                [PARTICLES.x; PARTICLES.z], PARTICLES.elem,opts);
                    end
                end
                
                if ~isempty(BC.Energy.BC)
                    % Correct T for temperatures that are lower than the
                    % boundary temperatures (likely due to roundoff errors)
                    ind = find(PARTICLES.HistVar.T<  min(BC.Energy.BC(2,:)) );
                    if ~isempty(ind)
                        PARTICLES.HistVar.T(ind)  = min(BC.Energy.BC(2,:));
                    end
                else
                    % use the temperatures on the integration points
                    ind = find(PARTICLES.HistVar.T<  min(INTP_PROPS.T(:)) );
                    if ~isempty(ind)
                        PARTICLES.HistVar.T(ind)  = min(INTP_PROPS.T(:));
                    end
                end
                
            case 'WaterFree' 
                % extrapolate water from INTP_PROPS to MESH
                MESH.HistVar.WaterFree  =   Extrapolate_IntpDataToNodes(MESH,INTP_PROPS.WaterFree, NUMERICS);
                MESH.HistVar.WaterFree(MESH.HistVar.WaterFree<0) = 0;
                MESH.HistVar.WaterFree(MESH.HistVar.WaterFree>100)=100;
                if isfield(INTP_PROPS_OLD,'WaterFree')
                    % Only interpolate free water increments from elements to particles
                    INTP_PROPS_OLD.WaterFree(INTP_PROPS_OLD.WaterFree<0)    =   0;
                    INTP_PROPS.WaterFree(INTP_PROPS.WaterFree<0)            =   0;
                    Mesh_dFree                 =   Extrapolate_IntpDataToNodes(MESH,INTP_PROPS.WaterFree-INTP_PROPS_OLD.WaterFree, NUMERICS);
                    dFree                      =   einterp_MVEP2(MESH, Mesh_dFree,  [PARTICLES.x; PARTICLES.z], PARTICLES.elem,opts);
                    if size(PARTICLES.HistVar.WaterFree,1)>1
                        PARTICLES.HistVar.WaterFree = PARTICLES.HistVar.WaterFree(:)';      % should be in collumn order
                    end
                    Free_Total               	=   einterp_MVEP2(MESH, MESH.HistVar.WaterFree,  [PARTICLES.x; PARTICLES.z], PARTICLES.elem,opts);
                    
                    
                    %   fac  = 0  - full interpolation
                    %   fac  = 1  - only increments are added
                    fac                         =   1;
                    PARTICLES.HistVar.WaterFree =   fac*(PARTICLES.HistVar.WaterFree + dFree) + (1-fac).*Free_Total;
                    PARTICLES.HistVar.WaterFree(PARTICLES.HistVar.WaterFree<0)=0;
                    PARTICLES.HistVar.WaterFree(PARTICLES.HistVar.WaterFree>max(MESH.HistVar.WaterFree(:)))=max(MESH.HistVar.WaterFree(:));
                end
      
            case 'WaterBound'
                % extrapolate water from INTP_PROPS to MESH
                MESH.HistVar.WaterBound  =   Extrapolate_IntpDataToNodes(MESH,INTP_PROPS.WaterBound, NUMERICS);
                MESH.HistVar.WaterBound(MESH.HistVar.WaterBound<0) = 0;
                if isfield(INTP_PROPS_OLD,'WaterBound')
                    % Only interpolate bound water increments from elements to particles
                    INTP_PROPS_OLD.WaterBound(INTP_PROPS_OLD.WaterBound<0)    =   0;
                    INTP_PROPS.WaterBound(INTP_PROPS.WaterBound<0)            =   0;
                    Mesh_dBound                 =   Extrapolate_IntpDataToNodes(MESH,INTP_PROPS.WaterBound-INTP_PROPS_OLD.WaterBound, NUMERICS);
                    dBound                      =   einterp_MVEP2(MESH, Mesh_dBound,  [PARTICLES.x; PARTICLES.z], PARTICLES.elem,opts);
                    if size(PARTICLES.HistVar.WaterBound,1)>1
                        PARTICLES.HistVar.WaterBound = PARTICLES.HistVar.WaterBound(:)';      % should be in collumn order
                    end
                    Bound_Total               	=   einterp_MVEP2(MESH, MESH.HistVar.WaterBound,  [PARTICLES.x; PARTICLES.z], PARTICLES.elem,opts);
                    
                    
                    %   fac  = 0  - full interpolation
                    %   fac  = 1  - only increments are added
                    fac                         =    1;
                    PARTICLES.HistVar.WaterBound =   fac*(PARTICLES.HistVar.WaterBound + dBound) + (1-fac).*Bound_Total;
                    PARTICLES.HistVar.WaterBound(PARTICLES.HistVar.WaterBound<0)=0;
                    PARTICLES.HistVar.WaterBound(PARTICLES.HistVar.WaterBound>max(MESH.HistVar.WaterBound(:)))=max(MESH.HistVar.WaterBound(:));
                end
                
            case {'Txx','Txy','Tyy'}    % Stresses
                if NUMERICS.InterpolationMethod.InterpolateOnlyIncrements
                    
                    % initialize to zero if required
                    if ~isfield(MESH.HistVar     ,CurrentField)
                        MESH.HistVar        =   setfield(MESH.HistVar,CurrentField,         Extrapolate_IntpDataToNodes(MESH, getfield(INTP_PROPS,CurrentField), NUMERICS ) );
                    end
                    
                    % Only increments are added
                    dFIELD_DATA             =   getfield(INTP_PROPS.Stress,CurrentField)    -   getfield(INTP_PROPS_OLD.Stress,CurrentField);
                    dMESH_DATA              =   Extrapolate_IntpDataToNodes(MESH, dFIELD_DATA, NUMERICS ) ;                            % extrapolate to nodes
                    dPARTICLES_DATA         =   einterp_MVEP2(MESH, dMESH_DATA, [PARTICLES.x; PARTICLES.z], PARTICLES.elem,opts);  % interpolate from nodes -> particles
                    
                    
                    
                    MESH.HistVar          	=   setfield(MESH.HistVar,CurrentField,         getfield(MESH.HistVar     ,CurrentField) + dMESH_DATA);                  	% store data on mesh
                    if size(getfield(PARTICLES.HistVar,CurrentField),1)>1
                        PARTICLES_DATA   	=   getfield(PARTICLES.HistVar,CurrentField);
                        PARTICLES.HistVar	=   setfield(PARTICLES.HistVar,CurrentField, PARTICLES_DATA(:)');               % should be in collumn order
                    end
                    PARTICLES.HistVar       =   setfield(PARTICLES.HistVar,CurrentField,    getfield(PARTICLES.HistVar,CurrentField) + dPARTICLES_DATA);               	% store data on particles
                    
                else
                    % Full stresses is used for interpolation
                    FIELD_DATA              =   getfield(INTP_PROPS.Stress,CurrentField);
                    MESH_DATA               =   Extrapolate_IntpDataToNodes(MESH, FIELD_DATA, NUMERICS ) ;                            % extrapolate to nodes
                    PARTICLES_DATA          =   einterp_MVEP2(MESH, MESH_DATA, [PARTICLES.x; PARTICLES.z], PARTICLES.elem,opts);  % interpolate from nodes -> particles
                    
                    MESH.HistVar          	=   setfield(MESH.HistVar,CurrentField,         MESH_DATA);                  	% store data on mesh
                    PARTICLES.HistVar       =   setfield(PARTICLES.HistVar,CurrentField,    PARTICLES_DATA);               	% store data on particles
                end
                
            case 'PHI'
                if NUMERICS.InterpolationMethod.InterpolateOnlyIncrements
                    % Only interpolate melt fraction increments from elements to particles
                    dPHI                    =   einterp_MVEP2(MESH.DARCY, MESH.DARCY.PHI-MESH.DARCY.PHI0, [PARTICLES.x; PARTICLES.z], PARTICLES.elem, opts);
                    
                    if not(isrow(PARTICLES.HistVar.PHI))
                        PARTICLES.HistVar.PHI   = PARTICLES.HistVar.PHI(:)'; % enforce row shape of data
                    end
                    
                    PARTICLES.HistVar.PHI  	=   PARTICLES.HistVar.PHI + dPHI;
                    PARTICLES.HistVar.dPHI  =   dPHI;
                    
                    % do not apply cut-off to PHI here because slightly
                    % negative values on particles should be allowed to be
                    % advected and to affect interpolation to integration
                    % points. cut-off is applied after interpolation to
                    % integration points, cp. ParticlesToIntegrationPoints
                else
                    % Interpolate full temperature field:
                    PARTICLES.HistVar.PHI 	=   einterp_MVEP2(MESH.DARCY, MESH.DARCY.PHI, [PARTICLES.x; PARTICLES.z], PARTICLES.elem, opts);
                end
                
            case 'Pf'
                PARTICLES.HistVar.Pf        = einterp_MVEP2(MESH.DARCY, MESH.DARCY.Pf, [PARTICLES.x; PARTICLES.z], PARTICLES.elem, opts);
            case 'Pc'
                if NUMERICS.InterpolationMethod.InterpolateOnlyIncrements
                    dP                      =   einterp_MVEP2(MESH.DARCY,MESH.DARCY.Pc-MESH.DARCY.Pc0,[PARTICLES.x; PARTICLES.z],PARTICLES.elem, opts);
                    
                    if not(isrow(PARTICLES.HistVar.Pc))
                        PARTICLES.HistVar.Pc    = PARTICLES.HistVar.Pc(:)';
                    end
                    
                    PARTICLES.HistVar.Pc        = PARTICLES.HistVar.Pc + dP;
                else
                    PARTICLES.HistVar.Pc        = einterp_MVEP2(MESH.DARCY, MESH.DARCY.Pc, [PARTICLES.x; PARTICLES.z], PARTICLES.elem, opts);
                end                
            case 'vf_x'
            %    vf_x                    =   Extrapolate_IntpDataToNodes(MESH, INTP_PROPS.vf_x, NUMERICS);
                vf_x_part               =   einterp_MVEP2(MESH.DARCY, MESH.DARCY.vf_x, [PARTICLES.x; PARTICLES.z], PARTICLES.elem, opts);
                
                if not(isrow(vf_x_part))
                    vf_x_part           =   vf_x_part(:)';
                end
                
                PARTICLES.HistVar.vf_x  =   vf_x_part;
            %    MESH.HistVar.(CurrentField) = vf_x; % store data on mesh
            
            case 'vf_z'
            %    vf_z                    =   Extrapolate_IntpDataToNodes(MESH, INTP_PROPS.vf_z, NUMERICS);
                vf_z_part               =   einterp_MVEP2(MESH.DARCY, MESH.DARCY.vf_z, [PARTICLES.x; PARTICLES.z], PARTICLES.elem, opts);
                
                if not(isrow(vf_z_part))
                    vf_z_part           =   vf_z_part(:)';
                end
                
                PARTICLES.HistVar.vf_z  =   vf_z_part;
            %    MESH.HistVar.(CurrentField) = vf_z; % store data on mesh
                
            otherwise
                % If we have history variables that are no 'special' cases
                % they should simply be present in INTP_PROPS
                if ~isfield(INTP_PROPS,CurrentField)
                    error(['I cannot find the HistoryVariable ',CurrentField, ' in the integration points structure INTP_PROPS'])
                end
                
                % if this is not the case, proceed as usual
                %                 if NUMERICS.InterpolationMethod.InterpolateOnlyIncrements & ( (~strncmp(CurrentField,'MeltFraction_PhaseDiagram',25)) & (~strncmp(CurrentField,'MeltFraction',12)) )
                if NUMERICS.InterpolationMethod.InterpolateOnlyIncrements && (~strncmp(CurrentField,'PHI',12) )
                    
                    %                 if NUMERICS.InterpolationMethod.InterpolateOnlyIncrements & ( (~strncmp(CurrentField,'MeltFraction_PhaseDiagram',12)))
                    
                    % MeltFraction_PhaseDiagram and MeltFraction should be
                    % fully interpolated (not incrementally), as melt
                    % extraction messes up things otherwise
                    
                    if ~isfield(MESH.HistVar     ,CurrentField)
                        FIELD_DATA      	=   getfield(INTP_PROPS,CurrentField);
                        MESH_DATA           =   Extrapolate_IntpDataToNodes(MESH, FIELD_DATA, NUMERICS);
                        MESH.HistVar        =   setfield(MESH.HistVar,CurrentField,   MESH_DATA );
                    end
                    
                    % Only increments are added
                    dFIELD_DATA            =   getfield(INTP_PROPS,CurrentField)    -   getfield(INTP_PROPS_OLD,CurrentField);
                    
                    dMESH_DATA             =   Extrapolate_IntpDataToNodes(MESH, dFIELD_DATA, NUMERICS ) ;                            % extrapolate to nodes
                    dPARTICLES_DATA        =   einterp_MVEP2(MESH, dMESH_DATA, [PARTICLES.x; PARTICLES.z], PARTICLES.elem,opts);  % interpolate from nodes -> particles
                    dPARTICLES_DATA(dPARTICLES_DATA>max(dMESH_DATA(:))) = max(dMESH_DATA(:));           % minimizes overshoots
                    
                    MESH.HistVar           =   setfield(MESH.HistVar,CurrentField,         getfield(MESH.HistVar     ,CurrentField) + dMESH_DATA);                  	% store data on mesh
                    if size(getfield(PARTICLES.HistVar,CurrentField),1)>1
                        PARTICLES_DATA   	=   getfield(PARTICLES.HistVar,CurrentField);
                        PARTICLES.HistVar	=   setfield(PARTICLES.HistVar,CurrentField, PARTICLES_DATA(:)');               % should be in collumn order
                    end
                    PARTICLES.HistVar      =   setfield(PARTICLES.HistVar,CurrentField,     getfield(PARTICLES.HistVar,CurrentField) + dPARTICLES_DATA);               	% store data on particles
                    
                else
                    
                    FIELD_DATA              =   getfield(INTP_PROPS,CurrentField);
                    %                     if strncmp(CurrentField,'MeltFraction',12)
                    %                         FIELD_DATA = repmat(mean(FIELD_DATA,2),[1 size(FIELD_DATA,2)]);
                    %                     end
                    
                    
                    
                    %% EXACTLY THIS EXTRAPOLATION/INTERPOLATION
                    
                    MESH_DATA               =   Extrapolate_IntpDataToNodes(MESH, FIELD_DATA, NUMERICS ) ;                            % extrapolate to nodes
                    PARTICLES_DATA          =   einterp_MVEP2(MESH, MESH_DATA, [PARTICLES.x; PARTICLES.z], PARTICLES.elem,opts);  % interpolate from nodes -> particles
                    PARTICLES_DATA(PARTICLES_DATA>max(MESH_DATA(:))) = max(MESH_DATA(:));           % minimizes overshoots
                    
                    if strncmp(CurrentField,'MeltFraction',12)
                        % no negative melt fractions, whoch might occur due
                        % to extrapolation errors
                        MESH_DATA(MESH_DATA<0)              = 0;
                        PARTICLES_DATA(PARTICLES_DATA<0)    = 0;
                    end
                    
                    MESH.HistVar          	=   setfield(MESH.HistVar,CurrentField,         MESH_DATA);                  	% store data on mesh
                    PARTICLES.HistVar       =   setfield(PARTICLES.HistVar,CurrentField,    PARTICLES_DATA);               	% store data on particles
                    
                    
                    
                    
                    
                end
        end
        
    end
    
end



%% (2) UPDATE COMPUTATIONAL VARIABLES ON PARTICLES
% Computational variables are those variables that are useful to have as
% an initial guess for the next timestep.
%
% Note: we always extrapolate the full fields, instead of only increments.

% Names of fields to be extrapolated from INTP->Particles
% Names of fields to be extrapolated from INTP->Particles
INTP_NameList       =   {'Strainrate.E2nd'  , 'Strainrate.E2nd_vp'  , 'Stress.T2nd',  'Mu_Eff','Mu_Vis',   'Rho',  'G', 'Plastic','Pressure','Stress.T2nd_new','MeltFraction', 'ExtractedMelt','MeltFraction_PhaseDiagram','MeltFraction_katz'};
PART_NameList       =   { 'E2nd'            ,     'E2nd_vp'     	, 'T2nd',         'Mu_Eff','Mu_Vis',   'Rho',  'G', 'Plastic','Pressure','T2nd_new'       ,'MeltFraction', 'ExtractedMelt','MeltFraction_PhaseDiagram','MeltFraction_katz'};
ExtrapolateLog      =   [    	1           ,              1    	,   1   ,            1    ,      1   ,     0 ,   0,   0      ,   0      ,  1               , 0,                0           , 0                         , 0                  ];      % interpolate logarithm or the absolute value?
AveragePerElement   =   [    	0           ,              0      	,   1   ,            0    ,      0   ,     0 ,   0,   0      ,   0      ,  1               , 0,                0           , 0                         , 0                  ];      % average properties over the element before extrapolating?


% Add strainrates 
INTP_NameList       =   [{'Strainrate.Exx','Strainrate.Eyy','Strainrate.Exy','Stress.Txx','Stress.Tyy','Stress.Txy'    },  INTP_NameList];
PART_NameList       =   [{'Exx',              'Eyy',             'Exy',      'Txx','Tyy','Txy'    },  PART_NameList];
ExtrapolateLog      =   [0,                     0,                 0,       0     , 0   , 0       ExtrapolateLog];      % interpolate logarithm or the absolute value?
AveragePerElement   =   [0,                     0,                 0,       0     , 0   , 0        AveragePerElement ];      % average properties over the element before extrapolating?



if NUMERICS.TwoPhase
    INTP_NameList       =   [INTP_NameList, 'Rho_f','Mu_f','kphi','Zeta_Eff','Kphi','Pc','Pf'];
    PART_NameList       =   [PART_NameList, 'Rho_f','Mu_f','kphi','Zeta_Eff','Kphi','Pc','Pf'];
    ExtrapolateLog      =   [ExtrapolateLog,    0,    1,     0,        1,       1,   0,   0];      % interpolate logarithm or the absolute value?
    AveragePerElement   =   [AveragePerElement, 1,    1,     1,        1,       1,   0,   0];
end
if isfield(INTP_PROPS,'WaterCapacity')
    INTP_NameList       =   [INTP_NameList,'WaterCapacity'];
    PART_NameList       =   [PART_NameList,'WaterCapacity'];
    ExtrapolateLog      =   [ExtrapolateLog,     0        ];
    AveragePerElement   =   [AveragePerElement,  1        ];
end
if NUMERICS.ComputeFiniteStrain
    INTP_NameList       =   [INTP_NameList,'Strainrate.Exx','Strainrate.Eyy','Strainrate.dVxdy','Strainrate.dVydx'];
    PART_NameList       =   [PART_NameList,    'Gxx'       ,      'Gyy'     ,       'Gxy'      ,     'Gyx'        ];
    ExtrapolateLog      =   [ExtrapolateLog,     0         ,        0       ,          0       ,        0         ];
    AveragePerElement   =   [AveragePerElement,  0         ,        0       ,          0       ,        0         ];
end

% Interpolate all the fields
MESH.CompVar        =   [];
if ~isfield(PARTICLES,'CompVar');
    PARTICLES.CompVar   =   [];
end
for iField=1:length(INTP_NameList)
    
    % The data at the integration points
    if isfield(INTP_PROPS,INTP_NameList{iField}) || ~isempty(strfind(INTP_NameList{iField},'.'))
        % if the field actually exists
        DATA_INTP       =   eval(['INTP_PROPS.',INTP_NameList{iField}]);
        
        if AveragePerElement(iField)==1
            % Average the properties of the integration points over the element (circumvents overshoots)
            DATA_INTP          =   repmat(mean(DATA_INTP,2),[1 size(DATA_INTP,2)]);
        end
        
        
        if ExtrapolateLog(iField)==1
            DATA_INTP   =   log10(DATA_INTP);                                           % we only extrapolate the logarithm of this field (more stable)
        end
        [data]          =   Extrapolate_IntpDataToNodes(MESH, DATA_INTP, NUMERICS);                          % extrapolate from INTP -> NODES
        
        if ExtrapolateLog(iField)==1
            data        =   10.^data;
        end
        MESH.CompVar    =   setfield(MESH.CompVar,PART_NameList{iField},         data);
        
        % Apply cutoffs
        switch INTP_NameList{iField}
            case 'Mu_Eff'
                MESH.CompVar.Mu_Eff( MESH.CompVar.Mu_Eff<NUMERICS.Viscosity.LowerCutoff) = NUMERICS.Viscosity.LowerCutoff;
                
                MESH.CompVar.Mu_Eff( MESH.CompVar.Mu_Eff>NUMERICS.Viscosity.UpperCutoff) = NUMERICS.Viscosity.UpperCutoff;
            case 'Stress.T2nd'
                MESH.CompVar.T2nd( MESH.CompVar.T2nd<0 ) = 0;
            case 'Strainrate.E2nd'
                MESH.CompVar.E2nd( MESH.CompVar.E2nd<0 ) = 0;
            case 'Strainrate.E2nd_vp'
                MESH.CompVar.E2nd_vp( MESH.CompVar.E2nd_vp<0 ) = 0;
            case 'Stress.T2nd_new'
                MESH.CompVar.T2nd_new( MESH.CompVar.T2nd_new<0 ) = 0;
            case 'kphi'
                MESH.CompVar.kphi( MESH.CompVar.kphi<0 ) = 0;
        end
        
        
        DATA_MESH       =   eval(['MESH.CompVar.',PART_NameList{iField}]);
        if ExtrapolateLog(iField)==1
            DATA_MESH	=   log10(DATA_MESH);                                           % we only extrapolate the logarithm of this field (more stable)
        end
        
        PART_DATA       =   einterp_MVEP2(MESH, DATA_MESH, [PARTICLES.x; PARTICLES.z], PARTICLES.elem,opts);
        PART_DATA(PART_DATA>max(DATA_MESH(:))) = max(DATA_MESH(:));                     % somewhat minimized overshoots
        PART_DATA(PART_DATA<min(DATA_MESH(:))) = min(DATA_MESH(:));
        
        if ExtrapolateLog(iField)==1
            PART_DATA   =   10.^PART_DATA;
        end
        
        PARTICLES.CompVar = setfield(PARTICLES.CompVar,PART_NameList{iField}, PART_DATA);
        
        % Compute the components of the incremental strain tensor
        % otherwise setfield
        if NUMERICS.ComputeFiniteStrain
            switch INTP_NameList{iField}
                case 'Strainrate.Exx'
                    Gxx = 1 + PART_DATA * dt;
                case 'Strainrate.Eyy'
                    Gyy = 1 + PART_DATA * dt;
                case 'Strainrate.dVxdy'
                    Gxy = PART_DATA * dt;
                case 'Strainrate.dVydx'
                    Gyx = PART_DATA * dt;
            end
        end
        
    end
end

% Compute the finite strain tensor
% SYNTAX: Gij are the components of the incremental strain tensor.
%         Fij are the components of the finite strain tensor.
% Nomenclature taken from M. Frehner & S. Schmalholz 2006
if NUMERICS.ComputeFiniteStrain
    % Initial finite strain tensor
    if ~isfield(PARTICLES.CompVar,'Fxx')
        PARTICLES.CompVar.Fxx = ones(size(Gxx));
        PARTICLES.CompVar.Fxy = ones(size(Gxy));
        PARTICLES.CompVar.Fyx = ones(size(Gyx));
        PARTICLES.CompVar.Fyy = ones(size(Gyy));
    end
    
    Fxx = PARTICLES.CompVar.Fxx;
    Fxy = PARTICLES.CompVar.Fxy;
    Fyx = PARTICLES.CompVar.Fyx;
    Fyy = PARTICLES.CompVar.Fyy;
    
    Fxx = Fxx .* Gxx + Fxy .* Gyx;
    Fxy = Fxx .* Gxy + Fxy .* Gyy;
    Fyx = Fyx .* Gxx + Fyy .* Gyx;
    Fyy = Fyx .* Gxy + Fyy .* Gyy;
    
    % Set the CompVar for the finite strain tensor
    PARTICLES.CompVar = setfield(PARTICLES.CompVar,'Fxx',Fxx);
    PARTICLES.CompVar = setfield(PARTICLES.CompVar,'Fxy',Fxy);
    PARTICLES.CompVar = setfield(PARTICLES.CompVar,'Fyx',Fyx);
    PARTICLES.CompVar = setfield(PARTICLES.CompVar,'Fyy',Fyy);
    
    %remove Gij from particles
    PARTICLES.CompVar = rmfield(PARTICLES.CompVar,'Gxx');
    PARTICLES.CompVar = rmfield(PARTICLES.CompVar,'Gyy');
    PARTICLES.CompVar = rmfield(PARTICLES.CompVar,'Gxy');
    PARTICLES.CompVar = rmfield(PARTICLES.CompVar,'Gyx');
    
end
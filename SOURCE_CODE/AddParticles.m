function Particles   =  AddParticles(Particles, Particles_new)
% AddParticles
%
% Adds all particles in the structure Particles_new to the structure PARTICLES
%
% Syntax
%   Particles   =	AddParticles(Particles, Particles_new );
%
%       Input:
%               Particles       -   Particles structure
%               Particles_new   -   new structure to which we add these  particles
%
%       Output:
%               Particles       -   Particles structure with new Particles added to it

% (Ragnar:) rewrote function in a recursive manner
% function relies on assumption that *every* field in PARTICLES (or its 
% sub-structs) is stored as a row vector

Pfields     = fieldnames(Particles);
for ifield = 1:length(Pfields)
    cName   = Pfields{ifield};
    cData   = Particles_new.(cName);
    
    if ~isstruct(cData)
        % combine previously given data_old with new cData from Particles_new
        data_old            = Particles.(cName);
        Particles.(cName)   = [data_old, cData];
    else
        % recursively call this function on any sub-struct
        Particles.(cName)   = AddParticles(Particles.(cName), Particles_new.(cName));
    end
    
end

end

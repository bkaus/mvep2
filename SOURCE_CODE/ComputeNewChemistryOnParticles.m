function PARTICLES = ComputeNewChemistryOnParticles(PARTICLES, MATERIAL_PROPS, CHAR)
% Takes P,T conditions on PARTICLES and a phase diagram and interpolates
% the liquid/solid chemistry of the phase diagram on the particles.
%
% PARTICLES = ComputeNewChemistryOnParticles(PARTICLES, MATERIAL_PROPS, CHAR)
%
%   Input:
%       PARTICLES
%       MATERIAL_PROPS
%       CHAR
%
%   Output:
%       PARTICLES


if size(PARTICLES.x,1)>size(PARTICLES.x,2)
    % Bring them in the correct shape if needed
    PARTICLES.x         = PARTICLES.x(:)';
    PARTICLES.z         = PARTICLES.z(:)';
    PARTICLES.phase     = PARTICLES.phase(:)';
    PARTICLES.HistVar.T = PARTICLES.HistVar.T(:)';
    end


if ~isfield(PARTICLES,'Chemistry')
    % Particles do not have chemistry on them yet. Here, we load the
    % InitialPhase diagram and interpolate chemistry from the diagram to
    % the particles
    
    if ~isfield(PARTICLES,'CompVar');
        % We might not have pressure defined yet on PARTICLES
        depth                       =   abs(PARTICLES.z-max(PARTICLES.z))*CHAR.Length;
        PARTICLES.CompVar.Pressure  =   3200*10*depth/CHAR.Stress;
    end
    
    % The melt fraction is re-computed based on current P,T conditions
    PARTICLES.Chemistry.MeltFraction    =   zeros(size(PARTICLES.x));
    
    
    for iphase=1:length(MATERIAL_PROPS) % loop over all phase
        
        if ~isempty(MATERIAL_PROPS(iphase).Chemistry)
            % Load relevant phase diagram
            filename = [MATERIAL_PROPS(iphase).Chemistry.Directory,filesep,MATERIAL_PROPS(iphase).Chemistry.InitialPhaseDiagram];
            load(filename,'Chemistry','Melt','rho_melt','rho_withoutMelt', 'P_Pa','T_K'); % load relevant data of phase diagram
            
            % interpolate all properties on particles
            
            % These are the relevant articles, for which this phase
            % diagram applies
            ind             =   find(PARTICLES.phases==iphase);
            
            % Interpolate chemistry and melt fraction
            PARTICLES       =   InterpolateRelevantFields(Chemistry,Melt, rho_melt, rho_withoutMelt, ind, PARTICLES, CHAR, P_Pa, T_K);
            
        end
    end
    
else

    
    RelevantPhaseDiagram_old = PARTICLES.Chemistry.RelevantPhaseDiagram;  %save the relevant phase diagram from the previous step    
    
    % Particles already have chemistry. We now look at the relevant phase
    % diagrams (the one that is closest in chemistry) and interpolate
    % properties
    
    if ~isfield(PARTICLES,'MeltExtraction');
        PARTICLES.MeltExtraction= [];
    end
        
    if ~isfield(PARTICLES.MeltExtraction,'ExtractedMelt');
        % Extracted melt is kept on particless
        PARTICLES.MeltExtraction.ExtractedMelt = zeros(size(PARTICLES.x));
    end
    
    if ~isfield(PARTICLES,'MeltFractionOld');
        PARTICLES.MeltFractionOld = zeros(size(PARTICLES.x));
    end

    % The melt fraction is re-computed based on current P,T conditions
    PARTICLES.Chemistry.MeltFraction    =   zeros(size(PARTICLES.x));
    
    
    for iphase=1:length(MATERIAL_PROPS) % loop over all phase
        
        if ~isempty(MATERIAL_PROPS(iphase).Chemistry)
            
            % 1) Particles that have never been molten, will use the initial phase
            % diagram to update properties
            
            % Load initial phase diagram
            filename = [MATERIAL_PROPS(iphase).Chemistry.Directory,filesep,MATERIAL_PROPS(iphase).Chemistry.InitialPhaseDiagram];
            load(filename,'Chemistry','Melt','rho_melt','rho_withoutMelt', 'P_Pa','T_K'); % load relevant data of phase diagram
            
            % find relevant particles
            ind             =   find(PARTICLES.phases==iphase & PARTICLES.MeltExtraction.ExtractedMelt<1e-6);
            
            % Interpolate chemistry on particles from initial phase diagram
            PARTICLES       =   InterpolateRelevantFields(Chemistry,Melt, rho_melt, rho_withoutMelt, ind, PARTICLES, CHAR, P_Pa, T_K);
            PARTICLES.Chemistry.RelevantPhaseDiagram(ind) = 0;
            
            % 2) Particles that have been molten before, need to know which
            % phase diagram is valid for them.
            
            % a) Compute relevant phase diagram (based on the closest match
            % in terms of chemistry)
            
            indMeltFraction     =   find(PARTICLES.phases==iphase & PARTICLES.MeltFractionOld>MATERIAL_PROPS(iphase).MeltExtraction.M_critical & PARTICLES.MeltExtraction.ExtractedMelt>1e-6); %find out where melt extraction occured during the previous time step            
            indMeltExtracted   	=   find(PARTICLES.phases==iphase & PARTICLES.MeltExtraction.ExtractedMelt>1e-6);
            
            BulkChemistry                       =   MATERIAL_PROPS(iphase).Chemistry.BulkChemistry;
            InterpolationPrecisionDecimalDigits =   MATERIAL_PROPS(iphase).Chemistry.InterpolationPrecisionDecimalDigits;
            PARTICLES                           =   ComputeClosestPhaseDiagram(PARTICLES, BulkChemistry, InterpolationPrecisionDecimalDigits , indMeltFraction,RelevantPhaseDiagram_old);
            
            
            % b) Loop over each of the phase diagrams & update properties
            %  on particles
            PhaseDiagrams   =   unique(PARTICLES.Chemistry.RelevantPhaseDiagram(indMeltExtracted));
            PhaseDiagrams(PhaseDiagrams==0)   =   [];                 % the initial diagram was already processed
            
            for iDiagram=1:length(PhaseDiagrams)
                % Fetch correct phase diagram and load it
                PhaseDiagramNumber  =   PhaseDiagrams(iDiagram);
                PhaseDiagramName    =   MATERIAL_PROPS(iphase).Chemistry.FileNames{PhaseDiagramNumber};
                filename            =   [MATERIAL_PROPS(iphase).Chemistry.Directory,filesep,PhaseDiagramName];
                load(filename,'Chemistry','Melt','rho_melt','rho_withoutMelt', 'P_Pa','T_K'); % load relevant data of phase diagram
                
                if any(isnan(Melt(:)))
                    error(['The phase diagram ',filename, ' has NaNs in Melt - Fix this!'])
                end
                
                % find relevant particles
                ind             =   find(PARTICLES.phases(:)==iphase & PARTICLES.Chemistry.RelevantPhaseDiagram(:)==PhaseDiagramNumber & PARTICLES.MeltExtraction.ExtractedMelt(:)>1e-6);
                
                % Interpolate chemistry on particles from initial phase diagram
                PARTICLES       =   InterpolateRelevantFields(Chemistry,Melt, rho_melt, rho_withoutMelt, ind, PARTICLES, CHAR, P_Pa, T_K);
            
                
            end
        end
    end
end

if any(isnan(PARTICLES.Chemistry.MeltFraction))
    error('Melt Fraction is NaN at some point, which indicates a problem with the phase diagram')
end




% Set some of the relevant fields to be History or Computational Variables
% such that they are automatically interpolated to integration points & are
% available for computing densities @ integration points etc.
PARTICLES.CompVar.MeltFraction          =   PARTICLES.Chemistry.MeltFraction;
PARTICLES.CompVar.DensitySolid          =   PARTICLES.Chemistry.DensitySolid;
PARTICLES.CompVar.DensityLiquid         =   PARTICLES.Chemistry.DensityLiquid;


%==========================================================================
function PARTICLES = InterpolateRelevantFields(Chemistry,Melt, DensityLiquid, DensitySolid, ind, PARTICLES, CHAR, P_Pa,T_K)
% This:
%
%   1) Initializes PARTICLES.Chemistry.{Solid/Liquid} with all chemical
%       components if this field does not yet exist
%
%   2) If the field exists, it interpolates the chemical components (such
%       as SiO2, etc.) from the phase diagram to the particles, for the subset
%       of particles indicated in ind.
%
%   3) Interpolates the melt fraction to the subset of particles from the
%       phase diagram.


if ~isfield(PARTICLES,'Chemistry')
    PARTICLES.Chemistry = [];
end

if ~isfield(PARTICLES.Chemistry,'RelevantPhaseDiagram')
    PARTICLES.Chemistry.RelevantPhaseDiagram = zeros(size(PARTICLES.x),'uint32');      % phase diagram 0 = InitialPhaseDiagram
end

% (P,T) conditions for which we interpolate values

% DIMENSIONALIZE IT!!!!
P_Particles     =   PARTICLES.CompVar.Pressure(ind)*CHAR.Stress;    % in Pa
T_Particles     =   PARTICLES.HistVar.T(ind)*CHAR.Temperature;      % in Kelvin, NOT Celcius! Phase diagrams MUST be defined in K as well!

% Find particles that are outside the validity range of diagram
id = find(P_Particles<min(P_Pa));
if ~isempty(id)
    warning([num2str(length(id)),' particles have a pressure that is lower than the min(P) of the phase diagram'])
    P_Particles(id) = min(P_Pa);
end
id = find(P_Particles>max(P_Pa));
if ~isempty(id)
    warning([num2str(length(id)),' particles have a pressure that is larger than the max(P) of the phase diagram'])
    P_Particles(id) = max(P_Pa);
end

id = find(T_Particles<min(T_K) ); 
if ~isempty(id)
    warning([num2str(length(id)),' particles have a temperature that is smaller than the min(T) of the phase diagram'])
    T_Particles(id) = min(T_K);
end
id = find(T_Particles>max(T_K) );
if ~isempty(id)
    warning([num2str(length(id)),' particles have a temperature that is larger than the max(T) of the phase diagram'])
    T_Particles(id) = max(T_K);
end




% Loop over the chemistry
Agregation  = fieldnames(Chemistry);
for iAgg = 1:length(Agregation) % usually either solid of liquid
    Components      =   getfield(Chemistry,Agregation{iAgg});     % extract all components of this field
    ComponentNames  =   fieldnames(Components);
    
    % Loop over all chemical components and interpolate properties to
    % particles
    
    for iC = 1:length(ComponentNames)
        if ~isfield(PARTICLES.Chemistry,Agregation{iAgg})
            PARTICLES.Chemistry = setfield(PARTICLES.Chemistry,Agregation{iAgg},[]);
        end
        
        % Initialize field to NaN's if they do not exist yet
        if ~isfield(getfield(PARTICLES.Chemistry,Agregation{iAgg}),ComponentNames{iC})
            PARTICLES.Chemistry = setfield(PARTICLES.Chemistry,Agregation{iAgg},ComponentNames{iC},ones(size(PARTICLES.HistVar.T))*NaN);     % initialize fields
        end
        
        Field               =   getfield(Chemistry          ,   Agregation{iAgg},   ComponentNames{iC});        % Chemical component of phase diagram
        Values              =   getfield(PARTICLES.Chemistry,   Agregation{iAgg},   ComponentNames{iC});        % NaN's first time we do this
        
        Values(ind)         =   interp2(P_Pa,T_K, Field,P_Particles,T_Particles,'*linear');                               % interpolate chemisttry from phase diagram -> particles
        PARTICLES.Chemistry =   setfield(PARTICLES.Chemistry,   Agregation{iAgg},   ComponentNames{iC},Values); % set back
        
    end
    
    AllComponent            =   getfield(PARTICLES.Chemistry,Agregation{iAgg}); % extract all components of this field
    AllComponentNames       =   fieldnames(AllComponent);
    ind_diffvalues          =   find(ismember(AllComponentNames,ComponentNames)==0); %find the difference in chemical components from particles and phase diagram
    
    % If the chemical components from loaded phase diagram is unequal the chemical components on the particles, set the values too zero 
    for iAC = 1:length(ind_diffvalues)
        PARTICLES.Chemistry =   setfield(PARTICLES.Chemistry,   Agregation{iAgg},   AllComponentNames{ind_diffvalues(iAC)},zeros(size(Values)));   
    end
    
end

% Interpolate Melt content from phase diagram
PARTICLES.Chemistry.MeltFraction(ind(:))   =   interp2(P_Pa,T_K, Melt,P_Particles,T_Particles);

% Solid density
if ~isfield(PARTICLES.Chemistry,'DensitySolid')
    PARTICLES.Chemistry.DensitySolid = ones(size(PARTICLES.x))*NaN;
end
PARTICLES.Chemistry.DensitySolid(ind)   =   interp2(P_Pa,T_K, DensitySolid,P_Particles,T_Particles);

% Liquid density
if ~isfield(PARTICLES.Chemistry,'DensityLiquid')
    PARTICLES.Chemistry.DensityLiquid = ones(size(PARTICLES.x))*NaN;
end
PARTICLES.Chemistry.DensityLiquid(ind)  =   interp2(P_Pa,T_K, DensityLiquid,P_Particles,T_Particles);

% Nondimensionalize variables
PARTICLES.Chemistry.DensitySolid(ind)   =   PARTICLES.Chemistry.DensitySolid(ind)/CHAR.Density;
PARTICLES.Chemistry.DensityLiquid(ind)  =   PARTICLES.Chemistry.DensityLiquid(ind)/CHAR.Density;




%==========================================================================
function PARTICLES = ComputeClosestPhaseDiagram(PARTICLES, BulkChemistryPhaseDiagrams, InterpolationPrecisionDecimalDigits, ind,RelevantPhaseDiagram_old)
% This finds the phase diagram that has a bulk chemistry that is closest to
% that of the PARTICLES

Names  =   fieldnames(BulkChemistryPhaseDiagrams);
num_oxides = length(Names);

ParticleCompositionRounded          =   zeros(length(PARTICLES.x(:)), num_oxides);
PhaseDiagramCompositionRound        =   zeros(length(BulkChemistryPhaseDiagrams.SiO2(:))  , num_oxides);

% Collect data into matrixes & round the chemical composition
for iname = 1:length(Names)
    
    ParticleCompositionRounded(:,iname)         =   getfield( PARTICLES.Chemistry.Solid, Names{iname});
    PhaseDiagramCompositionRounded(:,iname)     =   getfield(BulkChemistryPhaseDiagrams, Names{iname});
    
    
    ParticleCompositionRounded(:,iname)         =   roundsd(ParticleCompositionRounded    (:,iname), getfield(InterpolationPrecisionDecimalDigits,Names{iname}));
    PhaseDiagramCompositionRounded(:,iname)     =   roundsd(PhaseDiagramCompositionRounded(:,iname), getfield(InterpolationPrecisionDecimalDigits,Names{iname}));
   
    Precision(iname) = 10.^(-1*getfield(InterpolationPrecisionDecimalDigits,Names{iname})); % with how many digits accuracy do we compute this diagram?
end

ParticleCompositionRounded = ParticleCompositionRounded(ind,:);

% use kdtree-like algorithm
% NOTE: We should really have separate wrapper routine for finding the
% nearest point which can call a kdtree algorithm based on which system we
% are on
if exist('kdtree')==3      % KDTREE mex file exists
    tree                    =    kdtree(PhaseDiagramCompositionRounded);
    [PhaseDiagram,pntval]   =    kdtree_closestpoint(tree,ParticleCompositionRounded);
    


elseif exist('knnsearch')==2    % Alternative routine
    [PhaseDiagram] =    knnsearch(ParticleCompositionRounded,PhaseDiagramCompositionRounded);

else
    error('KDTREE and KNNSEARCH cannot be found; are directories specified ok?')
end


if ~isfield(PARTICLES.Chemistry,'CompositionalOffset')
    PARTICLES.Chemistry.CompositionalOffset = zeros(size(PARTICLES.x));
end

PARTICLES.Chemistry.RelevantPhaseDiagram(ind)   =   PhaseDiagram;

%If melt is extracted it should never use the same phase diagram after
%each other on the same particle

ParticlesRelevantPhaseDiagram_new  = PARTICLES.Chemistry.RelevantPhaseDiagram(ind); %find particles where melt extraction is present
ParticlesRelevantPhaseDiagram_old2 = RelevantPhaseDiagram_old(ind); % use the information from the relevant phase diagrams from the previous time step, where melt extraction is present in the current time step

ind_DiffRelevantPhaseDiagram       = find(ParticlesRelevantPhaseDiagram_new==ParticlesRelevantPhaseDiagram_old2); %compare the used phase diagrams from the current and previous time step
   
EqualPhases                        = unique(ParticlesRelevantPhaseDiagram_new(ind_DiffRelevantPhaseDiagram)); %find which used phase diagram is the same in the previous and current time step on the same particle at which melt extraction is active
 
for i = 1:length(EqualPhases)
    
    num_diagram   =  EqualPhases(i); 
     
    PhaseDiagramCompositionRounded2                = PhaseDiagramCompositionRounded;
    PhaseDiagramCompositionRounded2(num_diagram,:) = 0; %set all oxide values too zero from the phase diagram which did not change after melt extraction 
     
    %find the phase diagram number which did not change and replace it
    %with the next closest bulk chemistry diagram
    ind2 = ind(ind_DiffRelevantPhaseDiagram);
    ind1 = find(PARTICLES.Chemistry.RelevantPhaseDiagram(ind2)==num_diagram);

    ParticleCompositionRounded_new = ParticleCompositionRounded(ind_DiffRelevantPhaseDiagram(ind1),:); 

    if exist('kdtree')==3      % KDTREE mex file exists
       tree                        =    kdtree(PhaseDiagramCompositionRounded2);
       [PhaseDiagram,pntval_new]   =    kdtree_closestpoint(tree,ParticleCompositionRounded_new);
  
    elseif exist('knnsearch')==2    % Alternative routine
       [PhaseDiagram] =    knnsearch(ParticleCompositionRounded_new,PhaseDiagramCompositionRounded2);
    else
       error('KDTREE and KNNSEARCH cannot be found; are directories specified ok?')
    end
    
    PARTICLES.Chemistry.RelevantPhaseDiagram(ind2(ind1))               =   PhaseDiagram;
 
    ParticleCompositionRounded(ind_DiffRelevantPhaseDiagram(ind1),:)   =  ParticleCompositionRounded_new;
 
    if exist('kdtree')==3      % KDTREE mex file exists
       pntval(ind_DiffRelevantPhaseDiagram(ind1),:) = pntval_new;
    end
   
end

if exist('kdtree')==3      % KDTREE mex file exists
   Error       =   (ParticleCompositionRounded-pntval);  % misfit
   Error       =   Error./repmat(Precision,[size(pntval,1) 1]);    % normalize over precision
   Error       =   int32(Error);
end


if ~isfield(PARTICLES.Chemistry,'CompositionalError')
    for jname=1:num_oxides
        PARTICLES.Chemistry.CompositionalError.(Names{jname}) = zeros(size(PARTICLES.x));
    end
end

matrix_CompositionalError = zeros(num_oxides, length(PARTICLES.x));
matrix_CompositionalError(:,ind)              =   Error';

for jname = 1:length(Names)
    PARTICLES.Chemistry.CompositionalError.(Names{jname}) = ...
        PARTICLES.Chemistry.CompositionalError.(Names{jname}) + matrix_CompositionalError(jname,:);
end
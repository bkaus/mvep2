function [INTP_PROPS, NUMERICS, MATERIAL_PROPS] =  UpdatePropertiesAt_INTPS_PhaseRatio(INTP_PROPS, MATERIAL_PROPS,NUMERICS, CHAR, dt, MESH);
%UpdatePropertiesAt_INTPS
%
% Updates effective viscosity, density and elasticity at every integration
% point, such that we can feed the results into the Stokes solver.
% This routine uses the ratio of each of the phases at integration points,
% which has been determined at the beginning of the timestep.

% $Id: UpdatePropertiesAt_INTPS.m 5037 2013-11-25 09:17:39Z lkausb $

%% Initialize
nip                 =   size(INTP_PROPS.X,2);           % # of integration points
nel                 =   size(INTP_PROPS.X,1);           % # of elements

INTP_PROPS.Mu_Eff           =   zeros(nel,nip);
INTP_PROPS.Mu_Vis            =   zeros(nel,nip);
INTP_PROPS.Rho              =   zeros(nel,nip);

INTP_PROPS.G                =   zeros(nel,nip);
INTP_PROPS.Chi_Tau          =   zeros(nel,nip);
INTP_PROPS.Plastic          =   zeros(nel,nip);                 % did the point fail plastically or not?
if ~isfield(INTP_PROPS,'Stress')
    % used in 2D or 3D
    INTP_PROPS.Stress.Txx      =    zeros(nel,nip);
    INTP_PROPS.Stress.Txy      =    zeros(nel,nip);
    INTP_PROPS.Stress.Tyy      =    zeros(nel,nip);
    
    if NUMERICS.ndim==3; % only 3D
        INTP_PROPS.Stress.Tzz      =    zeros(nel,nip);
        INTP_PROPS.Stress.Tyz      =    zeros(nel,nip);
        INTP_PROPS.Stress.Txz      =    zeros(nel,nip);
    end
end
INTP_PROPS.Tau_yield=   zeros(nel,nip);

if isfield(INTP_PROPS,'T')
    INTP_PROPS.H        =   zeros(nel,nip);     % radioactive heat
    INTP_PROPS.k        =   zeros(nel,nip);     % conductivity
    INTP_PROPS.Cp       =   zeros(nel,nip);     % heat capacity
    if NUMERICS.LatentHeat % If there Latent Heat is not activated, alpha is
        %not computed in the integration point, and the code works as usual
    INTP_PROPS.alpha    =   zeros(nel,nip);
   
    end
    dMdT_Phase          =    [];
end

if isfield(INTP_PROPS,'PHI')
    INTP_PROPS.Kphi             = zeros(nel,nip);
    INTP_PROPS.Zeta_Eff         = zeros(nel,nip);
    INTP_PROPS.Chi_P            = zeros(nel,nip);
    INTP_PROPS.P_yield   = zeros(nel,nip);
    INTP_PROPS.kphi             = zeros(nel,nip);
    INTP_PROPS.Mu_f             = zeros(nel,nip);
    INTP_PROPS.Rho_f            = zeros(nel,nip);
end
if isfield(INTP_PROPS,'MeltFraction_PhaseDiagram')
    INTP_PROPS.MeltFraction_PhaseDiagram    =   zeros(nel,nip);
end
if isfield(INTP_PROPS,'WaterBound')
    INTP_PROPS.WaterCapacity    =   zeros(nel,nip);
    if isfield(MATERIAL_PROPS(1).Water,'SimplifiedDarcy')
        INTP_PROPS.V_WaterPercolation = zeros(nel,nip);
    elseif isfield(MATERIAL_PROPS(1).Water,'ConstantVelocity')
        INTP_PROPS.Water_Vx     =   zeros(nel,nip);
        INTP_PROPS.Water_Vz     =   zeros(nel,nip);
    end
end
if isfield(INTP_PROPS,'MeltFraction_katz')
    INTP_PROPS.MeltDensity      =   zeros(nel,nip);
end
if isfield(MATERIAL_PROPS(1),'EffectiveViscosityPartiallyMoltenRocks')
    INTP_PROPS.MeltViscosity    =   zeros(nel,nip);
end

%% Loop over phases, and compute the properties per phase



%==========================================================================
if isfield(NUMERICS.Plasticity,'NonLocalPlasticity')
    % Nonlocal plasticity is active
    switch NUMERICS.Plasticity.NonLocalPlasticity.Type
        case 'Gradient_Strainrate'
            
%             % Non-local smoothening of plastic parameters:
%             NonLocalRadius  = 	NUMERICS.Plasticity.NonLocalPlasticity.Radius;
%             if isfield(INTP_PROPS,'Strainrate')
%                 eII             =   INTP_PROPS.Strainrate.E2nd;
%                 
%                 [eII_NonLocal]  =   GradientPlasticity_DiffusionStep(MESH, (eII), NUMERICS, NonLocalRadius);
% %                 eII_NonLocal    =   10.^eII_NonLocal;
%                 
%                 alpha           =	NUMERICS.Plasticity.NonLocalPlasticity.alpha;
%                 eII_Plastic     =	(1-alpha)*eII + alpha*eII_NonLocal;
%                 INTP_PROPS.Strainrate.E2nd_vp = eII_Plastic;
%                 
%                 INTP_PROPS.Strainrate.E2nd    = eII_Plastic;
%                 INTP_PROPS.Strainrate.E2nd_nonlocal    = eII_Plastic;
%                 
%                 
% %                 INTP_PROPS.Strain  	=   INTP_PROPS.StrainNonLocal + INTP_PROPS.Strainrate.E2nd_nonlocal*dt;     % Update strain during iterations
%                 
%             end
                 
        case 'Gradient_Strain'
            % not within this routine
            
        otherwise
            error('Unknown gradient plasticity type')
    end
end
%==========================================================================




% In a first step, we compute densities, elastic properties and energy
% related quantities (on whoch viscosity does not dependent in a nonlinear
% manner)
for iphase=1:length(MATERIAL_PROPS)
    Proportion          =   repmat(INTP_PROPS.PHASE_PROP(:,iphase),[1 nip]);
    
    
    %% 1) Compute densities
    % In case we use phase diagrams, density is known already at
    % integration points, and we don't need to add it here. In case we use
    % CHEMISTRY, we already compute the correct bulk density
    [ Rho_s , MATERIAL_PROPS(iphase).Density, INTP_PROPS] =       ComputeDensity(INTP_PROPS, MATERIAL_PROPS(iphase).Density, CHAR, NUMERICS);
    if NUMERICS.TwoPhase
        switch iphase
            case 1
                INTP_PROPS.Rho_s   =  Rho_s;
            case 2
                % fluid phase
            otherwise
                error(['Two-Phase code cannot handle more than two phases, yet. ' ...
                    'Phase 1: (Crystallized) Rock, Phase 2: Silicate Melt.']);
        end
    else
        INTP_PROPS.Rho             =  INTP_PROPS.Rho +   Proportion.*Rho_s;
        INTP_PROPS.Rho_s           =  INTP_PROPS.Rho;
    end
    
    if isfield(INTP_PROPS,'PHI')
        Rho_f               =  ComputeFluidDensity(INTP_PROPS, MATERIAL_PROPS(iphase).FluidDensity);
        INTP_PROPS.Rho_f    =  INTP_PROPS.Rho_f + Proportion.*Rho_f;
    end
    
    % Compute the melt fraction in the current phase diagram
    if isfield(INTP_PROPS,'MeltFraction_PhaseDiagram')
        % This reads off the melt fraction from a phase diagram
        %   * This is the maximum melt fraction that can exist at any given location.
        %   * The 'real' melt fraction will depend on how much was already
        %   extracted to the surface during previous events, how much the
        %   rock was depleted and other parameters. This will be computed
        %   elsewhere. It computes even the dMdT (variation of melt in
        %   function of temperature at the same temperature) and the dMdP
        %   (variation of melt in function of pressure at the same
        %   temperature). 
        [MeltFraction_PhaseDiagram,dMdT_Phase,dMdP_Phase]          =   ComputeMeltFractionFluidDensity_PhaseTransitions(INTP_PROPS,  MATERIAL_PROPS(iphase).Density, CHAR, NUMERICS);

       INTP_PROPS.MeltFraction_PhaseDiagram  =   INTP_PROPS.MeltFraction_PhaseDiagram  + Proportion.*MeltFraction_PhaseDiagram;
    end
    
    if isfield(INTP_PROPS,'MeltFraction')
        [~,dMdT_Phase,dMdP_Phase]          =   ComputeMeltFractionFluidDensity_PhaseTransitions(INTP_PROPS,  MATERIAL_PROPS(iphase).Density, CHAR, NUMERICS);
    end
                
    
    if isfield(INTP_PROPS,'WaterBound')
        [ WaterCapacity , MATERIAL_PROPS(iphase).Water] =       ComputeWater(INTP_PROPS, MATERIAL_PROPS(iphase).Water, CHAR, NUMERICS);
        INTP_PROPS.WaterCapacity                        =       INTP_PROPS.WaterCapacity +   Proportion.*WaterCapacity;
        if isfield(MATERIAL_PROPS(iphase).Water,'SimplifiedDarcy')
            V_percolation                               =       ComputeWaterPercolation_SimlifiedDarcy(INTP_PROPS,MATERIAL_PROPS(iphase).Water.SimplifiedDarcy,CHAR,NUMERICS);
            INTP_PROPS.V_WaterPercolation               =       INTP_PROPS.V_WaterPercolation + Proportion.*V_percolation;
        elseif isfield(MATERIAL_PROPS(iphase).Water,'ConstantVelocity')
            INTP_PROPS.Water_Vx                         =       INTP_PROPS.Water_Vx      + Proportion.*MATERIAL_PROPS(iphase).Water.ConstantVelocity.Vx;
            INTP_PROPS.Water_Vz                         =       INTP_PROPS.Water_Vz      + Proportion.*MATERIAL_PROPS(iphase).Water.ConstantVelocity.Vz;
        end
    end
    if isfield(MATERIAL_PROPS(iphase),'MeltFractionLaw')
        [MeltFraction,MeltDensity]                      =       ComputeMeltFractionFluidDensity_Parameterized(INTP_PROPS,MATERIAL_PROPS(iphase).MeltFractionLaw,MATERIAL_PROPS(iphase).MeltDensityLaw,CHAR);
        INTP_PROPS.MeltFraction_katz                    =       INTP_PROPS.MeltFraction_katz + Proportion.*MeltFraction;
        INTP_PROPS.MeltDensity                          =       INTP_PROPS.MeltDensity + Proportion.*MeltDensity;
    end
    
    %% 2) Compute parameters for the energy equation if required
    if isfield(INTP_PROPS,'T')
        [ H ]        	=       ComputeRadioactiveElements(INTP_PROPS,  MATERIAL_PROPS(iphase).RadioactiveHeat);
        INTP_PROPS.H  	=       INTP_PROPS.H + Proportion.*H;
        
        [ Cp ]        	=       ComputeHeatCapacity(INTP_PROPS,MATERIAL_PROPS(iphase).LatentHeat ,MATERIAL_PROPS(iphase).HeatCapacity,dMdT_Phase,CHAR,NUMERICS);
        INTP_PROPS.Cp 	=       INTP_PROPS.Cp + Proportion.*Cp;
        
        [ K ]        	=       ComputeThermalConductivity(INTP_PROPS,  MATERIAL_PROPS(iphase).Conductivity, CHAR);
        INTP_PROPS.k 	=       INTP_PROPS.k + Proportion.*K;
       
        %If Latent heat is not compute, simply jump the computation and
        %initialisation of this property in the code. 
        if NUMERICS.LatentHeat
        [ alpha ]        	=       ComputeThermalExpansivity(INTP_PROPS,MATERIAL_PROPS(iphase).ThermalExpansivity,MATERIAL_PROPS(iphase).LatentHeat ,dMdP_Phase,CHAR,NUMERICS);
        INTP_PROPS.alpha 	=       INTP_PROPS.alpha + Proportion.*alpha;
        end
    end
    
    %% 3) Compute elastic shear and compaction moduli
    % Stresses are stored as History Variables on Particles, so they should
    % be known already
    G       =  ones(size(INTP_PROPS.X))*MATERIAL_PROPS(iphase).Elasticity.Constant.G;
    % Correct elasticity to avoid jumps in elastic props in an element
    G_vec = min(G,[],2);
    for i=1:size(G,2)
        G(:,i) = G_vec;
    end
    
    % elastic pore modulus
    if isfield(INTP_PROPS,'PHI')
        Kphi    =  ComputePoreModulus(INTP_PROPS,MATERIAL_PROPS(iphase).Elasticity);
        % Correct elasticity to avoid jumps in elastic props in an element
        Kphi_vec = min(Kphi,[],2);
        for i=1:size(Kphi,2)
            Kphi(:,i) = Kphi_vec;
        end
    end
    

    
    %% 4) Compute Visco-Elasto-Plastic Shear and Compaction Rheology

    % 4a) This is the viscous creep viscosity, which can include powerlaw rheology,
    % combined creeplaws etc.
    Mu_vis  =  ComputeEffectiveViscosity_ViscousCreep(INTP_PROPS,MATERIAL_PROPS(iphase).Viscosity, NUMERICS, CHAR, dt);
    % 4b) Apply melt weakening and initialise compaction and fluid viscosity if melt
    % fraction present
    if isfield(INTP_PROPS,'PHI')
        [Zeta_vis]          =   BulkViscosity(MATERIAL_PROPS(iphase).BulkViscosity, Mu_vis, INTP_PROPS, CHAR);
        [Mu_vis]            =   MeltWeakening(MATERIAL_PROPS(iphase).MeltWeakening, Mu_vis, INTP_PROPS, CHAR);
        
        
        if isfield(MATERIAL_PROPS(iphase),'Permeability')
            Permeability    =   ComputePermeability(INTP_PROPS, MATERIAL_PROPS(iphase).Permeability);
            INTP_PROPS.kphi =   INTP_PROPS.kphi + Proportion.*Permeability;
        end
        
        FluidViscosity    =  ComputeFluidViscosity(INTP_PROPS, MATERIAL_PROPS(iphase).FluidViscosity, CHAR);
        INTP_PROPS.Mu_f   =  INTP_PROPS.Mu_f + Proportion.*FluidViscosity;
    end
    
    INTP_PROPS.Mu_Vis                       = INTP_PROPS.Mu_Vis     + Proportion.*Mu_vis; % effective creep viscosity with melt weakening
    
    % 4c) Compute shear and compaction plastic failure
    %
    [Mu_viscoplastic  ,Plastic,Tau_y] 	=   ComputeEffectiveViscosity_Plasticity(INTP_PROPS,MATERIAL_PROPS(iphase).Plasticity, MATERIAL_PROPS, NUMERICS, Mu_vis, G, dt, MESH);     % effective viscosity in case of plasticity
    if isfield(INTP_PROPS,'PHI')
        [Zeta_eff,~,P_y]        =   ComputeCompactionViscosity_Plasticity(INTP_PROPS,MATERIAL_PROPS(iphase).Plasticity, Zeta_vis, Kphi, dt);
    end

    % 4d) Compute elastic weakening to complete visco-elasto-plastic
    % effective shear and compaction viscosities, Mu_eff, Zeta_eff
    % Compute trial stress, based on viscous creep law and old stress
    Chi_Tau     =   1./( 1                   + G.*dt./Mu_viscoplastic);         % VISCO-ELASTIC STRESS EVOLUTION PARAMETER
    Mu_eff      =   1./( 1./Mu_viscoplastic  +    1./(G*dt));                   % EFFECTIVE VISCO-ELASTIC(-PLASTIC) VISCOSITY
    if isfield(INTP_PROPS,'PHI')
        Chi_P   =   1./( 1          + Kphi.*dt./Zeta_eff ); % VISCO-ELASTIC STRESS EVOLUTION PARAMETER
        Zeta_eff=   1./( 1./Zeta_eff+        1./(Kphi*dt)); % EFFECTIVE VISCO-ELASTIC(-PLASTIC) VISCOSITY
    end
    
    INTP_PROPS.G                            = INTP_PROPS.G          + Proportion.*G;
    INTP_PROPS.Mu_Eff                       = INTP_PROPS.Mu_Eff     + Proportion.*Mu_eff;           % viscoelastoplastic viscosity
    INTP_PROPS.Chi_Tau                      = INTP_PROPS.Chi_Tau    + Proportion.*Chi_Tau;
    INTP_PROPS.Plastic(logical(Plastic))    = 1;
    INTP_PROPS.Tau_yield                    = INTP_PROPS.Tau_yield  + Proportion.*Tau_y;
    if isfield(INTP_PROPS,'PHI')
        INTP_PROPS.Kphi                     = INTP_PROPS.Kphi       + Proportion.*Kphi;
        INTP_PROPS.Zeta_Eff                 = INTP_PROPS.Zeta_Eff   + Proportion.*Zeta_eff;
        INTP_PROPS.Chi_P                    = INTP_PROPS.Chi_P      + Proportion.*Chi_P;
        INTP_PROPS.P_yield                  = INTP_PROPS.P_yield    + Proportion.*P_y;
    end
    
end


if isfield(INTP_PROPS,'T')
    if isfield(MATERIAL_PROPS(1).Density,'RayleighConvection')
        INTP_PROPS.RhoCp = INTP_PROPS.Cp;  % rho*Cp
    else
        INTP_PROPS.RhoCp = INTP_PROPS.Rho.*INTP_PROPS.Cp;  % rho*Cp
    end
end


% In case we have a MeltFraction present: update density and viscosity
% according to the melt fraction
if isfield(INTP_PROPS,'PHI')
    PHI        =   INTP_PROPS.PHI;
    
    % Arithmethic average
    INTP_PROPS.Rho      =   (1-PHI).*INTP_PROPS.Rho_s +   PHI.*INTP_PROPS.Rho_f;
    
    % Compute effective viscosity of partially molten rocks:
    if MATERIAL_PROPS(1).EffectiveViscosityPartiallyMoltenRocks.Constant.WeakenAboveCritical
        INTP_PROPS.Mu_Eff   =   ComputeEffectiveViscosityPartiallyMoltenRocks(PHI, INTP_PROPS.Mu_Eff, INTP_PROPS.Mu_f, MATERIAL_PROPS(1).EffectiveViscosityPartiallyMoltenRocks, CHAR);
    end
    
end
if isfield(INTP_PROPS,'MeltFraction_katz')
    INTP_PROPS.Rho  =   (1-INTP_PROPS.MeltFraction_katz).*INTP_PROPS.Rho + INTP_PROPS.MeltFraction_katz.*INTP_PROPS.MeltDensity;
    % weakening viscosity if melt fraction
    if isfield(MATERIAL_PROPS(1),'EffectiveViscosityPartiallyMoltenRocks')
        INTP_PROPS.Mu_Eff   =   ComputeEffectiveViscosityPartiallyMoltenRocks(INTP_PROPS.MeltFraction_katz, INTP_PROPS.Mu_Eff, INTP_PROPS.MeltViscosity, MATERIAL_PROPS(1).EffectiveViscosityPartiallyMoltenRocks, CHAR);
    end
end



% average per element:
% INTP_PROPS.Mu_Eff = repmat(mean(INTP_PROPS.Mu_Eff,2),[1 size(INTP_PROPS.Mu_Eff,2)]);

NUMERICS.LimitPressure = logical(0);    % don't limit pressure anymore after first iteration step of first timestep

%% Apply lower and upper cutoffs to viscosity if required
INTP_PROPS.Mu_Eff(INTP_PROPS.Mu_Eff<NUMERICS.Viscosity.LowerCutoff) = NUMERICS.Viscosity.LowerCutoff;
INTP_PROPS.Mu_Eff(INTP_PROPS.Mu_Eff>NUMERICS.Viscosity.UpperCutoff) = NUMERICS.Viscosity.UpperCutoff;
if isfield(INTP_PROPS,'PHI')
    INTP_PROPS.Zeta_Eff(INTP_PROPS.Zeta_Eff<10.*NUMERICS.Viscosity.LowerCutoff) = 10.*NUMERICS.Viscosity.LowerCutoff;
end




end


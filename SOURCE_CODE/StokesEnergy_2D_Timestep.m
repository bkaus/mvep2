function [ MESH, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, MATERIAL_PROPS,PARTICLES,BC, dt ] = StokesEnergy_2D_Timestep(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, CHAR, PARTICLES, dt, time_start)
% This function checks for the used Timestepping scheme
% Possible Timestepping methods:
% 1) Euler (default)
% 2) Runge-Kutta 2nd order
% 3) Runge-Kutta 4th order
%
% You can change the timestepping method in the variable
% 'NUMERICS.Timestepping.Method'
%
% The Runge Kutta methods described here are one order less exact for
% strainrates, stresses, etc ...! This is because we advect the velocity
% but the rest is interpolated in the last step of the runge kutta
% computation and not averaged!
% To get rid of this one can solve the system one last time to guarantee
% the correct interpolation

% Check for the used advection scheme
switch NUMERICS.Timestepping.Method
    case 'Euler'
        [MESH, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, MATERIAL_PROPS,BC,PARTICLES, dt] = StokesEnergy_2D_Solution(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, CHAR, PARTICLES, dt, time_start);
        
    case  'RK2'
        % This Runge Kutta (2nd order) implementation follows the wikipedia
        % documentation on: http://de.wikipedia.org/wiki/Klassisches_Runge-Kutta-Verfahren
        % (or rather this one?)
        % http://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods#Second-order_methods_with_two_stages
        %
        % k1  = f(x0     , k0)
        % k2  = f(x0+h/2 , k1)
        % k3  = f(x0+h   , k2)
        %
        % k1' = y0 + h/2 * k1
        % k2' = y0 + h * k2
        % while h = dt
        %
        % y(x1) = y(x0) + k3
        
        
        
        for Runge = 1:1:2
            
            NUMERICS.Timestepping.Runge = Runge;
            
            if NUMERICS.Timestepping.Runge == 1
                dt_Runge = dt/2;
                [MESH_CC, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, MATERIAL_PROPS,BC,PARTICLES, dt_Runge] = StokesEnergy_2D_Solution(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, CHAR, PARTICLES, dt_Runge);
                MESH_CC.NODES = MESH.NODES + MESH_CC.VEL * dt_Runge;
                
            elseif NUMERICS.Timestepping.Runge == 2
                dt_Runge = dt;
                [MESH_CC, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, MATERIAL_PROPS,BC,PARTICLES, dt_Runge] = StokesEnergy_2D_Solution(MESH_CC, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, CHAR, PARTICLES, dt_Runge);
                kx = MESH_CC.VEL(1,:);
                kz = MESH_CC.VEL(2,:);
                kp = MESH_CC.PRESSURE;
            end
            
        end
        
        if ~isfield(MESH,'VEL')
            MESH.VEL = zeros(size(MESH_CC.VEL));
        end
        
        if ~isfield(MESH,'PRESSURE')
            MESH.PRESSURE = zeros(size(MESH_CC.PRESSURE));
        end
        
        MESH.VEL(1,:) = kx;
        MESH.VEL(2,:) = kz;
        MESH.PRESSURE = kp;
        
        
        
        
    case  'RK4'
        % This Runge Kutta (4th order) implementation follows the wikipedia
        % documentation on: http://de.wikipedia.org/wiki/Klassisches_Runge-Kutta-Verfahren
        % k1  = f(x0     , k0)
        % k2  = f(x0+h/2 , k1)
        % k3  = f(x0+h/2 , k2)
        % k4  = f(x0+h   , k3)
        %
        % k1' = y0 + h/2 * k1
        % k2' = y0 + h/2 * k2
        % k3' = y0 + h   * k3
        % while h = dt
        %
        % y(x1) = y(x0) + 1/6 * (k1 + 2*k2 + 2*k3 + k4)
        
        
        kx = zeros(4,length(MESH.NODES));
        kz = zeros(4,length(MESH.NODES));
        for Runge = 1:1:4
            
            NUMERICS.Timestepping.Runge = Runge;
            
            if NUMERICS.Timestepping.Runge == 1
                dt_Runge = dt/2;
                [MESH_CC, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, MATERIAL_PROPS,BC,PARTICLES, dt_Runge] = StokesEnergy_2D_Solution(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, CHAR, PARTICLES, dt_Runge);
                MESH_CC.NODES = MESH.NODES + MESH_CC.VEL * dt_Runge;
                
            elseif NUMERICS.Timestepping.Runge == 2
                dt_Runge = dt/2;
                [MESH_CC, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, MATERIAL_PROPS,BC,PARTICLES, dt_Runge] = StokesEnergy_2D_Solution(MESH_CC, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, CHAR, PARTICLES, dt_Runge);
                MESH_CC.NODES = MESH.NODES + MESH_CC.VEL * dt_Runge;
                
            else
                dt_Runge = dt;
                [MESH_CC, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, MATERIAL_PROPS,BC,PARTICLES, dt_Runge] = StokesEnergy_2D_Solution(MESH_CC, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, CHAR, PARTICLES, dt_Runge);
                MESH_CC.NODES = MESH.NODES + MESH_CC.VEL * dt_Runge;
                
            end
            
            
            kx(Runge,:) = MESH_CC.VEL(1,:);
            kz(Runge,:) = MESH_CC.VEL(2,:);
            
            
            if size(MESH.ELEMS,1) == 9
                % Check for the Q2P1 as there Pressure is a matrix
                kp1(Runge,:) = MESH_CC.PRESSURE(1,:);
                kp2(Runge,:) = MESH_CC.PRESSURE(2,:);
                kp3(Runge,:) = MESH_CC.PRESSURE(3,:);
            else
                kp(Runge,:) = MESH_CC.PRESSURE;
            end
            
            
        end
        
        if ~isfield(MESH,'VEL')
            MESH.VEL = zeros(size(MESH_CC.VEL));
        end
        
        if ~isfield(MESH,'PRESSURE')
            MESH.PRESSURE = zeros(size(MESH_CC.PRESSURE));
        end
        
        MESH.VEL(1,:) = (1/6)*(kx(1,:) + 2*kx(2,:) + 2*kx(3,:) + kx(4,:));
        MESH.VEL(2,:) = (1/6)*(kz(1,:) + 2*kz(2,:) + 2*kz(3,:) + kz(4,:));
        
        if size(MESH.ELEMS,1) == 9
            % Check for the Q2P1 as there Pressure is a matrix
            MESH.PRESSURE(1,:) = (1/6)*(kp1(1,:) + 2*kp1(2,:) + 2*kp1(3,:) + kp1(4,:));
            MESH.PRESSURE(2,:) = (1/6)*(kp2(1,:) + 2*kp2(2,:) + 2*kp2(3,:) + kp2(4,:));
            MESH.PRESSURE(3,:) = (1/6)*(kp3(1,:) + 2*kp3(2,:) + 2*kp3(3,:) + kp3(4,:));
        else
            MESH.PRESSURE = (1/6)*(kp(1,:) + 2*kp(2,:) + 2*kp(3,:) + kp(4,:));
        end
        
    otherwise
        error('Unknown timestepping method! Choose between Euler, RK2, RK4')
        
end







function [MESH, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, MATERIAL_PROPS,BC,PARTICLES, dt] = StokesEnergy_2D_Solution(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, CHAR, PARTICLES, dt_Runge, time_start)

% Computes nonlinear Stokes solution for this timestep with the correct dt
[MESH, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, MATERIAL_PROPS, dt]    =   Stokes2d_vep(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, CHAR, PARTICLES, dt_Runge);
if SUCCESS==0;  return; end
% if NUMERICS.ShowTimingInformation
%     time_increment_Stokes2d_vep = cputime-time_start;
%     disp(['Timing increment:  Stokes2d_vep                                  =  ',num2str(time_increment_Stokes2d_vep),'s'])
% end
% 
% if NUMERICS.ShowTimingInformation
%     time_increment_TEMP = cputime-time_start;
%     disp(['Timing increment:  temp_2d solver                                =  ',num2str(time_increment_TEMP),'s'])
% end

% Compute Temperature solution if required [sequentially]
if isfield(INTP_PROPS,'T')
    % Update BC's for temperature
    [BC]                                =   SetBC_Thermal(MESH, BC, INTP_PROPS);
    
    if NUMERICS.ReverseModel==logical(0)
        % Compute energy equations
        [MESH, INTP_PROPS, sdof, SUCCESS]   =   temp2d(MESH, INTP_PROPS, BC, dt);
        
    end
    
end




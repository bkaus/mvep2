function NUMERICS = SVNVersionInfo(NUMERICS)
% Prints the current version of the code & and return version number 
%
% NUMERICS.version = SVNVersionInfo;
%
% $Id: SVNVersionInfo.m 4958 2013-11-11 14:02:59Z ltbaumann $
% =========================================================================

srcpath = which('SVNVersionInfo.m');
srcpath = srcpath(1:end-17);

[flag,versiontext] = system(['svn info ' srcpath ' | grep "Last Changed"']);

if flag == 1
   [flag,versiontext] = system(['svn info ' srcpath ' | grep "Letzte"']);   % In case of german language, you get the output in german
end

if flag == 0
    [~,statustext] = system(['svn status ' srcpath '| grep "M "']);
    disp('======== MilaminVEP2 is under version control ========');
    disp(' ');
    disp(versiontext);
    disp(statustext);
    disp('======================================================');

    % extract version number
    [~,vn_str] = system(['svn info ' srcpath ' | grep "Last Changed Rev" | cut -d: -f2']);
    NUMERICS.svn.ID = str2double(vn_str);
    NUMERICS.svn.info = versiontext;
    NUMERICS.svn.status = statustext;


else
    if 1==0
        disp('======================================================');
        disp('===== MilaminVEP2 is NOT under version control =======');
        disp('======================================================');
    end
    NUMERICS.svn.ID = NaN;
end




end
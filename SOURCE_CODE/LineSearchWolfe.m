function [beta_step_star,grad,fcost ] = LineSearchWolfe( beta_low,beta_high,NUMERICS,MESH,MATERIAL_PROPS,INTP_PROPS,BC,CHAR,PARTICLES,dt,sol_ini,dx,fcost_ini,grad_ini,dcost_du,design_index,Ind_cost,Sol_ini,phase,s_sol_reduced_cost,s_sol_reduced_drdx,nvel,GCOORD,ELEM2NODE,NODE2EQ,p1)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
k = 1;
beta_low_ini = beta_low;
stop_crit = 8;

while k<stop_crit
    %     % cubic interpolation
    %     r1 = dcost_du_old + dcost_du - 3*((fcost_old - fcost)/(beta_step_old - beta_step));
    %     r2 = sqrt(r1.^2 - dcost_du_old .* dcost_du);
    %     beta_step_new = mean(beta_step - (beta_step - beta_step_old) * ((dcost_du + r2 -r1)./(dcost_du - dcost_du_old + 2*r2)));
    
    beta_step = (1/2)*(beta_low + beta_high);   % give simple initial guess by decreasing beta  ( additionally get cubic to work)
    
    if beta_low_ini ~= beta_low & k~=1 | k==1   % only compute this if beta_low differs from the initial one
        % Compute cost for beta_low
        m = sol_ini + beta_low'.*dx;
        
        [m] = denormalize(NUMERICS,m,p1);
        [m] = nondimensionalize(NUMERICS,m,p1,CHAR);
        for par = 1:p1
            if length(NUMERICS.Adjoint.fields_Material_Props{par}) == 1
                MATERIAL_PROPS(phase{par}).(NUMERICS.Adjoint.fields_Material_Props{par}{1}) = m(par,1);
            elseif length(NUMERICS.Adjoint.fields_Material_Props{par}) == 2
                MATERIAL_PROPS(phase{par}).(NUMERICS.Adjoint.fields_Material_Props{par}{1}).(NUMERICS.Adjoint.fields_Material_Props{par}{2}) = m(par,1);
            elseif length(NUMERICS.Adjoint.fields_Material_Props{par}) == 3
                MATERIAL_PROPS(phase{par}).(NUMERICS.Adjoint.fields_Material_Props{par}{1}).(NUMERICS.Adjoint.fields_Material_Props{par}{2}).(NUMERICS.Adjoint.fields_Material_Props{par}{3}) = m(par,1);
            else
                display(['Input has too many fields for Parameter ',fields{par}])
            end
        end
        [m] = denondimensionalize(NUMERICS,m,p1,CHAR);
        [m] = normalize(NUMERICS,m,p1);
        
        [BC]                                                                =   SetBC(MESH, BC, INTP_PROPS);
        [INTP_PROPS, NUMERICS, MATERIAL_PROPS]                              =   UpdatePropertiesAt_INTPS_PhaseRatio(INTP_PROPS, MATERIAL_PROPS, NUMERICS, CHAR, dt);    
        [MESH, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, MATERIAL_PROPS, dt]  =   Stokes2d_vep(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, CHAR, PARTICLES, dt);
        Sol_vec = NUMERICS.Adjoint.Sol_vec;
        
        fcost_low = (1/2) * (Sol_vec(Ind_cost) - Sol_ini(Ind_cost))' * (Sol_vec(Ind_cost) - Sol_ini(Ind_cost));
    end
    
    % Compute cost for the interpolated beta
    m = sol_ini + beta_step'.*dx;
    
    [m] = denormalize(NUMERICS,m,p1);
    [m] = nondimensionalize(NUMERICS,m,p1,CHAR);
    for par = 1:p1
        if length(NUMERICS.Adjoint.fields_Material_Props{par}) == 1
            MATERIAL_PROPS(phase{par}).(NUMERICS.Adjoint.fields_Material_Props{par}{1}) = m(par,1);
        elseif length(NUMERICS.Adjoint.fields_Material_Props{par}) == 2
            MATERIAL_PROPS(phase{par}).(NUMERICS.Adjoint.fields_Material_Props{par}{1}).(NUMERICS.Adjoint.fields_Material_Props{par}{2}) = m(par,1);
        elseif length(NUMERICS.Adjoint.fields_Material_Props{par}) == 3
            MATERIAL_PROPS(phase{par}).(NUMERICS.Adjoint.fields_Material_Props{par}{1}).(NUMERICS.Adjoint.fields_Material_Props{par}{2}).(NUMERICS.Adjoint.fields_Material_Props{par}{3}) = m(par,1);
        else
            display(['Input has too many fields for Parameter ',fields{par}])
        end
    end
    [m] = denondimensionalize(NUMERICS,m,p1,CHAR);
    [m] = normalize(NUMERICS,m,p1);
    
        [BC]                                                                =   SetBC(MESH, BC, INTP_PROPS);
        [INTP_PROPS, NUMERICS, MATERIAL_PROPS]                              =   UpdatePropertiesAt_INTPS_PhaseRatio(INTP_PROPS, MATERIAL_PROPS, NUMERICS, CHAR, dt);    
        [MESH, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, MATERIAL_PROPS, dt]  =   Stokes2d_vep(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, CHAR, PARTICLES, dt);
    Sol_vec = NUMERICS.Adjoint.Sol_vec;
    J = NUMERICS.Adjoint.J;
    
    fcost = (1/2) * (Sol_vec(Ind_cost) - Sol_ini(Ind_cost))' * (Sol_vec(Ind_cost) - Sol_ini(Ind_cost));
    
    
    if (fcost > fcost_ini + 1e-4 * beta_step' * grad_ini) | (fcost >= fcost_low)
        beta_high = beta_step;
    else
        % Compute gradient
        dcost_du(Ind_cost) = (Sol_vec(Ind_cost)' - Sol_ini(Ind_cost)');
        
        dcost_du_compute = zeros(size(s_sol_reduced_cost));
        dcost_du_compute(NUMERICS.Adjoint.Free)               =   dcost_du(NUMERICS.Adjoint.Free);
        dcost_du_compute(length(NUMERICS.Adjoint.Free)+1:end) =   dcost_du(nvel+1:end);
        
        [m] = denormalize(NUMERICS,m,p1);
        [m] = nondimensionalize(NUMERICS,m,p1,CHAR);
        drdx_big = zeros(length(Sol_ini),p1);
        for par = 1:p1
            % Compute residual by forward finite differences
            h_max = 1e-28;
            h = max([(max(1e-6.*abs(m(par,1)))) h_max]);
            phase_temp = phase{par};
            [drdx_temp] = Compute_AdjointRes( NUMERICS,MATERIAL_PROPS,MESH,h,GCOORD,ELEM2NODE,INTP_PROPS,BC,Sol_vec,dt,CHAR,phase_temp,par);
            drdx(:,par) = drdx_temp;
            drdx_big(NODE2EQ(:,design_index{phase{par}}),par) = drdx(NODE2EQ(:,design_index{phase{par}}),par);
        end
        [m] = denondimensionalize(NUMERICS,m,p1,CHAR);
        [m] = normalize(NUMERICS,m,p1);
        
        drdx_compute = zeros(size(s_sol_reduced_drdx'));
        drdx_compute(NUMERICS.Adjoint.Free,:)               =   drdx_big(NUMERICS.Adjoint.Free,:);
        drdx_compute(length(NUMERICS.Adjoint.Free)+1:end,:)         =   drdx_big(nvel+1:end,:);
        
        % Compute (normalized) gradient using adjoint method
        psi = (J')\(dcost_du_compute');
        grad = - psi'*drdx_compute;
        [grad] = nondimensionalize(NUMERICS,grad',p1,CHAR);
        grad = grad';
        [grad] = normalize(NUMERICS,grad',p1);
        grad = grad';
        
        if abs(grad) <= abs(-0.9*grad_ini)   
            beta_step_star = beta_step;
            break
        end
        if grad*((beta_high-beta_low)')>=0
            beta_high = beta_low;
        end
        beta_low = beta_step;
        
    end
    display([num2str(k),'. LINE-SEARCH step finished (maximum ',num2str(stop_crit-1),')'])
    display(sprintf('\n'))
    k = k +1;
    beta_step_star = beta_step;
end

if exist('grad','var')
else
   grad = grad_ini;
end




% FUNCTIONS
% 1. normalize
function [m] = normalize(NUMERICS,m,p1)
for par = 1:p1
    m(par,1) = (m(par,1) - NUMERICS.Adjoint.bounds{par}(1))/(NUMERICS.Adjoint.bounds{par}(2) - NUMERICS.Adjoint.bounds{par}(1));
end

% 2. denormalize
function [m] = denormalize(NUMERICS,m,p1)
for par = 1:p1
    m(par,1) = m(par,1) * (NUMERICS.Adjoint.bounds{par}(2) - NUMERICS.Adjoint.bounds{par}(1)) + NUMERICS.Adjoint.bounds{par}(1);
end

% 3. nondimensionalize the variables
function [m] = nondimensionalize(NUMERICS,m,npar,CHAR)
for par = 1:npar
    if strcmp(NUMERICS.Adjoint.fields_Material_Props{par}(3),'n') == 1
        m(par,1) = m(par,1);
    elseif strcmp(NUMERICS.Adjoint.fields_Material_Props{par}(3),'e0') == 1
        m(par,1) = m(par,1)/(1/CHAR.Time);
    elseif isfield(CHAR,NUMERICS.Adjoint.fields_Material_Props{par}(1)) == 1
        m(par,1) = m(par,1)/CHAR.(NUMERICS.Adjoint.fields_Material_Props{par}{1});
    end
end

% 4. denondimensionalize the variables
function [m] = denondimensionalize(NUMERICS,m,npar,CHAR)
for par = 1:npar
    if strcmp(NUMERICS.Adjoint.fields_Material_Props{par}(3),'n') == 1
        m(par,1) = m(par,1);
    elseif strcmp(NUMERICS.Adjoint.fields_Material_Props{par}(3),'e0') == 1
        m(par,1) = m(par,1)*(1/CHAR.Time);
    elseif isfield(CHAR,NUMERICS.Adjoint.fields_Material_Props{par}(1)) == 1
        m(par,1) = m(par,1)*(CHAR.(NUMERICS.Adjoint.fields_Material_Props{par}{1}));
    end
end



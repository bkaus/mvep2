/*
  Input values including P,T[,X,Fcpx]:
  P(pressure,GPa),T(temperature,centrigrade),
  X(bulk water,wt fraction),and modal cpx (wt fraction).
  
           Compile the mex file
  mex -largeArrayDims ParameterizedMeltFraction_HydrousMantleMelting_Katz2003.c meltParam.c
  here -largeArrayDims for large dimensinal arrays, it will be default in the future */
 
 /* -----------------------Useage-------------------------
  [F,cf] = ParameterizedMeltFraction_HydrousMantleMelting_Katz2003(P,T)  for two inputs
  [F,cf] = ParameterizedMeltFraction_HydrousMantleMelting_Katz2003(P,T,X) for three inputs
  [F,cf] = ParameterizedMeltFraction_HydrousMantleMelting_Katz2003(P,T,X,Fcpx) for four inputs
 */

#include "meltParam.h"
#include "mex.h"
#include <math.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    meltParameter mp;
    double *F,*cf,*P,*T,*X,*Fcpx;
    double D,b,c;
    /* large dimensinal array,using mwSize/mwIndex instead of int,which is */ 
    mwIndex i,j,k;
    mwSize row,col,N;
    
    setMeltParamsToDefault(&mp);
    
    if (nrhs==2) {
        /* get the size of the input matrix*/
        row = mxGetM(prhs[0]);
        col = mxGetN(prhs[0]);
        /* get the points of input parameters */
        P = mxGetPr(prhs[0]);
        T = mxGetPr(prhs[1]);
        /* allocate space for X & Fcpx */
        N = mxGetNumberOfElements(prhs[0]);
        X = mxMalloc(sizeof(double)*N);
        Fcpx = mxMalloc(sizeof(double)*N);
        /* allocate space for output */
        plhs[0] = mxCreateDoubleMatrix(row,col,mxREAL);
        plhs[1] = mxCreateDoubleMatrix(row,col,mxREAL);
        F  = mxGetPr(plhs[0]);
        cf = mxGetPr(plhs[1]);
        
        for (j=0;j<col;j++) {
            for (i=0;i<row;i++) {
                k = i+j*row;
                X[k] = 0;
                /*Compute Fcpx depending on pressure*/
                if (P[k]<=2.8) { Fcpx[k]=0.12;
                }else if (P[k]>2.8 && P[k]<=13.5) {
                    Fcpx[k] = -0.000463*pow(P[k],3) + 0.009274*P[k]*P[k] - 0.04597*P[k] + 0.1917;
                }else {Fcpx[k] = 0.1221;}
                F[k] = MPgetFEquilib(P[k],T[k],X[k],Fcpx[k],&mp);
                /* Compute water concentration in fluid(melt)*/
                D = mp.D_water;
                b = -(X[k]/D + F[k]/D +1.0); c = X[k]/D*(1.0+F[k]);
                cf[k] = 0.5*(-b-sqrt(b*b-4.0*c));
                if (F[k]==0.0) cf[k] =1.0;
            }
        }
    }else if(nrhs==3) {
        /* get the size of the input matrix */
        row = mxGetM(prhs[0]);
        col = mxGetN(prhs[0]);
        /* get the points of input parameters */
        P = mxGetPr(prhs[0]);
        T = mxGetPr(prhs[1]);
        X = mxGetPr(prhs[2]);;
        N = mxGetNumberOfElements(prhs[0]);
        Fcpx = mxMalloc(sizeof(double)*N);
        /* allocate space for output */
        plhs[0] = mxCreateDoubleMatrix(row,col,mxREAL);
        plhs[1] = mxCreateDoubleMatrix(row,col,mxREAL);
        F = mxGetPr(plhs[0]);
        cf = mxGetPr(plhs[1]);
        
        for (j=0;j<col;j++) {
            for (i=0;i<row;i++) {
                k = i+j*row;
                /* Compute Fcpx depending on pressure */
                if (P[k]<=2.8) { Fcpx[k]=0.12;
                }else if (P[k]>2.8 && P[k]<=13.5) {
                    Fcpx[k] = -0.000463*pow(P[k],3) + 0.009274*P[k]*P[k] - 0.04597*P[k] + 0.1917;
                }else {Fcpx[k] = 0.1221;}
                F[k] = MPgetFEquilib(P[k],T[k],X[k],Fcpx[k],&mp);
                /* Compute water concentration in fluid(melt) */
                D = mp.D_water;
                b = -(X[k]/D + F[k]/D +1.0); c = X[k]/D*(1.0+F[k]);
                cf[k] = 0.5*(-b-sqrt(b*b-4.0*c));
                if (F[k]==0.0) cf[k] =1.0;
            }
        }
    }else if(nrhs==4) {
        /* get the size of the input matrix */
        row = mxGetM(prhs[0]);
        col = mxGetN(prhs[0]);
        /* get the points of input parameters */
        P = mxGetPr(prhs[0]);
        T = mxGetPr(prhs[1]);
        X = mxGetPr(prhs[2]);
        Fcpx=mxGetPr(prhs[3]);
        /* allocate space for output */
        plhs[0] = mxCreateDoubleMatrix(row,col,mxREAL);
        plhs[1] = mxCreateDoubleMatrix(row,col,mxREAL);
        F = mxGetPr(plhs[0]);
        cf = mxGetPr(plhs[1]);
        for (j=0;j<col;j++) {
            for (i=0;i<row;i++) {
                k = i+j*row;
                F[k] = MPgetFEquilib(P[k],T[k],X[k],Fcpx[k],&mp);
                /* Compute water concentration in fluid(melt) */
                D = mp.D_water;
                b = -(X[k]/D + F[k]/D +1.0); c = X[k]/D*(1.0+F[k]);
                cf[k] = 0.5*(-b-sqrt(b*b-4.0*c));
                if (F[k]==0.0) cf[k] =1.0;
            }
        }
    }else {mexErrMsgTxt("Incorrect number of input values! 2 minimum and 4 maximum.\n");}
    
    
    /* free space */
    if (nrhs==2) {mxFree(X);mxFree(Fcpx);}
    if (nrhs==3) mxFree(Fcpx);
    return;
}
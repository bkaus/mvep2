#include "meltParam.h"

/*
  private function prototypes
*/
float calcDT(float P,float X,float F,meltParameter *mp);
float calcF(float T,float dT,float P,float Fcpx,meltParameter *mp);
float FX_bal(float x1,float x2,float T,float P,float X,float Fcpx,meltParameter *mp);
float FT_bal(float x1,float x2,float T,float P,float X,float M,meltParameter *mp);
inline float FZero(float F, float T, float P, float X, float Fcpx, meltParameter *mp);
inline float HZero(float F,float T,float P,float X,float M,meltParameter *mp);

/*
  PUBLIC FUNCTIONS
*/

/*---------------------------------------------------------------------*/
/*
  Initialize a meltParameter struct to the default values given in 
  meltParam.h.
 */  
void setMeltParamsToDefault(meltParameter *mp)
/*---------------------------------------------------------------------*/
{
  mp->A1=DEFAULT_A1; mp->A2=DEFAULT_A2; mp->A3=DEFAULT_A3;
  mp->B1=DEFAULT_B1; mp->B2=DEFAULT_B2; mp->B3=DEFAULT_B3;
  mp->C1=DEFAULT_C1; mp->C2=DEFAULT_C2; mp->C3=DEFAULT_C3;
  mp->r1=DEFAULT_R1;           mp->r2=DEFAULT_R2; 
  mp->beta1=DEFAULT_BETA1;     mp->beta2=DEFAULT_BETA2;
  mp->K=DEFAULT_K;             mp->gamma=DEFAULT_GAMMA;
  mp->D_water=DEFAULT_D_WATER; mp->lambda=DEFAULT_LAMBDA;
  mp->chi1=DEFAULT_CHI1;       mp->chi2=DEFAULT_CHI2;
  mp->Cp=DEFAULT_CP;           mp->DS=DEFAULT_DS;
  return;
}

/*---------------------------------------------------------------------*/
/*
  Returns the equilibrium wt fraction degree of melting given pressure 
  (GPa), temperature (Centrigrade), bulk water (weight fraction) and 
  modal cpx (weight fraction).
 */  
float MPgetFReactive(float P,float T,float Cf,float M,meltParameter *mp)
/*---------------------------------------------------------------------*/
{
  float Fcpx = M/(mp->r1 + mp->r2*P);
  float Tsol, Tlhz, Tcpx, Tliq, dT, Cf_SAT, F;

  Cf_SAT = mp->chi1*pow(P,mp->lambda) + mp->chi2*P;
  Cf = (Cf<Cf_SAT) ? Cf:Cf_SAT;
  dT = mp->K*pow(100.0*Cf,mp->gamma);
  Tsol = mp->A1 + mp->A2*P + mp->A3*P*P;
  Tlhz = mp->B1 + mp->B2*P + mp->B3*P*P;
  Tcpx = pow(Fcpx,1.0/mp->beta1)*(Tlhz - Tsol) + Tsol;
  Tliq = mp->C1 + mp->C2*P + mp->C3*P*P;

  if (T<Tsol-dT) {
    F = 0.0; 
  } else if (T<Tcpx-dT) {
    F = pow(((T-(Tsol-dT))/(Tlhz-Tsol)),mp->beta1);
  } else if (T<Tliq-dT) {
    F = Fcpx + (1-Fcpx)*pow(((T-(Tcpx-dT))/(Tliq-Tcpx)),mp->beta2);
  } else {
    F = 1.0;
  }

  return F;
}

/*---------------------------------------------------------------------*/
/*
  Returns the equilibrium wt fraction degree of melting given pressure 
  (GPa), temperature (Centrigrade), bulk water (weight fraction) and 
  modal cpx (weight fraction).
 */  
float MPgetFEquilib(float P,float T,float X,float M,meltParameter *mp)
/*---------------------------------------------------------------------*/
{
  float Fcpx = M/(mp->r1 + mp->r2*P);
  float Tsol, Tlhz, Tcpx, Tliq, dT[3];

  Tsol = mp->A1 + mp->A2*P + mp->A3*P*P;
  Tlhz = mp->B1 + mp->B2*P + mp->B3*P*P;
  Tcpx = pow(Fcpx,1.0/mp->beta1)*(Tlhz - Tsol) + Tsol;
  Tliq = mp->C1 + mp->C2*P + mp->C3*P*P;

  dT[0] = calcDT(P,X,0.0,mp);  /* compute dT for F=0.0 */
  dT[1] = calcDT(P,X,Fcpx,mp); /* compute dT for F=Fcpx_out */
  dT[2] = calcDT(P,X,1.0,mp);  /* compute dT for F=1.0 */
  
  if (T<=(Tsol-dT[0])) {
    return 0.0;
  } else if (T<=(Tcpx-dT[1])) {
    return FX_bal(0.0,Fcpx,T,P,X,Fcpx,mp);
  } else if (T<=(Tliq-dT[2])) {
    return FX_bal(Fcpx,1.0,T,P,X,Fcpx,mp);
  } else 
    return 1.0;
}

/*---------------------------------------------------------------------*/
/*
  Returns the solidus temperature (Centigrade) given pressure,
  bulk water content.
 */  
float MPgetTSolidus(float P,float X,meltParameter *mp)
/*---------------------------------------------------------------------*/
{
  float Tsol, dT;
  Tsol = mp->A1 + mp->A2*P + mp->A3*P*P;
  dT   = calcDT(P,X,0.0,mp);  /* compute dT for F=0.0 */
  return Tsol - dT;
}

/*---------------------------------------------------------------------*/
/*
  Returns the temperature (Centigrade) given pressure, degree of melting,
  bulk water content and modal cpx.
 */  
float MPgetTEquilib(float P,float F,float X,float M,meltParameter *mp)
/*---------------------------------------------------------------------*/
{
  float Fcpx = M/(mp->r1 + mp->r2*P);
  float Tsol, Tlhz, Tcpx, Tliq;

  Tsol = mp->A1 + mp->A2*P + mp->A3*P*P;
  Tlhz = mp->B1 + mp->B2*P + mp->B3*P*P;
  Tcpx = pow(Fcpx,1.0/mp->beta1)*(Tlhz - Tsol) + Tsol;
  Tliq = mp->C1 + mp->C2*P + mp->C3*P*P;

  if (F<=0.0) {
    return Tsol - calcDT(P,X,0.0,mp);
  } else if (F<=Fcpx) {
    return pow(F,1.0/mp->beta1)*(Tlhz-Tsol) + Tsol - calcDT(P,X,F,mp);
  } else if (F<1.0) {
    return pow((F-Fcpx)/(1.0-Fcpx),1.0/mp->beta2)*(Tliq-Tcpx) + Tcpx - calcDT(P,X,F,mp);
  } else
    return Tliq - calcDT(P,X,1.0,mp);
}  

/*---------------------------------------------------------------------*/
/*
  The function whose root is the melt fraction that conserves enthalpy.
 */  
float MPgetFconsH(float P,float Ti,float X,float M,float *Tf,
		meltParameter *mp)
/*---------------------------------------------------------------------*/
{
  float Tsol,F;
  Tsol = mp->A1 + mp->A2*P + mp->A3*P*P;

  if (Ti<(Tsol-calcDT(P,X,0.0,mp))) {
    *Tf = Ti;
    return 0.0;
  } else {
    F  = FT_bal(0.0,1.0,Ti,P,X,M,mp);
    *Tf = MPgetTEquilib(P,F,X,M,mp);
    return F;
  }
}

/*
  PRIVATE FUNCTIONS
*/

/*---------------------------------------------------------------------*/
/*
  Compute the solidus lowering effect of bulk water content GIVEN a 
  degree of melting.
 */  
float calcDT(float P,float X,float F,meltParameter *mp)
/*---------------------------------------------------------------------*/
{
  float Xsat, Xmelt;
  Xsat  = mp->chi1*pow(P,mp->lambda) + mp->chi2*P; /* saturation content */
  Xmelt = X/( mp->D_water + F*(1.0-mp->D_water) ); /* melt water content */
  Xmelt = (Xmelt<Xsat) ? Xmelt:Xsat;               /* min of the two */
  return mp->K*pow(100.0*Xmelt,mp->gamma);         /* delta T due to water */
}

/*---------------------------------------------------------------------*/
/*
  The equations that give F from T, dT, P, Fcpx.
 */  
float calcF(float T,float dT,float P,float Fcpx,meltParameter *mp)
/*---------------------------------------------------------------------*/
{
  float Tsol, Tlhz, Tcpx, Tliq;

  Tsol = mp->A1 + mp->A2*P + mp->A3*P*P;
  Tlhz = mp->B1 + mp->B2*P + mp->B3*P*P;
  Tcpx = pow(Fcpx,1.0/mp->beta1)*(Tlhz - Tsol) + Tsol;
  Tliq = mp->C1 + mp->C2*P + mp->C3*P*P;

  if (T<=(Tsol-dT)) {
    return 0.0;
  } else if (T<=(Tcpx-dT)) {
    return pow((T - (Tsol-dT))/(Tlhz-Tsol),mp->beta1);
  } else if (T<=(Tliq-dT)) {
    return Fcpx + (1.0-Fcpx)*pow((T - (Tcpx-dT))/(Tliq-Tcpx),mp->beta2);
  } else 
    return 1.0;
}

/*---------------------------------------------------------------------*/
/*
  The function whose zero is the correct melt fraction for a given set
  of state variables.
 */  
inline float FZero(float F, float T, float P, float X, float Fcpx,
		   meltParameter *mp)
/*---------------------------------------------------------------------*/
{
  return calcF(T,calcDT(P,X,F,mp),P,Fcpx,mp) - F;
}

/*---------------------------------------------------------------------*/
/*
  The function whose root is the melt fraction that conserves enthalpy.
 */  
inline float HZero(float F,float T,float P,float X,float M,
		   meltParameter *mp)
/*---------------------------------------------------------------------*/
{
  return (MPgetTEquilib(P,F,X,M,mp)+273.0)*(mp->Cp+mp->DS*F) - (T+273.0)*mp->Cp;
}

#define UNUSED    1e20
#define MAXITS    60
#define X_ACCEPT  0.00001
#define SIGN(x,y) ( (y)>=0.0 ? fabs(x) : -fabs(x) )

/*---------------------------------------------------------------------*/
/*
  Adapted from Ridder's Method as described by Numerical Recipes in C.
 */  
float FX_bal(float x1,float x2,float T,float P,float X,float Fcpx,
	     meltParameter *mp)
/*---------------------------------------------------------------------*/
{
  int   j;
  float fh,fl,fm,fnew,s,xh,xl,xm,xnew,ans;

  fl = FZero(x1,T,P,X,Fcpx,mp);
  fh = FZero(x2,T,P,X,Fcpx,mp);
  
  if ( (fl>0.0 && fh<0.0) || (fl<0.0 && fh>0.0) ) {
    xl=x1; xh=x2; ans=UNUSED;

    for (j=1;j<=MAXITS;j++) {
      xm=0.5*(xl+xh);
      fm=FZero(xm,T,P,X,Fcpx,mp);
      s=sqrt(fm*fm-fl*fh);
      if (s==0.0) return ans;
      xnew=xm+(xm-xl)*((fl>=fh ? 1.0 : -1.0)*fm/s);
      if(fabs(xnew-ans) <= X_ACCEPT) return ans;
      ans=xnew;
      fnew=FZero(ans,T,P,X,Fcpx,mp);
      if (fnew==0.0) return ans;
      if (SIGN(fm,fnew) != fm) {
	xl=xm;
	fl=fm;
	xh=ans;
	fh=fnew;
      } else if (SIGN(fl,fnew) != fl) {
	xh=ans;
	fh=fnew;
      } else if (SIGN(fh,fnew) != fh) {
	xl=ans;
	fl=fnew;
      } else 
	printf("FX_bal error: never get here (1)\n");

      if (fabs(xh-xl) <= X_ACCEPT) return ans;
    }
    printf("FX_bal error: exceed max iterations\n");

  } else {
    if (fl==0.0) return x1;
    if (fh==0.0) return x2;
    printf("FX_bal error: never get here (2)\n");
  }
  return 0.0; /* never used */
}

/*---------------------------------------------------------------------*/
/*
  Adapted from Ridder's Method as described by Numerical Recipes in C.
 */  
float FT_bal(float x1,float x2,float T,float P,float X,float M,
	     meltParameter *mp)
/*---------------------------------------------------------------------*/
{
  int   j;
  float fh,fl,fm,fnew,s,xh,xl,xm,xnew,ans;

  fl = HZero(x1,T,P,X,M,mp);
  fh = HZero(x2,T,P,X,M,mp);
  
  if ( (fl>0.0 && fh<0.0) || (fl<0.0 && fh>0.0) ) {
    xl=x1; xh=x2; ans=UNUSED;

    for (j=1;j<=MAXITS;j++) {
      xm=0.5*(xl+xh);
      fm=HZero(xm,T,P,X,M,mp);
      s=sqrt(fm*fm-fl*fh);
      if (s==0.0) return ans;
      xnew=xm+(xm-xl)*((fl>=fh ? 1.0 : -1.0)*fm/s);
      if(fabs(xnew-ans) <= X_ACCEPT) return ans;
      ans=xnew;
      fnew=HZero(ans,T,P,X,M,mp);
      if (fnew==0.0) return ans;
      if (SIGN(fm,fnew) != fm) {
	xl=xm;
	fl=fm;
	xh=ans;
	fh=fnew;
      } else if (SIGN(fl,fnew) != fl) {
	xh=ans;
	fh=fnew;
      } else if (SIGN(fh,fnew) != fh) {
	xl=ans;
	fl=fnew;
      } else 
	printf("FX_bal error: never get here (1)\n");

      if (fabs(xh-xl) <= X_ACCEPT) return ans;
    }
    printf("FX_bal error: exceed max iterations\n");

  } else {
    if (fl==0.0) return x1;
    if (fh==0.0) return x2;
    printf("FX_bal error: never get here (2)\n");
  }
  return 0.0; /* never used */
}

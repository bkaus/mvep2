function   [ K ] 	=       ComputeThermalConductivity( INTP_PROPS,  Conductivity, CHAR);
% ComputeThermalConductivity -  updates the thermal conductivity

% $Id$


Type        =   fieldnames(Conductivity);
if size(Type,1) > 1
    error('You can currently only have one Conductivity type activated simultaneously')
end

switch Type{1}
    case 'Constant'
        Conductivity        =  Conductivity.Constant.k;
        K                   =  ones(size(INTP_PROPS.X))*Conductivity;
        
    case 'TemperatureDependent'
        % Following the equation in Clauser & Huenges 1995  (NOT the one used by Taras)
        % -> above 300C and 20 MPa there shouldn't be much change in
        %    conductivity this is why we ignore pressure here
        % -> porosity and mineralogy have major impact besides temperature
        %    that's why we have four different 'rock types'
        T                   =  INTP_PROPS.T*CHAR.Temperature-273.15;
        if strcmpi(Conductivity.TemperatureDependent.Type,'sediment') == 1
            k0 = 3;    % conductivity at room temperature
        elseif strcmpi(Conductivity.TemperatureDependent.Type,'volcanic') == 1
            k0 = 2.25;
        elseif strcmpi(Conductivity.TemperatureDependent.Type,'plutonic') == 1
            k0 = 2;
        elseif strcmpi(Conductivity.TemperatureDependent.Type,'metamorphic_quartz') == 1
            k0 = 7.75;
        elseif strcmpi(Conductivity.TemperatureDependent.Type,'metamorphic') == 1
            k0 = 2.75;
        else
            error('You should define a conductivity type (f.e. MATERIAL_PROPS.Conductivity.TemperatureDependent.Type = Sediment)')
        end
        K                   =  k0./(1.007 + T *(0.0036 - (0.0072/k0)));  
    case 'PressureTemperatureDependent'
        % Following the equation in Clauser & Huenges 1995  (also used in
        % I2VIS of Taras Gerya)
        T                   =  INTP_PROPS.T*CHAR.Temperature+77;
        % in the first iteration remove pressure dependence
        if isfield(INTP_PROPS,'Pressure')
            P               =  INTP_PROPS.Pressure*CHAR.Stress/100000;
        else
            P               = 0;
        end
        K                   =  (Conductivity.PressureTemperatureDependent.k1 + Conductivity.PressureTemperatureDependent.k2 ./ T) .* exp(Conductivity.PressureTemperatureDependent.k3 * P);
        
    otherwise
        error('Unknown Conductivity  law')
end

end

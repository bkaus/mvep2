function [MESH,mesh_input] = CreateMesh(varargin)
%CreateMesh is a general routine which creates either a triangular or a
%quadrilateral mesh for use with MILAMIN_VEP
%
%  MESH = CreateMesh(opts, mesh_input, [MESH], [KeepBounds])
%
%Arguments:
%    opts is a structure containing options that influence how the mesh is
%     constructed.
%     In case you use a triangular mesh, see a detailed
%     description of this in mtriangle, with the difference that there are a
%     few default options, which automatically create a bounding box if you
%     wish to do so. In case of a quadrilateral mesh, options are:
%       opts.element_type.velocity  =   'Q1';       % Shape function for velocity/temperature equations
%                                                     options:  Q1  (linear quadrilateral)
%                                                               Q2  (quadratic quadrilateral)
%                                                               T1  (linear triangle)
%                                                               T2  (quadratic triangle)
%       opts.element_type.pressure  =   'P0';       % Shape function for pressure
%                                                     options:  P0  (constant discontinuous)
%                                                               P-1 (linear discontinuous)
%                                                               Q1  (linear quadrilateral continuous)
%       opts.element_type.darcy     =   'Q1';       % [optional]: shape function for Darcy equation
%       opts.nx   = 101;      % number of nodes in x-direction
%       opts.nz   = 101;      % number of nodes in z-direction
%
%
%    mesh_input is a structure that contains a number of default
%     parameters. In case you use a triangular mesh, see MTRIANGLE. In case
%     you use a quadrilateral mesh, the options are:
%
%    MESH is an existing MESH structure that contains all information about
%     a preexisting mesh
%
%    KeepBounds is a boolean variable that determines if the mesh is
%     supposed to be static, meaning that only the surface topography will
%     be transferred to the new mesh and the remaining box keeps the same
%     dimensions.
%
% Example:
%       mesh_input.x_min            =   0;
%       mesh_input.z_min            =   0;
%       mesh_input.x_max            =   1;
%       mesh_input.z_max            =   1;
%       opts.element_type.velocity  =   'Q1'
%       opts.element_type.pressure  =   'P0'
%       [MESH] = CreateMesh(opts, mesh_input)
%
% See also:
%   MTRIANGLE, MQUADRILATERAL

% $Id$

% Various input forms
if nargin==0
    mesh_input          =   [];
    MESH                =   [];
    KeepBounds          =   false;
elseif nargin==1
    opts                =   varargin{1};
    mesh_input          =   [];
    MESH                =   [];
    KeepBounds          =   false;
elseif nargin==2
    opts                =   varargin{1};
    mesh_input          =   varargin{2};
    MESH                =   [];
    KeepBounds          =   false;
elseif nargin==3
    opts                =   varargin{1};
    mesh_input          =   varargin{2};
    MESH                =   varargin{3};
    KeepBounds          =   false;
elseif nargin==4
    opts                =   varargin{1};
    mesh_input          =   varargin{2};
    MESH                =   varargin{3};
    KeepBounds          =   varargin{4};
end

% Set default parameters
if ~isfield(mesh_input,'x_min');
    mesh_input.x_min = 0;
end
if ~isfield(mesh_input,'x_max');
    mesh_input.x_max = 1;
end
if ~isfield(mesh_input,'z_min');
    mesh_input.z_min = 0;
end
if ~isfield(mesh_input,'z_max');
    mesh_input.z_max = 1;
end
if ~isfield(opts,'element_type');
    opts.element_type.velocity = 'Q1';
    opts.element_type.pressure = 'P0';
else
    % Field
    if ~isfield(opts.element_type,'velocity')
        warning('You should define the field opts.element_type.velocity={Q2/Q1/T2/T1}')
        
        
        switch opts.element_type % for backwards compatibility
            case 'quad9'
                opts.element_type.velocity = 'Q2';
                opts.element_type.pressure = 'P-1';
            case 'quad4'
                opts.element_type.velocity = 'Q1';
                opts.element_type.pressure = 'P0';
            otherwise
                error('this backwards compatibility was not yet implemented')
        end
        
    end
    if ~isfield(opts.element_type,'pressure')
        warning('You should define the field opts.element_type.pressure={P0,P-1,Q1}')
        switch opts.element_type.velocity
            case {'Q2', 'T2'}
                opts.element_type.pressure = 'P-1';
            case {'Q1', 'T1'}
                opts.element_type.pressure = 'P0';
        end
    end
end


switch opts.element_type.velocity
    case {'T1','T2'} % 2D
        if ~isfield(opts,'max_tri_area')
            % set a default parameter if triangle area is not specified
            opts.max_tri_area     = 0.001;
        end
       
        if ~isempty(MESH)
            % extract information from prior mesh
            nnodes                      = max(max(MESH.ELEMS(1:3,:)));
            mesh_input.points           = MESH.NODES(:,1:nnodes);
            mesh_input.pointmarkers     = MESH.Point_id(1:nnodes);
            mesh_input.segments         = MESH.SEGMENTS;
            mesh_input.segmentmarkers   = MESH.segment_markers;
            mesh_input.triangles        = MESH.ELEMS(1:3,:);
            if isfield(mesh_input,'trianglearea') && ...
                any(opts.max_tri_area~=mesh_input.trianglearea)
                error('mesh_input.trianglearea should be equal to opts.max_tri_area.');
            end
        end
        
        %------------------------------------------------------------------
        % Triangular mesh
        %------------------------------------------------------------------
        if ~isfield(mesh_input,'points')
            % We do not have a bounding box yet
            % Setup domain - square box
            x_min        = mesh_input.x_min;
            x_max        = mesh_input.x_max;
            z_min        = mesh_input.z_min;
            z_max        = mesh_input.z_max;
            
            % in case x_vec and z_vec are not specified, create a
            % bounding box based on 4 corner points
            points   = [
                x_min z_min;
                x_max z_min;
                x_max z_max;
                x_min z_max]';
            segments            = [1 2; 2 3; 3 4; 4 1]';
            
            segmentmarkers     = [ 1; 2; 3; 4]';
            
            % Set triangle input
            mesh_input.points         = points;
            mesh_input.segments       = uint32(segments);
            mesh_input.segmentmarkers = uint32(segmentmarkers);
            
        end
        
        % default # of integration points
        switch opts.element_type.velocity
            case 'T1'
                nip = 3;
                 opts.element_type_name = 'tri3'; 
        
            case 'T2'
                nip = 7;
                opts.element_type_name = 'tri7'; 
            otherwise
                error('unknown element')
        end
        
        
        % Create triangular mesh
        MESH            =   mtriangle(opts, mesh_input);
        MESH.nip        =   nip;
        MESH.Point_id   =   MESH.node_markers;
        MESH.ndim       =   2;                                              % number of space dimensions
        
        % triangle vertices
        vertA           =   MESH.NODES(:,MESH.ELEMS(1,:));
        vertB           =   MESH.NODES(:,MESH.ELEMS(2,:));
        vertC           =   MESH.NODES(:,MESH.ELEMS(3,:));
        
        % triangle area
        MESH.AREA       =   .5*abs( ...
            vertA(1,:).*(vertB(2,:)-vertC(2,:)) + ...
            vertB(1,:).*(vertC(2,:)-vertA(2,:)) + ...
            vertC(1,:).*(vertA(2,:)-vertB(2,:)));
        
        % add sub-struct MESH.DARCY if two-phase flow is to be applied
        if isfield(opts.element_type,'darcy')
            
            switch [opts.element_type.velocity opts.element_type.darcy]
                case ['T2' 'T1']
                    nel                       =   size(MESH.ELEMS, 2);
                    MESH.DARCY.ELEMS          =   MESH.ELEMS(1:3,1:nel);          %
                    nnodes                    =   max(MESH.DARCY.ELEMS(:));       % max # nodes
                    
                    MESH.DARCY.restr          =   1:nnodes;
                    MESH.DARCY.NODES          =   MESH.NODES(:,MESH.DARCY.restr);
                    MESH.DARCY.node_markers   =   MESH.node_markers(1,1:nnodes);
                    MESH.DARCY.SEGMENTS       =   MESH.SEGMENTS;
                    MESH.DARCY.segment_markers=   MESH.segment_markers;
                    MESH.DARCY.nip            =   3;
                    MESH.DARCY.Point_id       =   MESH.Point_id(1:nnodes);
                    MESH.DARCY.ndim           =   2;
                    
                otherwise
                    error('Invalid combination of Stokes and Darcy elements.');
            end
            MESH.DARCY.element_type.darcy     = opts.element_type.darcy;
        end
        
        
    case {'Q2','Q1'} % 2D
        %------------------------------------------------------------------
        % quadrilateral mesh
        %------------------------------------------------------------------
        
        
        if ~isempty(MESH)
            
            % Extract bottom and top topography of the previous MESH
            X                   = 	MESH.NODES(1,:);
            Z                   =   MESH.NODES(2,:);
            X2d                 =   X(MESH.RegularGridNumber);
            Z2d                 =   Z(MESH.RegularGridNumber);
            
            mesh_input.Top_x    =   X2d(end,:);
            mesh_input.Top_z    =   Z2d(end,:);
            
            if ~KeepBounds
                % update bottom topography
                mesh_input.Bot_x    =   X2d(1  ,:);
                mesh_input.Bot_z    =   Z2d(1  ,:);
                
                % update min and max of mesh
                mesh_input.x_min    =   X2d(1,1);
                mesh_input.x_max    =   X2d(1,end);
                mesh_input.z_min    =   Z2d(1,1);
                mesh_input.z_max    =   Z2d(end,1);
            end
            
        end
        
        
        % Create x_vec and z_vec if necessary, which describe the regular
        % coordinates of the sides of the box in a normalized manner (from
        % 0-1), which is scaled later.
        if ~isfield(opts,'nx');
            nx          =   11;
        else
            nx          =   opts.nx;
        end
        
        if ~isfield(opts,'nz');
            nz          =   11;
        else
            nz          =   opts.nz;
        end
        
        
        if ~isfield(mesh_input,'x_vec');
            dx      =   1/(nx-1);
            x_vec   =   0:dx:1;
        else
            % scale from 0-1
            x_vec   =   mesh_input.x_vec;
            x_vec   =   x_vec/(max(x_vec)-min(x_vec));
            x_vec   =   x_vec - x_vec(1);
        end
        
        if ~isfield(mesh_input,'z_vec');
            dz      =   1/(nz-1);
            z_vec   =   0:dz:1;
        else
            % scale from 0-1
            z_vec   =   mesh_input.z_vec;
            z_vec   =   z_vec/(max(z_vec)-min(z_vec));
            z_vec   =   z_vec - z_vec(1);
        end
        
        % Scale to real box size
        x_min       =   mesh_input.x_min;
        x_max       =   mesh_input.x_max;
        z_min       =   mesh_input.z_min;
        z_max       =   mesh_input.z_max;
        
        mesh_input.x_vec       =   x_vec*(x_max-x_min) + x_min;
        mesh_input.z_vec       =   z_vec*(z_max-z_min) + z_min;
        
        % Detect errors
        if length(x_vec) ~= nx
            error('Length of x_vec should be equal to nx!')
        end
        if length(z_vec) ~= nz
            error('Length of z_vec should be equal to nz!')
        end
        
        
        % Interpolate bottom and top topography to x_vec horizontal
        % coordinates if necessary - create Top_x, Top_z, Bot_x, Bot_z
        % otherwise
        if ~isfield(mesh_input,'Bot_x');
            mesh_input.Bot_x   =   x_vec;
            mesh_input.Bot_z   =   ones(size(x_vec))*z_min;
            
        else
            % interpolate bottom & top topography until x_vec
            mesh_input.Bot_z    =   interp1(mesh_input.Bot_x,mesh_input.Bot_z,mesh_input.x_vec,'linear','extrap');
            mesh_input.Bot_x   =    mesh_input.x_vec;
        end
        
        if ~isfield(mesh_input,'Top_x');
            mesh_input.Top_x   =   x_vec;
            mesh_input.Top_z   =   ones(size(x_vec))*z_max;
        else
            Mean_Topo           =   mean(mesh_input.Top_z);     % mean Topography before remeshing
            if isfield(mesh_input,'FixedAverageTopography')
                Mean_Topo=mesh_input.FixedAverageTopography;
            end
            
            % interpolate bottom & top topography until x_vec
            mesh_input.Top_z    =   interp1(mesh_input.Top_x,mesh_input.Top_z,mesh_input.x_vec,'linear','extrap');
            mesh_input.Top_x   =    mesh_input.x_vec;
            
            
            % Ensure that the average topography after remeshing is the
            % same as before
            if ~isnan(Mean_Topo)
                diff_Topo           =   mean(mesh_input.Top_z  ) - Mean_Topo;
                mesh_input.Top_z    =   mesh_input.Top_z    - diff_Topo;
            end
            
            
        end
        
        switch opts.element_type.velocity
            case {'Q2'} % 2D
                % ensure that the middle nodes are not bended
                mesh_input.Top_z(2:2:end-1) = (mesh_input.Top_z(1:2:end-2)+mesh_input.Top_z(3:2:end))/2;
                
        end
        
        
        % Catch errors
        if any(isnan(mesh_input.Top_z))
            error('NaN in mesh_input.Top_z')
        end
        if any(isnan(mesh_input.Bot_z))
            error('NaN in mesh_input.Bot_z')
        end
        
        % Create the quadrilateral mesh
        MESH = mquadrilateral(opts, mesh_input);
        
        % Store some useful data:
        MESH.dx_min = min(diff( mesh_input.x_vec));
        MESH.dx_max = max(diff( mesh_input.x_vec));
        MESH.dz_min = min(diff( mesh_input.z_vec));
        MESH.dz_max = max(diff( mesh_input.z_vec));
        MESH.ndim       =   2;                                              % number of space dimensions
        
        % add sub-struct MESH.DARCY if two-phase flow is to be applied
        if isfield(opts.element_type,'darcy')
            
            switch [opts.element_type.velocity opts.element_type.darcy]
                case ['Q2' 'Q1']
                    restr                           = MESH.RegularGridNumber(1:2:end,1:2:end);
                    MESH.DARCY.restr                = restr(:)';
                    MESH.DARCY.NODES                = ...
                        MESH.NODES(:,restr);
                    ind = ...
                        reshape(1:.25*(nx+1)*(nz+1),.5*(nz+1),.5*(nx+1));
                    bl  = ind(1:end-1,1:end-1); % bottom left element corner
                    br  = ind(1:end-1,2:end);   % bottom right
                    tr  = ind(2:end,2:end);     % top right
                    tl  = ind(2:end,1:end-1);   % top left
                    MESH.DARCY.ELEMS                = zeros(4, numel(MESH.RegularElementNumber),'uint32');
                    MESH.DARCY.ELEMS(:,MESH.RegularElementNumber)   = ...
                        uint32([bl(:)'; br(:)'; tr(:)'; tl(:)']);
                    MESH.DARCY.RegularGridNumber    = ind;
                    MESH.DARCY.RegularElementNumber = MESH.RegularElementNumber;
                    Point_id                        = MESH.Point_id(MESH.RegularGridNumber(1:2:end,1:2:end));
                    MESH.DARCY.Point_id                   = Point_id(:)';
                otherwise
                    error('Invalid combination of Stokes and Darcy elements.');
            end
            MESH.DARCY.element_type.darcy     = opts.element_type.darcy;
        end
        
        
        
        
        
        %     case {'cube8','cube27'}
        %         if ~isfield(mesh_input,'y_min');
        %             mesh_input.y_min = 0;
        %         end
        %         if ~isfield(mesh_input,'y_max');
        %             mesh_input.y_max = 1;
        %         end
        %
        %         % cubic mesh - same format as quads but 3D
        %         if ~isempty(MESH)
        %
        %             % Extract min/max topography of the previous MESH
        %             X                   = 	MESH.NODES(1,:);
        %             Y                   = 	MESH.NODES(2,:);
        %             Z                   =   MESH.NODES(3,:);
        %             X3d                 =   X(MESH.RegularGridNumber);
        %             Y3d                 =   X(MESH.RegularGridNumber);
        %             Z3d                 =   Z(MESH.RegularGridNumber);
        %
        %             mesh_input.Top_x    =   X3d(end,:);
        %             mesh_input.Top_y    =   Y3d(end,:);
        %             mesh_input.Top_z    =   Z3d(end,:);
        %             mesh_input.Bot_x    =   X3d(1  ,:);
        %             mesh_input.Bot_y    =   Y3d(1  ,:);
        %             mesh_input.Bot_z    =   Z3d(1  ,:);
        %
        %             mesh_input.x_min    =   X3d(1,1);
        %             mesh_input.x_max    =   X3d(1,end);
        %             mesh_input.y_min    =   Y3d(1,1);
        %             mesh_input.y_max    =   Y3d(1,end);
        %             mesh_input.z_min    =   Z3d(1,1);
        %             mesh_input.z_max    =   Z3d(end,1);
        %
        %         end
        %
        %
        %         % Create x_vec, y_vec, z_vec if necessary, which describe the regular
        %         % coordinates of the sides of the box in a normalized manner (from
        %         % 0-1), which is scaled later.
        %         if ~isfield(opts,'nx');
        %             nx          =   11;
        %         else
        %             nx          =   opts.nx;
        %         end
        %
        %         if ~isfield(opts,'ny');
        %             ny          =   12;
        %         else
        %             ny          =   opts.ny;
        %         end
        %
        %         if ~isfield(opts,'nz');
        %             nz          =   13;
        %         else
        %             nz          =   opts.nz;
        %         end
        %
        %
        %         if ~isfield(mesh_input,'x_vec');
        %             dx      =   1/(nx-1);
        %             x_vec   =   0:dx:1;
        %         else
        %             % scale from 0-1
        %             x_vec   =   mesh_input.x_vec;
        %             x_vec   =   x_vec/(max(x_vec)-min(x_vec));
        %             x_vec   =   x_vec - x_vec(1);
        %         end
        %
        %         if ~isfield(mesh_input,'y_vec');
        %             dy      =   1/(ny-1);
        %             y_vec   =   0:dy:1;
        %         else
        %             % scale from 0-1
        %             y_vec   =   mesh_input.y_vec;
        %             y_vec   =   y_vec/(max(y_vec)-min(y_vec));
        %             y_vec   =   y_vec - y_vec(1);
        %         end
        %
        %         if ~isfield(mesh_input,'z_vec');
        %             dz      =   1/(nz-1);
        %             z_vec   =   0:dz:1;
        %         else
        %             % scale from 0-1
        %             z_vec   =   mesh_input.z_vec;
        %             z_vec   =   z_vec/(max(z_vec)-min(z_vec));
        %             z_vec   =   z_vec - z_vec(1);
        %         end
        %
        %         % Scale to real box size
        %         x_min       =   mesh_input.x_min;
        %         x_max       =   mesh_input.x_max;
        %         y_min       =   mesh_input.y_min;
        %         y_max       =   mesh_input.y_max;
        %         z_min       =   mesh_input.z_min;
        %         z_max       =   mesh_input.z_max;
        %
        %         mesh_input.x_vec       =   x_vec*(x_max-x_min) + x_min;
        %         mesh_input.y_vec       =   y_vec*(y_max-y_min) + y_min;
        %         mesh_input.z_vec       =   z_vec*(z_max-z_min) + z_min;
        %
        %         % Detect errors
        %         if length(x_vec) ~= nx
        %             error('Length of x_vec should be equal to nx!')
        %         end
        %         if length(y_vec) ~= ny
        %             error('Length of y_vec should be equal to ny!')
        %         end
        %         if length(z_vec) ~= nz
        %             error('Length of z_vec should be equal to nz!')
        %         end
        %
        %
        %         % Create the quadrilateral mesh
        %         MESH = mcubic(opts, mesh_input);
        %
        %         % Store some useful data:
        %         MESH.dx_min = min(diff( mesh_input.x_vec));
        %         MESH.dx_max = max(diff( mesh_input.x_vec));
        %         MESH.dy_min = min(diff( mesh_input.y_vec));
        %         MESH.dy_max = max(diff( mesh_input.y_vec));
        %         MESH.dz_min = min(diff( mesh_input.z_vec));
        %         MESH.dz_max = max(diff( mesh_input.z_vec));
        %
        %         MESH.ndim       =   3;            % number of space dimensions
        %
    otherwise
        error('element type not yet implemented')
        
end
MESH.element_type = opts.element_type;


if min(diff(unique(MESH.ELEMS(:))))<1
    error('Problem with the mesh as not all nodes are listed in MESH.ELEMS')
end
    

% add information about pressure shape function
switch MESH.element_type.pressure
    case 'P0'
        MESH.pressure.np = 1;
    case {'P-1','T1'}
        if MESH.ndim==2
            MESH.pressure.np = 3;
        end
    case 'Q1'
        MESH.pressure.np = 4;
end


% If we do two-phase flow, check whether we use the same shape function for
% compaction and fluid pressure
if isfield(opts.element_type,'darcy')
    if ~strcmp(opts.element_type.darcy,opts.element_type.pressure)
        error('For two phase flow calculations, the shape function for fluid pressure (opts.element_type.darcy) MUST be the same as the one for compaction pressure (opts.element_type.pressure)')
    end
    
end





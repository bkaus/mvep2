function NUMERICS = AddConstantErosionAndSedimentationToCode(varargin)
% AddConstantErosionAndSedimentationToCode
%   Sets erosional parameters for the top of the computational domain
%
%   Syntax:
%       NUMERICS = AddConstantErosionAndSedimentationToCode(NUMERICS, ErosionRate_mmYear, [SedimentationRate_mmYear], [ElevationErosion_m], [ElevationSedimentation_m])
%
%       Input:
%               NUMERICS                 -   NUMERICS structure (can be empty [])
%               ErosionRate_mmYear       -   Erosion rate in mm/yr
%               SedimentationRate_mmYear -   [optional parameter, default=0]    that indicates tghe sedimentation rate in mm/yr
%               ElevationErosion_m       -   [optional, default=0].             Elevation above which erosion is applied
%               ElevationSedimentation_m -   [optional, default=0].             Elevation below which sedimentation is applied
%
%       Output:
%              	NUMERICS                -   NUMERICS structure (which is later accordingly non-dimensionalized)
%
%


% Extract input parameters
NUMERICS                        =   varargin{1};
ErosionRate_mmYear              =   varargin{2};

% Default parameters
SedimentationRate_mmYear        =   0;
ElevationErosion_m              =   0;
ElevationSedimentation_m        =   0;

if nargin>2
    SedimentationRate_mmYear    =   varargin{3};
end
if nargin>3
    ElevationErosion_m          =   varargin{4};
end
if nargin>4
    ElevationSedimentation_m	=   varargin{5};
end


SecYear                         = 3600*24*365.25;   % Seconds per Year
NUMERICS.ErosionSedimentation.Method                                      =	 'Constant_SedimentationErosionRates';
NUMERICS.ErosionSedimentation.Erosion_Velocity                            =   ErosionRate_mmYear*1e-3/SecYear;           % in m/s
NUMERICS.ErosionSedimentation.Sedimentation_Velocity                      =   SedimentationRate_mmYear*1e-3/SecYear;     % in m/s
NUMERICS.ErosionSedimentation.ElevationBelowWhichToApplySedimentation     =   ElevationSedimentation_m;                        % Elevation below which we apply sedimentation
NUMERICS.ErosionSedimentation.ElevationAboveWhichToApplyErosion           =   ElevationErosion_m;  
            
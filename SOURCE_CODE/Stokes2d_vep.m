function [MESH, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, MATERIAL_PROPS, dt] = Stokes2d_vep(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, CHAR, PARTICLES, dt)
% Viscoelastoplastic FEM solver for incompressible flow.
%
% This routine performs the nonlinear iterations for plasticity and
% powerlaw using fixed point iterations & calls Stokes2d_ve for the
% viscoelastic Stokes solver.

% $Id$


cpu_start           =   cputime;

% Numerical parameters
AccuracyPowerlaw    =   NUMERICS.Nonlinear.Tolerance;                       % cutoff criteria
max_iter            =   NUMERICS.Nonlinear.MaxNumberIterations;             % max # of nonlinear iterations
min_iter            =   NUMERICS.Nonlinear.MinNumberIterations;           	% min # of nonlinear iterations

if ~isfield(MESH,'VEL');
    MESH.VEL        =   zeros(size(MESH.NODES));
end

nvel            =   prod(size(MESH.NODES));
ErrorPowerlaw   =   realmax;
niter           =   0;
time_StokesSolver=  0;      % record time spend in the (linear) Stokes solver
num_StokesSolver =  0;
time_start      =   cputime;
cpustart        =   cputime;

if NUMERICS.ShowTimingInformation
    disp( '----- ')
end

%% If necessary, perform Picard iterations, which are usually sufficient
INTP_PROPS.Strain_old = INTP_PROPS.Strain;
IterationError      =   0;
MESH.NODES0         =   MESH.NODES;
while ((ErrorPowerlaw>AccuracyPowerlaw) & (niter<max_iter)) | (niter<min_iter)
    
    
    MESH.NODES  =       MESH.NODES0;
    
    
    VEL_ITER0                               =   MESH.VEL;
    niter                                   =   niter+1;
    NUMERICS.Nonlinear.CurrentNonlinearIter =   niter;
    
    % 1) Compute effective viscosity, density and elastic shear module @ every integration point
    switch NUMERICS.Nonlinear.Method
        case 'PhaseRatioIntegrationPoints'
            % This method computes properties at the integration points
            % based on the phase ratio of each of the particles.
            [INTP_PROPS, NUMERICS, MATERIAL_PROPS] 	=   UpdatePropertiesAt_INTPS_PhaseRatio(INTP_PROPS, MATERIAL_PROPS, NUMERICS, CHAR, dt, MESH);
            
        case 'ParticleBased'
            % Update properties at the particles
            [INTP_PROPS, NUMERICS]                  =   UpdatePropertiesAt_INTPS_Particlebased(PARTICLES, INTP_PROPS, MESH, MATERIAL_PROPS, NUMERICS, CHAR, BC, dt);
    end
    if NUMERICS.ShowTimingInformation
        time_increment = cputime-time_start;
        disp(['  Stokes timing :  Update properties at integration points       =  ',num2str(time_increment,'%2.2f'),'s;   ',num2str(cputime-cpustart,'%2.2f'),'s'])
        cpustart   = cputime;
    end
    
    switch NUMERICS.Nonlinear.IterationMethod
        case 'NonlinearResidual'
            % REMARK: DON'T USE THIS CODE YET - ITS UNDER DEVELOPMENT
            
            if NUMERICS.LinearSolver.FreeSurfaceStabilizationAlgorithm ~= 0
                % Newton does currently not work with FSSA activated
                % (might not actually be necessary)
                NUMERICS.LinearSolver.FreeSurfaceStabilizationAlgorithm = 0;
            end
            
            method_local    = 1;   % 1-picard, 2-newton
            omega           = NUMERICS.Nonlinear.Omega;
            
            if niter==1
                % picard step
                [INTP_PROPS, NUMERICS, MATERIAL_PROPS]                      =   UpdatePropertiesAt_INTPS_PhaseRatio(INTP_PROPS, MATERIAL_PROPS, NUMERICS, CHAR, dt, MESH);
                [MESH, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, Sol_vec]     =   Stokes2d_ve_solver(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt);
                [nonLinear_Res , Res_vec, OUTPUT_MAT]                       =   Stokes2D_ve_NonlinearResidual(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt, Sol_vec, CHAR,logical(0));
                nonLinear_Res_init = nonLinear_Res;
            end
            
            if niter>NUMERICS.Nonlinear.NumberPicardIterations
                method_local = 2;   % switch to Newton after a few iterations
            end
            
            cpu_t0                                  =   cputime;
            Free        =   OUTPUT_MAT.Free;
            
            if      method_local==1
                A           =   OUTPUT_MAT.VV;
                sdof                        =   length(OUTPUT_MAT.VV) + size(OUTPUT_MAT.Q,2);
                nv                          =   length(OUTPUT_MAT.VV);
                np                          =   size(OUTPUT_MAT.Q,2);
                J                           =   sparse(sdof, sdof);
                J(1:nv,1:nv)            =   A;
                J(1:nv,nv+1:sdof)       =   OUTPUT_MAT.Q;
                J(nv+1:sdof,1:nv)       =   OUTPUT_MAT.Q';
                J(nv+1:sdof,nv+1:sdof) =    (-1/(1e10*100)).*speye(np,np);
                
            elseif  method_local==2
                sdof                        =   length(OUTPUT_MAT.J) + size(OUTPUT_MAT.VP,2);
                nv                          =   length(OUTPUT_MAT.J);
                np                          =   size(OUTPUT_MAT.VP,2);
                J                           =   sparse(sdof, sdof);
                J(1:nv,1:nv)            =   OUTPUT_MAT.J;
                J(1:nv,nv+1:sdof)       =   OUTPUT_MAT.VP;
                J(nv+1:sdof,1:nv)       =   OUTPUT_MAT.PV;
                J(nv+1:sdof,nv+1:sdof) =    (-1/(1e10*100)).*speye(np,np);
            end
            
            % Assemble the global residual
            Rhs                         =   [OUTPUT_MAT.ResVel(Free); OUTPUT_MAT.ResPres];
            
            if niter>1
                dSol_old = dSol;
            end
            
            dSol                        =   J\-Rhs;      % solve
            
            
            
            % Update solution vector
            dSol_V                      =   dSol(1:length(Free));
            dSol_P                      =   dSol(length(Free)+1:end);
            
            
            LineSearchMethod  = {'Jeremic','Perez_Foguet'};
            
            LineSearchMethod = LineSearchMethod{1};
            
            if  method_local==2
                
                switch LineSearchMethod
                    case 'Jeremic'
                        
                        [nonLinear_Res0, Res_vec0]  =   Stokes2D_ve_NonlinearResidual(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt, Sol_vec, CHAR, logical(0));
                        omega                       =   1;
                        alpha                       =   0.25;
                        
                        dF_dx_P                     =   Res_vec0*0;
                        temp                        =   J*dSol;
                        dF_dx_P(Free)               =   temp(Free);
                        dF_dx_P(nvel+1:end)         =   temp(length(Free)+1:end);
                        Fnew                        =   Res_vec0 + alpha*omega*dF_dx_P;
                        
                        
                        Sol_vec_trial               =   Sol_vec;
                        Sol_vec_trial(Free)         =   Sol_vec_trial(Free)          +  omega*dSol_V;
                        Sol_vec_trial(nvel+1:end)   =   Sol_vec_trial(nvel+1:end)    +  omega*dSol_P;
                        [nonLinear_Res, Res_vec, OUTPUT_MAT]    =   Stokes2D_ve_NonlinearResidual(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt, Sol_vec_trial, CHAR, logical(0));
                        
                        % Line-search method described in
                        %  Jeremic (2001) Line search techniques for elasto-plastic
                        %  finite element computations in geomechanics. Comm. Num.
                        %  Meth. Eng. 17(2) p. 115-126
                        while nonLinear_Res>norm(Fnew)
                            
                            omega = omega*0.5;
                            
                            dF_dx_P                     =   Res_vec0*0;
                            dF_dx_P(Free)               =   temp(Free);
                            dF_dx_P(nvel+1:end)         =   temp(length(Free)+1:end);
                            Fnew                        =   Res_vec0 + alpha*omega*dF_dx_P;
                            
                            
                            Sol_vec_trial               =   Sol_vec;
                            Sol_vec_trial(Free)         =   Sol_vec_trial(Free)          +  omega*dSol_V;
                            Sol_vec_trial(nvel+1:end)   =   Sol_vec_trial(nvel+1:end)    +  omega.*dSol_P;
                            
                            % Update stresses and strainrates @ integration points
                            %                         INTP_PROPS                                                  =   Stokes2D_ve_UpdatedStressesStrainrates(Sol_vec, MESH, NUMERICS, INTP_PROPS, MATERIAL_PROPS, BC, dt);
                            
                            [nonLinear_Res, Res_vec, OUTPUT_MAT]    =   Stokes2D_ve_NonlinearResidual(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt, Sol_vec_trial, CHAR, logical(0));
                            
                            
                            if method_local==1 & omega<0.5
                                break;
                            elseif method_local==2 & omega<0.01
                                break;
                            end
                            
                            
                        end
                        
                    case 'Perez_Foguet'
                        % according to algorithm A1 in
                        % Perez-Foguet, A., Armero, F., 2001. On the formulation of closest-point projection algorithms in elastoplasticity?part II: Globally convergent schemes. International Journal for Numerical Methods in Engineering 53, 331?374.
                        
                        
                        % New line-search algorithm
                        eta     =   0.1;
                        beta    =   1e-4;
                        alpha   =   1;
                        j       =   1;
                        M       =   dot(Res_vec,Res_vec)/2;
                        Mder    =   -2*M;
                        
                        while j<5
                            
                            % update solution and compute new residual
                            Sol_vec_trial               =   Sol_vec;
                            Sol_vec_trial(Free)         =   Sol_vec_trial(Free)          +  alpha*dSol_V;
                            Sol_vec_trial(nvel+1:end)   =   Sol_vec_trial(nvel+1:end)    +  alpha.*dSol_P;
                            [nonLinear_Res, Res_vec]    =   Stokes2D_ve_NonlinearResidual(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt, Sol_vec_trial, CHAR, logical(0));
                            
                            Mnew                        =   dot(Res_vec,Res_vec)/2;
                            
                            if Mnew <= (1-2*alpha*beta)*M
                                break;                      % convergence
                                
                            else
                                
                                % update alpha if not converged
                                alpha_new = max( [ eta*alpha, -alpha^2*Mder/( 2*(Mnew -M - alpha*Mder) )  ] )
                                
                                %   M = Mnew;
                                alpha = alpha_new;
                                
                                
                            end
                            
                            
                            j = j+1;
                            
                        end
                        
                        omega = alpha;
                        
                end
                
            else
                % P, assume omega=1;
                if niter>1
                    Angle = acos(dot(dSol,dSol_old)/(norm(dSol)*norm(dSol_old)));
                    
                else
                    Angle =  acos(dot(dSol,dSol)/(norm(dSol)*norm(dSol)));
                    
                end
                
                if      Angle<=pi/8
                    %                     omega = 2.5;
                    omega = 1.0;
                    
                elseif  Angle>pi/8 &  Angle<19*pi/20
                    omega = 1.0;
                elseif  Angle>=19*pi/20
                    omega = 0.5;
                    
                end
                
                Sol_vec_trial               =   Sol_vec;
                Sol_vec_trial(Free)         =   Sol_vec_trial(Free)          +  omega*dSol_V;
                Sol_vec_trial(nvel+1:end)   =   Sol_vec_trial(nvel+1:end)    +  omega.*dSol_P;
            end
            
            SUCCESS = 1;
            minDetJ = 1;
            
            SolverTime                              =   cputime-cpu_t0;                     % time of solver (not computing Jacobian)
            time_StokesSolver                       =   time_StokesSolver + SolverTime;
            num_StokesSolver                        =   num_StokesSolver + 1;
            
            % Compute nonlinear residual and jacobian through finite difference
            if niter<NUMERICS.Nonlinear.NumberPicardIterations
                % no need to compute jacobian if we do Picard
                [nonLinear_Res , Res_vec, OUTPUT_MAT, MESH]   =   Stokes2D_ve_NonlinearResidual(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt, Sol_vec_trial, CHAR, logical(0));
            else
                [nonLinear_Res , Res_vec, OUTPUT_MAT, MESH]   =   Stokes2D_ve_NonlinearResidual(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt, Sol_vec_trial, CHAR, logical(1));
            end
            
            f2              =   [OUTPUT_MAT.f; OUTPUT_MAT.g];   % body forces
            f1              =   Res_vec - f2;                   % stresses from momentum
            
            f1(OUTPUT_MAT.Bc_ind) = 0;
            f2(OUTPUT_MAT.Bc_ind) = 0;
            
            nonLinear_Res2  =  norm(f1+f2)/(norm(f1) + norm(f2));
            
            IterationError(niter)                   =   nonLinear_Res2;          % keep track of convergence
            Sol_vec                                 =   Sol_vec_trial;
            
            % ConvergenceAnton = norm(dSol(1:length(Free)))/norm(Sol_vec_trial(1:length(Free)));          % check velocity convergence the way anto describes iy
            
            ErrorPowerlaw           =   mean(abs(MESH.VEL(:)-VEL_ITER0(:)))./max(abs(MESH.VEL(:)));
            
            % Update stresses and strainrates
            % INTP_PROPS                                                  =   Stokes2D_ve_UpdatedStressesStrainrates(Sol_vec, MESH, NUMERICS, INTP_PROPS, MATERIAL_PROPS, BC, dt);
            
            str = [' Nonlinear iteration ',num2str(niter),' rel. error=',num2str(ErrorPowerlaw),'; nonlinear residual=',num2str(nonLinear_Res),'; nonlinear residual2=',num2str(nonLinear_Res2), '; omega=',num2str(omega)];
            % ErrorPowerlaw                           =   nonLinear_Res2;
            
            if      method_local==1
                str= [str, '; Picard step'];
            elseif  method_local==2
                str= [str, '; Newton step'];
            end
            
            disp(str)
            
            if mod(niter,30)==0
                % viscosity continuation
                NUMERICS.Viscosity.UpperCutoff = NUMERICS.Viscosity.UpperCutoff*1.75;
                NUMERICS.Viscosity.UpperCutoff*CHAR.Viscosity
                
                if NUMERICS.Viscosity.UpperCutoff*CHAR.Viscosity>1e23;
                    NUMERICS.Viscosity.UpperCutoff = 1e23/CHAR.Viscosity;
                end
                
            end
            
            
            % Check if picard has reduced the residual sufficiently such that we can switch to newton
            %             if niter>1
            %                 ResidualReduction = nonLinear_Res/IterationError(1);
            %
            %                 if  method_local==1 & (ResidualReduction<0.01 | nonLinear_Res<5e-5)
            %                     disp(['switching to newton'])
            %                     NUMERICS.Nonlinear.NumberPicardIterations = niter-1; % switch to newton
            %                     [nonLinear_Res , Res_vec, OUTPUT_MAT, MESH]   =   Stokes2D_ve_NonlinearResidual(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt, Sol_vec_trial, CHAR, logical(1));
            %                     nonLinear_Res                           =   norm(Res_vec)/norm(Sol_vec);
            %                 end
            %
            %
            %
            if niter<3
                nonLinear_Res_init = nonLinear_Res;
            end
            
        case 'Picard'
            
            cpu_t0                                  =   cputime;
            [MESH, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, Sol_vec]    =   Stokes2d_ve_solver(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt);
            SolverTime                              =   cputime-cpu_t0;
            time_StokesSolver                       =   time_StokesSolver + SolverTime;
            num_StokesSolver                        =   num_StokesSolver + 1;
            if NUMERICS.ShowTimingInformation
                time_increment = cputime-time_start;
                disp(['  Stokes timing :  Viscoelastic Stokes solver                    =  ',num2str(time_increment,'%2.2f'),'s;   ',num2str(cputime-cpustart,'%2.2f'),'s'])
                cpustart   = cputime;
            end
            
            % Compute relative error in velocity ---------------------------------=
            ErrorPowerlaw           =   mean(abs(MESH.VEL(:)-VEL_ITER0(:)))./max(abs(MESH.VEL(:)));
            disp([' Nonlinear iteration ',num2str(niter),' rel. error=',num2str(ErrorPowerlaw)])
            
            
            
            
            
            %             % For cross-checking we can here compute the NL residual:
            %             [nonLinear_Res , Res_vec, OUTPUT_MAT]   =   Stokes2D_ve_NonlinearResidual(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, dt, Sol_vec, CHAR,logical(0));
            %             nonLinear_Res
            %             f2              =   [OUTPUT_MAT.f; OUTPUT_MAT.g];   % body forces
            %             f1              =   Res_vec - f2;                   % stresses from momentum
            %
            %             f1(OUTPUT_MAT.Bc_ind) = 0;
            %             f2(OUTPUT_MAT.Bc_ind) = 0;
            %
            %             nonLinear_Res2  =  norm(f1+f2)/(norm(f1) + norm(f2))
            
            %             nonLinear_Res                           =   norm(Res_vec)/norm(Sol_vec)
            
        otherwise
            error('Unknown nonlinear iteration method')
    end
    
    %======================================================================
    % Update strain during iteration steps
    INTP_PROPS.Strain       = 	INTP_PROPS.Strain_old + dt*INTP_PROPS.Strainrate.E2nd_vp;
    %     INTP_PROPS.Strain_local =	INTP_PROPS.Strain;      % store local strain increment
    
    INTP_PROPS.Strain_local =	INTP_PROPS.Strain;      % store local strain increment
    
    
    %==========================================================================
    if isfield(NUMERICS.Plasticity,'NonLocalPlasticity')
        % Nonlocal plasticity is active
        switch NUMERICS.Plasticity.NonLocalPlasticity.Type
            case 'Gradient_Strainrate'
                % not within this routine
                %                 if isfield(INTP_PROPS,'StrainNonLocal')
                %                     INTP_PROPS.Strain = INTP_PROPS.StrainNonLocal;
                %                 end
                
                
%                 NonLocalRadius                  = 	NUMERICS.Plasticity.NonLocalPlasticity.Radius;
%                 alpha                           =	NUMERICS.Plasticity.NonLocalPlasticity.alpha;
%                 
%                 % apply diffusion step with corresponding lengthscale
%                 [INTP_PROPS.Strain_nonlocal]    =   GradientPlasticity_DiffusionStep(MESH, INTP_PROPS.Strain, NUMERICS, NonLocalRadius);
%                 
%                 %                 [Mesh_Strain_Average,INTP_PROPS.Strain_nonlocal] = NonLocalPlasticity_Averaging(MESH,INTP_PROPS, NonLocalRadius, 'Bell-Shaped Average');
%                 %                 [Mesh_Strain_Average,INTP_PROPS.Strain_nonlocal] = NonLocalPlasticity_Averaging(MESH,INTP_PROPS, NonLocalRadius, 'ArithmeticAverage');
%                 
%                 
%                 INTP_PROPS.Strain               =   (1-alpha)*INTP_PROPS.Strain_local + alpha*INTP_PROPS.Strain_nonlocal;
                
            case 'Gradient_Strain'
                % Non-local smoothening of plastic parameters:
                NonLocalRadius                  = 	NUMERICS.Plasticity.NonLocalPlasticity.Radius;
                alpha                           =	NUMERICS.Plasticity.NonLocalPlasticity.alpha;
                
                % apply diffusion step with corresponding lengthscale
                [INTP_PROPS.Strain_nonlocal]    =   GradientPlasticity_DiffusionStep(MESH, INTP_PROPS.Strain, NUMERICS, NonLocalRadius);
                
                %                 [Mesh_Strain_Average,INTP_PROPS.Strain_nonlocal] = NonLocalPlasticity_Averaging(MESH,INTP_PROPS, NonLocalRadius, 'Bell-Shaped Average');
                %                 [Mesh_Strain_Average,INTP_PROPS.Strain_nonlocal] = NonLocalPlasticity_Averaging(MESH,INTP_PROPS, NonLocalRadius, 'ArithmeticAverage');
                
                
                INTP_PROPS.Strain               =   (1-alpha)*INTP_PROPS.Strain_local + alpha*INTP_PROPS.Strain_nonlocal;
                
                
            otherwise
                error('Unknown nonlocal plasticity type')
        end
    end
    %==========================================================================
    
    
    
    
    %     % Use the VERMEER definition here (alpha=1 = only nonlocal, alpha=3 = his recommendation):
    %     alpha = 1;
    %     if alpha>0
    %         NonLocalRadius          =   2000;
    % %       AveragingMethod      	=   {'ArithmeticAverage','Bell-Shaped Average'};
    % %       AveragingMethod         =   AveragingMethod{2};
    % %       [MESH.HistVar.Strain_nonlocal, INTP_PROPS.Strain_nonlocal] = NonLocalPlasticity_Averaging(MESH,INTP_PROPS, NonLocalRadius/CHAR.Length,AveragingMethod); % compute nonlocal strain
    %
    %         [INTP_PROPS.Strain_nonlocal, MESH.HistVar.Strain_nonlocal] = GradientPlasticity_DiffusionStep(MESH, INTP_PROPS.Strain, NUMERICS, 1*NonLocalRadius/CHAR.Length);
    %
    %         INTP_PROPS.Strain       =   (1-alpha)*INTP_PROPS.Strain_local + alpha*INTP_PROPS.Strain_nonlocal;
    %
    %     else
    %
    %     end
    %
    %
    %======================================================================
    
    
    
    %     if 1==0
    %         % Plotting
    %         figure(1234), clf
    %         subplot(211)
    %         PlotMesh(MESH,OUTPUT_MAT.residual_2D(1,:)), colorbar, shading flat
    %
    %         subplot(212)
    %         PlotMesh(MESH,OUTPUT_MAT.residual_2D(2,:)), colorbar, shading flat
    %
    %         %             subplot(413)
    %         %             Sol_P                      =   Sol_vec(nvel+1:end)';
    %         %             pcolor(dSol_P(MESH.RegularElementNumber)/norm(Sol_P)); colorbar, shading flat
    %         %             title('dP')
    %         %
    %         %             subplot(414)
    %         %             dSol_vel =    Sol_vec*0;
    %         %             dSol_vel(Free)         =   dSol_V;
    %         %
    %         %
    %         %             PlotMesh(MESH,dSol_vel(1:2:end)/max(abs(Sol_vec(1:2:end)))), colorbar, shading flat
    %         %             title('dVx')
    %         %
    %
    %         drawnow
    %
    %
    %         figure(333), clf
    %         INTP_PROPS                                                  =   Stokes2D_ve_UpdatedStressesStrainrates(Sol_vec, MESH, NUMERICS, INTP_PROPS, MATERIAL_PROPS, BC, dt);
    %         plotclr(INTP_PROPS.X(:),INTP_PROPS.Z(:),INTP_PROPS.Pressure(:),'o')
    %
    %         %             INTP_PROPS                  =   UpdateStressesStrainsAfterTimestep(INTP_PROPS, dt);
    %         %
    %     end
    
end

% Set back strain to 'old' value, to not confuse subsequent routines
% INTP_PROPS.Strain =  INTP_PROPS.Strain_old;

switch NUMERICS.Nonlinear.IterationMethod
    case 'NonlinearResidual'
        % Update stresses and strainrates on integration points
        INTP_PROPS = Stokes2D_ve_UpdatedStressesStrainrates(MESH, NUMERICS, INTP_PROPS, MATERIAL_PROPS, BC, dt);
    otherwise
end

if NUMERICS.TwoPhase
    MESH.div_vs0    = MESH.div_vs;
end

time_FullSolver = cputime-cpu_start;

% Display timing information
disp(['Solver took ',num2str(time_FullSolver),'s in total; ',num2str(time_StokesSolver),'s for the Stokes solver or '    ,num2str(time_StokesSolver/num_StokesSolver),'s/linear Stokes solver step.'])

% if we did not iterate above, we might not have updated the INTP_PROPS for the temperature solver
if niter==0
    switch NUMERICS.Nonlinear.Method
        case 'PhaseRatioIntegrationPoints'
            % This method computes properties at the integration points
            % based on the phase ratio of each of the particles.
            [INTP_PROPS, dummy, MATERIAL_PROPS]     =   UpdatePropertiesAt_INTPS_PhaseRatio(INTP_PROPS, MATERIAL_PROPS, NUMERICS, CHAR, dt);
            
        case 'ParticleBased'
            % Update properties at the particles
            [INTP_PROPS]                            =   UpdatePropertiesAt_INTPS_Particlebased(PARTICLES, INTP_PROPS, MESH, MATERIAL_PROPS, NUMERICS, CHAR, BC, dt);
    end
    
end

if isfield(NUMERICS,'Adjoint')
    if NUMERICS.Adjoint.Adjoint == 1
        NUMERICS.Adjoint.J = J;      % give the Jacobian to the Adjoint solver as it needs to have the Jacobian matrix
        NUMERICS.Adjoint.Sol_vec = Sol_vec;
        NUMERICS.Adjoint.Free = Free;
    end
end

NUMERICS.Nonlinear.IterationError = IterationError;

if NUMERICS.ShowTimingInformation
    disp( '----- ')
end



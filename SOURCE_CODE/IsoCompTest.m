function [NUMERICS,surface_data] = IsoCompTest(MESH,NUMERICS,CHAR,itime,time,surface_data)
%
% [NUMERICS,surface_data] = IsoCompTest(MESH,CHAR,itime,time,surface_data)
% 
% Test for isostatic compensation
% =========================================================================


% extract data from mesh 
Rho2d     = MESH.CompVar.Rho(MESH.RegularGridNumber)     *CHAR.Density;
P2d       = MESH.CompVar.Pressure(MESH.RegularGridNumber)*CHAR.Stress;
Plith     = P2d(1,:);
Plith_dev = Plith - mean(Plith);
IsoTopo   = Plith_dev./mean(Rho2d)/9.81;

% check whether field exists
if ~isfield(surface_data,'topoiso')
    surface_data.topoiso{1}= [];
end
surface_data.topoiso{itime} = IsoTopo;


% Compute skewness and its derivative
NUMERICS.IsoComp.skew(itime)  = skewness(IsoTopo);
NUMERICS.IsoComp.time(itime)  = time*CHAR.Time/CHAR.SecYear/1e3;
if itime > 1
    NUMERICS.IsoComp.dskew(itime) = diff(NUMERICS.IsoComp.skew(itime-1:itime));
    NUMERICS.IsoComp.dtime(itime) = diff(NUMERICS.IsoComp.time(itime-1:itime));
else
    NUMERICS.IsoComp.dskew(itime) = NUMERICS.IsoComp.skew(itime);
    NUMERICS.IsoComp.dtime(itime) = NUMERICS.IsoComp.time(itime);
end

% test whether the running mean of the difference in skewness dropped below the threshold 
if itime >= NUMERICS.IsoComp.N_rm+1
    % compute stopping criteria as running mean
    Dskew = sum(abs(NUMERICS.IsoComp.dskew(end-(NUMERICS.IsoComp.N_rm-1):end)));
    Dtime = sum(abs(NUMERICS.IsoComp.dtime(end-(NUMERICS.IsoComp.N_rm-1):end)));

    stopcrit = Dskew/Dtime;

    NUMERICS.IsoComp.stopcrit(itime) = stopcrit;


	display(['Isostatic equilibration: ' num2str(stopcrit)]);
    
    if (stopcrit < NUMERICS.IsoComp.threshold ) 
        NUMERICS.IsoComp.StopNow = true;
        display('-- Isostatic equilibration has been completed --');
    end
end

if itime >= NUMERICS.IsoComp.Nmax 
    NUMERICS.IsoComp.StopNow = true;
    display('-- Isostatic equilibration has been completed due to Nmax --');
end

end

function s = skewness(x)
    % The output size for [] is a special case, handle it here.
    if isequal(x,[]), s = NaN; return; end;

    % Figure out which dimension nanmean will work along.
    dim = find(size(x) ~= 1, 1);
    if isempty(dim), dim = 1; end


    % Need to tile the output of nanmean to center X.
    tile = ones(1,max(ndims(x),dim));
    tile(dim) = size(x,dim);

    % Center X, compute its third and second moments, and compute the
    % uncorrected skewness.
    x0 = x - repmat(nanmean(x,dim), tile);
    s2 = nanmean(x0.^2,dim); % this is the biased variance estimator
    m3 = nanmean(x0.^3,dim);
    s = m3 ./ s2.^(1.5);
end

function m = nanmean(x,dim)
    % Find NaNs and set them to zero
    nans = isnan(x);
    x(nans) = 0;

    if nargin == 1 % let sum deal with figuring out which dimension to use
        % Count up non-NaNs.
        n = sum(~nans);
        n(n==0) = NaN; % prevent divideByZero warnings
        % Sum up non-NaNs, and divide by the number of non-NaNs.
        m = sum(x) ./ n;
    else
        % Count up non-NaNs.
        n = sum(~nans,dim);
        n(n==0) = NaN; % prevent divideByZero warnings
        % Sum up non-NaNs, and divide by the number of non-NaNs.
        m = sum(x,dim) ./ n;
    end
end
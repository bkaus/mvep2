function [C_Prefactor,Phi_Prefactor] = StrainWeakeningPlasticity(INTP_PROPS,StrainWeakening);
% Applies strain weakening of cohesion and friction angle based on
% accumulated strain. A simple linear weakening is typically
% employed.
%
% Original: 7.24.2008 Boris Kaus
% Modified to take Particles data as input, Nov. 2010, BK
%
% $Id$


WeakeningType           =   fieldnames(StrainWeakening);
if size(WeakeningType,1) > 1
    error('You can only have one weaking type activated')
end

switch WeakeningType{1}
    case 'Linear'
        
        % Type of strain employed to weaken
        Strain              =   INTP_PROPS.Strain;
        
        Strain_Start        =   StrainWeakening.Linear.Strain_Start* ones(size(Strain));
        Strain_End          =   StrainWeakening.Linear.Strain_End* ones(size(Strain));
        C_Start             =   ones(size(Strain));
        C_End               =   StrainWeakening.Linear.Prefactor_End_Cohesion* ones(size(Strain));
        Phi_Start           =   ones(size(Strain));
        Phi_End             =   StrainWeakening.Linear.Prefactor_End_FrictionAngle* ones(size(Strain));
        
        
        C_Prefactor         =   zeros(size(Strain));
        Phi_Prefactor       =   zeros(size(Strain));
        
        % Fully weakened
        ind                 =   find(Strain>=Strain_End);
        C_Prefactor(ind) 	=   C_End(ind);
        Phi_Prefactor(ind)	=   Phi_End(ind);
        
        % Being weakened
        ind                 =   find(Strain>Strain_Start & Strain<Strain_End);
        Factor              =   (Strain(ind)-Strain_Start(ind))./(Strain_End(ind)-Strain_Start(ind));
        C_Prefactor(ind) 	=   C_Start(ind)   + Factor.*(C_End(ind)   - C_Start(ind));
        Phi_Prefactor(ind)	=   Phi_Start(ind) + Factor.*(Phi_End(ind) - Phi_Start(ind));
        
        % Not weakened
        ind                 =   find(Strain<=Strain_Start);
        C_Prefactor(ind)    =   C_Start(ind);
        Phi_Prefactor(ind)	=   Phi_Start(ind);
        
    case 'Nonlinear_Rey2014'
        % As implemented in 
         % Nonlinear weakening according to Rey et al. (2014) Nature.
        %
        %  eta = eta_ref*(1-(1-xi_end)*(e_p/strain)^0.25) for e_p<strain
        % 
        %  eta = eta_ref*xi_end   for e_p>strain
        %
        % with xi_end  = the weakening at the end 
        %       strain = strain after which weakeing finished  (Strain_End)
        
        
        % Type of strain employed to weaken
        Strain              =   INTP_PROPS.Strain;
        
        Strain_End          =   StrainWeakening.Nonlinear_Rey2014.Strain_End* ones(size(Strain));
        C_Start             =   ones(size(Strain));
        C_End               =   StrainWeakening.Nonlinear_Rey2014.Prefactor_End_Cohesion* ones(size(Strain));
        Phi_Start           =   ones(size(Strain));
        Phi_End             =   StrainWeakening.Nonlinear_Rey2014.Prefactor_End_FrictionAngle* ones(size(Strain));
        
        
        C_Prefactor         =   zeros(size(Strain));
        Phi_Prefactor       =   zeros(size(Strain));
        
        % Fully weakened
        ind                 =   find(Strain>=Strain_End);
        C_Prefactor(ind) 	=   C_End(ind);
        Phi_Prefactor(ind)	=   Phi_End(ind);
        
        
        % Initial weakening
        ind                 =   find(Strain<=Strain_End);
        C_Prefactor(ind)    =   C_Start(ind)  .*(1-(1-  C_End(ind)).*(Strain(ind)./Strain_End(ind)).^0.25);
        Phi_Prefactor(ind)	=   Phi_Start(ind).*(1-(1-Phi_End(ind)).*(Strain(ind)./Strain_End(ind)).^0.25);
        
        
    otherwise
        error('Unknown strain weakening type')
end


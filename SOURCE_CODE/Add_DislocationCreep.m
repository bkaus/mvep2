function [MATERIAL_PROPS] = Add_DislocationCreep(MATERIAL_PROPS, PhaseNumber, RockFlowLawName)
% Add_DislocationCreep adds predefined rheological dislocation creep
% parameters to a given phase. These parameters are usually taken from the
% literature.
%
% Syntax:
%   [MATERIAL_PROPS] = Add_DislocationCreep(MATERIAL_PROPS, Phase, RockFlowLawName)
%
%       Input:
%               MATERIAL_PROPS  -   Structure containing material properties
%                                   for the various phases.
%               Phase           -   The number of the phase to which you want
%                                   to add these creep law parameters.
%               RockFlowLawName -   Name of the flowlaw. See the file
%                                   Add_DislocationCreep.m to have a look at
%                                   what is available.
%       Output:
%               MATERIAL_PROPS  -   Updated material properties structure
%
% We assume that the creeplaw has the form:
%
%   eII = A*Tau^n * C_OH^r *exp( - (E + P*V)/(R*T))*exp(alpha*phi)
%
%   eII     -   strain rate             [1/s        ]
%   Tau     -   stress                  [Pa         ]
%   P       -   Pressure                [Pa         ]
%   R       -   Universal gas constant  [=8.3145
%   A       -   prefactor, typically given in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]
%   n       -   powerlaw exponent       [   ]
%   E       -   Activation Energy       [J/mol]
%   V       -   Activation volums       [J/Pa/mol] = [m^3/mol]
%   C_OH    -   Water fugacity in H/10^6 Si  (see Hirth & Kohlstedt 2003 for a description)
%   r       -   Powerlaw exponent of C_OH term
%   alpha   -   melt fraction prefactor
%   phi     -   melt fraction
%
%   In addition, we take into account that the creeplaws are typically
%   measured under uniaxial or triaxial compression, whereas we need them
%   in tensorial format (this gives some geometrical factors).
%
% See also: Add_DiffusionCreep, ex_ViscousRheologyProfile

% $Id$


switch RockFlowLawName
    
    case  'Dry Olivine - Ranalli 1995'
        A                   =   2.5e4;
        n                   =   3.5;
        E                   =   532e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'UniAxial';     % Add the transformation from uni-axial -> tensorial form or not?
        V                   =   17e-6;          % Activation volume [J/mol]
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;             % melt fraction prefactor
        
        
    case 'Wet Olivine - Ranalli 1995'
        A                   =   2.0e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        n                   =   4.0;
        E                   =   471e3;
        TensorCorrection    =   'UniAxial';     % Add the transformation from uni-axial -> tensorial form or not?
        V                   =   0;              % Activation volume
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;             % melt fraction prefactor
        
    case 'Quartz Diorite - Hansen and Carter (1982)'
        % Taken from Carter and Tsenn (1986). Flow properties of
        % continental lithosphere
        % page 18.
        A                   =   10^-1.5;
        n                   =   2.4;
        E                   =   212e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'SimpleShear';     % Add the transformation from uni-axial -> tensorial form or not? - Not comitted
        V                   =   0;              % Activation volume
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;             % melt fraction prefactor
        
    case 'Diabase - Caristan (1982)'
        % Taken from J. de Bremond d'Ars et al./Tectonophysics (1999). Hydrothermalism and Diapirism in the Archaean:
        % gravitational instability constrains.
        % page 5.
        A                   =   6e-2;
        n                   =   3.05;
        E                   =   276e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'UniAxial';     % Add the transformation from uni-axial -> tensorial form or not? - Not comitted
        V                   =   0;              % Activation volume
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;             % melt fraction prefactor
        
    case 'Tumut Pond Serpentinite - Raleigh and Paterson (1965)'
        % Taken from J. de Bremond d'Ars et al./Tectonophysics (1999). Hydrothermalism and Diapirism in the Archaean:
        % gravitational instability constrains.
        % page 5.
        A                   =   6.3e-7;
        n                   =   2.8;
        E                   =   66e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'UniAxial';     % Add the transformation from uni-axial -> tensorial form or not? - Not comitted
        V                   =   0;              % Activation volume
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;             % melt fraction prefactor
        
    case 'Wet Quarzite - Ranalli 1995'
        % used in LI, Z. H., GERYA, T. V. and BURG, J.-P. (2010),
        %         Influence of tectonic overpressure on PT paths of HPUHP rocks in continental collision zones: thermomechanical modelling.
        %         Journal of Metamorphic Geology, 28: 227247. doi: 10.1111/j.1525-1314.2009.00864.x
        %         Table.2
        % in Ranalli 1995 (page 334 Table 10.3)
        % 'Uniaxial' nach "Analytische Untersuchung intralhosph??rischer Deformation und Numerische Methoden zur Bestimmung
        %            krustaler Spannungsdom??nen"; Dissertation Fakult??t der Physik TH Karlsruhe
        %            S.48
        A                   =   3.2e-4;
        n                   =   2.3;
        E                   =   154e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'UniAxial';     % Add the transformation from uni-axial -> tensorial form or not?
        V                   =   0;              % Activation volume
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;             % melt fraction prefactor
        
    case 'Quartzite - Ranalli 1995'
        % used in LI, Z. H., GERYA, T. V. and BURG, J.-P. (2010),
        %         Influence of tectonic overpressure on PT paths of HPUHP rocks in continental collision zones: thermomechanical modelling.
        %         Journal of Metamorphic Geology, 28: 227247. doi: 10.1111/j.1525-1314.2009.00864.x
        %         Table.2
        % in Ranalli 1995 (page 334 Table 10.3)
        % in Ranalli 1995 (page 334 Table 10.3)
        % 'Uniaxial' nach "Analytische Untersuchung intralhosph??rischer Deformation und Numerische Methoden zur Bestimmung
        %            krustaler Spannungsdom??nen"; Dissertation Fakult??t der Physik TH Karlsruhe
        %            S. 48
        A                   =   6.7e-6;
        n                   =   2.4;
        E                   =   156e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'UniAxial';     % Add the transformation from uni-axial -> tensorial form or not?
        V                   =   0;              % Activation volume
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;             % melt fraction prefactor
    
        case 'Hirth et al. Quarzite (2001)'
        % 
        A                   =   10.^(-11.2);
        n                   =   4;
        E                   =   135e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'UniAxial';     % Add the transformation from uni-axial -> tensorial form or not?
        V                   =   0;              % Activation volume
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;             % melt fraction prefactor
   
    case 'Felsic Granulite - Ranalli 1995'
        
        % used for continental crust in: Duretz et al. (2016), "Thermo-mechanical modeling of the
        % obduction process based on the Oman Ophiolite case"
        %
        % in Ranalli 1995, "Rheology of the Earth" (page 334 Table 10.3)
        %
        A                   =   8e-3;
        n                   =   3.1;
        E                   =   243e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'UniAxial';     % Add the transformation from uni-axial -> tensorial form or not?
        V                   =   0;              % Activation volume
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;              % melt fraction prefactor
        
    case 'Mafic Granulite - Ranalli 1995'
        % used in LI, Z. H., GERYA, T. V. and BURG, J.-P. (2010),
        %         Influence of tectonic overpressure on PT paths of HPUHP rocks in continental collision zones: thermomechanical modelling.
        %         Journal of Metamorphic Geology, 28: 227247. doi: 10.1111/j.1525-1314.2009.00864.x
        %         Table.2
        % in Ranalli 1995 (page 334 Table 10.3)
        % in Ranalli 1995 (page 334 Table 10.3)
        % 'Uniaxial' nach "Analytische Untersuchung intralhosph??rischer Deformation und Numerische Methoden zur Bestimmung
        %            krustaler Spannungsdom??nen"; Dissertation Fakult??t der Physik TH Karlsruhe
        %            S. 48
        A                   =   1.4e4;
        n                   =   4.2;
        E                   =   445e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'UniAxial';     % Add the transformation from uni-axial -> tensorial form or not?
        V                   =   0;              % Activation volume
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;             % melt fraction prefactor
        
    case 'Maryland (strong) diabase - Mackwell et al.  1998'
        % Mackwell, Zimmerman & Kohlstedt (1998). High-temperature deformation
        % of dry diabase with application to tectonics on Venus. JGR
        % 103. B1. 975-984.
        %
        % page 980.
        A                   =   8;
        n                   =   4.7;
        E                   =   485e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'UniAxial';     % Add the transformation from uni-axial -> tensorial form or not?
        V                   =   0;              % Activation volume
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;             % melt fraction prefactor
       
        case 'Gullfakks_LowPressure_Sandstone'
          
            A                   =   exp(-10);
            n                   =   2;
            E                   =   0;
            MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
            TensorCorrection    =   'UniAxial';     % Add the transformation from uni-axial -> tensorial form or not?
            V                   =   0;              % Activation volume
            
            C_OH_0              =   1;              % Basic water content (deactivated if r=0)
            r                   =   0;              % powerlaw exponent of water content
            alpha               =   0;             % melt fraction prefactor
            
            
    case 'Wet Quarzite - Ueda et al (2008)'
        % Parameters used in Ueda et al (PEPI 2008)
        A                   =   10^-3.5;
        n                   =   2.3;
        E                   =   154e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'UniAxial';     % Add the transformation from uni-axial -> tensorial form or not?
        V                   =   0;              % Activation volume
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;             % melt fraction prefactor
        
    case 'Diabase - Huismans et al (2001)'
        
        % parameters used in Huismans et al
        A                   =   3.2e-20;
        n                   =   3.05;
        E                   =   276e3;
        MPa                 =   logical(0);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'UniAxial';     % Add the transformation from uni-axial -> tensorial form or not?
        V                   =   0;              % Activation volume
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;             % melt fraction prefactor
        
    case 'Granite - Huismans et al (2001)'
        % parameters used in Huismans et al
        A                   =   3.16e-26;
        n                   =   3.3;
        E                   =   186.5e3;
        r                   =   0;
        
        MPa                 =   logical(0);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'UniAxial';     % Add the transformation from uni-axial -> tensorial form or not?
        V                   =   0;              % Activation volume
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;             % melt fraction prefactor
        
    case 'Dry Upper Crust  - SchmalholzKausBurg(2009)';
        % granite - BurgAndPodladchikov(1999)
        A                   =   3.16e-26;
        n                   =   3.3;
        E                   =   190e3;
        r                   =   0;
        
        MPa                 =   logical(0);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'UniAxial';     % Add the transformation from uni-axial -> tensorial form or not?
        V                   =   0;              % Activation volume
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;             % melt fraction prefactor
        
    case 'Dry Lower Crust  - SchmalholzKausBurg(2009)';
        % Columbia diabase in table 1 in the main paper
        A                   =   1.2e-26;
        n                   =   4.7;
        E                   =   485e3;
        r                   =   0;
        
        MPa                 =   logical(0);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'UniAxial';     % Add the transformation from uni-axial -> tensorial form or not?
        V                   =   0;              % Activation volume
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;             % melt fraction prefactor
        
        
    case 'Weak Lower Crust  - SchmalholzKausBurg(2009)'
        % diabase - BurgAndPodladchikov(1999)
        A                   =   3.2e-20;
        n                   =   3.0;
        E                   =   276e3;
        r                   =   0;
        
        MPa                 =   logical(0);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'UniAxial';     % Add the transformation from uni-axial -> tensorial form or not?
        V                   =   0;              % Activation volume
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;             % melt fraction prefactor
        
    case 'Plagioclase(An75 - Ranalli 1995)'
        A                   =   3.3e-4;
        n                   =   3.2;
        E                   =   238e3;
        r                   =   0;
        
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'UniAxial';     % Add the transformation from uni-axial -> tensorial form or not?
        V                   =   0;              % Activation volume
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;             % melt fraction prefactor
        
        
    case 'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003'
        % after Hirth, G. & Kohlstedt (2003), D. Rheology of the upper mantle
        % and the mantle wedge: A view from the experimentalists.
        % Inside the subduction Factory 83?105.
        %
        % Table 1, "wet dislocation" parameters
        A                   =   1600;
        n                   =   3.5;
        E                   =   520e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'SimpleShear';     % Add the transformation from uni-axial -> tensorial form or not?
        V                   =   22e-6;          % Activation volume
        
        
        C_OH_0              =   1000;           % Basic water content (deactivated if r=0)
        r                   =   1.2;            % powerlaw exponent of water content
        alpha               =   40;             % melt fraction prefactor, between 30 and 45
        
        
    case 'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003 (constant C_OH)'
        % after Hirth, G. & Kohlstedt (2003), D. Rheology of the upper mantle
        % and the mantle wedge: A view from the experimentalists.
        % Inside the subduction Factory 83?105.
        %
        % Table 1, "wet dislocation (constant C_OH)" parameters
        A                   =   90;
        n                   =   3.5;
        E                   =   480e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'SimpleShear'; 	% Add the transformation from uni-axial -> tensorial form or not?
        V                   =   11e-6;          % Activation volume
        
        
        C_OH_0              =   1000;           % Basic water content (deactivated if r=0)
        r                   =   1.2;            % powerlaw exponent of water content
        alpha               =   40;             % melt fraction prefactor, between 30 and 45
        
        %         eII =  A*Tau^n * C_OH^r *exp( - (E + P*V)/(R*T))
        
        
        
    case 'Dry Olivine dislocation creep- Hirth and Kohlstedt 2003'
        % after Hirth, G. & Kohlstedt (2003), D. Rheology of the upper mantle
        % and the mantle wedge: A view from the experimentalists.
        % Inside the subduction Factory 83?105.  
        A                   =   1.1e5;
        n                   =   3.5;
        E                   =   530e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'SimpleShear';     % Add the transformation from uni-axial -> tensorial form or not?
        
        % Activation volume varies, according to their table 2:
        % possible values are between (6-27)e-6  [discarding negatives]
        V                   =   15e-6;          % Activation volume
        %
        % Table 1, "dry dislocation" parameters
      
        
        
        C_OH_0              =   1;               % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   40;             % melt fraction prefactor, between 30 and 45
        
    case 'Olivine - BurgAndPodladchikov(1999)';
        A                   =   7.1e-14;
        n                   =   3.0;
        E                   =   510e3;
        MPa                 =   logical(0);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'SimpleShear';     % Add the transformation from uni-axial -> tensorial form or not?
        
        % Activation volume varies, according to their table 2:
        % possible values are between (6-27)e-6  [discarding negatives]
        V                   =   0;          % Activation volume
        
        
        C_OH_0              =   1;               % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;              % melt fraction prefactor
        
    case 'Wet Upper Mantle - BurgSchmalholz(2008)';
        % used in  SchmalholzKausBurg(2009), Geology (wet olivine)
        A                   =   2e-21;
        n                   =   4.0;
        E                   =   471e3;
        MPa                 =   logical(0);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'SimpleShear';     % Add the transformation from uni-axial -> tensorial form or not?
        
        % Activation volume varies, according to their table 2:
        % possible values are between (6-27)e-6  [discarding negatives]
        V                   =   0;          % Activation volume
        
        
        C_OH_0              =   1;               % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;              % melt fraction prefactor
        
        
        
        
    case 'Granite  - TirelEtAl(2008)';
        % used in  SchmalholzKausBurg(2009), Geology
        A                   =   1.25e-9;
        n                   =   3.2;
        E                   =   123e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'SimpleShear';     % Add the transformation from uni-axial -> tensorial form or not?
        
        % Activation volume varies, according to their table 2:
        % possible values are between (6-27)e-6  [discarding negatives]
        V                   =   0;          % Activation volume
        
        
        C_OH_0              =   1;               % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;              % melt fraction prefactor
        
        
        
    case 'Dry Olivine dislocation creep- Rey et al. 2014- Asthenosphere'
        % Used in Rey et al. (2014), Rey, P.F., Coltice, N., Flament, N., 2014. Spreading continents kick-started plate tectonics. Nature 513, 405-408.
        %
        % Table 1, "dry dislocation" parameters
        A                   =   7.0e4;
        n                   =   3.0;
        E                   =   520e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'None';     % Add the transformation from uni-axial -> tensorial form or not?
        
        % Activation volume varies, according to their table 2:
        % possible values are between (6-27)e-6  [discarding negatives]
        V                   =   0;          % Activation volume
        
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;              % melt fraction prefactor
        
    case 'Dry Olivine dislocation creep- Rey et al. 2014- SCLM'
        % Used in Rey et al. (2014), Rey, P.F., Coltice, N., Flament, N., 2014. Spreading continents kick-started plate tectonics. Nature 513, 405-408.
        %
        % Table 1, "dry dislocation" parameters
        A                   =   7.0e2;
        n                   =   3.0;
        E                   =   520e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'None';     % Add the transformation from uni-axial -> tensorial form or not?
        
        % Activation volume varies, according to their table 2:
        % possible values are between (6-27)e-6  [discarding negatives]
        V                   =   0;          % Activation volume
        
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;              % melt fraction prefactor
        
    case 'Dry Olivine dislocation creep- Rey et al. 2014- BCov'
        % Used in Rey et al. (2014), Rey, P.F., Coltice, N., Flament, N., 2014. Spreading continents kick-started plate tectonics. Nature 513, 405-408.
        %
        % Table 1, "dry dislocation" parameters
        A                   =   7.0e7;
        n                   =   3.0;
        E                   =   520e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'None';     % Add the transformation from uni-axial -> tensorial form or not?
        
        % Activation volume varies, according to their table 2:
        % possible values are between (6-27)e-6  [discarding negatives]
        V                   =   0;          % Activation volume
        
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;              % melt fraction prefactor
        
        
    case 'Crust Rey et al. 2014'
        % Used in Rey et al. (2014), Rey, P.F., Coltice, N., Flament, N., 2014. Spreading continents kick-started plate tectonics. Nature 513, 405-408.
        %
        % Table 1, "dry dislocation" parameters
        A                   =   2.0e7;
        n                   =   3.2;
        E                   =   244e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'None';     % Add the transformation from uni-axial -> tensorial form or not?
        
        % Activation volume varies, according to their table 2:
        % possible values are between (6-27)e-6  [discarding negatives]
        V                   =   0;          % Activation volume
        
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;              % melt fraction prefactor
        
        
        
        
        
    case 'OceanicLid Rey et al. 2014'
        % Used in Rey et al. (2014), Rey, P.F., Coltice, N., Flament, N., 2014. Spreading continents kick-started plate tectonics. Nature 513, 405-408.
        %
        % Table 1, "dry dislocation" parameters
        A                   =   2.0e7;
        n                   =   3.2;
        E                   =   245e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'None';     % Add the transformation from uni-axial -> tensorial form or not?
        
        % Activation volume varies, according to their table 2:
        % possible values are between (6-27)e-6  [discarding negatives]
        V                   =   0;          % Activation volume
        
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;              % melt fraction prefactor
       
        
        
    case  'Dry Olivine - Ranalli 1995 stronger'
        % Implemented by Fernando Marques
        
        A                   =   2.5;
        n                   =   3.5;
        E                   =   532e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        TensorCorrection    =   'UniAxial';     % Add the transformation from uni-axial -> tensorial form or not?
        V                   =   17e-6;             % Activation volume
        
        C_OH_0              =   1;              % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        alpha               =   0;              % melt fraction prefactor
        
        
    case 'Wet Clinopyroxene - ChenEtAl(2006)'
        A                   =   10^6.7;
        n                   =   2.7;
        E                   =   630e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        
        C_OH_0              =   20e-6;               % Basic water content (deactivated if r=0)
        r                   =   3;              % powerlaw exponent of water content
        V                   =   0;
        TensorCorrection    =   'SimpleShear';     % Add the transformation from uni-axial -> tensorial form or not?
        alpha               =   0;              % melt fraction prefactor
        
    case 'Ara rocksalt - Urai et al.(2008)'
   		% Ara rocksalt as published in Urai et al.(2008) 
        A                   =   1.82e-9;
        n                   =   5;
        E                   =   32.4e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        
        C_OH_0              =   1;               % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        V                   =   0;
        TensorCorrection    =   'UniAxial';     % Add the transformation from uni-axial -> tensorial form or not?
        alpha               =   0;              % melt fraction prefactor
        
   case 'Polycrystalline Anhydrite - Mueller and Briegel(1978)'
        A                   =   10^1.5;
        n                   =   2;
        E                   =   152.3e3;
        MPa                 =   logical(1);     % is A in units of [MPa^(-n)s^(-1)] or [Pa^(-n)s^(-1)]?
        
        C_OH_0              =   1;               % Basic water content (deactivated if r=0)
        r                   =   0;              % powerlaw exponent of water content
        V                   =   0;
        TensorCorrection    =   'UniAxial';     % Add the transformation from uni-axial -> tensorial form or not?
        alpha               =   0;              % melt fraction prefactor
        
    otherwise
        error(['Unknown dislocation creep parameters:  ', RockFlowLawName])
        
        
end



%% Add the creeplaw parameters to the structure


% Lab. experiments are typically done under simple shear or uni-axial
% compression, which requires a correction in order to use them in
% tensorial format as we do here. An explanation is given in the textbook
% of taras Gerya, chapter 6.
%
% NOTE: this
switch TensorCorrection
    case 'UniAxial'
        F2      =   1/2^((n-1)/n)/3^((n+1)/2/n);    % uni-axial compression test
    case 'SimpleShear'
        F2      =   1/2^((2*n-1)/n);            	% simple shear test
    case 'None'
        F2      =   1/2;                            % because we define tau= 2*eta*e_II         (factor 2 effect)
    otherwise
        error('Unknown tensor correction in dislocation creep')
end

% Convert from [MPa^(-n)s^(-1)] to [Pa^(-n)s^(-1)] if required
if MPa
    Astar = [1e6.*A.^(-1./n)].^(-n);        % transfer to Pa, apply correction, transfer back to Pa^(-n)s^(-1)
else
    Astar = A;
end

% Dislocation creep parameters:
MATERIAL_PROPS(PhaseNumber).Viscosity.DislocationCreep.Name   	=   RockFlowLawName;
MATERIAL_PROPS(PhaseNumber).Viscosity.DislocationCreep.A        =   Astar;   	% prefactor           in [Pa^(-n)s^(-1) ]
MATERIAL_PROPS(PhaseNumber).Viscosity.DislocationCreep.n        =   n;      	% powerlaw exponent   in [              ]
MATERIAL_PROPS(PhaseNumber).Viscosity.DislocationCreep.E        =   E;      	% Activation energy   in [J/mol         ]
MATERIAL_PROPS(PhaseNumber).Viscosity.DislocationCreep.V        =   V;          % activation volume   in [J/Pa/mol      ]=[m^3/mol]
MATERIAL_PROPS(PhaseNumber).Viscosity.DislocationCreep.F2       =   F2;         % Prefactor to go to tensorial format
MATERIAL_PROPS(PhaseNumber).Viscosity.DislocationCreep.C_OH_0   =   C_OH_0; 	% water content of rock (irrelevant if r=0)
MATERIAL_PROPS(PhaseNumber).Viscosity.DislocationCreep.r        =   r;          % power exponent of water content
MATERIAL_PROPS(PhaseNumber).Viscosity.DislocationCreep.alpha    =   alpha;      % melt fraction prefactor


end



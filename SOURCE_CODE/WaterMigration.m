function    [INTP_PROPS]   =   WaterMigration(PARTICLES,MESH,INTP_PROPS,BC,CHAR,NUMERICS,dt)
% Water migration scheme between particles
% Now only use an element-wise scheme as described in the paper of Quinquis & Buiter,2014,Solid Earth:
% Testing the effects of basic numerical implementations of water migration on models of subduction dynamics
%
% Water migration has two steps:1)oversaturated particles dehydrate and
% undersaturated particles hydrate 2) if there are still free water, then
% free water mover along pressure gradient and solid velocity
%
% INPUT:
%       PARTICLES 
%                   -   HistVar.WaterBound
%                   -   HistVar.WaterFree
%                   -   CompVar.WaterCapacity
% Output:
%       INTP_PROPS
%                   -   WaterBound
%                   -   WaterFree


% profile on
% Set MUTILS options
opts = NUMERICS.mutils;
% Copy solid phase to water phase
if size(PARTICLES.HistVar.WaterFree,1)>1
    PARTICLES.HistVar.WaterFree = PARTICLES.HistVar.WaterFree(:)';
    PARTICLES.HistVar.WaterBound = PARTICLES.HistVar.WaterBound(:)';
end

% % before (de)hydration, make sure every element has some average particles
% % in case of stronge divergence in local strong convection
% % NUMERICS.Particles.MinPerElement    =   NUMERICS.WaterParticles.MinPerElement;
% % NUMERICS.Particles.MaxPerElement    =   NUMERICS.WaterParticles.MaxPerElement;
% % NUMERICS.Particles.NumParticlesToInjectPerElement = NUMERICS.WaterParticles.NumParticlesToInjectPerElement;
% [PARTICLES, ind_new_PARTICLES,ind_ToBeDeleted]      	=   InjectOrDeleteParticles(MESH,PARTICLES,NUMERICS, BC);
% PARTICLES.HistVar.WaterFree(ind_new_PARTICLES)  = 0;
% PARTICLES.HistVar.WaterBound(ind_new_PARTICLES) = 0;
% % deal with (deleted and) injected particles

%--------------------------------------------------------------------------
% Step 1: compute bound & free water for all particles
%--------------------------------------------------------------------------
% Compute bound water
PARTICLES.HistVar.WaterBound = PARTICLES.HistVar.WaterBound + PARTICLES.HistVar.WaterFree;%Add last timestep free water
% Compute free water
PARTICLES.HistVar.WaterFree = PARTICLES.HistVar.WaterBound - PARTICLES.CompVar.WaterCapacity;
% only find the element which has free water
ind     =   find(PARTICLES.HistVar.WaterFree>0);
elem    =   unique(PARTICLES.elem(ind));

%--------------------------------------------------------------------------
% Step 2: Hydration and Dehydration
%--------------------------------------------------------------------------
for i=1:length(elem)
    iel  	    =   elem(i);
    ind         =   find(PARTICLES.elem==iel);
    ind1        =   find(PARTICLES.HistVar.WaterFree(ind)>0);%oversaturated phases
    ind2        =   find(PARTICLES.HistVar.WaterFree(ind)<0);%undersaturated phases
    % turn to global indice
    ind1        =   ind(ind1);
    ind2        =   ind(ind2);
    Over        =   sum(PARTICLES.HistVar.WaterFree(ind1));%positive value
    Under       =   sum(PARTICLES.HistVar.WaterFree(ind2));%negative value
    
    % check the total free water and undersaturated water in one element
    if Over>=abs(Under)%then all particles saturated
        PARTICLES.HistVar.WaterBound(ind)     =   PARTICLES.CompVar.WaterCapacity(ind);%dehydration and hydration
        PARTICLES.HistVar.WaterFree(ind2)     =   0;%unsaturated particles reach saturation and has no free water
        PARTICLES.HistVar.WaterFree(ind1)     =   (Over+Under)/length(ind1);%oversaturated water average remaining free water
    else%then only oversaturated particles saturated and others get free water by proportion
        PARTICLES.HistVar.WaterBound(ind1)    =   PARTICLES.CompVar.WaterCapacity(ind1);%oversaturated particles dehydration to be saturated
        PARTICLES.HistVar.WaterBound(ind2)    =   PARTICLES.HistVar.WaterBound(ind2) + PARTICLES.HistVar.WaterFree(ind2)./Under*Over;%undersaturated particles get free water by proportion
        PARTICLES.HistVar.WaterFree(ind)      =   0;% no free water for all particles in the element
    end
end
PARTICLES.HistVar.WaterFree(PARTICLES.HistVar.WaterFree<0)=0;
% interpolate bound water to integration points
[INTP_PROPS.WaterBound] = ParticlesToIntegrationPoints_Water(PARTICLES,'WaterBound',MESH,INTP_PROPS,NUMERICS);


%--------------------------------------------------------------------------
% Step 3: Moving free water for next step
%--------------------------------------------------------------------------
if isfield(INTP_PROPS,'V_WaterPercolation')
    % move the particles with free water:2-order Runge-Kutta,pressure
    % gradient asumed to be invariant during the time
    ind                  =   find(PARTICLES.HistVar.WaterFree>0);
%     if ~isempty(ind)
        WaterVEL = ComputeDarcyVelocity(PARTICLES,MESH,INTP_PROPS,NUMERICS,CHAR);%Compute water particles velocity using simplified Darcy equation   
%         tmp                  =   PARTICLES;
%         tmp.x(ind)           =   tmp.x(ind) + WaterVEL.Vx(ind).*dt/2;
%         tmp.z(ind)           =   tmp.z(ind) + WaterVEL.Vz(ind).*dt/2;
%         WaterVEL = ComputeDarcyVelocity(PARTICLES,MESH,INTP_PROPS,NUMERICS,CHAR);
        PARTICLES.x(ind)    =   PARTICLES.x(ind) + WaterVEL.Vx(ind).*dt;
        PARTICLES.z(ind)    =   PARTICLES.z(ind) + WaterVEL.Vz(ind).*dt;
        
        % CAREFULL!Because some water particles probably have been deleted
        [PARTICLES]        =   ComputeElementsAndLocalCoordinates_Particles(MESH, PARTICLES, NUMERICS);
        [PARTICLES,ind_new_waterpart,ind_toBeDeleted]      	=   InjectOrDeleteParticles(MESH,PARTICLES,NUMERICS, BC);
        
        if ~isempty(ind_new_waterpart)% we inject dry particles, how about deleted particles? usually no deletion
            PARTICLES.HistVar.WaterBound(ind_new_waterpart)=0;
            PARTICLES.HistVar.WaterFree(ind_new_waterpart)=0;
        end
%     end
    % interpolation free water to integration points
    [INTP_PROPS.WaterFree] = ParticlesToIntegrationPoints_Water(PARTICLES,'WaterFree',MESH,INTP_PROPS,NUMERICS);
    
elseif isfield(INTP_PROPS,'Water_Vz')
    % free water moves with constant velocity
    WaterMesh_Vel        =   zeros(size(MESH.NODES));
    WaterMesh_Vel(1,:)   =   Extrapolate_IntpDataToNodes(MESH,INTP_PROPS.Water_Vx, NUMERICS);
    WaterMesh_Vel(2,:)   =   Extrapolate_IntpDataToNodes(MESH,INTP_PROPS.Water_Vx, NUMERICS);
    [V_MARKERS]          =   einterp_MVEP2(MESH, WaterMesh_Vel, [PARTICLES.x; PARTICLES.z], PARTICLES.elem,opts);
    WaterPart.Vx         =   V_MARKERS(1,:);
    WaterPart.Vz         =   V_MARKERS(2,:);
    ind                  =   find(PARTICLES.HistVar.WaterFree>0);
    PARTICLES.x(ind)    =   PARTICLES.x(ind) + WaterPart.Vx(ind).*dt;% all particles has the same velocity
    PARTICLES.z(ind)    =   PARTICLES.z(ind) + WaterPart.Vz(ind).*dt;
    % CAREFULL!Because some water particles probably have been deleted
    [PARTICLES]        =   ComputeElementsAndLocalCoordinates_Particles(MESH, PARTICLES, NUMERICS);
    [PARTICLES,ind_new_waterpart,ind_toBeDeleted]      	=   InjectOrDeleteParticles(MESH,PARTICLES,NUMERICS, BC);
    
    if ~isempty(ind_new_waterpart)% we inject dry particles
        PARTICLES.HistVar.WaterBound(ind_new_waterpart)=0;
        PARTICLES.HistVar.WaterFree(ind_new_waterpart)=0;
    end
    
    % interpolation free water to integration points
    [INTP_PROPS.WaterFree] = ParticlesToIntegrationPoints_Water(PARTICLES,'WaterFree',MESH,INTP_PROPS,NUMERICS);
else
    error('Unknown water migration scheme!')
end
% profile viewer
% profile off

function Water = ComputeDarcyVelocity(PARTICLES,MESH,INTP_PROPS,NUMERICS,CHAR)
% water velocity depends on pressure gradient and velocity of solid phase
% V_fluid = V_solid + K/phi/eta_f*(divP-Rho_fluid*g)
% more details in the paper of Faccenda et al.,2012: Fluid flow during slab unbending and dehydration:
% Implications for intermediate-depth seismicity, slab weakening and deep water recycling
    
opts = NUMERICS.mutils;

%% 1) Compute perssure gradient along mesh
P           =   MESH.PRESSURE(MESH.RegularElementNumber);
X           =   MESH.NODES(1,:);
Z           =   MESH.NODES(2,:);
X_center    =   mean(X(MESH.ELEMS),1);
Z_center    =   mean(Z(MESH.ELEMS),1);
X_center    =   X_center(MESH.RegularElementNumber);
Z_center    =   Z_center(MESH.RegularElementNumber);

dPdx0       =   diff(P,1,2)./diff(X_center,1,2);
dPdz0       =   diff(P,1,1)./diff(Z_center,1,1);
% Extrapolate pressure gradient to grids
X_center1   =   (X_center(:,1:end-1)+X_center(:,2:end))/2;
Z_center1   =   (Z_center(:,1:end-1)+Z_center(:,2:end))/2;
F           =   scatteredInterpolant(X_center1(:),Z_center1(:),dPdx0(:),'linear','nearest');
dPdx        =   F(X,Z);

X_center1   =   (X_center(1:end-1,:)+X_center(2:end,:))/2;
Z_center1   =   (Z_center(1:end-1,:)+Z_center(2:end,:))/2;
F           =   scatteredInterpolant(X_center1(:),Z_center1(:),dPdz0(:),'linear','nearest');
dPdz        =   F(X,Z);

v_percolate         =   Extrapolate_IntpDataToNodes(MESH,INTP_PROPS.V_WaterPercolation, NUMERICS);
RhoG                =   1000*9.8/CHAR.Density/CHAR.Gravity;
f.VEL(1,:)          =   MESH.VEL(1,:)-v_percolate.*dPdx;
f.VEL(2,:)          =   MESH.VEL(2,:)-v_percolate.*(dPdz+RhoG);

% interpolate Water velocity from mesh to particles
[V_MARKERS] =   einterp_MVEP2(MESH, f.VEL, [PARTICLES.x; PARTICLES.z], PARTICLES.elem,opts);
Water.Vx    =   V_MARKERS(1,:);
Water.Vz    =   V_MARKERS(2,:);


%---------------------------------------------------------------------------------------------
function [Data_intp] = ParticlesToIntegrationPoints_Water(PARTICLES,Property,MESH,INTP_PROPS,NUMERICS)
% This routine interpolate the amount of bound water or free water from
% particles to a 2D regular matrix

% 1) Create regular grid ------------------------------------------
MinX            = 	min(MESH.NODES(1,:));
MinZ            =	min(MESH.NODES(2,:));
MaxX            =  	max(MESH.NODES(1,:));
MaxZ            =   max(MESH.NODES(2,:)); % + 0.02*(max(MESH.NODES(2,:))-min(MESH.NODES(2,:)));
NzGrid          =   size(MESH.RegularElementNumber,1)*2;
NxGrid          = 	size(MESH.RegularElementNumber,2)*2;
DxGrid          = 	(MaxX-MinX)/(NxGrid-1);
DzGrid       	= 	(MaxZ-MinZ)/(NzGrid-1);
[Xgrid,Zgrid]	=   meshgrid(MinX:DxGrid:MaxX, MinZ:DzGrid:MaxZ);

% 2) Map particles to 2D grid -------------------------------------
PartX_int                           =       round((PARTICLES.x-MinX)/DxGrid)+1;
PartZ_int                           =       round((PARTICLES.z-MinZ)/DzGrid)+1;
Dist_X                              =       (PARTICLES.x-((PartX_int-1)*DxGrid+MinX)); % normalized distance in x-direction
Dist_Z                              =       (PARTICLES.z-((PartZ_int-1)*DzGrid+MinZ)); % normalized distance in z-direction
Dist_X                              =       abs(Dist_X)*2;
Dist_Z                              =       abs(Dist_Z)*2;
Distance                            =       sqrt(Dist_X.^2 + Dist_Z.^2)/sqrt(DxGrid^2 + DzGrid^2);
Weight                              =       1-Distance;

PartX_int(isinf(PartX_int))         =       NaN;
PartZ_int(isinf(PartX_int))         =       NaN;
PartX_int(PartX_int>NxGrid)         =       NaN;
PartZ_int(PartZ_int>NzGrid)         =       NaN;
PartX_int(PartX_int<1)              =       1;
PartZ_int(PartZ_int<1)              =       1;
ind                                 =       find(~isnan(PartX_int) | ~isnan(PartZ_int));
% Get data of this field on particles (element-wise)
Part_data                           =   eval(['PARTICLES.HistVar.',Property]);

PartInd                             =       ones(size(PartX_int))*NaN;
PartInd(ind)                        =       sub2ind([NzGrid NxGrid], PartZ_int(ind) , PartX_int(ind) );  % index of every particle
if size(Part_data,1)==1
    ind_active                      =       find(~isnan(PartInd(:)') & ~isnan(Part_data(:)'));
else
    ind_active                      =       find(~isnan(PartInd(:)') );
end
Weight                              =       Weight(ind_active);
Weight_2D                           =       full(sparse2(PartZ_int(ind_active),PartX_int(ind_active),Weight(:),NzGrid, NxGrid));
Part_data                           =       Part_data(ind_active);

% Average data in a distance-based manner
Data_2D                     =   full(sparse2(PartZ_int(ind_active),PartX_int(ind_active),Weight(:).*Part_data(:),NzGrid, NxGrid));
Data_2D(Weight_2D==0)       =   NaN;
Data_2D                     =   full(Data_2D)./Weight_2D;

% Deal with data points that are NaN [where we have no particles]
if any(isnan(Data_2D(:)))
    % Just in case this still didn't fix it, use the old
    % kdtree approach
    %
    % This process is a slow, and since we repeat
    % this for every field in 'HistVar' and 'CompVar',
    % whereas the particle distribution remains the
    % same, we only have to compute the tree once
    
    switch NUMERICS.NearestNeighborhoodAlgorithm
        case 'kdtree_mex'
            
            CreateNewTree = logical(1);
            %                     if ~isempty(tree)
            %                         CreateNewTree = logical(0);          % same amount of NaN's as before, so most not necessary to build a new tree
            %
            %                     end
            ind_noNaN               =   find(~isnan(Data_2D(:)));
            
            if CreateNewTree
                
                ReferencePts        =   [Xgrid(ind_noNaN) Zgrid(ind_noNaN)];
                tree                =   kdtree(ReferencePts);
            end
            iid                     =   find(isnan(Data_2D(:)));
            TestPoints              =   [Xgrid(iid) Zgrid(iid)];
            [ClosestPtIndex]        =   kdtree_closestpoint(tree,TestPoints);
            
        case 'knnsearch'
            ind_noNaN               =   find(~isnan(Data_2D(:)));
            ReferencePts            =   [Xgrid(ind_noNaN) Zgrid(ind_noNaN)];
            iid                     =   find(isnan(Data_2D(:)));
            TestPoints              =   [Xgrid(iid) Zgrid(iid)];
            
            [ClosestPtIndex]        =   knnsearch(TestPoints, ReferencePts);
            
            
        otherwise
            error('Unknown NearestNeighborhoodAlgorithm')
            
    end
    
else
    ind_noNaN               =   find(~isnan(Data_2D(:)));
    ClosestPtIndex          =   [];
end

iiid                      =     find(isnan(Data_2D(:)));
Data_2D(iiid)             =     Data_2D(ind_noNaN(ClosestPtIndex));

% Interpolate data from grid -> integration points
Data_intp               =   interp2(Xgrid,Zgrid,Data_2D, INTP_PROPS.X,INTP_PROPS.Z,'*nearest');
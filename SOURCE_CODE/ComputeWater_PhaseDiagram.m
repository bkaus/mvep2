function [Water PhaseDiagramData] = ComputeWater_PhaseDiagram(P,T,PhaseDiagramData,NUMERICS)
% Load a phase diagram and interpolates water content from that

% $id$

%% (1) Load phase diagram
load(PhaseDiagramData.Name)



%only use interp2 to interpolate the values because phase diagram is also
%meshgrid data, much more faster
[T2d,P2d]       =   meshgrid(T_K,P_Pa);
Tmin            =   min(T_K);
Tmax            =   max(T_K);
Pmin            =   min(P_Pa);
Pmax            =   max(P_Pa);
Water           =   zeros(size(P));% all values outside the P-T domain are set to be zeros
ind             =   find(P>=Pmin & P<=Pmax & T>=Tmin & T<=Tmax);
Water(ind)      =   interp2(T2d,P2d,WaterContent,T(ind),P(ind));

if 1==0
if ~isfield(PhaseDiagramData,'WS')
    WS = [];
else
    WS = PhaseDiagramData.WS;
end
%% Use MUTILS to interpolate water content
[Water,WS]   =   Interpolate_WaterContent(T_K,P_Pa,WaterContent,[T';P'],WS,NUMERICS);
PhaseDiagramData.WS =   WS; % speeds up future interpolations
end



function [V_MARKERS,WS] = Interpolate_WaterContent(T_K,P_Pa,WaterContent,MARKERS,WS,NUMERICS)
% This routine uses MUTILS functionality to interpolate values from a phase
% diagram to markers or intgration points
%
% MUTILS OPTIONS
opts    =   NUMERICS.mutils;

if isempty(WS)
    % Create a workspace that will later be stored and contains all phase
    % diagram information
    [x2d,y2d]   =   meshgrid(T_K,P_Pa);
    
    
    % Create delaunay triangulation
    WS.MESH.NODES       =   [x2d(:)';  y2d(:)'];
    DT                  =   DelaunayTri(WS.MESH.NODES');
    trep                =   TriRep(DT.Triangulation, WS.MESH.NODES(1,:)',WS.MESH.NODES(2,:)');
    WS.MESH.ELEMS       =   uint32(DT.Triangulation');
    
    % Create Delaunay triangulation for particles outside domain
    WS.POINTS_boundary  =   [x2d(:,1),y2d(:,1);x2d(:,end),y2d(:,end);x2d(1,2:end-1)',y2d(1,2:end-1)';x2d(end,2:end-1)',y2d(end,2:end-1)'];
    WS.DT_bound         =   DelaunayTri(WS.POINTS_boundary);
end

% Add values
WS.V            =   WaterContent(:)';
WS.V_Boundary   =   [WaterContent(:,1);WaterContent(:,end);WaterContent(1,2:end-1)';WaterContent(end,2:end-1)'];

% find the enclosing triangle
[T,WS,stats]    =   tsearch2(WS.MESH.NODES,WS.MESH.ELEMS,MARKERS,WS,uint32([]),opts);
WS.T            =   T;

% Interpolate properties of particles that are within the computational domain.
ind             =   find(T>0);
V_MARKERS       =   zeros(1,length(MARKERS));
if ~isempty(ind)
    [V_MARKERS(ind)]    =   einterp_MVEP2(WS.MESH,WS.V,MARKERS(:,ind),T(ind),opts);
end

% Interpolate particles that outside the computational domain
ind             =   find(T==0);
PI              =   dsearchn(WS.POINTS_boundary,WS.DT_bound.Triangulation,MARKERS(:,ind)');
V_MARKERS(ind)  =   WS.V_Boundary(PI);
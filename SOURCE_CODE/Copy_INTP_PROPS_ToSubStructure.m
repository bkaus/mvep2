function INTP_PROPS_ELEM = Copy_INTP_PROPS_ToSubStructure(INTP_PROPS, elem_ind, ip_ind)
% Copies integration point data to a (smaller) substructure
%
% INTP_PROPS_ELEM = Copy_INTP_PROPS_ToSubStructure(INTP_PROPS, elem_ind, ip_ind)
%
%   where 
%       elem_ind:       an index that contains the elements to be copied.
%       ip_ind:         an index that contains the integration points to be copied
%
%

if isempty(ip_ind)
    ip_ind = size(INTP_PROPS.X,2);  % use all integration points
end


INTP_PROPS_ELEM             =   [];

Var_names                   =   fieldnames(INTP_PROPS);
for iField=1:length(Var_names)
    data                    =   getfield(INTP_PROPS, Var_names{iField});
    if ~isstruct(data)
        switch Var_names{iField}
            case {'PHASE_PROP','TotNumPart'}
                INTP_PROPS_ELEM     =   setfield(INTP_PROPS_ELEM,Var_names{iField},         data(elem_ind,:)        );
                case {'PHASE_PROP','ElementArea'}
                INTP_PROPS_ELEM     =   setfield(INTP_PROPS_ELEM,Var_names{iField},         data(elem_ind,:)        );
            otherwise
          
                INTP_PROPS_ELEM     =   setfield(INTP_PROPS_ELEM,Var_names{iField},         data(elem_ind,ip_ind)   );
        end
    else
        
        % get all the names of the structure
        INTP_PROPS_ELEM.(Var_names{iField}) = [];
        Struct_names               =   fieldnames(INTP_PROPS.(Var_names{iField}));
        for i=1:length(Struct_names)
            data                =   getfield(INTP_PROPS.(Var_names{iField}), Struct_names{i});
            if ~isstruct(data)
                INTP_PROPS_ELEM.(Var_names{iField}) = setfield(INTP_PROPS_ELEM.(Var_names{iField}),Struct_names{i},         data(elem_ind,ip_ind));
            else
                error('can currently only handle one layer of structure in INTP_PROPS')
            end
        end
        
    end
end


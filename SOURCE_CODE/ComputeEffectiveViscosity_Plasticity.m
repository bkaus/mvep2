function [ Mu_eff, Plastic, Tau_yield, NUMERICS,INTP_PROPS] = ComputeEffectiveViscosity_Plasticity(INTP_PROPS,  PlasticCreepLaw, MATERIAL_PROPS, NUMERICS, Mu_viscous, G, dt, MESH)
% ComputeEffectiveViscosity_Plasticity
%
%   Compute the effective viscosity due to plastic creep at integration
%   points

% $Id$

nip         =   size(INTP_PROPS.X,2);           % # of integration points
nel         =   size(INTP_PROPS.X,1);           % # of elements
Mu_eff  	=   Mu_viscous;                     % initially the viscosity is that of the viscous creeplaw
Plastic     =   zeros(nel,nip);                 % is a point plastic or not?

if isfield(INTP_PROPS.Stress,'T2nd0')
    Tau_old	=   INTP_PROPS.Stress.T2nd0;         % old stress
else
    Tau_old	=   zeros(nel,nip);
end

if isfield(INTP_PROPS,'Strainrate')
    eII  	=   INTP_PROPS.Strainrate.E2nd;   	% second invariant of strain rate
else
    eII     =   ones(nel,nip);
end
eII_star    =   eII + Tau_old./(2*G*dt);        % tau_star in nton's definition  [NOTE THAT WE IGNORE ROTATION IN THIS!!)

% Compute trial stress, based on viscous creep law and old stress
MU          =   1./( (1./Mu_viscous + 1./(G*dt))  );        % EFFECTIVE VISCOSITY
CHI         =   1./( (1     + G.*dt./Mu_viscous)  );
Tau_trial 	=   2*MU.*eII + CHI.*Tau_old;


%% Compute the yield stress
Type        =   sort(fieldnames(PlasticCreepLaw));

switch Type{1}
    case 'Constant'
        Tau_yield   =   ones(nel,nip)*PlasticCreepLaw.Constant.YieldStress;
        
    case 'DruckerPrager'
        % Drucker Prager, or Mohr Coulomb (plasticity-dependent) yield
        % stress
        if isfield(INTP_PROPS,'Pressure')
            P       =   INTP_PROPS.Pressure;
            
            P       = repmat(mean(P,2),[1 size(P,2)]);        % average per element
            
        else
            P       =   ones(nel,nip);
        end
        g           =   MATERIAL_PROPS(1).Gravity.Value;                       % gravitational acceleration, required to estimate lithostatic pressures
        rho         =   mean(INTP_PROPS.Rho(:));
        
        
        Cohesion  	=   ones(nel,nip)*PlasticCreepLaw.DruckerPrager.Cohesion;
        Phi         =   ones(nel,nip)*PlasticCreepLaw.DruckerPrager.FrictionAngle;
        
        % apply strain weakening if required
        if isfield(PlasticCreepLaw,'StrainWeakening')
            [C_Prefactor,Phi_Prefactor]  =   StrainWeakeningPlasticity(INTP_PROPS,PlasticCreepLaw.StrainWeakening);
            
            Cohesion    =   Cohesion.*C_Prefactor;
            Phi         =   Phi.*Phi_Prefactor;
        end
        
        % If melt weakening is present, we reduce the friction angle
        if isfield(INTP_PROPS,'lambdaMelt')
            % The paramater lambdaMelt reduces the effective friction
            % angle due to melt fluid pressure.
            %
            % In general, the yield strength is given by:
            %   Syield = c*cos(phi) + sin(phi)*(P-Pfluid)
            % or:
            %   Syield = c*cos(phi) + sin(phi)*P*(1-Pfluid/P)
            % or:
            %   Syield = c*cos(phi) + sin(phi)*P*lambda
            % where lambda = (1-Pfluid/P)
            %
            % Often Pfluid is not explicitly computed, but we specify
            % lambda instead.
            % In that case we an compute an effective friction angle with:
            % sin(phi_eff) =   sin(phi_dry)*lambda
            % phi_eff      =   asin( sin(phi_dry)*lambda )
            % if phi_dry is given in degrees, it needs to be transformed
            % too/from rad
            
            
            % One of the shortcomings of the current models is that we can only
            % use a single lambda for the whole computational domain. We
            % compute the average lambda here, in case two different values
            % are used within the code
            num=0;
            lambda=0;
            for iphase=1:length(MATERIAL_PROPS)
                if ~isempty(MATERIAL_PROPS(iphase).MeltExtraction)
                    if isfield(MATERIAL_PROPS(iphase).MeltExtraction,'MeltWeakeningPlasticityFactor')
                        lambda  =   lambda + MATERIAL_PROPS(iphase).MeltExtraction.MeltWeakeningPlasticityFactor;
                    else
                        lambda  =   lambda + 1;
                    end
                    num     =   num+1;
                end
            end
            lambda  = lambda./num;
            
            ind = find(INTP_PROPS.lambdaMelt>1-eps);
            
            lambda_elements = ones(size(INTP_PROPS.lambdaMelt));
            lambda_elements(ind) = lambda;
            
            % Compute effective friction angle with weakening
            Phi     =   asin(sin(Phi/180*pi).*lambda_elements)/pi*180;
            
        end
        
        
        % Limit the pressure if we (1) use a viscoplastic setup and (2) do
        % the first iteration and timestep
        LimitPressure           =   NUMERICS.LimitPressure;                                         % Should we use a pressure limiter or not? Typically used for
        
        [Tau_yield]             =   ComputeYieldStressDruckerPrager(P,Cohesion , Phi , rho,g, INTP_PROPS.Z, LimitPressure);
        
        
    case 'DruckerPrager_MaxYield'
        % Drucker Prager, or Mohr Coulomb (plasticity-dependent) yield
        % stress with a (constant) maximum yield stress defined
        
        if isfield(INTP_PROPS,'Pressure')
            P       =   INTP_PROPS.Pressure;
            
            P = repmat(mean(P,2),[1 size(P,2)]);        % average per element
            
        else
            P       =   ones(nel,nip);
        end
        g           =   MATERIAL_PROPS(1).Gravity.Value;                       % gravitational acceleration, required to estimate lithostatic pressures
        rho         =   mean(INTP_PROPS.Rho(:));
        
        
        Cohesion  	=   ones(nel,nip)*PlasticCreepLaw.DruckerPrager_MaxYield.Cohesion;
        Phi         =   ones(nel,nip)*PlasticCreepLaw.DruckerPrager_MaxYield.FrictionAngle;
        MaxYield    =   PlasticCreepLaw.DruckerPrager_MaxYield.MaxYieldStress;
        
        % apply strain weakening if required
        if isfield(PlasticCreepLaw,'StrainWeakening')
            [C_Prefactor,Phi_Prefactor]  =   StrainWeakeningPlasticity(INTP_PROPS,PlasticCreepLaw.StrainWeakening);
            
            Cohesion    =   Cohesion.*C_Prefactor;
            Phi         =   Phi.*Phi_Prefactor;
        end
        
        
        % Limit the pressure if we (1) use a viscoplastic setup and (2) do
        % the first iteration and timestep
        LimitPressure           =   NUMERICS.LimitPressure;                                         % Should we use a pressure limiter or not? Typically used for
        
        [Tau_yield]             =   ComputeYieldStressDruckerPrager(P,Cohesion , Phi , rho,g, INTP_PROPS.Z, LimitPressure);
        
        
        % Apply maximum yield stress
        Tau_yield(Tau_yield>MaxYield) = MaxYield;
        
        
    case 'DruckerPrager_ViscoPlasticRegularization'
        % Drucker Prager, or Mohr Coulomb (plasticity-dependent) yield
        % stress with a (constant) maximum yield stress defined
        
        if isfield(INTP_PROPS,'Pressure')
            P       =   INTP_PROPS.Pressure;
            
            P = repmat(mean(P,2),[1 size(P,2)]);        % average per element
            
        else
            P       =   ones(nel,nip);
        end
        g           =   MATERIAL_PROPS(1).Gravity.Value;                       % gravitational acceleration, required to estimate lithostatic pressures
        rho         =   mean(INTP_PROPS.Rho(:));
        
        
        Cohesion  	=   ones(nel,nip)*PlasticCreepLaw.DruckerPrager_ViscoPlasticRegularization.Cohesion;
        Phi         =   ones(nel,nip)*PlasticCreepLaw.DruckerPrager_ViscoPlasticRegularization.FrictionAngle;
        VP_eta      =   ones(nel,nip)*PlasticCreepLaw.DruckerPrager_ViscoPlasticRegularization.VP_eta;
        
        % apply strain weakening if required
        if isfield(PlasticCreepLaw,'StrainWeakening')
            [C_Prefactor,Phi_Prefactor]  =   StrainWeakeningPlasticity(INTP_PROPS,PlasticCreepLaw.StrainWeakening);
            
            Cohesion    =   Cohesion.*C_Prefactor;
            Phi         =   Phi.*Phi_Prefactor;
        end
        
        
        % Limit the pressure if we (1) use a viscoplastic setup and (2) do
        % the first iteration and timestep
        LimitPressure           =   NUMERICS.LimitPressure;                                         % Should we use a pressure limiter or not? Typically used for
        
        [Tau_yield]             =   ComputeYieldStressDruckerPrager(P,Cohesion , Phi , rho,g, INTP_PROPS.Z, LimitPressure);
        
        % Add the VP regularization to the yield stress
        if isfield(INTP_PROPS,'Strainrate')
            if isfield(INTP_PROPS.Strainrate,'E2nd_plastic')
                E2nd_pl                 =   INTP_PROPS.Strainrate.E2nd_plastic;     % plastic strainrate invariant
                Tau_yield               =   Tau_yield + abs(E2nd_pl).*VP_eta;
            end
        end
        
        
        % Apply maximum yield stress
%         Tau_yield(Tau_yield>MaxYield) = MaxYield;
        
    case 'DepthDependentVonMises'
        Cohesion  	=   ones(nel,nip)*PlasticCreepLaw.DepthDependentVonMises.Cohesion;
        Phi         =   ones(nel,nip)*PlasticCreepLaw.DepthDependentVonMises.FrictionAngle;
        MaxYield    =   PlasticCreepLaw.DepthDependentVonMises.MaxYieldStress;
        
        % lithostatic pressure on INTP_PROPS (has to be computed beforehand)
        P_lithos 	=   INTP_PROPS.P_lithos;
        % precompute cosine and sign functions as they are computationally expensive
        cosPhi_rad	=   cos(Phi/180*pi);
        sinPhi_rad	=   sin(Phi/180*pi);
        
        Tau_yield     =   P_lithos.*sinPhi_rad + Cohesion.*cosPhi_rad;
        % Apply maximum yield stress
        Tau_yield(Tau_yield>MaxYield) = MaxYield;
        
    case 'DruckerPragerGriffithMurrell'
        % Drucker Prager
        
        % porosity-exponential interpolant x to get an "effective" pressure
        % from compaction and fluid pressure
        x   =  (INTP_PROPS.PHI-1e-4)./(1e-3-1e-4);
        x   =  max(x,0);
        x   =  min(x,1);
        P   =  (1-x).*INTP_PROPS.Pf + INTP_PROPS.Pc;
        %         P   =  repmat(mean(P,2),[1 size(P,2)]);  % average per element
        
        g       =  MATERIAL_PROPS(1).Gravity.Value;  % gravitational acceleration, required to estimate lithostatic pressures
        rho     =  mean(INTP_PROPS.Rho(:));
        
        Cohesion  =  ones(nel,nip)*PlasticCreepLaw.(Type{1}).Cohesion;
        Phi       =  ones(nel,nip)*PlasticCreepLaw.(Type{1}).FrictionAngle;
        
        [Tau_yield_DP]  =  ComputeYieldStressDruckerPrager(P,Cohesion , Phi , rho,g, INTP_PROPS.Z, NUMERICS.LimitPressure);
        
        % following line now matches eq. (37), DOI:10.1093/gji/ggt306
        Tau_yield_GM        =  P + PlasticCreepLaw.(Type{1}).sigma_T;
        
        Tau_yield           =  min(Tau_yield_DP,Tau_yield_GM);
        
        Tau_yield(INTP_PROPS.PHI<1e-4) = Tau_yield_DP(INTP_PROPS.PHI<1e-4);
        
        Tau_yield  =  max(1e-4*PlasticCreepLaw.(Type{1}).Cohesion,Tau_yield);
        
    otherwise
        error('Unknown plastic creep law')
end

%% Put a maximum to the allowed yield stress
Tau_yield(Tau_yield>NUMERICS.Plasticity.MaximumYieldStress) = NUMERICS.Plasticity.MaximumYieldStress;

% This has the same effect as the formulation below:
% denominator =   max(1e-30,(2.*eII.*G*dt-Tau_yield+Tau_old));
% MuPlastic   =   Tau_yield.*G.*dt./denominator;
% Mu_eff      =   min(Mu_eff,MuPlastic);

%==========================================================================
eII_Plastic = eII;
if isfield(NUMERICS.Plasticity,'NonLocalPlasticity')
    % Nonlocal plasticity is active
    switch NUMERICS.Plasticity.NonLocalPlasticity.Type
        case 'Gradient_YieldStress'
            
            % Non-local smoothening of plastic parameters:
            NonLocalRadius  = 	NUMERICS.Plasticity.NonLocalPlasticity.Radius;
            %             [eII_NonLocal]  =   GradientPlasticity_DiffusionStep(MESH, eII, NUMERICS, NonLocalRadius);
            [Tau_yield_NonLocal]  =   GradientPlasticity_DiffusionStep(MESH, Tau_yield, NUMERICS, NonLocalRadius);
            
            alpha           =	NUMERICS.Plasticity.NonLocalPlasticity.alpha;
            %             eII_Plastic     =	(1-alpha)*eII + alpha*eII_NonLocal;
            Tau_yield     =	(1-alpha)*Tau_yield + alpha*Tau_yield_NonLocal;
            
            eII_Plastic = eII;
        case 'Gradient_Strainrate'
            if isfield(INTP_PROPS,'StrainRate');
                eII     =   INTP_PROPS.Strainrate.E2nd_vp;
            end
            [eII_NonLocal]  =   GradientPlasticity_DiffusionStep(MESH, eII, NUMERICS, NUMERICS.Plasticity.NonLocalPlasticity.Radius);
            %             [~,eII_NonLocal] = NonLocalPlasticity_Averaging(MESH,INTP_PROPS, NUMERICS.Plasticity.NonLocalPlasticity.Radius, 'ArithmeticAverage', eII);
            
            
            alpha           =	NUMERICS.Plasticity.NonLocalPlasticity.alpha;
            eII_Plastic     =	(1-alpha)*eII + alpha*eII_NonLocal;
            
            
        case 'Gradient_Strain'
            % not within this routine
            
        otherwise
            error('Unknown gradient plasticity type')
    end
end
%==========================================================================





%% Correct viscosity for plastic yielding
ind         =   find(Tau_trial>=Tau_yield);
if ~isempty(ind)
    
    % Looks complicated, but stems from modifying Mu_viscous until
    % Tau_trial=Tau_yield
    %
    % eII = Tau_yield/(2*Mu_viscoplastic) + (Tau_yield-Tau_old)/(2*G*dt), solved
    % for Mu_viscoplastic
    %     MuPlastic   =   Tau_yield.*G.*dt./denominator;
    
    denominator =   max(1e-30,(2.*eII_Plastic(ind).*G(ind)*dt-Tau_yield(ind)+Tau_old(ind)));
    %     denominator =   max(1e-30,(2.*eII(ind).*G(ind)*dt-Tau_yield(ind)+Tau_old(ind)));
    
    MuPlastic   =   Tau_yield(ind).*G(ind).*dt./denominator;
    Mu_eff(ind) =   MuPlastic;
    
    
    Plastic(ind)  =  1;
end

% Mu_yield    = -Tau_yield.*G.*dt./min(-1e-30,Tau_yield-2.*G.*dt.*eII-Tau_old);
% Mu_eff = min(Mu_eff, Mu_yield);

end
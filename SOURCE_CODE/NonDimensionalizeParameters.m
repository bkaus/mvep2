function [mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(varargin);
%NonDimensionalizeParameters Nondimensionalizes input parameters
%   NonDimensionalizes all input parameters & complains if it doesn't know
%   a parameter
%

% $Id$

if (nargin~=6)
    error('incorrect number of input parameters')
end
mesh_input            	=       varargin{1};
BC                      =       varargin{2};
MATERIAL_PROPS          =       varargin{3};
PARTICLES               =       varargin{4};
NUMERICS                =       varargin{5};
CHAR                    =       varargin{6};

% Set default characteristic parameters
if ~isfield(CHAR,'Length')
    CHAR.Length         =       100e3;
end
if ~isfield(CHAR,'Viscosity')
    CHAR.Viscosity      =       1e20;
end
if ~isfield(CHAR,'Time')
    CHAR.Time           =       1/1e-15;
end
if ~isfield(CHAR,'Temperature')
    CHAR.Temperature 	=       873;
end

if isfield(NUMERICS,'TwoPhase')
    if NUMERICS.TwoPhase &  ~isfield(CHAR,'Porosity')
        CHAR.Porosity   =   1;
    end
end


% in case we did not define the field energy
if ~isfield(BC,'Energy')
    BC.Energy 	=       struct();
end


% Derived characteristic values
CHAR.Velocity           =       CHAR.Length/CHAR.Time;
CHAR.Stress             =       CHAR.Viscosity/CHAR.Time;
CHAR.Force              =       CHAR.Stress*CHAR.Length^2;
CHAR.Watt               =       CHAR.Force*CHAR.Length/CHAR.Time;
CHAR.Joule              =       CHAR.Force*CHAR.Length;
CHAR.kg                 =       CHAR.Stress*CHAR.Length*CHAR.Time^2;
CHAR.Density            =       CHAR.kg/CHAR.Length^3;
CHAR.Gravity            =       CHAR.Length/CHAR.Time^2;
CHAR.Conductivity       =       CHAR.Watt/CHAR.Length/CHAR.Temperature;
CHAR.HeatCapacity       =       CHAR.Joule/CHAR.kg/CHAR.Temperature;
CHAR.RadioactiveHeat  	=       CHAR.Watt/CHAR.Length^3;
CHAR.Permeability       =       CHAR.Length^2;

% Some useful things to know
CHAR.SecYear            =       3600*24*365.25;
CHAR.cm_Year            =       100*CHAR.SecYear;       % from m/s -> cm/Year
CHAR.mm_Year            =       1e3*CHAR.SecYear;       % from m/s -> mm/Year


%% ========================================================================
% Make input parameters dimensionless

%% mesh coordinates
mesh_input.x_min        =       mesh_input.x_min/CHAR.Length;
mesh_input.x_max        =       mesh_input.x_max/CHAR.Length;
mesh_input.z_min        =       mesh_input.z_min/CHAR.Length;
mesh_input.z_max        =       mesh_input.z_max/CHAR.Length;
if isfield(mesh_input,'y_min'); %for 3d
    mesh_input.y_min        =       mesh_input.y_min/CHAR.Length;
    mesh_input.y_max        =       mesh_input.y_max/CHAR.Length;
end



if isfield(mesh_input,'x_vec')
    mesh_input.x_vec    =       mesh_input.x_vec/CHAR.Length;
end
if isfield(mesh_input,'z_vec')
    mesh_input.z_vec    =       mesh_input.z_vec/CHAR.Length;
end
if isfield(mesh_input,'Bot_x')
    mesh_input.Bot_x    =       mesh_input.Bot_x/CHAR.Length;
end
if isfield(mesh_input,'Bot_z')
    mesh_input.Bot_z    =       mesh_input.Bot_z/CHAR.Length;
end
if isfield(mesh_input,'Top_x')
    mesh_input.Top_x    =       mesh_input.Top_x/CHAR.Length;
end
if isfield(mesh_input,'Top_z')
    mesh_input.Top_z    =       mesh_input.Top_z/CHAR.Length;
end
%for 3d
if isfield(mesh_input,'y_vec')
    mesh_input.y_vec    =       mesh_input.y_vec/CHAR.Length;
end
if isfield(mesh_input,'Bot_y')
    mesh_input.Bot_y    =       mesh_input.Bot_y/CHAR.Length;
end
if isfield(mesh_input,'Top_y')
    mesh_input.Top_y    =       mesh_input.Top_y/CHAR.Length;
end


if isfield(mesh_input,'FixedAverageTopography')
    mesh_input.FixedAverageTopography = mesh_input.FixedAverageTopography/CHAR.Length;
end



%% BC's
if isfield( BC.Stokes, 'BG_strrate')
    BC.Stokes.BG_strrate = BC.Stokes.BG_strrate/(1/CHAR.Time);
end
if isfield( BC.Stokes, 'SS_strrate')
    BC.Stokes.SS_strrate = BC.Stokes.SS_strrate/(1/CHAR.Time);
end


% internal BC
if isfield( BC.Stokes, 'internal')
    if isfield(BC.Stokes.internal,'xrange')
        for iBC=1:length(BC.Stokes.internal.xrange)
            BC.Stokes.internal.xrange{iBC} = BC.Stokes.internal.xrange{iBC} .* (1/CHAR.Length);
        end
    end
    if isfield(BC.Stokes.internal,'zrange')
        for iBC=1:length(BC.Stokes.internal.zrange)
            BC.Stokes.internal.zrange{iBC} = BC.Stokes.internal.zrange{iBC} .* (1/CHAR.Length);
        end
    end
    if isfield( BC.Stokes.internal, 'vx')
        for iBC=1:length(BC.Stokes.internal.vx)
            BC.Stokes.internal.vx(iBC) = BC.Stokes.internal.vx(iBC) .* (1/CHAR.Velocity);
        end
    end
    if isfield( BC.Stokes.internal, 'vz')
        for iBC=1:length(BC.Stokes.internal.vz)
            BC.Stokes.internal.vz(iBC) = BC.Stokes.internal.vz(iBC) .* (1/CHAR.Velocity);
        end
    end
    if isfield( BC.Stokes.internal, 'strrateX')
        for iBC=1:length(BC.Stokes.internal.strrateX)
            BC.Stokes.internal.strrateX(iBC) = BC.Stokes.internal.strrateX(iBC) .* (1/CHAR.Time);
        end
    end
    
end

if isfield( BC.Energy, 'Value')
    if isfield( BC.Energy.Value, 'Top')
        BC.Energy.Value.Top     =   BC.Energy.Value.Top/CHAR.Temperature;
    end
    if isfield( BC.Energy.Value, 'Bottom')
        BC.Energy.Value.Bottom 	=   BC.Energy.Value.Bottom/CHAR.Temperature;
    end
    if isfield( BC.Energy.Value, 'Left')
        BC.Energy.Value.Left     =   BC.Energy.Value.Left/CHAR.Temperature;
    end
    if isfield( BC.Energy.Value, 'Right')
        BC.Energy.Value.Right 	=   BC.Energy.Value.Right/CHAR.Temperature;
    end
    if isfield( BC.Energy.Value, 'Back')
        BC.Energy.Value.Back     =   BC.Energy.Value.Back/CHAR.Temperature;
    end
    if isfield( BC.Energy.Value, 'Front')
        BC.Energy.Value.Front 	=   BC.Energy.Value.Front/CHAR.Temperature;
    end
end

% for bottom boundary condition
if isfield(BC.Energy,'StepLike')
    if strcmp(lower(BC.Energy.Bottom),'stepliketemperature_mid')
        BC.Energy.StepLike.LeftX        =   BC.Energy.StepLike.LeftX/CHAR.Length;
        BC.Energy.StepLike.RightX       =   BC.Energy.StepLike.RightX/CHAR.Length;
        BC.Energy.T.StepLike.Rest       =   BC.Energy.T.StepLike.Rest/CHAR.Temperature;
        BC.Energy.T.StepLike.Step       =   BC.Energy.T.StepLike.Step/CHAR.Temperature;
    elseif strcmp(lower(BC.Energy.Bottom),'stepliketemperature')
        BC.Energy.T.StepLike.Right      =   BC.Energy.T.StepLike.Right/CHAR.Temperature;
        BC.Energy.T.StepLike.Left       =   BC.Energy.T.StepLike.Left/CHAR.Temperature;
        BC.Energy.StepLike.TransitionX  =   BC.Energy.StepLike.TransitionX/CHAR.Length;
        
    end
end

%% PARTICLES
if isfield(PARTICLES, 'x');
    PARTICLES.x             =   PARTICLES.x/CHAR.Length;
    PARTICLES.z             =   PARTICLES.z/CHAR.Length;
    HistVar_names           =   fieldnames(PARTICLES.HistVar);
    for ifield = 1:length(HistVar_names);
        CurrentField    =   HistVar_names{ifield};
        Data            =   getfield(PARTICLES.HistVar,CurrentField);
        
        switch CurrentField
            case 'T'
                Data = Data/CHAR.Temperature;
            case {'Txx','Tyy','Txy'}
                Data = Data/CHAR.Stress;
            case {'PHI'}
                % no need to make it ND
            case 'Pc'
                Data = Data/CHAR.Stress;
            case {'vf_x','vf_z'}
                Data = Data/CHAR.Velocity;
            case 'RhoHist'
                Data = Data/CHAR.Density;
            case {'MeltFraction_Extracted'}
            case {'MeltFraction_PhaseDiagram'}
            case {'WaterBound','WaterFree'}
            case {'MeltFraction_katz'}
            case {'StrainNonLocal'}
            otherwise
                error('Unknown History Variable on Particles; do not know how to non-dimensionalize it')
        end
        
        PARTICLES.HistVar       =   setfield(PARTICLES.HistVar,CurrentField,    Data);               	% store data on particles
        
    end
end


%% Material properties
for iphase=1:size(MATERIAL_PROPS,2)
    
    PHASE_PROPS     =   MATERIAL_PROPS(iphase);
    
    
    % Dimensionalize viscosity
%     Names           =   fieldnames(PHASE_PROPS.Viscosity);
%     for i=1:size(Names,1)
%         Type        =   Names{i};
%         
%         switch lower(Type)
%             case 'constant'
%                 PHASE_PROPS.Viscosity.(Type).Mu = PHASE_PROPS.Viscosity.(Type).Mu/CHAR.Viscosity;
%                 
%             case 'powerlaw'
%                 PHASE_PROPS.Viscosity.(Type).Mu0 = PHASE_PROPS.Viscosity.(Type).Mu0/CHAR.Viscosity;         % prefactor viscosity
%                 PHASE_PROPS.Viscosity.(Type).e0  = PHASE_PROPS.Viscosity.(Type).e0/(1/CHAR.Time);           % characteristic strainrate
%                 
%                 % The n exponent should not be non-dimensionalized
%                 
%             case 'dislocationcreep'
%                 % we keep this in dimensional units, and convert it inside
%                 % the viscosity routine
%                 
%             case 'diffusioncreep'
%                 % we keep this in dimensional units, and convert it inside
%                 % the viscosity routine
%             case 'peierlscreep'
%                 % we keep this in dimensional units, and convert it inside
%                 % the viscosity routine
%             case 'parameterizeddislocationcreep'
%                 % we keep this in dimensional units, and convert it inside
%                 % the viscosity routine
%             case 'parameterizeddistancedependentviscosity'
%                 % we keep this in dimensional units, and convert it inside
%                 % the viscosity routine
%             case 'strainweakening'
%                 % no action required since it is all dimensionless
%                 
%             case 'oldparameterizeddislocationcreep'
%                 % we keep this in dimensional units, and convert it inside
%                 % the viscosity routine
%             case 'binghamherschelbulkleycreep'
%                 % we keep this in dimensional units, and convert it inside
%                 % the viscosity routine
%             case 'blankenbach'
%                 % we keep this in dimensional units, and convert it inside
%                 % the viscosity routine
%             case 'temperaturedependent'
%                 % we keep this in dimensional units, and convert it inside
%                 % the viscosity routine
%             case 'parameterizedtemperatureanddepthdependentcreep'
%                 % we keep this in dimensional units, and convert it inside
%                 % the viscosity routine
%             case 'parameterizeddislocationdiffusioncreep'
%                 % we keep this in dimensional units, and convert it inside
%                 % the viscosity routine
%             otherwise
%                 error(['Do not know how to non-dimensionalize Viscosity of Type ',Type ])
%         end
%     end

    if isfield(PHASE_PROPS,'FluidViscosity')
        if       isfield(PHASE_PROPS.FluidViscosity,'Constant')
            % PHASE_PROPS.FluidViscosity.Constant.Mu = PHASE_PROPS.FluidViscosity.Constant.Mu/CHAR.Viscosity;        % in Pas
        elseif   isfield(PHASE_PROPS.FluidViscosity,'BittnerSchmeling_1995')
            % PHASE_PROPS.FluidViscosity.BittnerSchmeling_1995.Mu0 = PHASE_PROPS.FluidViscosity.BittnerSchmeling_1995.Mu0/CHAR.Viscosity;        % in Pas
        end
    end
    
    
    % Density
    Names           =   fieldnames(PHASE_PROPS.Density);
    for i=1:size(Names,1)
        Type        =   Names{i};
        
        switch lower(Type)
            case 'constant'
                PHASE_PROPS.Density.(Type).Rho = PHASE_PROPS.Density.(Type).Rho/CHAR.Density;
            case {'rayleighconvection','solcx_benchmark'}
                
            case 'temperaturedependent'
                PHASE_PROPS.Density.(Type).Rho0  = PHASE_PROPS.Density.(Type).Rho0/CHAR.Density;
                PHASE_PROPS.Density.(Type).T0    = PHASE_PROPS.Density.(Type).T0/CHAR.Temperature;
                PHASE_PROPS.Density.(Type).alpha = PHASE_PROPS.Density.(Type).alpha/(1/CHAR.Temperature);
                
                %             case 'temperaturepressuredependent'
                %                 PHASE_PROPS.Density.(Type).Rho0  = PHASE_PROPS.Density.(Type).Rho0/CHAR.Density;
                %                 PHASE_PROPS.Density.(Type).T0    = PHASE_PROPS.Density.(Type).T0/CHAR.Temperature;
                %                 PHASE_PROPS.Density.(Type).alpha = PHASE_PROPS.Density.(Type).alpha/(1/CHAR.Temperature);
                %                 PHASE_PROPS.Density.(Type).K0    = PHASE_PROPS.Density.(Type).K0/CHAR.Stress;
                %                 PHASE_PROPS.Density.(Type).K0p   = PHASE_PROPS.Density.(Type).K0p;
                
                
            case 'temperaturepressuredependent'
                PHASE_PROPS.Density.(Type).Rho0  = PHASE_PROPS.Density.(Type).Rho0/CHAR.Density;
                PHASE_PROPS.Density.(Type).T0    = PHASE_PROPS.Density.(Type).T0/CHAR.Temperature;
                
                PHASE_PROPS.Density.(Type).alpha = PHASE_PROPS.Density.(Type).alpha/(1/CHAR.Temperature);
                PHASE_PROPS.Density.(Type).beta  = PHASE_PROPS.Density.(Type).beta/(1/CHAR.Stress);
                
                
            case 'referencemodel'
                PHASE_PROPS.Density.(Type).Rho   = PHASE_PROPS.Density.(Type).Rho/CHAR.Density;
                PHASE_PROPS.Density.(Type).z     = PHASE_PROPS.Density.(Type).z/CHAR.Length;
                
            case 'phasediagram'
                
            case 'depthdependent'
                if isfield(PHASE_PROPS.Density.DepthDependent,'Rho')
                    PHASE_PROPS.Density.(Type).Rho = PHASE_PROPS.Density.(Type).Rho/CHAR.Density;
                end
                
            case 'fromchemistry'
                % We compute the density from chemistry for this particular
                % phase
                
                
            otherwise
                error(['Do not know how to non-dimensionalize Density of Type ',Type ])
        end
        
    end
    
    if isfield(PHASE_PROPS,'FluidDensity')
        if isfield(PHASE_PROPS.FluidDensity,'Constant')
            PHASE_PROPS.FluidDensity.Constant.Rho = PHASE_PROPS.FluidDensity.Constant.Rho/CHAR.Density;              % in kg/m3
        else
            % no other cases implemented yet
            error('not yet implemented')
        end
    end
    
    % Elasticity
    Names           =   fieldnames(PHASE_PROPS.Elasticity);
    for i=1:size(Names,1)
        Type        =   Names{i};
        
        switch lower(Type)
            case 'constant'
                PHASE_PROPS.Elasticity.(Type).G = PHASE_PROPS.Elasticity.(Type).G/CHAR.Stress;
            otherwise
                error(['Do not know how to non-dimensionalize shear Elasticity of Type ',Type ])
        end
        
    end
    
    % Poro-Elasticity
    Names           =   fieldnames(PHASE_PROPS.Elasticity);
    for i=1:size(Names,1)
        Type        =   Names{i};
        
        switch lower(Type)
            case {'constant'}
                PHASE_PROPS.Elasticity.(Type).K0 = PHASE_PROPS.Elasticity.(Type).K0/CHAR.Stress;
            otherwise
                error(['Do not know how to non-dimensionalize Poro-Elasticity of Type ',Type ])
        end
        
    end
    
    
    % Plasticity
    Names           =   fieldnames(PHASE_PROPS.Plasticity);
    for i=1:size(Names,1)
        Type        =   Names{i};
        
        switch lower(Type)
            case 'constant'
                PHASE_PROPS.Plasticity.(Type).YieldStress   = PHASE_PROPS.Plasticity.(Type).YieldStress/CHAR.Stress;
                PHASE_PROPS.Plasticity.(Type).YieldPressure = PHASE_PROPS.Plasticity.(Type).YieldPressure/CHAR.Stress;
            case 'druckerprager'
                PHASE_PROPS.Plasticity.(Type).Cohesion = PHASE_PROPS.Plasticity.(Type).Cohesion/CHAR.Stress;
            case 'druckerprager_maxyield'
                PHASE_PROPS.Plasticity.(Type).Cohesion          = PHASE_PROPS.Plasticity.(Type).Cohesion/CHAR.Stress;
                PHASE_PROPS.Plasticity.(Type).MaxYieldStress    = PHASE_PROPS.Plasticity.(Type).MaxYieldStress/CHAR.Stress;
            case 'druckerprager_viscoplasticregularization'
                PHASE_PROPS.Plasticity.(Type).Cohesion          = PHASE_PROPS.Plasticity.(Type).Cohesion/CHAR.Stress;
                PHASE_PROPS.Plasticity.(Type).VP_eta            = PHASE_PROPS.Plasticity.(Type).VP_eta/CHAR.Viscosity;
            case 'strainweakening'
                % all in ND units
            case 'depthdependentvonmises'
                PHASE_PROPS.Plasticity.(Type).Cohesion          = PHASE_PROPS.Plasticity.(Type).Cohesion/CHAR.Stress;
                PHASE_PROPS.Plasticity.(Type).MaxYieldStress    = PHASE_PROPS.Plasticity.(Type).MaxYieldStress/CHAR.Stress;
            case 'druckerpragergriffithmurrell'
                PHASE_PROPS.Plasticity.(Type).Cohesion = PHASE_PROPS.Plasticity.(Type).Cohesion/CHAR.Stress;
                PHASE_PROPS.Plasticity.(Type).sigma_T = PHASE_PROPS.Plasticity.(Type).sigma_T/CHAR.Stress;
            otherwise
                error(['Do not know how to non-dimensionalize Plasticity of Type ',Type ])
        end
        
    end
    
    % Thermal Conductivity
    Names           =   fieldnames(PHASE_PROPS.Conductivity);
    for i=1:size(Names,1)
        Type        =   Names{i};
        
        switch lower(Type)
            case 'constant'
                PHASE_PROPS.Conductivity.(Type).k =  PHASE_PROPS.Conductivity.(Type).k/CHAR.Conductivity;
            case 'temperaturedependent'
                
            case 'pressuretemperaturedependent'
                
            otherwise
                error(['Do not know how to non-dimensionalize Conductivity of Type ',Type ])
        end
        
    end
    
    % Heat Capacity
    Names           =   fieldnames(PHASE_PROPS.HeatCapacity);
    for i=1:size(Names,1)
        Type        =   Names{i};
        
        switch lower(Type)
            case 'constant'
                PHASE_PROPS.HeatCapacity.(Type).Cp =  PHASE_PROPS.HeatCapacity.(Type).Cp/CHAR.HeatCapacity;
            otherwise
                error(['Do not know how to non-dimensionalize HeatCapacity of Type ',Type ])
        end
        
    end
    
    
    % Radioactive heat
    Names           =   fieldnames(PHASE_PROPS.RadioactiveHeat);
    for i=1:size(Names,1)
        Type        =   Names{i};
        
        switch lower(Type)
            case 'constant'
                PHASE_PROPS.RadioactiveHeat.(Type).Q =  PHASE_PROPS.RadioactiveHeat.(Type).Q/CHAR.RadioactiveHeat;
            otherwise
                error(['Do not know how to non-dimensionalize RadioactiveHeat of Type ',Type ])
        end
        
    end
    
    % Gravitational acceleration
    if ~isempty(PHASE_PROPS.Gravity)
        PHASE_PROPS.Gravity.Value    =    PHASE_PROPS.Gravity.Value/(CHAR.Gravity);
    end
    
    if isfield(PHASE_PROPS,'Permeability')
        if isfield(PHASE_PROPS.Permeability,'Constant')
            PHASE_PROPS.Permeability.Constant.k_0 = PHASE_PROPS.Permeability.Constant.k_0/CHAR.Permeability;
        elseif isfield(PHASE_PROPS.Permeability,'Keller2013')
            PHASE_PROPS.Permeability.Keller2013.k_0    = PHASE_PROPS.Permeability.Keller2013.k_0/CHAR.Permeability;
        else error('Permeability law not recognized.');
        end
    end
    
    if isfield(PHASE_PROPS,'Water')
        % water migration 
        if isfield(PHASE_PROPS.Water,'SimplifiedDarcy')
            if isfield(PHASE_PROPS.Water.SimplifiedDarcy,'Constant')
                PHASE_PROPS.Water.SimplifiedDarcy.Constant.V_percolation = PHASE_PROPS.Water.SimplifiedDarcy.Constant.V_percolation/CHAR.Permeability*CHAR.Viscosity;
            elseif isfield(PHASE_PROPS.Water.SimplifiedDarcy,'MeltDependent')
                PHASE_PROPS.Water.SimplifiedDarcy.MeltDependent.K0 = PHASE_PROPS.Water.SimplifiedDarcy.MeltDependent.K0/CHAR.Permeability;
                PHASE_PROPS.Water.SimplifiedDarcy.MeltDependent.eta_f = PHASE_PROPS.Water.SimplifiedDarcy.MeltDependent.eta_f/CHAR.Viscosity;
                PHASE_PROPS.Water.SimplifiedDarcy.MeltDependent.Phi0 = PHASE_PROPS.Water.SimplifiedDarcy.MeltDependent.Phi0/1;
                PHASE_PROPS.Water.SimplifiedDarcy.MeltDependent.n = PHASE_PROPS.Water.SimplifiedDarcy.MeltDependent.n/1;
                PHASE_PROPS.Water.SimplifiedDarcy.MeltDependent.V_percolation_default = PHASE_PROPS.Water.SimplifiedDarcy.MeltDependent.V_percolation_default/(CHAR.Permeability/CHAR.Viscosity);
            elseif isfield(PHASE_PROPS.Water.SimplifiedDarcy,'MeltGrainsizeDependent')
                PHASE_PROPS.Water.SimplifiedDarcy.MeltGrainsizeDependent.d = PHASE_PROPS.Water.SimplifiedDarcy.MeltGrainsizeDependent.d/CHAR.Length;
                PHASE_PROPS.Water.SimplifiedDarcy.MeltGrainsizeDependent.eta_f = PHASE_PROPS.Water.SimplifiedDarcy.MeltGrainsizeDependent.eta_f/CHAR.Viscosity;
                PHASE_PROPS.Water.SimplifiedDarcy.MeltGrainsizeDependent.C = PHASE_PROPS.Water.SimplifiedDarcy.MeltGrainsizeDependent.C/1;
                PHASE_PROPS.Water.SimplifiedDarcy.MeltGrainsizeDependent.n = PHASE_PROPS.Water.SimplifiedDarcy.MeltGrainsizeDependent.n/1; 
                PHASE_PROPS.Water.SimplifiedDarcy.MeltGrainsizeDependent.V_percolation_default = PHASE_PROPS.Water.SimplifiedDarcy.MeltGrainsizeDependent.V_percolation_default/(CHAR.Permeability/CHAR.Viscosity);
            end
        elseif isfield(PHASE_PROPS.Water,'ConstantVelocity')
            PHASE_PROPS.Water.ConstantVelocity.Vx = PHASE_PROPS.Water.ConstantVelocity.Vx/CHAR.Velocity;
            PHASE_PROPS.Water.ConstantVelocity.Vz = PHASE_PROPS.Water.ConstantVelocity.Vz/CHAR.Velocity;
        end
    end
    if NUMERICS.LatentHeat
            PHASE_PROPS.ThermalExpansivity.Constant.alpha = PHASE_PROPS.ThermalExpansivity.Constant.alpha./(1/CHAR.Temperature);
    end
    MATERIAL_PROPS(iphase) = PHASE_PROPS;
    
end
%% ========================================================================


%% Nondimensionalize globally defined variables
% adiabatic heating
if isfield(MATERIAL_PROPS(1),'AdiabaticHeating')
    MATERIAL_PROPS(1).AdiabaticHeating.Constant.alpha =  MATERIAL_PROPS(1).AdiabaticHeating.Constant.alpha/(1/CHAR.Temperature);
end


%% NUMERICS
if ~isempty(NUMERICS.Viscosity)
    if isfield(NUMERICS.Viscosity,'LowerCutoff')
        
        
        % it is likely that this parameter is in dimensional units
        NUMERICS.Viscosity.LowerCutoff = NUMERICS.Viscosity.LowerCutoff/CHAR.Viscosity;
        
        
    end
    if isfield(NUMERICS.Viscosity,'UpperCutoff')
        
        % it is likely that this parameter is in dimensional units
        NUMERICS.Viscosity.UpperCutoff = NUMERICS.Viscosity.UpperCutoff/CHAR.Viscosity;
        
    end
    
    if isfield(NUMERICS.Plasticity,'MaximumYieldStress')
        NUMERICS.Plasticity.MaximumYieldStress = NUMERICS.Plasticity.MaximumYieldStress/CHAR.Stress;
    end
    if  isfield(NUMERICS.Plasticity,'NonLocalPlasticity')
        if isfield(NUMERICS.Plasticity.NonLocalPlasticity,'Radius')
            NUMERICS.Plasticity.NonLocalPlasticity.Radius = NUMERICS.Plasticity.NonLocalPlasticity.Radius/CHAR.Length;
        end
    end        
    
    
    if isfield(NUMERICS.Plasticity,'AddLithostaticPressure')
        NUMERICS.Plasticity.AddLithostaticPressure = NUMERICS.Plasticity.AddLithostaticPressure/CHAR.Stress;
    end
    
    if isfield(NUMERICS,'dt_max')
        NUMERICS.dt_max = NUMERICS.dt_max/CHAR.Time;
    end
end



%% EROSION AND SEDIMENTATION PARAMETERS
if ~isempty(NUMERICS.ErosionSedimentation)
    switch NUMERICS.ErosionSedimentation.Method
        case 'none'
            % nothing to be done
        case 'Constant_SedimentationErosionRates'
            % Erosion model with constant rate above/below defined
            % elevations
            NUMERICS.ErosionSedimentation.Erosion_Velocity                            =   NUMERICS.ErosionSedimentation.Erosion_Velocity/CHAR.Velocity;           % from m/s to ND units
            
            
            
            NUMERICS.ErosionSedimentation.ElevationBelowWhichToApplySedimentation     =   NUMERICS.ErosionSedimentation.ElevationBelowWhichToApplySedimentation/CHAR.Length;        % Elevation below which we apply sedimentation
            NUMERICS.ErosionSedimentation.ElevationAboveWhichToApplyErosion           =   NUMERICS.ErosionSedimentation.ElevationAboveWhichToApplyErosion/CHAR.Length;              % Elevation above which we apply erosion with a constant rate
            
            
            
            
        case 'FastErosionWithSedimentation_Particles'
            % Infinitely fast erosion model with a constant sedimentation rate
            NUMERICS.ErosionSedimentation.ElevationAboveWhichToApplyErosion           =   NUMERICS.ErosionSedimentation.ElevationAboveWhichToApplyErosion/CHAR.Length;              % Elevation above which we apply erosion with a constant rate
            NUMERICS.ErosionSedimentation.SedimentationTimeInterval_years             =   NUMERICS.ErosionSedimentation.SedimentationTimeInterval_years*CHAR.SecYear/CHAR.Time;     % converted from years to seconds and non-dimensionalized
            
            if isfield( NUMERICS.ErosionSedimentation,'Sedimentation_Velocity')
                NUMERICS.ErosionSedimentation.Sedimentation_Velocity                      =   NUMERICS.ErosionSedimentation.Sedimentation_Velocity/CHAR.Velocity;     % from m/s to ND units
            end
            if isfield( NUMERICS.ErosionSedimentation,'TimeInterval')
                NUMERICS.ErosionSedimentation.TimeInterval          =   NUMERICS.ErosionSedimentation.TimeInterval/CHAR.Time;
                NUMERICS.ErosionSedimentation.SedimentationRateTime =   NUMERICS.ErosionSedimentation.SedimentationRateTime/CHAR.Velocity;
            end
            
            
        otherwise
            error('I dont know the erosion/sedimentation method you are trying to use')
            
    end
end
%% ========================================================================



if ~isfield(NUMERICS,'time_start')
    NUMERICS.time_start     =   1;
end

if ~isfield(NUMERICS,'time_end')
    NUMERICS.time_end       =   1e5;
end


end


function Particles  =  DeleteOrReorderParticles(Particles, DeleteParticles, ind)
% DeleteOrReorderParticles
% Either deletes or reorders particles from the structure
%
% Syntax
%   Particles   =       DeleteOrReorderParticles(Particles, DeleteParticles, ind);
%
%       Input:
%               Particles       -   Particles structure
%               DeleteParticles -   logical(1):   Delete Particles; otherwise
%                                                 it reorders the particles
%               ind         -   In case we delete Particles: the logical
%                                   index that indicates which ones are to be deleted.
%                               In case we reorder Particles: an index
%                                   array that indicates how to reorder the
%                                   them.

% (Ragnar:) rewrote the function in a recursive manner such that it does not
% care what the fieldnames are or how many levels of sub-structs are stacked

% Usually, one would expect recursions to be more memory-intense but Matlab
% kind of anticipates what we plan to do, cp. http://www.matlabtips.com/copy-on-write/

Pfields     = fieldnames(Particles);

for ifield = 1:length(Pfields)
    % extract name and data of current subfield
    cName   = Pfields{ifield};
    cData   = Particles.(cName);
    
    if ~isstruct(cData)
        % extract or reorder data if it is not a struct itself
        if DeleteParticles
            cData(ind)   = [];
        else
            cData        = cData(ind);
        end
        
        Particles.(cName)   = cData;
        
    else
        % recursively call function on sub-struct
        Particles.(cName)   = DeleteOrReorderParticles(cData, DeleteParticles, ind);
    end
    
end
function BC = InternalPushingBoundaryConditions(varargin)
% Specifies an internal pushing boundary condition for use with MVEP2.
%
% Several methods are implemented:
%
% INITIALISATION (BEGINNING OF CODE):
%       BC = InternalPushingBoundaryConditions(BC, Xbox_coords, Zbox_coords, Type,  Velocities, PushingType)
%
%       Xbox_coords = [Xleft Xright  ];      % left/right coordinates of box
%       Zbox_coords = [Zbot  Ztop    ];      % bottom/top coordinates of box
%       Type        =
%                       'Vx'                -   Vx-velocity only
%                       'Vz'                -   Vz-velocity only
%                       'VxVz'              -   if both Vx and Vz are specified
%       Velocities  = [Vx [Vz]]             -   Pushing velocities in m/s; either Vx or Vx&Vz depending on "Type"
%       PushingType =
%                       'stagnant'          -   Pushing area is not updated during the simulation
%                       'advected'          -   Pushing area is advected with the flow
%                       'remove'            -   Removes the pushing condition altogether
%
%       Note: You can have several pushing blocks/regions in the code by calling the routine several times.
%
%
% DURING SIMULATION
%   1)     BC = InternalPushingBoundaryConditions(BC,dt);
%           If PushingType='advected', the pushing blocks will be advected with the pushing velocities and timesteps dt
%
%   2)    BC = InternalPushingBoundaryConditions(BC,'remove');
%           Removes the internal pushing boundary condition.
%
% See also:
%       SetBC

%$Id$


BC = varargin{1};

if nargin==2
    AdvectBox = logical(0);
    if isnumeric(varargin{2})
        dt          =   varargin{2};
        if isfield(BC.Stokes,'internal')
            AdvectBox = logical(1);
        end
    end
    
elseif nargin==6
    
    Xbox_coords =   varargin{2};
    Zbox_coords =   varargin{3};
    Type        =   varargin{4};
    Velocities  =   varargin{5};
    PushingType =   varargin{6};
    
else
    error('Unknown number of input parameters')
    
end

if nargin==2
    if AdvectBox
        % advect the pushing boxes with the given velocities
        
        PushingFieldNumber      =   length(BC.Stokes.internal.Type);
        for iBox = 1:PushingFieldNumber
            switch BC.Stokes.internal.Type{iBox}
                case 'Vx'
                    BC.Stokes.internal.xrange{iBox}    =    BC.Stokes.internal.xrange{iBox} + dt*BC.Stokes.internal.vx(iBox);
                case 'Vz'
                    BC.Stokes.internal.zrange{iBox}    =    BC.Stokes.internal.zrange{iBox} + dt*BC.Stokes.internal.vz(iBox);
                case 'VxVz'
                    BC.Stokes.internal.xrange{iBox}    =    BC.Stokes.internal.xrange{iBox} + dt*BC.Stokes.internal.vx(iBox);
                    BC.Stokes.internal.zrange{iBox}    =    BC.Stokes.internal.zrange{iBox} + dt*BC.Stokes.internal.vz(iBox);
            end
        end
        
    elseif strcmp(varargin{2},'remove')
        % Remove the internal pushing BC altogether
        if isfield(BC.Stokes,'internal')
            
            BC.Stokes = rmfield(BC.Stokes,'internal');
        end
        
    end
    
    
    
    
elseif nargin==6
    %% Initialize the pushing box setup
    
    %
    if ~isfield(BC.Stokes,'internal')
        BC.Stokes.internal     	=   [];
        PushingFieldNumber      =   1;
    else
        PushingFieldNumber      =   length(BC.Stokes.internal.Type) + 1 ;
    end
    
    BC.Stokes.internal.Type{PushingFieldNumber}        =  Type;
    BC.Stokes.internal.xrange{PushingFieldNumber}      =  Xbox_coords;
    BC.Stokes.internal.zrange{PushingFieldNumber}      =  Zbox_coords;
    switch Type
        case 'Vx'
            BC.Stokes.internal.vx(PushingFieldNumber)  =  Velocities(1);
        case 'Vz'
            BC.Stokes.internal.vz(PushingFieldNumber)  =  Velocities(1);
        case 'VxVz'
            BC.Stokes.internal.vx(PushingFieldNumber)  =  Velocities(1);
            BC.Stokes.internal.vz(PushingFieldNumber)  =  Velocities(2);
        otherwise
            error(['Unknown type of internal pushing boundary condition: ',Type])
    end
    
    
    BC.Stokes.internal.PushingType         =   PushingType;    % save type of pushing BC;
    
    
end



function PARTICLES = ModifyPhaseNumbersDueToPhaseTransitions(PARTICLES, MATERIAL_PROPS, CHAR)
% ModifyPhaseNumbersDueToPhaseTransitions modifies the phase numbers of
% PARTICLES due to a phase transition.
%
% This only applies is at least some of the phases compute their density
% from a phase diagram


for iphase = 1:length(MATERIAL_PROPS)
    if  isfield(MATERIAL_PROPS(iphase).Density,'PhaseDiagram')
        if isfield(MATERIAL_PROPS(iphase).Density.PhaseDiagram,'PhaseRename')
            PhaseRename = MATERIAL_PROPS(iphase).Density.PhaseDiagram.PhaseRename;
            
            %% (1) Load the phase diagram
            load(MATERIAL_PROPS(iphase).Density.PhaseDiagram.Name,'PhaseNumbers','P_Pa','T_K');
            
            %% (2) Error checking
            ChangePhaseNumbers      =   logical(0);
            if exist('PhaseNumbers','var')
                ChangePhaseNumbers      =   logical(1);
                
                % catch potential errors
                NumberPhase_PhaseDiagram = length(unique(PhaseNumbers(:)));
                if NumberPhase_PhaseDiagram ~= length(PhaseRename)
                   error(['The length of PhaseTransitions.PhaseRename for phase ',num2str(iphase),' should be the same as the number of phases in the Phase diagram which is ', num2str(NumberPhase_PhaseDiagram ) ]);
                end
            end
            
            %% (3) Change phase numbers if required
            if ChangePhaseNumbers
                indPart             =   find(PARTICLES.phases==iphase);
                P                   =   PARTICLES.CompVar.Pressure(indPart)*CHAR.Stress;     % Pressure    [Pa]
                T                   =   PARTICLES.HistVar.T(indPart)*CHAR.Temperature;       % Temperature [K]
                
                % assume a regular grid, which allows us to quickly find the nearest particle
                dP                  =   P_Pa(2)-P_Pa(1);
                dT                  =   T_K(2)-T_K(1);
                iP                  =   round( double(P(:)-P_Pa(1))/dP)+1;
                iT                  =   round( double(T(:)-T_K(1) )/dT)+1;
                iP(iP<1)            =   1;
                iP(iP>length(P_Pa)) =   length(P_Pa);
                
                iT(iT<1)            =   1;
                iT(iT>length(T_K))  =   length(T_K);
                
                ind                 =   sub2ind(size(PhaseNumbers),iT,iP);
                id                  =   find(~isnan(ind));
                Change_Phase        =   PhaseNumbers(ind(id));
                
                NewPhaseNumbers     =   zeros(size(ind));
                NewPhaseNumbers(id) =   PhaseRename( Change_Phase+1);       % the new phase numbers

                
                PARTICLES.phases(indPart) = NewPhaseNumbers;
            end
            
        end
    end
end
function [ipx, ipw] = ip_quad(nip)
% Defines integration point location and weights for quadrilateral FEM shape
% functions.

% $Id$

switch nip;

    case 1;
          ipx    =  [0 0];
          ipw    =  2;
          
    case 4;
        
        % They are reordered here, such that the extrapolation INTP -> NODES
        % works
        ipx(1,:) = [-0.577350269189626; -0.577350269189626];
        ipx(2,:) = [ 0.577350269189626; -0.577350269189626];
        ipx(3,:) = [ 0.577350269189626;  0.577350269189626];
        ipx(4,:) = [-0.577350269189626;  0.577350269189626];
        
        ipw(1:4) = 1;
        
    case 9;
        pointx(1)	=   -0.77459666924148;
        pointx(2)	=    0;
        pointx(3)	=    0.77459666924148;
        %pointx      =   (pointx+1)/2;
        
        weightx(1)	=    0.55555555555556;
        weightx(2)	=    0.88888888888889;
        weightx(3)	=    0.55555555555556;
        
        
        
        
        
        ii=1;
        for i=1:3
            for j=1:3
                ipx(ii,:) = [pointx(i) pointx(j)];
                ipw(ii)   = weightx(i)*weightx(j);
                
                ii=ii+1;
            end
        end
        
        
        % reorder them the same way as the element nodes
        id  =  [1 7 9 3 4 8 6 2 5];
        
        ipx = ipx(id,:);
        ipw = ipw(id);
        
      
        
        
    otherwise
        error('unknown number of integration points ind ip_quad')

end
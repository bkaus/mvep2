function PARTICLES = ComputeMeltFractionFromPhaseDiagrams(PARTICLES, MATERIAL_PROPS, CHAR)
% Takes P,T conditions on PARTICLES and a phase diagram and interpolates
% the melt fraction of the phase diagram on the particles.
%
% PARTICLES = ComputeNewChemistryOnParticles(PARTICLES, MATERIAL_PROPS, CHAR)
%
%   Input:
%       PARTICLES
%       MATERIAL_PROPS
%       CHAR
%
%   Output:
%       PARTICLES


if size(PARTICLES.x,1)>size(PARTICLES.x,2)
    % Bring them in the correct shape if needed
    PARTICLES.x         = PARTICLES.x(:)';
    PARTICLES.z         = PARTICLES.z(:)';
    PARTICLES.phase     = PARTICLES.phase(:)';
    PARTICLES.HistVar.T = PARTICLES.HistVar.T(:)';
end


if ~isfield(PARTICLES,'MeltExtraction');
    PARTICLES.MeltExtraction= [];
end
PARTICLES.MeltExtraction.MeltFraction_PhaseDiagram = zeros(size(PARTICLES.x));

for iphase=1:length(MATERIAL_PROPS) % loop over all phase
    
    if isfield(MATERIAL_PROPS(iphase).Density,'PhaseDiagram')
        
        % 1) Particles that have never been molten, will use the initial phase
        % diagram to update properties
        
        % Load initial phase diagram
        filename = [MATERIAL_PROPS(iphase).Density.PhaseDiagram.Name];
        vars = whos('-file',filename);
        if ismember('Melt', {vars.name});
            % If melt exists on the phase diagram
            
            % load diagram
            load(filename,'Melt', 'P_Pa','T_K');
            
            % find relevant particles
            ind             =   find(PARTICLES.phases==iphase);
            
            % Interpolate chemistry on particles from initial phase diagram
            PARTICLES       =   InterpolateRelevantFields(Melt, ind, PARTICLES, CHAR, P_Pa, T_K);
            
            
        end
        
    end
end


if any(isnan(PARTICLES.MeltExtraction.MeltFraction_PhaseDiagram))
    error('Melt Fraction is NaN at some point, which indicates a problem with the phase diagram')
end




% Set some of the relevant fields to be History or Computational Variables
% such that they are automatically interpolated to integration points & are
% available for computing densities @ integration points etc.
% PARTICLES.CompVar.MeltFraction_PhaseDiagram          =   PARTICLES.MeltExtraction.MeltFraction_PhaseDiagram;


%==========================================================================
function PARTICLES = InterpolateRelevantFields(Melt, ind, PARTICLES, CHAR, P_Pa,T_K)
% This:
%
%   1) Interpolates the melt fraction to the subset of particles from the
%       phase diagram.


% (P,T) conditions for which we interpolate values

% DIMENSIONALIZE IT!!!!
P_Particles     =   PARTICLES.CompVar.Pressure(ind)*CHAR.Stress;    % in Pa
T_Particles     =   PARTICLES.HistVar.T(ind)*CHAR.Temperature;      % in Kelvin, NOT Celcius! Phase diagrams MUST be defined in K as well!

% Find particles that are outside the validity range of diagram
id = find(P_Particles<min(P_Pa));
if ~isempty(id)
    warning([num2str(length(id)),' particles have a pressure that is lower than the min(P) of the phase diagram'])
    P_Particles(id) = min(P_Pa);
end
id = find(P_Particles>max(P_Pa));
if ~isempty(id)
    warning([num2str(length(id)),' particles have a pressure that is larger than the max(P) of the phase diagram'])
    P_Particles(id) = max(P_Pa);
end

id = find(T_Particles<min(T_K) );
if ~isempty(id)
    warning([num2str(length(id)),' particles have a temperature that is smaller than the min(T) of the phase diagram'])
    T_Particles(id) = min(T_K);
end
id = find(T_Particles>max(T_K) );
if ~isempty(id)
    warning([num2str(length(id)),' particles have a temperature that is larger than the max(T) of the phase diagram'])
    T_Particles(id) = max(T_K);
end

% Interpolate Melt content from phase diagram
PARTICLES.MeltExtraction.MeltFraction_PhaseDiagram(ind(:))   =   interp2(P_Pa,T_K, Melt,P_Particles,T_Particles,'*nearest');



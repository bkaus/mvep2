function [BC] =   SetBC_FluidPressure(MESH, BC, INTP_PROPS, MATERIAL_PROPS)
% Sets boundary conditions for the fluid pressure in the coupled
% Stokes-Darcy system

% $Id$

GCOORD              =       MESH.DARCY.NODES;
Point_id            =       MESH.DARCY.Point_id;
BoundaryCondition   =       BC.Darcy;

%==========================================================================
% BOTTOM BOUNDARY CONDITION
%==========================================================================
switch lower(BoundaryCondition.Bottom)
    case 'zero flux'
        BCbottom = [];
    case 'melt reservoir'
        phi_max = BoundaryCondition.phi_max;
        % select those bottom boundary nodes (via Point_id) that have
        % porosity >= .1%
        Points_melt_res = find(Point_id==1 & MESH.DARCY.TwoPhaseInd);
        rho_bulk        = (1-phi_max) * MATERIAL_PROPS(1).Density.Constant.Rho + ...
            phi_max * MATERIAL_PROPS(1).TwoPhase.FluidDensity.Constant.rho_0;
        %rho_s           = MATERIAL_PROPS(1).Density.Constant.Rho;
        grav            = MATERIAL_PROPS(1).Gravity.Value;
        
        % in Darcy_MissingBlocks these values are multiplied with the depth
        bc_values       = -grav * rho_bulk * MESH.DARCY.NODES(2,Points_melt_res);
        
        % writing bc_values to third row of BCbottom implies that they are
        % taken as is upon matrix assembly (not multiplied by z coordinate)
        BCbottom =   [Points_melt_res;
            1*ones(size(Points_melt_res));   bc_values;   ...
            zeros(size(Points_melt_res));    zeros(size(Points_melt_res))];
    case 'dirichlet'
        Points_bot      = find(Point_id==1);
        BottomValue     = BoundaryCondition.Value.Bottom;
        BCbottom        = [Points_bot; ...
            1*ones(size(Points_bot)); BottomValue * ones(size(Points_bot)); ...
            zeros(size(Points_bot)); zeros(size(Points_bot))];
    otherwise
        error('Bottom boundary condition for fluid pressure is not implemented.')
end

%==========================================================================
% TOP BOUNDARY CONDITION
%==========================================================================
switch lower(BoundaryCondition.Top)
    case 'zero flux'
        BCtop   = [];
    case 'dirichlet'
        Points_top      = find(Point_id==3);
        TopValue        = BoundaryCondition.Value.Top;
        BCtop           = [Points_top; ...
            1*ones(size(Points_top)); TopValue * ones(size(Points_top));
            zeros(size(Points_top)); zeros(size(Points_top))];
    otherwise
        error('Top boundary condition for fluid pressure is not implemented.')
end

%==========================================================================
% LEFT BOUNDARY CONDITION
%==========================================================================
switch lower(BoundaryCondition.Left)
    case 'zero flux'
        BCleft  = [];
    case 'periodic'
        Points_left     = find(Point_id==2);
        [~,ind]         =   sort(GCOORD(2,Points_left));
        Points_left     =   Points_left(ind);
        BCleft          = [Points_left; ones(size(Points_left))];
    otherwise
        error('Left boundary condition for fluid pressure is not implemented.')
end

%==========================================================================
% RIGHT BOUNDARY CONDITION
%==========================================================================
switch lower(BoundaryCondition.Right)
    case 'zero flux'
        BCright = [];
    case 'periodic'
        Points_right    = find(Point_id==2);
        [~,ind]         = sort(GCOORD(2,Points_right));
        Points_right    = Points_right(ind);
        BCright         = [Points_right; ones(size(Points_right))];
    otherwise
        error('Right boundary condition for fluid pressure is not implemented.')
end

% For compatibility with MILAMIN
if strcmpi(BoundaryCondition.Left,'periodic') && strcmpi(BoundaryCondition.Right,'periodic')
    BC.Darcy.BC     = [BCbottom, BCtop];
    BC.Darcy.PERIOD = [BCleft(1,:); BCright(1,:); BCleft(2,:);];
else
    BC.Darcy.BC     = [BCleft, BCbottom, BCtop, BCright];
    BC.Darcy.PERIOD = [];
end


%==========================================================================
% (Check the BC's and) remove doubles
%==========================================================================
% extract two upper rows (to determine DOF)
if ~isempty(BC.Darcy.BC)
    tmp             = BC.Darcy.BC(1:2,:)';
    % remove duplicates and save first occurence of duplicate entries
    [tmp,ord,~]     = unique(tmp,'rows');
    
    % re-combine two upper and three bottom rows
    BC.Darcy.BC    = [tmp'; BC.Darcy.BC(3:5,ord)];
end
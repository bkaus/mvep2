function MESH = RefineMesh(MESH, opts, ind_TrianglesToBeRefined)
% This refines certain elements of a triangular mesh
switch MESH.element_type.velocity
    case {'T1','T2'}
        
        
        
        if ~isfield(opts,'min_angle')
            opts.min_angle                              = 20;
        end
        if ~isfield(opts,'refinement')
            opts.refinement                             = 3;
        end
        
        no_elems                                        = size(MESH.ELEMS,2);
        opts.other_options                              = 'r';
        
        % Zeros or negative values will switch off triangle area constraints,
        % refine such that refined triangle has only quarter of previous area.
        % Note: opts.max_tri_area decides about flag that is used when calling
        % triangle routine. You should distinguish the following two cases:
        %   (1) max_tri_area scalar: zero or negative value implies globally no
        %           triangle area constraint, positive value is enforced globally,
        %           i.e., for each triangle.
        %   (2) max_tri_area vector: sets flag -a for triangle routine such that
        %           the area constraints are going to be read from the field
        %           mesh_input.trianglearea
        %contains one value per triangle of previous
        %           mesh, zeros or negative values imply no constraints, positive
        %           value(s) are enforced for the respective triangles.
        opts.max_tri_area                               = zeros(1,no_elems);
        
        % Element to be refined should have 1/(refinement) of previous area.
        % Note: Adjacent elements might also be refined.
        opts.max_tri_area(ind_TrianglesToBeRefined)     = MESH.AREA(ind_TrianglesToBeRefined)/opts.refinement;
        
        % mesh_input.trianglearea contains one value per triangle of previous mesh,
        % zeros or negative values imply no constraints, positive value(s) are
        % enforced for the respective triangles.
        mesh_input.trianglearea                         = opts.max_tri_area;
        
        
        % Set dummy values (not actually used)
        mesh_input.x_min = 0;
        mesh_input.x_max = 1;
        mesh_input.z_min = 0;
        mesh_input.z_max = 1;
    
        % Refine the actual mesh
        MESH                          = CreateMesh(opts, mesh_input, MESH);
        
    otherwise
        MESH = MESH;
        warning('Mesh refinement can only be done for triangular elements')
        
end

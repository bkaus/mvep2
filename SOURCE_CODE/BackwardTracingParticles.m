function [Location] = BackwardTracingParticles(Directory,starting_filenumber, StartingLocation,Step);
% This routine uses MILAMIN_VEP output files to trace a particle backwards
% in time and generate, for example, a P-T path for the particle. This is
% done with the velocities that are saved at the various timesteps.
%
% A 4th order Runga-Kutta method is used for advecting.
%
% Usage:
%
%  [Location] = BackwardTracingParticles(Directory,starting_filenumber, StartingLocation);
%
%       Input:
%               Directory               -   Directory which has the MVEP2 output files
%               starting_filenumber     -   Starting file #
%               StartingLocation        -   Structure with the following fields:
%                                           StartingLocation.x - in [m]
%                                           StartingLocation.z - in [m]
%               Step                    -   Spacing between the Outputs
%                                           (Attention the higher the
%                                           spacing the less exact the
%                                           tracing)
%                                            
%
% $Id$

% % Directory       = 'CrustalConvectionMultiplePulses_HiRes';
% % starting_filenumber = 10;
% Directory = pwd;
% starting_filenumber = 13;

% % Indicate the starting location in m's - you can indicate several starting points
% StartingLocation_x  = 1900e3;
% StartingLocation_z  = -100e3;

AddDirectories;


% Filename of current timestep
curdir2             =   pwd;
Current_fname       =   ['Output','_', num2str(starting_filenumber+1e6),'.mat']
%cd(Directory)
load(Current_fname);    % will give error message if not available

% In order to significantly speed up this routine, we only take nodal points into
% account that are sufficiently close to the Location we are looking for.
BoundingBoxSize     =   5e3;          % we


% Create row vectors out of it
Location.x          =   StartingLocation.x(:)/CHAR.Length;     % dimensionless
Location.z          =   StartingLocation.z(:)/CHAR.Length;     % dimensionless
Location.time       =   time;


% Only check
BoundingBoxSize     =   BoundingBoxSize/CHAR.Length;

min_x               =   min(Location.x(:,end)) - BoundingBoxSize;
max_x               =   max(Location.x(:,end)) + BoundingBoxSize;
min_z               =   min(Location.z(:,end)) - BoundingBoxSize;
max_z               =   max(Location.z(:,end)) + BoundingBoxSize;

ind_nearby_nodes    =   find( (MESH.NODES(1,:)    > min_x)  & (MESH.NODES(1,:) < max_x ) & (MESH.NODES(2,:) > min_z)  & (MESH.NODES(2,:) < max_z ) );
ind_nearby_intp     =   find( (INTP_PROPS.X           > min_x)  & (INTP_PROPS.X        < max_x ) & (INTP_PROPS.Z        > min_z)  & (INTP_PROPS.Z         < max_z ) );

while length(ind_nearby_nodes)<10
    display('Bounding Box is too small .. Made it larger to find nearby nodes and integration points!')
    BoundingBoxSize = 2*BoundingBoxSize
    min_x               =   min(Location.x(:,end))-BoundingBoxSize;
    max_x               =   max(Location.x(:,end))+BoundingBoxSize;
    min_z               =   min(Location.z(:,end))-BoundingBoxSize;
    max_z               =   max(Location.z(:,end)+BoundingBoxSize);
    
    ind_nearby_nodes    =   find( (MESH.NODES(1,:)    > min_x)  & (MESH.NODES(1,:) < max_x ) & (MESH.NODES(2,:) > min_z)  & (MESH.NODES(2,:) < max_z ) );
    %     ind_nearby_intp     =   find( (INTP_PROPS.X           > min_x)  & (INTP_PROPS.X        < max_x ) & (INTP_PROPS.Z        > min_z)  & (INTP_PROPS.Z         < max_z ) );
end

while length(ind_nearby_intp)<10
    display('Bounding Box is too small .. Made it larger to find nearby nodes and integration points!')
    BoundingBoxSize = 2*BoundingBoxSize
    min_x               =   min(Location.x(:,end))-BoundingBoxSize;
    max_x               =   max(Location.x(:,end))+BoundingBoxSize;
    min_z               =   min(Location.z(:,end))-BoundingBoxSize;
    max_z               =   max(Location.z(:,end)+BoundingBoxSize);
    
    %     ind_nearby_nodes    =   find( (MESH.NODES(1,:)    > min_x)  & (MESH.NODES(1,:) < max_x ) & (MESH.NODES(2,:) > min_z)  & (MESH.NODES(2,:) < max_z ) );
    ind_nearby_intp     =   find( (INTP_PROPS.X           > min_x)  & (INTP_PROPS.X        < max_x ) & (INTP_PROPS.Z        > min_z)  & (INTP_PROPS.Z         < max_z ) );
end

if isfield(MESH.CompVar,'Litho_Overpressure')
    Location.Litho_Overpressure = griddata(double(MESH.NODES(1,ind_nearby_nodes)),double(MESH.NODES(2,ind_nearby_nodes)),MESH.CompVar.Litho_Overpressure(ind_nearby_nodes),Location.x(:,end), Location.z(:,end));
end
if isfield(MESH.CompVar,'Lithostatic_Pressure')
    Location.Lithostatic_Pressure = griddata(double(MESH.NODES(1,ind_nearby_nodes)),double(MESH.NODES(2,ind_nearby_nodes)),MESH.CompVar.Lithostatic_Pressure(ind_nearby_nodes),Location.x(:,end), Location.z(:,end));
end
Location.Temperature =   griddata(double(MESH.NODES(1,ind_nearby_nodes)),double(MESH.NODES(2,ind_nearby_nodes)),MESH.TEMP(ind_nearby_nodes),Location.x(:,end), Location.z(:,end));
Location.Pressure    =   griddata(double(MESH.NODES(1,ind_nearby_nodes)),double(MESH.NODES(2,ind_nearby_nodes)),MESH.CompVar.Pressure(ind_nearby_nodes),Location.x(:,end), Location.z(:,end));

if isfield(INTP_PROPS,'E2nd')    % we have strainrate info @ integration points
    Location.E2nd =   griddata(double(INTP_PROPS.X(ind_nearby_intp)),double(INTP_PROPS.Z(ind_nearby_intp)),double(INTP_PROPS.E2nd(ind_nearby_intp)),Location.x(:,end), Location.z(:,end));
end
if isfield(INTP_PROPS,'T2nd')    % we have stress invariant info @ integration points
    Location.T2nd =   griddata(double(INTP_PROPS.X(ind_nearby_intp)),double(INTP_PROPS.Z(ind_nearby_intp)),double(INTP_PROPS.T2nd(ind_nearby_intp)),Location.x(:,end), Location.z(:,end));
end

if isfield(INTP_PROPS,'Strain')    % we have strain info @ integration points
    Location.Strain =   griddata(double(INTP_PROPS.X(ind_nearby_intp)),double(INTP_PROPS.Z(ind_nearby_intp)),double(INTP_PROPS.Strain(ind_nearby_intp)),Location.x(:,end), Location.z(:,end));
end

%% Compute Particle Location and Parameters for every timestep without the first one


% define Step so that minimum Step = 3 because we do not save all the
% properties for the first timesteps!
if Step>=3
    Stepx = Step;
else
    Stepx = 3;
end

for num =(starting_filenumber-Step):-Step:Stepx
    
    % load file one timestep ago
    Old_fname        = ['Output','_', num2str((num)+1e6),'.mat'];
    if exist(Old_fname,'file')
        load_file=logical(1);
    else
        load_file=logical(0);
    end
    
    
    if load_file % we found the old file
        Old_fname
        % Load the required data of the current timestep
        load(Current_fname,'MESH','INTP_PROPS','time')
        
        CurrentStep.X       	=   double(MESH.NODES(1,:));
        CurrentStep.Z           =   double(MESH.NODES(2,:));
        CurrentStep.time       	=   time;
        CurrentStep.VELX        =   MESH.VEL(1,:);
        CurrentStep.VELZ        =   MESH.VEL(2,:);
        CurrentStep.x2d         =   CurrentStep.X(MESH.RegularGridNumber);
        CurrentStep.z2d         =   CurrentStep.Z(MESH.RegularGridNumber);
        CurrentStep.Vx2d        =   CurrentStep.VELX(MESH.RegularGridNumber);
        CurrentStep.Vz2d        =   CurrentStep.VELZ(MESH.RegularGridNumber);
        CurrentStep.Temp2d      =   MESH.TEMP(MESH.RegularGridNumber);
        if isfield(MESH.CompVar,'Litho_Overpressure')
        CurrentStep.Litho_Overpressure = MESH.CompVar.Litho_Overpressure(MESH.RegularGridNumber);
        end
        if isfield(MESH.CompVar,'Lithostatic_Pressure')
        CurrentStep.Lithostatic_Pressure = MESH.CompVar.Lithostatic_Pressure(MESH.RegularGridNumber);
        end
        
        % Load the required data of the old timestep
        load(Old_fname,'MESH','INTP_PROPS','time')                 % change this later to only load a few fields
        
        MESH_OLD            =   MESH;
        
        OldStep.X       	=   double(MESH.NODES(1,:));
        OldStep.Z           =   double(MESH.NODES(2,:));
        OldStep.time       	=   time;                                           %
        OldStep.VELX        =   MESH.VEL(1,:);
        OldStep.VELZ        =   MESH.VEL(2,:);
        OldStep.x2d         =   OldStep.X(MESH.RegularGridNumber);
        OldStep.z2d         =   OldStep.Z(MESH.RegularGridNumber);
        OldStep.Vx2d        =   OldStep.VELX(MESH.RegularGridNumber);
        OldStep.Vz2d        =   OldStep.VELZ(MESH.RegularGridNumber);
        OldStep.Temp2d      =   MESH.TEMP(MESH.RegularGridNumber);
        OldStep.Pres2d      =   MESH.CompVar.Pressure(MESH.RegularGridNumber);
        if isfield(MESH.CompVar,'Litho_Overpressure')
        OldStep.Litho_Overpressure = MESH.CompVar.Litho_Overpressure(MESH.RegularGridNumber);
        end
        if isfield(MESH.CompVar,'Lithostatic_Pressure')
        OldStep.Lithostatic_Pressure = MESH.CompVar.Lithostatic_Pressure(MESH.RegularGridNumber);
        end
        
        % Compute relevant points
        BoundingBoxSize     =   3e3;
        min_x               =   min(Location.x(:,end))-BoundingBoxSize;
        max_x               =   max(Location.x(:,end))+BoundingBoxSize;
        min_z               =   min(Location.z(:,end))-BoundingBoxSize;
        max_z               =   max(Location.z(:,end)+BoundingBoxSize);
        
        ind_nearby_nodes    =   find( (CurrentStep.x2d  > min_x)  & (CurrentStep.x2d < max_x ) & (CurrentStep.z2d > min_z)  & (CurrentStep.z2d < max_z ) );
        ind_nearby_intp     =   find( (INTP_PROPS.X           > min_x)  & (INTP_PROPS.X        < max_x ) & (INTP_PROPS.Z        > min_z)  & (INTP_PROPS.Z         < max_z ) );
        
        
        clear MESH.NODES  MESH.VEL RegularGridNumber time;
        Current_fname       =   Old_fname; % update filename
        
        dt                  =    CurrentStep.time- OldStep.time;    % timestep
        
        
        % Compute grids and velocity at dt/2 (use linear interpolation in
        % time)
        x_half              =   ( OldStep.x2d  + CurrentStep.x2d )/2;
        z_half              =   ( OldStep.z2d  + CurrentStep.z2d )/2;
        Vx_half             =   ( OldStep.Vx2d + CurrentStep.Vx2d )/2;
        Vz_half             =   ( OldStep.Vz2d + CurrentStep.Vz2d )/2;
        
        % We follow the wikipedia nomenclature here for RK4, with the
        % difference that h=-dt  (see
        % http://en.wikipedia.org/wiki/Runge?Kutta_methods) plus that we do
        % it in 2D:
        %
        % y_n+1 = y_n + 1/6*(k1 + 2*k2 + 2*k3 + k4)
        % t_n+1 = t_n - dt;
        %
        % where
        %     k1 = -dt*f(t_n       , y_n         )
        %     k2 = -dt*f(t_n-1/2*dt, y_n + 1/2*k1)
        %     k3 = -dt*f(t_n-1/2*dt, y_n + 1/2*k2)
        %     k4 = -dt*f(t_n-    dt, y_n +     k3)
        
        
        % Determine if we can use this routine (newer MATLAB versions)
        if exist('TriScatteredInterp')>0
            FastInterpolationMethod = logical(1);
        else
            FastInterpolationMethod = logical(0);
        end
        
        if FastInterpolationMethod
            F      =   TriScatteredInterp(CurrentStep.x2d(:),CurrentStep.z2d(:),CurrentStep.Vx2d(:));    % construct interpolant
            Vx1    =   F(Location.x(:,end),Location.z(:,end));      % Vx @ particle
            F.V    =   CurrentStep.Vz2d(:);                         % Grid remains the same;only the values change
            Vz1    =   F(Location.x(:,end),Location.z(:,end));      % Vz @ particle
            
        else
            Vx1    =   griddata(CurrentStep.x2d(ind_nearby_nodes),CurrentStep.z2d(ind_nearby_nodes),CurrentStep.Vx2d(ind_nearby_nodes),Location.x(:,end),Location.z(:,end));        % Vx @ particle
            Vz1    =   griddata(CurrentStep.x2d(ind_nearby_nodes),CurrentStep.z2d(ind_nearby_nodes),CurrentStep.Vz2d(ind_nearby_nodes),Location.x(:,end),Location.z(:,end));        % Vz @ particle
        
        end
        k1_x = - dt*Vx1;
        k1_z = - dt*Vz1;
        
        
        if FastInterpolationMethod
            F      =   TriScatteredInterp(x_half(:),z_half(:),Vx_half(:));    % construct interpolant
            Vx2    =   F( Location.x(:,end)  + 0.5*k1_x,  Location.z(:,end)  + 0.5*k1_z);      % Vx @ particle
            F.V    =   Vz_half(:);                         % Grid remains the same;only the values change
            Vz2    =   F( Location.x(:,end)  + 0.5*k1_x,  Location.z(:,end)  + 0.5*k1_z);      % Vz @ particle
            
        else
            Vx2    =   griddata(x_half(ind_nearby_nodes),z_half(ind_nearby_nodes),Vx_half(ind_nearby_nodes), Location.x(:,end)  + 0.5*k1_x,  Location.z(:,end)  + 0.5*k1_z);        % Vx @ particle
            Vz2    =   griddata(x_half(ind_nearby_nodes),z_half(ind_nearby_nodes),Vz_half(ind_nearby_nodes), Location.x(:,end)  + 0.5*k1_x,  Location.z(:,end)  + 0.5*k1_z);        % Vz @ particle
            
        end
        k2_x = - dt*Vx2;
        k2_z = - dt*Vz2;
        
        if FastInterpolationMethod
            F      =   TriScatteredInterp(x_half(:),z_half(:),Vx_half(:));    % construct interpolant
            Vx3    =   F( Location.x(:,end)  + 0.5*k2_x,  Location.z(:,end)  + 0.5*k2_z);      % Vx @ particle
            F.V    =   Vz_half(:);                         % Grid remains the same;only the values change
            Vz3    =   F( Location.x(:,end)  + 0.5*k2_x,  Location.z(:,end)  + 0.5*k2_z);      % Vz @ particle
            
        else
            Vx3    =   griddata(x_half(ind_nearby_nodes),z_half(ind_nearby_nodes),Vx_half(ind_nearby_nodes),Location.x(:,end)  + 0.5*k2_x, Location.z(:,end)  + 0.5*k2_z);        % Vx @ particle
            Vz3    =   griddata(x_half(ind_nearby_nodes),z_half(ind_nearby_nodes),Vz_half(ind_nearby_nodes),Location.x(:,end)  + 0.5*k2_x, Location.z(:,end)  + 0.5*k2_z);        % Vz @ particle
            
        end
        k3_x = - dt*Vx3;
        k3_z = - dt*Vz3;
        
        if FastInterpolationMethod
            F      =   TriScatteredInterp(OldStep.x2d(:),OldStep.z2d(:),OldStep.Vx2d(:));    % construct interpolant
            Vx4    =   F(Location.x(:,end)  + k3_x, Location.z(:,end)  + k3_z);      % Vx @ particle
            F.V    =   OldStep.Vz2d(:);                         % Grid remains the same;only the values change
            Vz4    =   F(Location.x(:,end)  + k3_x, Location.z(:,end)  + k3_z);      % Vz @ particle
            
        else
            Vx4    =   griddata(OldStep.x2d(ind_nearby_nodes),OldStep.z2d(ind_nearby_nodes),OldStep.Vx2d(ind_nearby_nodes),Location.x(:,end)  + k3_x, Location.z(:,end)  + k3_z);        % Vx @ particle
            Vz4    =   griddata(OldStep.x2d(ind_nearby_nodes),OldStep.z2d(ind_nearby_nodes),OldStep.Vz2d(ind_nearby_nodes),Location.x(:,end)  + k3_x, Location.z(:,end)  + k3_z);        % Vz @ particle
        end
        k4_x = - dt*Vx4;
        k4_z = - dt*Vz4;
        
        % Compute location of the particle @ OldStep
        Location.x(:,end+1) = Location.x(:,end) + 1/6*(k1_x + 2*k2_x + 2*k3_x + k4_x);
        Location.z(:,end+1) = Location.z(:,end) + 1/6*(k1_z + 2*k2_z + 2*k3_z + k4_z);
        
        % Compute properties at the particle (temperature, etc)
        Location.time(end+1)= OldStep.time;
        
        if FastInterpolationMethod
            F                               =   TriScatteredInterp(OldStep.x2d(:),OldStep.z2d(:),OldStep.Temp2d(:));
            Location.Temperature(:,end+1)   =   F(Location.x(:,end), Location.z(:,end));
            
            F.V                             =   MESH.CompVar.Pressure(:);
            Location.Pressure(:,end+1)      =   F(Location.x(:,end), Location.z(:,end));
            
            if isfield(MESH.CompVar,'Litho_Overpressure')
             F                              = TriScatteredInterp(OldStep.x2d(:),OldStep.z2d(:),OldStep.Litho_Overpressure(:));
             Location.Litho_Overpressure(:,end+1) = F(Location.x(:,end), Location.z(:,end));
            end
            if isfield(MESH.CompVar,'Lithostatic_Pressure')
             F                              = TriScatteredInterp(OldStep.x2d(:),OldStep.z2d(:),OldStep.Lithostatic_Pressure(:));
             Location.Lithostatic_Pressure(:,end+1) = F(Location.x(:,end), Location.z(:,end));
            end
            
            
        else
            
            if isfield(MESH.CompVar,'Litho_Overpressure')
            Location.Litho_Overpressure(:,end+1) = griddata(OldStep.x2d(ind_nearby_nodes),OldStep.z2d(ind_nearby_nodes),OldStep.Litho_Overpressure(ind_nearby_nodes),Location.x(:,end), Location.z(:,end));
            end
            if isfield(MESH.CompVar,'Lithostatic_Pressure')
            Location.Lithostatic_Pressure(:,end+1) = griddata(OldStep.x2d(ind_nearby_nodes),OldStep.z2d(ind_nearby_nodes),OldStep.Lithostatic_Pressure(ind_nearby_nodes),Location.x(:,end), Location.z(:,end));
            end
            Location.Temperature(:,end+1) =   griddata(OldStep.x2d(ind_nearby_nodes),OldStep.z2d(ind_nearby_nodes),OldStep.Temp2d(ind_nearby_nodes),Location.x(:,end), Location.z(:,end));
            Location.Pressure(:,end+1)    =   griddata(OldStep.x2d(ind_nearby_nodes),OldStep.z2d(ind_nearby_nodes),OldStep.Pres2d(ind_nearby_nodes),Location.x(:,end), Location.z(:,end));
            
        end
        
        
        
        if FastInterpolationMethod
            F_Intp                          =  TriScatteredInterp(double(INTP_PROPS.X(ind_nearby_intp)),double(INTP_PROPS.Z(ind_nearby_intp)),double(INTP_PROPS.Pressure(ind_nearby_intp)));
            
            if isfield(INTP_PROPS,'E2nd')    % we have strainrate info @ integration points
                F_Intp.V                    =       double(INTP_PROPS.E2nd(ind_nearby_intp)); 
                Location.E2nd(:,end+1)      =       F_Intp(Location.x(:,end), Location.z(:,end));
            end
            
             if isfield(INTP_PROPS,'T2nd')    % we have strainrate info @ integration points
                F_Intp.V                    =       double(INTP_PROPS.T2nd(ind_nearby_intp)); 
                Location.T2nd(:,end+1)      =       F_Intp(Location.x(:,end), Location.z(:,end));
             end
            
        else
            
            
            if isfield(INTP_PROPS,'E2nd')    % we have strainrate info @ integration points
                Location.E2nd(:,end+1) =   griddata(double(INTP_PROPS.X(ind_nearby_intp)),double(INTP_PROPS.Z(ind_nearby_intp)),double(INTP_PROPS.E2nd(ind_nearby_intp)),Location.x(:,end), Location.z(:,end));
            end
            
            if isfield(INTP_PROPS,'T2nd')    % we have stress invariant info @ integration points
                Location.T2nd(:,end+1) =   griddata(double(INTP_PROPS.X(ind_nearby_intp)),double(INTP_PROPS.Z(ind_nearby_intp)),double(INTP_PROPS.T2nd(ind_nearby_intp)),Location.x(:,end), Location.z(:,end));
            end
            
        end
        
    
        
    end
    
    
    
    
end
cd(curdir2) % back to the original directory


time=CurrentStep.time;

% Transform into dimensional units before outputting
SecYear = 3600*24*365.25;
Location.time_years             =   Location.time*CHAR.Time/SecYear;                    % in  years
Location.x                      =   Location.x*CHAR.Length;                             % in m
Location.z                      =   Location.z*CHAR.Length;                             % in m
Location.Temperature_Celcius    =   Location.Temperature*CHAR.Temperature-273.15;       % in C

Location = rmfield(Location,'Temperature');     % to avoid confusion in which units this is
Location = rmfield(Location,'time_years');      % to avoid confusion in which units this is

if any(Location.Pressure<0)
    ind = find(Location.Pressure<0);
    Location.Pressure(ind) = 0;
end

if isfield(Location,'Pressure')
    Location.Pressure            =   Location.Pressure*CHAR.Stress;     % in Pa?? how to nondimensionalyz in MILAMIN_VEP2??          %%% Not know whether these nondimensionalyzes are correct
end

if isfield(Location,'Litho_Overpressure')
    Location.Litho_Overpressure = Location.Litho_Overpressure*CHAR.Stress;
end

if isfield(Location,'Lithostatic_Pressure')
    Location.Lithostatic_Pressure = Location.Lithostatic_Pressure*CHAR.Stress;
end

if isfield(Location,'T2nd')
    Location.T2nd                =   Location.T2nd*CHAR.Stress;         % in Pa
end
if isfield(Location,'E2nd')
    Location.E2nd                =   Location.E2nd*1/CHAR.Time;                    % 1/s
end











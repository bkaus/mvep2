function [ipval] = P2IP_LinearLeastSquares(plocalx,plocalz,pelem,pval,nel,iplocalx,iplocalz)

% Particle to integration point interpolation using bilinear least squares
% regression
% this uses local coordinates
%
% INPUT
% plocalx, plocalz : local coordinates of particles
% pelem: element where particle resides
% pval: value of parameter to be interpolated
% nel: number of elements
% iplocalx,iplocalz : local coordinates of integration points
%
% OUTPUT: 
% ipval: interpolated values at integration points
%
% Marcel Thielmann, 4,2013
% 10.01.2014 - vectorized the element loop and added more sophisticated
% clipping of visocsity values


global CorrectionType

if size(plocalx,2)>1
    plocalx = plocalx';
    plocalz = plocalz';
    pelem = pelem';
    pval = pval';
end



% TEST PARAMETERS:
if nargin == 0
    npart       = 6;
    [plocalx,plocalz]   = meshgrid( ([0.01:0.98/(npart-1):0.99]-0.5)*2  ); 
    plocalx             = plocalx(:);
    plocalz             = plocalz(:);
    pelem               = plocalx*0+1;
    % linear
    pval                = 2*plocalx + 3*plocalz + 4; % linear dependence of the particle values on the coordinates
    % jump 1-dimensional
    pval(plocalx<=1)               = 1;
    pval(plocalx>=0 & plocalz >=0) = 1e3;
    % jump 2-dimenisonal
    
    nel                 = 1;
    [iplocalx,iplocalz] = meshgrid([-0.577350269189626 0.577350269189626]);  
    iplocalx            = [iplocalx(:)]';
    iplocalz            = [iplocalz(:)]';
    
end


plocalx  = plocalx(:);
plocalz  = plocalz(:);
pelem    = pelem(:);   
pval     = pval(:);

if isempty(CorrectionType)
    CorrectionType = 'Full';
end


RelaxFactor = 0.01; % how much are the values allowed to over- and undershoot?
it_max      = 100; % maximum number of iterations for overshoot correction

np     = length(plocalx);
nip    = length(iplocalx);
% vectorized version
% for elements
a11            = accumarray([pelem ones(np,1)], plocalx.*plocalx);
a12            = accumarray([pelem ones(np,1)], plocalx.*plocalz);
a13            = accumarray([pelem ones(np,1)], plocalx);
a22            = accumarray([pelem ones(np,1)], plocalz.*plocalz);
a23            = accumarray([pelem ones(np,1)], plocalz);
a33            = accumarray([pelem ones(np,1)], ones(size(plocalz)));
r1             = accumarray([pelem ones(np,1)], plocalx.*pval);
r2             = accumarray([pelem ones(np,1)], plocalz.*pval);
r3             = accumarray([pelem ones(np,1)], pval);

% compute the determinant of the A matrix (rule of Sarrus)
detA = a11.*a22.*a33 + 2.*a12.*a13.*a23 - a13.^2.*a22 - a11.*a23.^2 - a12.^2.*a33;

% now compute the solution vector (Cramers rule)
c1   = ( (a22.*a33-a23.^2).*r1   + (a13.*a23-a12.*a33).*r2 + (a12.*a23-a13.*a22).*r3 )./detA;
c2   = ( (a23.*a13-a12.*a33).*r1 + (a11.*a33-a13.^2).*r2   + (a12.*a13-a11.*a23).*r3 )./detA;
c3   = ( (a12.*a23-a13.*a22).*r1 + (a12.*a13-a11.*a23).*r2 + (a11.*a22-a12.^2).*r3   )./detA;

% compute the values at the integration points
IPLOCALX = repmat(iplocalx',nel,1);
IPLOCALZ = repmat(iplocalz',nel,1);

C1          = repmat(c1,1,length(iplocalx));
C2          = repmat(c2,1,length(iplocalx));
C3          = repmat(c3,1,length(iplocalx));

ipval  = C1.*IPLOCALX + C2.*IPLOCALZ + C3;
   
% determine min and max values of particles in each element
MinValPart            = accumarray([pelem ones(np,1)], pval,[],@min);
MaxValPart            = accumarray([pelem ones(np,1)], pval,[],@max);

% determine min and max values of ipval in each element + the index
[MinVal,col_min] = min(ipval,[],2);
[MaxVal,col_max] = max(ipval,[],2);

% compute values @ nodes
NLOCALX = round(IPLOCALX);
NLOCALZ = round(IPLOCALZ);

nval    = C1.*NLOCALX + C2.*NLOCALZ + C3;

switch CorrectionType
    case 'Part'
        
        % determine min and max values of particles in each element
        MinValPart            = accumarray([pelem ones(np,1)], pval,[],@min);
        MaxValPart            = accumarray([pelem ones(np,1)], pval,[],@max);
        
        % determine min and max values of ipval in each element + the index
        [MinVal,col_min] = min(ipval,[],2);
        [MaxVal,col_max] = max(ipval,[],2);
        
        row              = [1:size(ipval,1)]';
        
        under = MinVal<MinValPart*0.9;
        over  = MaxVal>MaxValPart*1.01;
        
        shoots = (under | over); % this vector contains all the element indices that are overshooting
        
        
        if sum(shoots) > 0 % if there are any under or overshoots
            
            col_min = col_min(shoots);
            col_max = col_max(shoots);
            row     = row(shoots);
            
            c       = c3(shoots);
            
            ind_min = sub2ind(size(ipval),row,col_min);
            ind_max = sub2ind(size(ipval),row,col_max);
            
            XMin = IPLOCALX(ind_min);
            ZMin = IPLOCALZ(ind_min);
            
            XMax = IPLOCALX(ind_max);
            ZMax = IPLOCALZ(ind_max);
            
            % get the min and the max values and compute a slope from
            % those
            
            [MinVal,indmin] = max([MinVal(shoots),MinValPart(shoots)],[],2);
            [MaxVal,indmax] = min([MaxVal(shoots),MaxValPart(shoots)],[],2);
            
            f1  = (MaxVal-c) - ( (MaxVal-MinVal)./(XMax-XMin) ).*XMax;
            f2  = (ZMax-ZMin)./(XMax-XMin) .*XMax + ZMax;
            
            c2  = f1./f2;
            c1  = ( (MaxVal-MinVal)- c2.*(ZMax-ZMin) )./(XMax-XMin);
            
            C1          = repmat(c1,1,length(iplocalx));
            C2          = repmat(c2,1,length(iplocalx));
            C3          = repmat(c,1,length(iplocalx));
            
            IPLOCALX = repmat(iplocalx',sum(shoots),1);
            IPLOCALZ = repmat(iplocalz',sum(shoots),1);
            
            ipval_corr  = C1.*IPLOCALX + C2.*IPLOCALZ + C3;
            
            ipval(shoots,:) = ipval_corr;
        end
    case 'Full'
        
        MaxValPart = repmat(MaxValPart,1,length(iplocalx));
        MinValPart = repmat(MinValPart,1,length(iplocalx));
        
        [RowMax,ColMax] = find( (nval-MaxValPart)./abs(MaxValPart) > RelaxFactor); % relax the upper limit a bit
        [RowMin,ColMin] = find( (nval-MinValPart)./abs(MinValPart) < -RelaxFactor); % relax the lower limit a bit
        RowShoot        = unique([RowMax;RowMin]);
                
        MaxValShoot     = MaxValPart(RowShoot,:);
        MinValShoot     = MinValPart(RowShoot,:);
        
        nlocalShootX    = NLOCALX(RowShoot,:);
        nlocalShootZ    = NLOCALZ(RowShoot,:);
        
        it = 0;
        while ~isempty(RowShoot) % iteratively correct C1, C2 and C3 in the over- and undershooting elements
            it = it+1;
            nvalShoot       = nval(RowShoot,:); % only consider elements that are overshooting
            nvalShoot(nvalShoot>MaxValShoot) = MaxValShoot(nvalShoot>MaxValShoot);
            nvalShoot(nvalShoot<MinValShoot) = MinValShoot(nvalShoot<MinValShoot);
            
            % compute the new coefficients (makes use of the IP coordinate symmetry)
            C1_ou = sum(nvalShoot.*nlocalShootX,2)./sum(nlocalShootX.*nlocalShootX,2);
            C2_ou = sum(nvalShoot.*nlocalShootZ,2)./sum(nlocalShootZ.*nlocalShootZ,2);
            C3_ou = sum(nvalShoot,2)./sum(ones(size(nvalShoot)),2);
            
            C1_ou          = repmat(C1_ou,1,size(nlocalShootX,2));
            C2_ou          = repmat(C2_ou,1,size(nlocalShootX,2));
            C3_ou          = repmat(C3_ou,1,size(nlocalShootX,2));
            
            nvalShoot      = C1_ou.*nlocalShootX + C2_ou.*nlocalShootZ + C3_ou;
            
            nval(RowShoot,:) = nvalShoot;
            C1(RowShoot,:) = C1_ou;
            C2(RowShoot,:) = C2_ou;
            C3(RowShoot,:) = C3_ou;
            
            
            [RowMax,ColMax] = find( (nval-MaxValPart)./abs(MaxValPart) > RelaxFactor); % relax the upper limit a bit
            [RowMin,ColMin] = find( (nval-MinValPart)./abs(MinValPart) < -RelaxFactor); % relax the lower limit a bit
            RowShoot        = unique([RowMax;RowMin]);
            
            MaxValShoot     = MaxValPart(RowShoot,:);
            MinValShoot     = MinValPart(RowShoot,:);
            
            nlocalShootX    = NLOCALX(RowShoot,:);
            nlocalShootZ    = NLOCALZ(RowShoot,:);
            
            if it>it_max
               break; % stop the iterations if a maximum number of iterations is reached 
            end

        end
        
        % now recompute the ip values in the over- and undershooting elements
        ipval  = C1.*IPLOCALX + C2.*IPLOCALZ + C3;
        
        
        
    case 'nonnegative'
        
    case 'none'
        
end



function [UV] = local_coordinates(MESH,points,point_elems)
% A function, used for testing MUTILS, that computes local element
% coordinates of randomly placed markers in an unstructured triangular mesh.
% It is only needed if MUTILS is not installed on the system, and we run MVEP2
% with matlab-only routines.
% In other cases, the einterp MEX function (Part of MUTILS) computes the local
% coordinates internally.
ndim = size(MESH.NODES, 1);
nel  = length(MESH.ELEMS);

nnel = size(MESH.ELEMS,1);

if nnel==3 | nnel==7
    % we are dealing with triangular elements
    ENOD_X = reshape(MESH.NODES(1,MESH.ELEMS(1:3,:)), 3,nel);
    ENOD_Y = reshape(MESH.NODES(2,MESH.ELEMS(1:3,:)), 3,nel);
    
    area  = ENOD_X(2,:).*ENOD_Y(3,:) - ENOD_X(3,:).*ENOD_Y(2,:) + ...
        ENOD_X(3,:).*ENOD_Y(1,:) - ENOD_X(1,:).*ENOD_Y(3,:) + ...
        ENOD_X(1,:).*ENOD_Y(2,:) - ENOD_X(2,:).*ENOD_Y(1,:);
    
    ENOD_X_LONG = ENOD_X(:,point_elems);
    ENOD_Y_LONG = ENOD_Y(:,point_elems);
    
    eta = zeros(ndim,length(points));
    eta(1,:)  = ENOD_X_LONG(2,:).*ENOD_Y_LONG(3,:) - ENOD_X_LONG(3,:).*ENOD_Y_LONG(2,:) + ...
        ENOD_X_LONG(3,:).*points(2,:) - points(1,:).*ENOD_Y_LONG(3,:) + ...
        points(1,:).*ENOD_Y_LONG(2,:) - ENOD_X_LONG(2,:).*points(2,:);
    
    eta(2,:)  = ENOD_X_LONG(3,:).*ENOD_Y_LONG(1,:) - ENOD_X_LONG(1,:).*ENOD_Y_LONG(3,:) + ...
        ENOD_X_LONG(1,:).*points(2,:) - points(1,:).*ENOD_Y_LONG(1,:) + ...
        points(1,:).*ENOD_Y_LONG(3,:) - ENOD_X_LONG(3,:).*points(2,:);
    
    area_long = area(point_elems);
    
    eta(1,:) = eta(1,:)./area_long;
    eta(2,:) = eta(2,:)./area_long;
    
    eta1 = eta(1,:);
    eta2 = eta(2,:);
    eta3 = 1-eta1-eta2;
    UV = [eta2; eta3];      % local coordinates as required in MVEP2
    
elseif nnel==4
    % we have quad elements
    %
    % note: this routine is copied from einterp_quad.c and
    % modified/vectorized for matlab
    
    % we are dealing with triangular elements
    ENOD_X  =   reshape(MESH.NODES(1,MESH.ELEMS(1:4,:)), 4,nel);
    ENOD_Y  =   reshape(MESH.NODES(2,MESH.ELEMS(1:4,:)), 4,nel);
    
    
    elem    =   point_elems;
    
    a_x     =   ENOD_X(1,elem);
    b_x     =   ENOD_X(2,elem);
    c_x     =   ENOD_X(3,elem);
    d_x     =   ENOD_X(4,elem);
    A       =   0.25*((b_x-a_x)+(c_x-d_x));
    B       =   0.25*((c_x-a_x)+(d_x-b_x));
    C       =   0.25*((a_x-b_x)+(c_x-d_x));
    D       =   0.25*((a_x+b_x)+(c_x+d_x)) - points(1,:);
    
    a_y     =   ENOD_Y(1,elem);
    b_y     =   ENOD_Y(2,elem);
    c_y     =   ENOD_Y(3,elem);
    d_y     =   ENOD_Y(4,elem);
    E       =   0.25*((b_y-a_y)+(c_y-d_y));
    F       =   0.25*((c_y-a_y)+(d_y-b_y));
    G       =   0.25*((a_y-b_y)+(c_y-d_y));
    H       =   0.25*((a_y+b_y)+(c_y+d_y)) - points(2,:);
    
    resx    =   a_x-c_x;
    resy    =   a_y-c_y;
    u       =   resx.*resx + resy.*resy;
    
    resx    =   b_x-d_x;
    resy    =   b_y-d_y;
    v       =   resx.*resx + resy.*resy;
    
    res_init        =   v;
    ind             =   find(u>v);
    res_init(ind)   =   u(ind);
    
    u       =   zeros(size(v));
    res     =   1;
    while (max(abs(res))>1e-12)
        K11   = A+C.*v;
        K12   = B+C.*u;
        K21   = E+G.*v;
        K22   = F+G.*u;
        detK  = 1.0./(K11.*K22 - K12.*K21);
        
        resx  = ((A.*u+B.*v)+C.*u.*v)+D;
        resy  = ((E.*u+F.*v)+G.*u.*v)+H;
        res   = (resx.*resx + resy.*resy).*res_init;
        
        u     = u + (-K22.*resx + K12.*resy).*detK;
        v     = v + ( K21.*resx - K11.*resy).*detK;
    end
    
    UV= [u; v];      % local coordinates as required in MVEP2
    
    
    
    
    
end

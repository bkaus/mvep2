function [Rho_Solid, Rho_Liquid, PhaseDiagramData] = ComputeDensity_PhaseTransitions(P,T, PhaseDiagramData, NUMERICS)
% Loads a phase diagram and interpolates density from that.

% $Id$

%% (1) Load phase diagram
load(PhaseDiagramData.Name)

%% (2) Load the correct density field to be interpolated
% If "Melt" (=melt fraction") exists as a variable, you should load
% "rho_withoutMelt", as the effective density that will be used in the code
% will later on depend on how much melt was extracted etc.
if exist('rho_withoutMelt','var')
    Density_PhaseDiagram = rho_withoutMelt;
    
    if ~exist('rho_melt','var')
        error('if you specify rho_withoutMelt, you should also specify the rho_melt variable')
    end
    
else
    Density_PhaseDiagram = rho;
    
    if exist('Melt','var')
        % if "Melt" exist as a variable (='Melt fraction') but
        % "rho_withoutMelt" not, give an error message
        error(['If you specify "Melt", you should also specify "rho_withoutMelt" and "rho_melt" in PhaseDiagram: ',PhaseDiagramData.Name])
    end
    
end

if exist('rho_melt','var')
    Density_Liquid = rho_melt;
else
    Density_Liquid = NaN;
end

% Storing the WS gives wrong results in some cases, so better not to
% WS = [];
 
if ~isfield(PhaseDiagramData,'WS')
    WS = [];
else
    WS = PhaseDiagramData.WS;
end

%% Correct data points that are outside the range of the diagram
id = find(P < min(P_Pa));
if ~isempty(id)
%    warning([num2str(length(id)),' INTPS have a pressure that is lower than the min(P) of the phase diagram'])
    P(id) = min(P_Pa);
end
id = find(P > max(P_Pa));
if ~isempty(id)
%     warning([num2str(length(?id)),' INTPS have a pressure that is larger than the max(P) of the phase diagram'])
    P(id) = max(P_Pa);
end

id = find(T < min(T_K) );
if ~isempty(id)
%     warning([num2str(length?(id)),' INTPS have a temperature that is smaller than the min(T) of the phase diagram'])
    T(id) = min(T_K);
end
id = find(T > max(T_K) );
if ~isempty(id)
%     warning([num2str(length(id)),' INTPS have a temperature that is larger than the max(T) of the phase diagram'])
    T(id) = max(T_K);
end



%% (2) Use MUTILS to interpolate densities
[rho_interp, WS]        =   Interpolate_WithinPhaseDiagram(P_Pa,T_K, Density_PhaseDiagram, [P'; T'], WS, NUMERICS);
PhaseDiagramData.WS     =   WS;  % speeds up future interpolations
Rho_Solid               =   rho_interp;


%% (3) Deal with partial melting and compute density of liquid
if length(Density_Liquid(:))>1
    [rho_interp, WS]        =   Interpolate_WithinPhaseDiagram(P_Pa,T_K, Density_Liquid, [P'; T'], WS, NUMERICS);
    PhaseDiagramData.WS     =   WS;  % speeds up future interpolations
    Rho_Liquid              =   rho_interp;
else
    Rho_Liquid              =   ones(size(Rho_Solid))*Density_Liquid;
end


end



%--------------------------------------------------------------------------
function [V_MARKERS, WS] = Interpolate_WithinPhaseDiagram(P_Pa,T_K, rho, MARKERS, WS, NUMERICS);
% This routine uses MUTILS functionality to interpolate values from a phase
% diagram to markers or integration points
%
% MUTILS OPTIONS
opts = NUMERICS.mutils;

if isempty(WS)
    % Create a workspace that will later be stored and contains all phase
    % diagram information
    
    [x2d, y2d]      =   meshgrid(P_Pa, T_K);
    
    % Create delaunay triangulation (only required the first time for a particular density diagram)
    WS.MESH.NODES       =   [x2d(:)'; y2d(:)'];
    DT                  =   DelaunayTri(WS.MESH.NODES');
    trep                =   TriRep(DT.Triangulation, WS.MESH.NODES(1,:)', WS.MESH.NODES(2,:)');
    WS.MESH.ELEMS       =   uint32(DT.Triangulation');
    
    % Create Delaunay triangulation for particles outside domain
    WS.POINTS_boundary  =   [x2d(:,1), y2d(:,1); x2d(:,end), y2d(:,end); x2d(1,2:end-1)', y2d(1,2:end-1)'; x2d(end,2:end-1)', y2d(end,2:end-1)'];
    WS.DT_Bound         =   DelaunayTri(WS.POINTS_boundary);

end

%% ADD VALUES
WS.V            =   rho(:)';      % workspace
WS.V_Boundary   =   [rho(:,1); rho(:,end); rho(1,2:end-1)'; rho(end,2:end-1)'];


% find the enclosing triangle
[T, WS, stats]          =   tsearch2(WS.MESH.NODES, WS.MESH.ELEMS, MARKERS,WS, uint32([]), opts);
WS.T                    =   T;


% Interpolate properties of particles that are within the computational domain.
ind                     = find(T>0);
V_MARKERS               = zeros(1,length(MARKERS));
if ~isempty(ind)
    [V_MARKERS(ind)] 	= einterp_MVEP2(WS.MESH, WS.V, MARKERS(:,ind), T(ind), opts);
end

% Interpolate particles that are outside the computational domain
ind                     = find(T==0);
if ~isempty(T)
    PI                  = dsearchn(WS.POINTS_boundary,WS.DT_Bound.Triangulation , MARKERS(:,ind)');
    V_MARKERS(ind)   	= WS.V_Boundary(PI);
end



end

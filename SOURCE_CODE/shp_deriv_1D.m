function  [N, dNdu] = shp_deriv_1D(ipx, nnod)
% Shape functions  and derivatives for 1D elements

% $Id$

switch nnod;

    case 2;
        for i=1:size(ipx,1)
            eta1        = ipx(i);
            SHP(1,1)    = 1/2*(1-eta1);
            SHP(1,2)    = 1/2*(1+eta1);

            DERIV(1,1)  =   -1/2;   %w.r.t. eta1
            DERIV(1,2)  =    1/2;   %w.r.t. eta1
            
            dNdu{i}     =   DERIV;
            N{i}        =   SHP;
        end

    case 3;

        for i=1:size(ipx,1)
            eta1    = ipx(i,1);
           
            % shape functions
            SHP      = zeros(1,3);
            SHP(1,1) = 0.5*eta1*(eta1	- 1      );
            SHP(1,2) =          (1    - eta1^2 );
            SHP(1,3) = 0.5*eta1*(eta1	+ 1      );
            

            % derivative of shape function w.r.t. natural coordinates
            DERIV(1,1) =    eta1 - 0.5;
            DERIV(1,2) = -2*eta1;
            DERIV(1,3) =    eta1 + 0.5;
            
            dNdu{i} = DERIV;
            N{i}    = SHP;
        end

end

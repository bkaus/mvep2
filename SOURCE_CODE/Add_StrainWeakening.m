function [MATERIAL_PROPS] = Add_StrainWeakening(varargin)
% Add_StrainWeakening adds strain weakening to either a 'viscous' or a
% 'plastic' rheology.
%
% Syntax:
%   [MATERIAL_PROPS] = Add_StrainWeakening(MATERIAL_PROPS, Phase, Rheology, WeakeningType, [various argument])
%
%       Input:
%               MATERIAL_PROPS  -   Structure containing material properties
%                                   for the various phases.
%               Phase           -   The number of the phase to which you want
%                                   to add these creep law parameters.
%               Rheology        -   Either 'viscous' or 'plastic'
%               WeakeningType   -   The type of weakening being applied.
%
%                   Currently implemented:
%                       'Linear'        -   Linear Strain weakening
%
%               Prefactor_End_Cohesion  -    what is the prefactor of cohesion
%                                               after weakening?
%               Prefactor_End_Friction  -    what is the prefactor of friction
%                                               angle after weakening?
%
%
%       Output:
%               MATERIAL_PROPS  -   Updated material properties structure
%
% $Id: Add_DislocationCreep.m 5007 2013-11-14 21:08:41Z lkausb $

MATERIAL_PROPS  =   varargin{1};
PhaseNumber     =   varargin{2};
Rheology        =   varargin{3};
WeakeningType   =   varargin{4};

% checking input parameters
if (~strcmp(Rheology,'Viscosity') & ~strcmp(Rheology,'Plasticity'))
    error(['Unknown rheology ',Rheology,' to which you want to apply strain weakening; Choose Viscosity or Plasticity'])
end

switch WeakeningType
    case 'Linear'
        % We linearly weaken both cohesion and friction angle, starting at
        % strain Strain_Start and finishing at strain Strain_End.
        
        % In case of DruckerPrager Plastic rheology, both Cohesion and
        %       Friction angle can be weakened.
        % In case of a viscous rheology we weaken all phases by a constant
        % factor
        
        % Beginning & end of strain weakening
        WeakeningStructure.Strain_Start = varargin{5};  % strain at which you start weakening
        WeakeningStructure.Strain_End   = varargin{6};  % strain at which you finish weakening and have a fully weakened phase
        
        switch Rheology
            case 'Plasticity'
                WeakeningStructure.Prefactor_End_Cohesion       = varargin{7};
                WeakeningStructure.Prefactor_End_FrictionAngle  = varargin{8};
            otherwise
                WeakeningStructure.Prefactor_End = varargin{7};
        end
    
    case 'Nonlinear_Rey2014'
        % Nonlinear weakening according to Rey et al. (2014) Nature.
        %
        %  eta = eta_ref*(1-(1-xi_end)*(e_p/strain)^0.25) for e_p<strain
        % 
        %  eta = eta_ref*xi_end   for e_p>strain
        %
        % with xi_end  = the weakening at the end 
        %       strain = strain after which weakeing finished  (Strain_End)
        
        
        % Beginning & end of strain weakening
        WeakeningStructure.Strain_Start = varargin{5}*0;  % strain at which you start weakening  [actually always zero in this case]
        WeakeningStructure.Strain_End   = varargin{6};  % strain at which you finish weakening and have a fully weakened phase [strain above]
        
        switch Rheology
            case 'Plasticity'
                WeakeningStructure.Prefactor_End_Cohesion       = varargin{7};
                WeakeningStructure.Prefactor_End_FrictionAngle  = varargin{8};
            otherwise
                WeakeningStructure.Prefactor_End = varargin{7};
        end
        
        
    otherwise
        error('Unknown strain weakening law')
end





% Add to structure
MATERIAL_PROPS(PhaseNumber).(Rheology).StrainWeakening.(WeakeningType) = WeakeningStructure;


end



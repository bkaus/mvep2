function V_percolation = ComputeWaterPercolation_SimlifiedDarcy(INTP_PROPS,WaterPercolationLaw,CHAR,NUMERICS)
% compute permeability using simplified darcy equation that assuming fluid
% pressure equals solid pressure, here Percolation is defined:
% V_percolation = K/eta_f/Phi

% INPUT:

% OUTPUT:

% ComputeWaterPermeability_SimlifiedDarcy.m 2015-06-23 $ljeff

V_percolation           =   ones(size(INTP_PROPS.X));
Type                    =   fieldnames(WaterPercolationLaw);

if size(Type,1) > 1
    error('You can currently only have one water permeability type activated simultaneously');% maybe more??
end

switch Type{1}
    case 'Constant'
        V_percolation       =   V_percolation*WaterPercolationLaw.Constant.V_percolation;
        
    case 'MeltDependent'
        % taking the equation from Faccenda et al.,2009, Nature
        % K = K0 (Phi/Phi0)^n 
        K0              =   WaterPercolationLaw.MeltDependent.K0;
        eta_f           =   WaterPercolationLaw.MeltDependent.eta_f;
        Phi0            =   WaterPercolationLaw.MeltDependent.Phi0;
        n               =   WaterPercolationLaw.MeltDependent.n;
        Phi             =   INTP_PROPS.MeltFraction_yjf;
        K               =   K0.*(Phi./Phi0).^n;
        ind             =   find(Phi==0);V_percolation(ind) = WaterPercolationLaw.MeltDependent.V_percolation_default;
        ind             =   find(Phi~=0);V_percolation(ind) = K(ind)./Phi(ind)/eta_f;
        V_percolation   =   reshape(V_percolation,size(INTP_PROPS.X));
    case 'MeltGrainsizeDependent'
        % K = phi^n*d^2/C where phi is melt fraction, d is grain size, C is
        % related Geometry
        Phi             =   INTP_PROPS.MeltFraction_yjf;
        d               =   WaterPercolationLaw.MeltGrainsizeDependent.d;
        C               =   WaterPercolationLaw.MeltGrainsizeDependent.C;
        eta_f           =   WaterPercolationLaw.MeltGrainsizeDependent.eta_f;
        n               =   WaterPercolationLaw.MeltGrainsizeDependent.n;
        K               =   Phi.^n.*d.^2./C;
        ind             =   find(Phi==0);V_percolation(ind) = WaterPercolationLaw.MeltGrainsizeDependent.V_percolation_default;
        ind             =   find(Phi~=0);V_percolation(ind) = K(ind)./Phi(ind)/eta_f;
        V_percolation   =   reshape(V_percolation,size(INTP_PROPS.X));
    otherwise
        error('Unkown water permeability law')
end
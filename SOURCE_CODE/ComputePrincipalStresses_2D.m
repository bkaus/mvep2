function [S1, S3, Angle] = ComputePrincipalStresses_2D(INTP_PROPS)
% Computes principal stresses from given stress values
%
% Use PlotDifferentialStresses_2D to visualize the stresses!
%
% Boris Kaus
%
% $Id$

MaxDiff = ones(size(INTP_PROPS.Stress.Txx))*NaN;
MinDiff = ones(size(INTP_PROPS.Stress.Txx))*NaN;
Angle   = ones(size(INTP_PROPS.Stress.Txx))*NaN;
S1      = ones(size(INTP_PROPS.Stress.Txx))*NaN;         % Sigma 1, 
S3      = ones(size(INTP_PROPS.Stress.Txx))*NaN;         % Sigma 2,

for i=1:size(INTP_PROPS.Stress.Txx,1)
    for j=1:size(INTP_PROPS.Stress.Txx,2)
        
        if ~isnan(INTP_PROPS.Stress.Txx(i,j))
            original_stress      = [INTP_PROPS.Stress.Txx(i,j), INTP_PROPS.Stress.Txy(i,j); INTP_PROPS.Stress.Txy(i,j) INTP_PROPS.Stress.Tyy(i,j)];

            % Eignevalue
            [Vectors, Principal] = eig (original_stress);                       % Eigenvalue
            principal_stress     = Principal;                                   % Principal stresses
            Value                = Vectors (2,1)/Vectors (1,1);                 % Slope of eigenvector is a ratio of its (y/x)-components
            RadianOrientation    = atan(Value);
            PrincipalOrientation = RadianOrientation * (180/pi) ;               % Principal stress orientation in degrees

            Angle(i,j)      = RadianOrientation;
            Angle_grad(i,j) = PrincipalOrientation;


%            S1 = (INTP_PROPS.Stress.Txx(i,j) + INTP_PROPS.Stress.Tyy(i,j))/2 + sqrt( ((INTP_PROPS.Stress.Txx(i,j)-INTP_PROPS.Stress.Tyy(i,j))/2)^2 + INTP_PROPS.Stress.Txy(i,j)^2 );
%            S2 = (INTP_PROPS.Stress.Txx(i,j) + INTP_PROPS.Stress.Tyy(i,j))/2 - sqrt( ((INTP_PROPS.Stress.Txx(i,j)-INTP_PROPS.Stress.Tyy(i,j))/2)^2 + INTP_PROPS.Stress.Txy(i,j)^2 );

            MeanStress(i,j) = (Principal(1,1) + Principal(2,2))/2;
            MaxDiff(i,j)    =   Principal(1,1)-MeanStress(i,j);
            MinDiff(i,j)    =   Principal(2,2)-MeanStress(i,j);
            S1(i,j)         =   Principal(1,1);
            S3(i,j)         =   Principal(2,2);
            
        end

    end
end

function INTP_PROPS =   UpdateStressesStrainsAfterTimestep(INTP_PROPS, dt);
% Updates stresses and strains at the end of a timestep
%

% $Id$


% Update stresses
INTP_PROPS.Stress.Txx       =   INTP_PROPS.Stress.Txx_new;      
INTP_PROPS.Stress           =   rmfield(INTP_PROPS.Stress,'Txx_new');
INTP_PROPS.Stress.Tyy       =   INTP_PROPS.Stress.Tyy_new;      
INTP_PROPS.Stress           =   rmfield(INTP_PROPS.Stress,'Tyy_new');
INTP_PROPS.Stress.Txy       =   INTP_PROPS.Stress.Txy_new;   	
INTP_PROPS.Stress           =   rmfield(INTP_PROPS.Stress,'Txy_new');
INTP_PROPS.Stress.T2nd    	=   INTP_PROPS.Stress.T2nd_new;     
% INTP_PROPS.Stress           =   rmfield(INTP_PROPS.Stress,'T2nd_new');

% Update properties at the end of the timestep
if ~isfield(INTP_PROPS,'Strain')
    INTP_PROPS.Strain = zeros(size(INTP_PROPS.Strainrate.E2nd));
end
% INTP_PROPS.Strain   =   INTP_PROPS.Strain + dt*INTP_PROPS.Strainrate.E2nd_pl;

% if isfield(INTP_PROPS.Strainrate,'E2nd_nonlocal')
%     if ~isfield(INTP_PROPS,'StrainNonLocal')
%          INTP_PROPS.StrainNonLocal = zeros(size(INTP_PROPS.Strainrate.E2nd));
%     end
%     INTP_PROPS.StrainNonLocal = INTP_PROPS.StrainNonLocal + INTP_PROPS.Strainrate.E2nd_nonlocal*dt;
%     INTP_PROPS.Strain   =  INTP_PROPS.StrainNonLocal;
% end
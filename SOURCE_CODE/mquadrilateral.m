function [MESH] = mquadrilateral(opts, mesh_input)
%
% Creates a regular quadrilateral mesh with optional a bottom or a top
% topography and [optional] irregular spacing.

% $Id$


nx       =   length(mesh_input.x_vec);
nz       =   length(mesh_input.z_vec);
switch opts.element_type.velocity
    case 'Q1'
        nel_x    =   (nx-1);
        nel_z    =   (nz-1);
        nnodel   =   4;
        nip      =   4;     % assume gauss quadrature
    case {'Q2'}
        nel_x    =   (nx-1)/2;
        nel_z    =   (nz-1)/2;
        nnodel   =   9;
        nip      =   9;
    otherwise
        error('Unknown quadrilateral element type')
end

if mod(nel_x,1)~=0
    error('choose uneven points of nodes in x-direction!')
end
if mod(nel_z,1)~=0
    error('choose uneven points of nodes in z-direction!')
end

% Extract data
x_vec   =    mesh_input.x_vec;
z_vec   =    mesh_input.z_vec;
Bot_z   =    mesh_input.Bot_z;
Top_z   =    mesh_input.Top_z;

switch opts.element_type.velocity
    case 'Q2'
        % 9-node quad elements needs the center nodes to be in the middle for
        % our particle location algorithm to work properly
        x_vec(2:2:end-1) = (x_vec(3:2:end)+x_vec(1:2:end-2))/2;
        z_vec(2:2:end-1) = (z_vec(3:2:end)+z_vec(1:2:end-2))/2;
end

% Create regular mesh
[x2d,z2d]               =   meshgrid(x_vec, z_vec);

% initialize ELEM2NODE and GCOORD
ELEM2NODE = zeros(nnodel,nel_x*nel_z);
GCOORD    = zeros(2,size(x2d,1)*size(x2d,2));

% Scale to Top & Bottom topography
z_vec_scaled = (z_vec-z_vec(1))./(z_vec(end)-z_vec(1));    % scale from 0..1
for iz=1:length(z_vec)
    z2d(iz,:) = z_vec_scaled(iz).*(Top_z-Bot_z) + Bot_z;    % deform grid based on top and bottom topography
end


%create numbering system
number                          =   zeros(size(x2d));

number(find(number==number))    =   find(number'==number');
%nip     =    nnodel;        % typically the case in 2D
nip_1D  =    sqrt(nip);

numberIntp                                  =   zeros(nel_z*nip_1D,  nel_x*nip_1D);
numberIntp(find(numberIntp==numberIntp))    =   find(numberIntp'==numberIntp');

% Set Pointid [used to identify boundaries while setting BCs]
Point_id                        =   zeros(size(number));
Point_id(:  ,1  )               =   4;
Point_id(:  ,end)               =   2;
Point_id(1  ,:  )               =   1;
Point_id(end,:  )               =   3;

Point_id                         =   Point_id(:)';
Point_id(find(isnan(number(:)))) = [];



% Number all elements
iel                             =   1;
RegularElementNumber            =   zeros(nel_z,nel_x);
Intp_Numbering                  =   zeros(nel_z*nel_x,nip);
Intp_Numbering1                 =   zeros(nel_z*nel_x,nip);


switch opts.element_type.velocity
    case 'Q1'
        
        for ielx=1:nel_z
            
            % Vectorized computation of integration points --------------------
            ielz_vec = 1:nel_x;
            
            iix     =  2*(ielx-1)+1;
            iiz     =  2*(ielz_vec-1)+1;
            el_num_vec = [1:length(iiz)]+(iix-1)*nel_z;
            
            % Integration point numbering ----------
            ind_x   =   [iix*ones(size(iiz));     (iix*ones(size(iiz))+1);    iix*ones(size(iiz));    (iix*ones(size(iiz))+1)  ];
            ind_z   =   [iiz;     iiz ;     iiz+1  ; iiz+1  ];
            
            %         ind     =   sub2ind([nel_z*nip_1D,  nel_x*nip_1D],ind_x,ind_z);
            
            ind    = ind_x + (ind_z - 1).*(nel_z*nip_1D);
            
            Intp_Numbering(el_num_vec,:) = numberIntp(ind)';
            %------------------------------------------------------------------
            
            
            
            % vectorize grid generation for quadrilateral elements-------------
            %
            % Quadrilateral, 4-node element
            ix      = ielx;
            iz      = [1:nel_x]';
            
            ind_x   = [ix*ones(size(iz)) ix*ones(size(iz)) ix*ones(size(iz))+1 ix*ones(size(iz))+1  ];
            ind_z   = [iz iz+1   iz+1 iz];
            
            
            %         ind     = sub2ind(size(number),ind_x,ind_z);
            ind                             =    ind_x + (ind_z - 1).*(nz);  % faster..
            
            
            iel_vec                         =   iel:iel+length(iz)-1;
            ELEM2NODE(:,iel_vec)            =   number(ind');
            iel                             =   iel+length(iz);
            
            RegularElementNumber(ielx, :)   =   iel_vec;
            %------------------------------------------------------------------
        end
        
        
        
        
        
    case {'Q2'}
        
        el_num = 0;
        for ielx=1:nel_z
            
            for ielz=1:nel_x
                
                el_num = el_num+1;
                
                % Element numbering --------------------
                ix      = 2*(ielx-1)+1;
                iz      = 2*(ielz-1)+1;
                
                % Quad element
                ind_z   = [iz iz+2  iz+2  iz    iz+1 iz+2 iz+1 iz   iz+1];
                ind_x   = [ix ix    ix+2  ix+2  ix   ix+1 ix+2 ix+1 ix+1];
                
                %                 ind1                 =   sub2ind(size(number),ind_x,ind_z);
                ind = ind_x + (ind_z - 1).*(nz);  % faster..
                
                
                
                iel_vec             =   iel:iel+length(iz)-1;
                ELEM2NODE(:,iel)    =   number(ind);
                RegularElementNumber(ielx, ielz) = iel;
                iel                 =   iel+1;
                
                
                % Integration point numbering ----------
                iix     =  3*(ielx-1)+1;
                iiz     =  3*(ielz-1)+1;
                
                
                ind_x   =   [iix     iix+1    iix+2  iix    iix+1   iix+2 iix   iix+1   iix+2 ];
                ind_z   =   [iiz     iiz      iiz    iiz+1  iiz+1   iiz+1 iiz+2 iiz+2   iiz+2 ];
                %                 ind1     =   sub2ind(size(numberIntp),ind_x,ind_z);
                ind = ind_x + (ind_z - 1).*(nel_z*nip_1D);  % faster..
                
                
                Intp_Numbering(el_num,:) = numberIntp(ind);
                
                
            end
            
            
        end
        
        
        
        
        %     if nnodel==9
        %         %         ix = 2*(ielx-1)+1;
        %         %         iz = 2*(ielz-1)+1;
        %
        %         iz      = 2*([1:nel_x]'-1)+1;
        %
        %         ix      = ones(size(iz))*ielx;
        %
        %         % Quad element
        %         ind_z = [iz iz+2  iz+2  iz    iz+1 iz+2 iz+1 iz   iz+1];
        %         ind_x = [ix ix    ix+2  ix+2  ix   ix+1 ix+2 ix+1 ix+1];
        %
        %         ind     = sub2ind(size(number),ind_x,ind_z);
        %         iel_vec = iel:iel+length(iz)-1;
        %         ELEM2NODE(:,iel_vec) = number(ind');
        %         iel=iel+length(iz);
        %
        %     end
        
    otherwise
        error('unknown number of nodes!')
end


% Create ELEM2NODE for the linear Macroelement
NODES(1,number(:)) = x2d(:);
NODES(2,number(:)) = z2d(:);

nel                 =   size(ELEM2NODE,2);
Phases              =   ones(1,nel);
PHASE_POINTS        =   [ ];
RegularGridNumber   =   number;

RegularIntegrationPoints.Intp_Numbering = Intp_Numbering;
RegularIntegrationPoints.numberIntp     = numberIntp;


% Compute element neighbors (useful for later sorting)
NeighborNumbers_2D                  =   zeros( [size(RegularElementNumber) ] +2,'uint32');
NeighborNumbers_2D(2:end-1,2:end-1) =   uint32(RegularElementNumber);
NEIGHBORS                           =   zeros(8,nel,'uint32');
ind                                 =   NeighborNumbers_2D(2:end-1,2:end-1);
ind                                 =   ind(:);

data                                =   NeighborNumbers_2D(1:end-2,1:end-2);
NEIGHBORS(1,ind)                    =   data(:);
data                                =   NeighborNumbers_2D(1:end-2,2:end-1);
NEIGHBORS(2,ind)                    =   data(:);
data                                =   NeighborNumbers_2D(1:end-2,3:end  );
NEIGHBORS(3,ind)                    =   data(:);
data                                =   NeighborNumbers_2D(2:end-1,3:end  );
NEIGHBORS(4,ind)                    =   data(:);
data                                =   NeighborNumbers_2D(3:end  ,3:end  );
NEIGHBORS(5,ind)                    =   data(:);
data                                =   NeighborNumbers_2D(3:end  ,2:end-1);
NEIGHBORS(6,ind)                    =   data(:);
data                                =   NeighborNumbers_2D(3:end  ,1:end-2);
NEIGHBORS(7,ind)                    =   data(:);
data                                =   NeighborNumbers_2D(2:end-1,1:end-2);
NEIGHBORS(8,ind)                    =   data(:);


% Store DATA in appropriate structure
MESH.NODES                          =   NODES;
MESH.ELEMS                          =   uint32(ELEM2NODE);
MESH.node_markers                   =   Point_id;
MESH.RegularGridNumber              =   number;
MESH.RegularIntegrationPoints       =   RegularIntegrationPoints;
MESH.RegularElementNumber           =   RegularElementNumber;
MESH.nip                            =   nip;
MESH.Point_id                       =   Point_id;
MESH.NEIGHBORS                      =   NEIGHBORS;


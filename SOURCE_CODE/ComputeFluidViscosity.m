function  [ Mu_f ]	= 	ComputeFluidViscosity(INTP_PROPS,FluidViscosity,CHAR)
% ComputeFluidViscosity -  updates the fluid viscosity

% $Id: ComputeHeatCapacity.m 4919 2013-11-07 19:22:05Z lkausb $


Type        =   fieldnames(FluidViscosity);
if size(Type,1) > 1
    error('You can currently only have one FluidViscosity type activated simultaneously')
end

switch Type{1}
    case 'Constant'
        Mu_f 	=       ones(size(INTP_PROPS.X)).*FluidViscosity.Constant.Mu;
        
    otherwise
        error('Unknown FluidViscosity law')
end

% non-dimensionalise fluid viscosity
Mu_f  =  Mu_f./CHAR.Viscosity;

end

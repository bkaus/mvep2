function PARTICLES = ZeroAllParticleFields(PARTICLES)
% Sets all fields in the PARTICLES structure to zero
% including HistVar and CompVar
%
% Syntax:
%
%  PARTICLES = ZeroAllParticleFields(PARTICLES)
%
%

%$Id$


% Zero out main fields, except HistVar and CompVar
Var_names = fieldnames(PARTICLES);
for ifield = 1:length(Var_names);
    % Get data of this field on particles (element-wise)
    Part_data	=   getfield(PARTICLES,Var_names{ifield});
    if ~isstruct(Part_data)
        Part_data       =   Part_data*0;
        PARTICLES       =   setfield(PARTICLES,Var_names{ifield},         Part_data);
    end
end

% Clear HistVar
Var_names = fieldnames(PARTICLES.HistVar);
for ifield = 1:length(Var_names);
    % Get data of this field on particles (element-wise)
    Part_data	=   getfield(PARTICLES.HistVar,Var_names{ifield});
    if ~isstruct(Part_data)
        Part_data           =   Part_data*0;
        PARTICLES.HistVar  	=   setfield(PARTICLES.HistVar,Var_names{ifield},         Part_data);
    end
end

% Clear CompVar
Var_names = fieldnames(PARTICLES.CompVar);
for ifield = 1:length(Var_names);
    % Get data of this field on particles (element-wise)
    Part_data	=   getfield(PARTICLES.CompVar,Var_names{ifield});
    if ~isstruct(Part_data)
        Part_data           =   Part_data*0;
        PARTICLES.CompVar  	=   setfield(PARTICLES.CompVar,Var_names{ifield},         Part_data);
    end
end


% Clear MeltExtraction
if isfield(PARTICLES,'MeltExtraction')
    Var_names = fieldnames(PARTICLES.MeltExtraction);
    for ifield = 1:length(Var_names);
        % Get data of this field on particles (element-wise)
        Part_data	=   getfield(PARTICLES.MeltExtraction,Var_names{ifield});
        if ~isstruct(Part_data)
            Part_data                   =   Part_data*0;
            PARTICLES.MeltExtraction  	=   setfield(PARTICLES.MeltExtraction,Var_names{ifield},         Part_data);
        end
    end
end

% Clear Chemistry
if isfield(PARTICLES,'Chemistry')
    Var_names = fieldnames(PARTICLES.Chemistry);
    for ifield = 1:length(Var_names);
        
        
        if isfield(PARTICLES.Chemistry,'Liquid')
            Var_names1 = fieldnames(PARTICLES.Chemistry.Liquid);
            for ifield1 = 1:length(Var_names1);
        
                % Get data of this field on particles (element-wise)
                Part_data	=   getfield(PARTICLES.Chemistry.Liquid, Var_names1{ifield1});
                if ~isstruct(Part_data)
                    Part_data                   =   Part_data*0;
                    PARTICLES.Chemistry.Liquid  =   setfield(PARTICLES.Chemistry.Liquid, Var_names1{ifield1},         Part_data);
                end
            end
        end
        
        if isfield(PARTICLES.Chemistry,'Solid')
            Var_names2 = fieldnames(PARTICLES.Chemistry.Solid);
            for ifield2 = 1:length(Var_names2);
                  % Get data of this field on particles (element-wise)
                Part_data	=   getfield(PARTICLES.Chemistry.Solid, Var_names2{ifield2});
                if ~isstruct(Part_data)
                    Part_data                   =   Part_data*0;
                    PARTICLES.Chemistry.Solid  	=   setfield(PARTICLES.Chemistry.Solid, Var_names2{ifield2},         Part_data);
                end
            end
        end
            
        if isfield(PARTICLES.Chemistry,'Stable_Phases')
            Var_names3 = fieldnames(PARTICLES.Chemistry.Stable_Phases);
            for ifield3 = 1:length(Var_names3);
                  % Get data of this field on particles (element-wise)
                Part_data	=   getfield(PARTICLES.Chemistry.Stable_Phases, Var_names3{ifield3});
                if ~isstruct(Part_data)
                    Part_data                           =   Part_data*0;
                    PARTICLES.Chemistry.Stable_Phases  	=   setfield(PARTICLES.Chemistry.Stable_Phases, Var_names3{ifield3},         Part_data);
                end
            end
        end
        
        % Get data of this field on particles (element-wise)
        Part_data	=   getfield(PARTICLES.Chemistry, Var_names{ifield});
        if ~isstruct(Part_data)
            Part_data               =   Part_data*0;
            PARTICLES.Chemistry  	=   setfield(PARTICLES.Chemistry, Var_names{ifield},         Part_data);
        end
    end
end
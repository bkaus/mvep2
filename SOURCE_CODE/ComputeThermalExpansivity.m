function  [ alpha ]	= 	ComputeThermalExpansivity(INTP_PROPS, ThermalExpansivity,LatentHeat ,dMdP_Phase, CHAR, NUMERICS);
% ComputeHeatCapacity -  updates the heat capacity

% $Id$


Type        =   fieldnames(ThermalExpansivity);
if size(Type,1) > 1
    error('You can currently only have one HeatCapacity type activated simultaneously')
end

switch Type{1}
    case 'Constant'
        ThermalExpansivity 	=       ThermalExpansivity.Constant.alpha;
        
        alpha              =       ones(size(INTP_PROPS.X))*ThermalExpansivity;
    
     
        alpha=alpha+((dMdP_Phase.*LatentHeat.Constant.QL)./(INTP_PROPS.T*CHAR.Temperature))*CHAR.Temperature; 
   
   otherwise
        error('Unknown ThermalExpansivity  law')
end

end
%--------------------------------------------------------------------------

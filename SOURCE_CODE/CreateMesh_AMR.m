function [MESH] = CreateMesh_AMR(opts, mesh_input, MESH, PARTICLES, RefinementCriterium)
% Adaptive MESH refinement using triangular elements
%
% The original basic mesh is re-created when this routine is called and then
% refined. The previously used (and possibly already refined mesh) is *not*
% used as an input parameter (yet) and, hence, not iteratively refined.
%
%  [MESH] = CreateMesh_AMR(opts, mesh_input, MESH, PARTICLES, ParticlesThatFullfillRefinementCriterium)
%
% INPUT:
%       opts:                	usual option structure for meshes with:
%                            	opts.AMR.NumberRefinementSteps  =   [3,2,2,...Number of refinement levels]
%       mesh_input:             usual mesh input structure (boundary coordinates)
%       PARTICLES               PARTICLES object
%       RefinementCriterium     vector of size(PARTICLES.x) which has a number at
%                               locations where a PARTICLE fulfills one of
%                               the defined refinement criterias. You can
%                               define as much criterias as you want but they should be
%                               ordered from coarse to fine and have linear increasing 
%                               full numbers in the vector (e.g.: 
% 
                                        %     % refinemetn layer 1
                                        %     ind                         =   find(PARTICLES.phases == Lithosphere);
                                        %     RefinementCriterium(ind)    =   1;
                                        %     
                                        %     % refinemetn layer 2
                                        %     ind                         =   find(PARTICLES.phases == Upper_Crust);
                                        %     RefinementCriterium(ind)    =   2;
                                        %     
                                        %     % refinemetn layer 3
                                        %     ind                         =   find(PARTICLES.phases == Weak_channel );
                                        %     RefinementCriterium(ind)    =   3;
% OUTPUT:
%       MESH                    the refined mesh

% 1) Create the basic mesh
if size(RefinementCriterium)~=size(PARTICLES.x)
    error('size of ParticlesRefinementCriterium should be equal to size(PARTICLES.x)')
end


%% WE NEED TO DO SOMETHING BETTER HERE, TO ENSURE THAT WE TAKE TOPOGRAPHY INTO ACCOUNT AND THAT THE RESULTING MESHES DO NOT BECOME FINER EVERY TIMESTEP

MESH                =       CreateMesh(opts, mesh_input);

% 2) Create a kdtree object for particles
ReferencePts        =       [PARTICLES.x(:) PARTICLES.z(:)];
tree                =       kdtree(ReferencePts);

if isfield(opts,'AMR')
    NumberRefSteps      =       opts.AMR.NumberRefinementSteps;
else
    NumberRefSteps      =       3;
end

for RefinementLayer = 1:max(RefinementCriterium)
    for RefinementStep = 1:NumberRefSteps(RefinementLayer)
        % Compute center of elements
        X               =       MESH.NODES(1,:);
        Z               =       MESH.NODES(2,:);
        Xc              =       mean(X(MESH.ELEMS));
        Zc              =       mean(Z(MESH.ELEMS));
        
        % Find closest particles
        TestPoints      =       [Xc(:), Zc(:)];
        indClosest      =       kdtree_closestpoint(tree,TestPoints);
        
        % Execute the rules to find the relevant elements
        
        % Extract get relevant field
        Data_elems      =       RefinementCriterium(indClosest);
        
        % Mark data
        ind_TRI2bRefined=       find(Data_elems==RefinementLayer);
        
        MESH            =       RefineMesh(MESH, opts, ind_TRI2bRefined);
    end
end
function PARTICLES = InitializeParticleFields(PARTICLES)
% Initialize uninitialized but required particles fields.
%

% $Id$

PARTICLES= CheckParticleFields_Rows(PARTICLES);

if ~isfield(PARTICLES.HistVar,'Strain')
    PARTICLES.HistVar.Strain = zeros(size(PARTICLES.phases));
end

if ~isfield(PARTICLES,'number')
    PARTICLES.number = uint64(1:length(PARTICLES.x));
end

% stress components
if ~isfield(PARTICLES.HistVar,'Txx')
    PARTICLES.HistVar.Txx   = zeros(size(PARTICLES.x));
end
if ~isfield(PARTICLES.HistVar,'Tyy')
    PARTICLES.HistVar.Tyy   = zeros(size(PARTICLES.x));
end
if ~isfield(PARTICLES.HistVar,'Txy')
    PARTICLES.HistVar.Txy   = zeros(size(PARTICLES.x));
end
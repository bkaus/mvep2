function L_all = Compute_FSSA_vec(nnodel, GCOORD, ELEM2NODE, LOC2GLOB, INTP_PROPS, dt, GravAngle, Gravity, nedof, nelblo, nel,NUMERICS)
% Free Surface Stabilization Algorithm
%
% Computes a stabilization term which greatly enhances the timestep that can be
% employed in computations that involve deformation of a free surface.
%
% This is the vectorized version of the code, that implements the full Le
% matrix.
%

% $Id$

% Note: with the FSSA2 algorithm, we only construct  half the system

% Adjustable parameters:
FSSA_OmegaFactor    =   NUMERICS.LinearSolver.FreeSurfaceStabilizationAlgorithm;

L_all       =   zeros(nedof*(nedof+1)/2,nel);

if (nel<nelblo)
    nelblo = nel;
end

%==================================================================
% BLOCK LOOP - MATRIX COMPUTATION
%==================================================================
nblo        = ceil(nel/nelblo);
il          = 1;
iu          = nelblo;
for ib = 1:nblo
    L_block     =   zeros(nedof*(nedof+1)/2, nelblo);
    
    ECOORD_x    =   reshape( GCOORD(1,ELEM2NODE(:,il:iu)), nnodel, nelblo);
    ECOORD_y    =   reshape( GCOORD(2,ELEM2NODE(:,il:iu)), nnodel, nelblo);
    mean_rho    =   mean(Gravity*INTP_PROPS.Rho(il:iu,:),2)';                           % density*g
    
    
    % Compute it in a vectorized manner
    if nnodel==9 | nnodel==4 | nnodel==7
        
        L_block(:)          =   0;
        if nnodel==9 | nnodel==7
            % gauss-lobatto quadrature
            nn_surf         =   3;
            weight          =   [1/3; 4/3; 1/3];
            ipx             =   [-1 ;  0; 1];
            
        elseif nnodel==4
            % gauss-lobatto quadrature
            nn_surf         =   2;
            weight          =   [1 1];
            ipx             =   [-1 1];
        end
        nip_1D          =   length(ipx);
        
        if nnodel==9 | nnodel==4
            nedges = 4;
        elseif nnodel==7
            nedges = 3;
        end
        
        [Ni_1d, GNi_1d] =   shp_deriv_1D(ipx(:), nn_surf);           % shape fct.
        
        
        for edge_number=1:nedges
            if (nnodel==9)
                %Q2P1 element
                if edge_number==1
                    id   	=   [1 5 2];
                elseif edge_number==2
                    id   	=   [2 6 3];
                elseif edge_number==3
                    id   	=   [3 7 4];
                elseif edge_number==4
                    id   	=   [4 8 1];
                end
            elseif (nnodel==7)
                %T2P1 element
                if edge_number==1
                    id   	=   [1 6 2];
                elseif edge_number==2
                    id   	=   [2 4 3];
                elseif edge_number==3
                    id   	=   [3 5 1];
                end
                
                
            elseif nnodel==4
                %Q1P0 element
                if edge_number==1
                    id   	=   [1 2];
                elseif edge_number==2
                    id   	=   [2 3];
                elseif edge_number==3
                    id   	=   [3 4];
                elseif edge_number==4
                    id   	=   [4 1];
                end
            end
            
            
            SurfaceInt = zeros(2*2*nn_surf,nelblo);
            for i=1:nn_surf
                ii = (i-1)*4;
                
                for intp=1:nip_1D
                    
                    % Compute normal and detJ
                    s_x         =       GNi_1d{intp}*ECOORD_x(id,:);
                    s_y         =       GNi_1d{intp}*ECOORD_y(id,:);
                    detJ        =       sqrt(s_x.^2 + s_y.^2);              % determinant of jacobian
                    n_x         =        s_y./detJ;                         % normal in x-direction
                    n_y         =       -s_x./detJ;                         % normal in y-direction
                    
                    % Compute surface integral
                    SurfaceInt(ii+1,:) = SurfaceInt(ii+1,:) + FSSA_OmegaFactor*mean_rho.*dt.*GravAngle(1).*n_x.*Ni_1d{intp}(i).*detJ.*weight(intp);      % g_x*n_x
                    %                             SurfaceInt(ii+2,:) = SurfaceInt(ii+2,:) + FSSA_OmegaFactor*mean_rho.*dt.*GravAngle(1).*n_y.*Ni_1d{intp}(i).*detJ.*weight(intp);      % g_x*n_y
                    %                             SurfaceInt(ii+3,:) = SurfaceInt(ii+3,:) + FSSA_OmegaFactor*mean_rho.*dt.*GravAngle(2).*n_x.*Ni_1d{intp}(i).*detJ.*weight(intp);      % g_y*n_x
                    SurfaceInt(ii+4,:) = SurfaceInt(ii+4,:) + FSSA_OmegaFactor*mean_rho.*dt.*GravAngle(2).*n_y.*Ni_1d{intp}(i).*detJ.*weight(intp);      % g_y*n_y
                end
                
            end
            
            
            % construct local2global
            for i=1:nn_surf
                loc2glob_x((i-1)*4+1:i*4)    = 	[(id(i)-1)*2 + 1,   (id(i)-1)*2 + 2, (id(i)-1)*2 + 1,  (id(i)-1)*2 + 2];
                loc2glob_y((i-1)*4+1:i*4)    = 	[(id(i)-1)*2 + 1,   (id(i)-1)*2 + 1, (id(i)-1)*2 + 2,  (id(i)-1)*2 + 2];
            end
            
            
            %                     % this can be done faster (few loops can be removed):
            %                     ind_vec1=[];
            %                         for ii=1:length(loc2glob_x)
            %
            %
            %
            %                         indx  = 1;
            %                         for i = 1:nedof
            %                             for j = i:nedof
            %
            %                                 if i==loc2glob_x(ii) & j==loc2glob_y(ii)
            % %                                     L_block(:,indx) = L_block(:,indx) +  SurfaceInt(ii,:)' ;
            %                                      [indx loc2glob_x(ii) loc2glob_y(ii) ii]
            %                                      ind_vec1 = [ind_vec1 indx];
            %                                 end
            %
            %                                 indx=indx+1;
            %                             end
            %                         end
            %
            %                     end
            
            
            % Faster version (bit compllicated if we have half the
            % matrix only)
            ind             =   ones(nedof,nedof); ind = tril(ind);
            id_vec          =   cumsum(ind(:));
            id              =   loc2glob_y>=loc2glob_x;
            loc2glob_y      =   loc2glob_y(id);
            loc2glob_x      =   loc2glob_x(id);
            %ind_vec         =   sub2ind([nedof nedof],loc2glob_y,loc2glob_x);
            ind_vec         =   loc2glob_y + (loc2glob_x - 1).*nedof;                 % same as line above but w/out error checking
            ind_vec         =   id_vec(ind_vec);
            ii_vec          =   find(id);
            
            % Faster version:
            for ii=1:length(loc2glob_x)
                indx = ind_vec(ii);
                L_block(indx,:) = L_block(indx,:) +  SurfaceInt(ii_vec(ii),:);
            end
            
            
            
            
            
        end     % end of loop over element edges
        
        
        % Add to L_all
        
        %==============================================================
        % ix) WRITE DATA INTO GLOBAL STORAGE
        %==============================================================
        L_all(:,il:iu)      =   L_block;
        
        
        %==============================================================
        % READJUST START, END AND SIZE OF BLOCK. REALLOCATE MEMORY
        %==============================================================
        il  = il+nelblo;
        if(ib==nblo-1)
            nelblo 	    = nel-iu;
            L_block     = zeros(nedof*(nedof+1)/2, nelblo);
        end
        iu  = iu+nelblo;
        
        
    else
        error('FSSA not implemented for this element!');
    end
    
end



%         % CONSTRUCT SYMMETRIC MATRIX
%         if exist('sparse_create')>0
%             opts.symmetric          =   1;      % symmetric
%             opts.n_node_dof         =   2;      % 2 dof
%             L                       =   sparse_create(ELEM2NODE, L_all, opts);
%
%         else
%             % CONSTRUCT LOCAL2GLOBAL
%             ndim                    =   2;
%             ELEM_DOF                = zeros(nedof, nel,'int32');
%             ELEM_DOF(1:ndim:end,:)  = reshape(LOC2GLOB(1,ELEM2NODE),nnodel, nel);
%             ELEM_DOF(2:ndim:end,:)  = reshape(LOC2GLOB(2,ELEM2NODE),nnodel, nel);
%
%             % We only form the lower part of the velocity matrix
%             % (which should be symmetric)
%             indx_j                  = repmat(1:nedof,nedof,1); indx_i = indx_j';
%             indx_i                  = tril(indx_i); indx_i = indx_i(:); indx_i = indx_i(indx_i>0);
%             indx_j                  = tril(indx_j); indx_j = indx_j(:); indx_j = indx_j(indx_j>0);
%             L_i     = ELEM_DOF(indx_i(:),:);
%             L_j     = ELEM_DOF(indx_j(:),:);
%             indx       = L_i < L_j;
%             tmp        = L_j(indx);
%             L_j(indx)  = L_i(indx);
%             L_i(indx)  = tmp;
%
%             m = max(L_i(:));
%             n = max(L_j(:));
%
%             L                       =   sparse2(L_i(:)              ,    L_j(:)         ,       L_all(:), m, n);
%         end


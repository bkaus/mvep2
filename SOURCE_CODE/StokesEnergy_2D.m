function [PARTICLES, MESH, INTP_PROPS, NUMERICS, MATERIAL_PROPS, dt, SUCCESS] = StokesEnergy_2D( MESH, BC, PARTICLES, MATERIAL_PROPS, dt,NUMERICS,CHAR)
%STOKES_ENERGY_2D is a 2D (viscoelastoplastic) Stokes & temperature solver
%
% It assumes that we have a marker and cell method and performs one timestep
%
% Syntax:
%        [PARTICLES, MESH, INTP_PROPS, NUMERICS, MATERIAL_PROPS, dt, SUCCESS] 	=   StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt,NUMERICS,CHAR);
%
%   Input:
%           MESH            -   Object containing the mesh, created with CreateMesh
%           BC              -   Object containing the boundary conditions
%           PARTICLES       -   The Particles/Markers object
%           MATERIAL_PROPS  -   Material properties for each of the phases
%           dt              -   timestep (in nondimensional units)
%           NUMERICS        -   Input parameter containing numerical parameters
%           CHAR            -   Input parameter containing characteristic values used for non-dimensionalization
%
%   Output:
%           PARTICLES       -   As above, bit with updated properties
%           MESH            -   The mesh, which has the solution attached to it.
%           INTP_PROPS      -   Integration point properties (for plotting)
%           NUMERICS        -   Updated NUMERICS structure
%           MATERIAL_PROPS  -   Updated MATERIAL_PROPS (can contain phase
%                               diagrams to speed up future calculations)
%           dt              -   timestep
%           SUCCESS         -   Indicates if we could solve the equations
%                               or not

% $Id$

%% Get input parameters
if isfield(NUMERICS,'Length')
    error('Did you confuse the NUMERICS & CHAR as input parameters of StokesEnergy2D?')
end
if isempty(NUMERICS)
    error('You should have NUMERICS as an input paremeter')
end

if nargout<4
    error('you should at least have the following output variables in StokesEnergy2D: [PARTICLES, MESH, INTP_PROPS, NUMERICS]')
end


time_start = cputime;
cpustart   = cputime;

% REMARK: ALL OF THE BELOW SHOULD BE PUT IN A DIFFERENT ROUTINE CALLED
% SOMETHING LIKE INITIALIZE_FIELDS_TIMESTEP OR SO. THE

%NUMERICS        =   SetDefaultNumericalParameters(NUMERICS);                % add default parameters if required

if isfield(MESH,'DARCY') && isfield(NUMERICS.LinearSolver,'StaticPresCondensation')
    % static pressure condensation works for Stokes, not for 2-phase flow
    NUMERICS.LinearSolver.StaticPresCondensation    = false;
end

NUMERICS = CheckAdaptiveInterpolation(NUMERICS,MATERIAL_PROPS, MESH, CHAR, dt);
if MESH.ndim<3;
    PARTICLES       =   InitializeParticleFields(PARTICLES);                    % Add uninitialized particle HistVar fields if required
    if NUMERICS.ShowTimingInformation
        time_increment_InitializeParticleFields = cputime-time_start;
        
        disp(['                           Subroutine                               Total|   Function '])
        disp(['Timing increment:  InitializeParticleFields                      =  ',num2str(time_increment_InitializeParticleFields,'%2.2f'),'s;   ',num2str(cputime-cpustart,'%2.2f'),'s'])
        cpustart   = cputime;
    end
    
    PARTICLES       =   CorrectParticlesForPerodicity(PARTICLES, BC, MESH);     % Correct particles for periodic BC if required
    if NUMERICS.ShowTimingInformation
        time_increment_CorrectParticlesForPerodicity = cputime-time_start;
        disp(['Timing increment:  CorrectParticlesForPerodicity                 =  ',num2str(time_increment_CorrectParticlesForPerodicity,'%2.2f'),'s;   ',num2str(cputime-cpustart,'%2.2f'),'s'])
        cpustart   = cputime;
    end
    
    %    PARTICLES       =   GeometricallyReorderParticles(PARTICLES,MESH);          % Reorder particles such that they are in each other's vicinity
    
    %% 1) Compute properties from particles -> integration points
    [PARTICLES]     =   ComputeElementsAndLocalCoordinates_Particles(MESH, PARTICLES, NUMERICS);     % Elements and local coordinates within element of particles
    if NUMERICS.ShowTimingInformation
        time_increment_ComputeElementsAndLocalCoordinates_Particles = cputime-time_start;
        disp(['Timing increment:  ComputeElementsAndLocalCoordinates_Particles  =  ',num2str(time_increment_ComputeElementsAndLocalCoordinates_Particles,'%2.2f'),'s;   ',num2str(cputime-cpustart,'%2.2f'),'s'])
        cpustart   = cputime;
    end
    
    
    %% Check the amount of particles per element and inject or delete particles if required
%     PARTICLES      	=   InjectOrDeleteParticles(MESH,PARTICLES,NUMERICS, BC);
    if NUMERICS.ShowTimingInformation
        time_increment_InjectOrDeleteParticles = cputime-time_start;
        disp(['Timing increment:  InjectOrDeleteParticles                       =  ',num2str(time_increment_InjectOrDeleteParticles,'%2.2f'),'s;   ',num2str(cputime-cpustart,'%2.2f'),'s'])
        cpustart   = cputime;
    end
    
    if isfield(PARTICLES,'Vx')
        PARTICLES.CompVar.Vx = PARTICLES.Vx;
        PARTICLES.CompVar.Vz = PARTICLES.Vz;
    end
    
    Method                  =   NUMERICS.InterpolationMethod.ParticlesToIntegrationPoints;
    [INTP_PROPS, MESH]      =   ParticlesToIntegrationPoints(PARTICLES,MESH,  BC, Method,  size(MATERIAL_PROPS,2), NUMERICS);           % Compute phase proportions in every element
    if NUMERICS.ShowTimingInformation
        time_increment_ParticlesToIntegrationPoints = cputime-time_start;
        disp(['Timing increment:  ParticlesToIntegrationPoints                  =  ',num2str(time_increment_ParticlesToIntegrationPoints,'%2.2f'),'s;   ',num2str(cputime-cpustart,'%2.2f'),'s'])
        cpustart   = cputime;
    end
    
    % test whether input parameters make sense or not:
    TestWhetherModelParametersAreSensible(CHAR, MESH, INTP_PROPS, PARTICLES);
    if NUMERICS.ShowTimingInformation
        time_increment_TestWhetherModelParametersAreSensible = cputime-time_start;
        disp(['Timing increment:  TestWhetherModelParametersAreSensible         =  ',num2str(time_increment_TestWhetherModelParametersAreSensible,'%2.2f'),'s;   ',num2str(cputime-cpustart,'%2.2f'),'s'])
        cpustart   = cputime;
    end
    
    if isfield(INTP_PROPS,'E2nd')
        % Update strainrate guess of last timestep
        INTP_PROPS.Strainrate.E2nd = INTP_PROPS.E2nd;
    end
    
    if isfield(INTP_PROPS,'E2nd_vp')
        % Update strainrate guess of last timestep
        INTP_PROPS.Strainrate.E2nd_vp = INTP_PROPS.E2nd_vp;
    end
    
    if isfield(MESH,'Vx')
        % if we have an initial guess of velocity, we use it to speed up
        % nonlinear iterations.
        [MESH.VEL(1,:)]                     =   Extrapolate_IntpDataToNodes(MESH, INTP_PROPS.Vx, NUMERICS); % extrapolate from INTP -> NODES
        [MESH.VEL(2,:)]                     =   Extrapolate_IntpDataToNodes(MESH, INTP_PROPS.Vz, NUMERICS); % extrapolate from INTP -> NODES
    end
    
elseif MESH.ndim==3
    %Properties assigned in driver since no particles
    GCOORD_x=MESH.NODES(1,:);
    GCOORD_y=MESH.NODES(2,:);
    GCOORD_z=MESH.NODES(3,:);
    INTP_PROPS.X=GCOORD_x(MESH.ELEMS)';
    INTP_PROPS.Y=GCOORD_y(MESH.ELEMS)';
    INTP_PROPS.Z=GCOORD_z(MESH.ELEMS)';
    nel=(size(MESH.ELEMS,2));
    nnodel=(size(MESH.ELEMS,1));
    INTP_PROPS.PHASE_PROP=ones(nel,1);
    INTP_PROPS.K=MATERIAL_PROPS.K(MESH.Phases);
    INTP_PROPS.H=MATERIAL_PROPS.H(MESH.Phases);
    INTP_PROPS.RHO_CP=MATERIAL_PROPS.RHO_CP(MESH.Phases);
    INTP_PROPS.Rho =MATERIAL_PROPS.Density.Constant.Rho*ones(nel,1);
    INTP_PROPS.T=MATERIAL_PROPS.T;
    INTP_PROPS.AdiabaticHeat= zeros(nel,nnodel);
    
end

%% Store 'old' fields
MESH_OLD                                =   MESH;
INTP_PROPS_OLD                          =   INTP_PROPS;

if isfield(MESH,'DARCY')
    MESH.DARCY.PHI0     = MESH.DARCY.PHI;
    if ~isfield(MESH.DARCY, 'Pc')
        MESH.DARCY.Pc   = zeros(size(MESH.DARCY.PHI));
    end
    MESH.DARCY.Pc0  = MESH.DARCY.Pc;
    
    % Initialize compaction pressure on integration points
    % to zero if not available (i.e., during first time-step only).
    if ~isfield(INTP_PROPS, 'Pressure')
        INTP_PROPS.Pressure   =   zeros(size(INTP_PROPS.X));
    end
    if ~isfield(INTP_PROPS, 'Pc')
        INTP_PROPS.Pc         =   zeros(size(INTP_PROPS.X));
    end
    INTP_PROPS.Pc0        =   INTP_PROPS.Pc;
    if ~isfield(INTP_PROPS, 'Pf')
        INTP_PROPS.Pf         =   zeros(size(INTP_PROPS.X));
    end
end


%% Update BC's for stokes
[BC]                                    =   SetBC(MESH, BC, INTP_PROPS);
if isfield(BC,'Darcy')
    BC                                  =   SetBC_FluidPressure(MESH, BC, INTP_PROPS, MATERIAL_PROPS);
end
if NUMERICS.ShowTimingInformation
    time_increment_SetBC = cputime-time_start;
    disp(['Timing increment:  SetBC                                         =  ',num2str(time_increment_SetBC),'s;   ',num2str(cputime-cpustart),'s'])
    cpustart = cputime;
end


%%=========================================================================
%% PERFORM TIMESTEPPING

%% Compute nonlinear Stokes solution for this timestep
[ MESH, INTP_PROPS, SUCCESS, minDetJ, NUMERICS, MATERIAL_PROPS,PARTICLES,BC, dt ] = StokesEnergy_2D_Timestep(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, CHAR, PARTICLES, dt, time_start);
if NUMERICS.ShowTimingInformation
    time_increment_ParticlesToIntegrationPoints = cputime-time_start;
    disp(['Timing increment:  Stokes/Energy Solver                          =  ',num2str(time_increment_ParticlesToIntegrationPoints,'%2.2f'),'s;   ',num2str(cputime-cpustart,'%2.2f'),'s'])
    cpustart = cputime;
end

%% END PERFORM TIMESTEPPING
%%=========================================================================

% post-processing step: compute fluid velocity
if isfield(MESH,'DARCY')
    MESH    = fluidVelocity(MESH, INTP_PROPS, MATERIAL_PROPS, NUMERICS, CHAR);
end



%% Update stresses as we successfully did a (nonlinear) timestep by the time we arrived here
INTP_PROPS                  =   UpdateStressesStrainsAfterTimestep(INTP_PROPS, dt);
if NUMERICS.ShowTimingInformation
    time_increment_UpdateStressesStrainsAfterTimestep = cputime-time_start;
    disp(['Timing increment:  UpdateStressesStrainsAfterTimestep            =  ',num2str(time_increment_UpdateStressesStrainsAfterTimestep,'%2.2f'),'s;   ',num2str(cputime-cpustart,'%2.2f'),'s'])
    cpustart   = cputime;
end

%% Couple partial melting/phase diagram to the code
if isfield(PARTICLES.HistVar,'MeltFraction_PhaseDiagram') & NUMERICS.TwoPhase==logical(0) & strcmp(NUMERICS.MeltExtraction.Type,'none')
    % this essentially assumes that
    %  (1) There is NO melt extraction and all melt stays in the rock
    %  (2) We do not use a two-phase flow formulation that computes the melt fraction independently
    %
    % Note that if we DO have melt extraction, we can do this by changing
    % the MeltFraction AFTER calling StokesEnergy_2D (overwriting this)
    INTP_PROPS.MeltFraction         =   INTP_PROPS.MeltFraction_PhaseDiagram;
end

%% Water Migration
if isfield(PARTICLES,'CompVar') & isfield(PARTICLES.CompVar,'WaterCapacity')
    cpu1=cputime;
    [INTP_PROPS]   =   WaterMigration(PARTICLES,MESH,INTP_PROPS,BC,CHAR,NUMERICS,dt);
    cpu2=cputime-cpu1
end

if MESH.ndim<3;
    %% Update properties from nodes & integration points -> particles [stress, strainrate, history variables]
    [PARTICLES, MESH]       	=   UpdatePropertiesAt_PARTICLES(MESH, PARTICLES, INTP_PROPS, MESH_OLD, INTP_PROPS_OLD, NUMERICS, BC,dt);
    
    if isfield(PARTICLES.HistVar,'T')
        if  min(PARTICLES.HistVar.T*CHAR.Temperature-273)<0 && CHAR.Temperature>1
            disp('particles have a negative temperature')   % we presumably compute with dimensional units and T is negative
        end
    end
    
    %% Compute lithostatic pressure if flag is true
    if NUMERICS.ComputeLithostaticOverpressure
        MESH                    = ComputeLithostatic_Overpressure(MESH,INTP_PROPS,MATERIAL_PROPS);
        if isfield(MESH.element_type,'darcy')
            % store on MESH.DARCY if available
            LithostaticPressure = MESH.CompVar.LithostaticPressure(MESH.DARCY.restr);
            MESH.DARCY.LithostaticPressure  = LithostaticPressure(:)';
            MESH.DARCY.DynamicPressure      = MESH.DARCY.Pf - MESH.DARCY.LithostaticPressure;
        end
    end
    
    %% Change phases on particles if a phase diagram was employed which wants to do that
    PARTICLES                   =   ModifyPhaseNumbersDueToPhaseTransitions(PARTICLES, MATERIAL_PROPS, CHAR);
end
if NUMERICS.ShowTimingInformation
    time_increment_UpdatePropertiesAt_PARTICLES = cputime-time_start;
    disp(['Timing increment:  UpdatePropertiesAt_PARTICLES                  =  ',num2str(time_increment_UpdatePropertiesAt_PARTICLES,'%2.2f'),'s;   ',num2str(cputime-cpustart,'%2.2f'),'s'])
    cpustart   = cputime;
end



% if max(PARTICLES.HistVar.T)>1 | min(PARTICLES.HistVar.T)<0
%     'stop here'
% end

end


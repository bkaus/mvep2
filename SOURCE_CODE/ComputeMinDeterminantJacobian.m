function minDetJ = ComputeMinDeterminantJacobian(MESH)
% Computes the minimum determinant of the jacobian of a given FE mesh
% (if this is negative it's no good)
%

% $Id$




nel             =   size(MESH.ELEMS,2);
nelblo_input    =   400;
nelblo          =   nelblo_input;
nelblo          =   min(nel, nelblo);
nblo            =   ceil(nel/nelblo);

GCOORD          =   MESH.NODES;
ELEM2NODE       =   MESH.ELEMS;

ndim            =	MESH.ndim;
nip             =   MESH.nip;                                    % Set's # of integration points
nnodel          =   size(MESH.ELEMS,1);
nedof           =   nnodel*ndim;

if nnodel==7 & size(ELEM2NODE,1)==6
    
    % ADDING COORDINATES FOR 7TH POINT IN CENTER
    ELEM2NODE(7,:)  = nnod+1:nnod+nel;
    GCOORD          = [GCOORD, [ mean(reshape(GCOORD(1, ELEM2NODE(1:3,:)), 3, nel));...
        mean(reshape(GCOORD(2, ELEM2NODE(1:3,:)), 3, nel))  ]    ];
end

switch MESH.element_type.pressure
    case 'P0'
        np          =   1;   % constant
    case 'P-1'
        if ndim==2
            np      =   3;   % linear discontinuous, 2D
        elseif ndim==3
            np      =   4;        
         end
    case 'Q1'
        np          =   4;   % linear continuous
    otherwise
        error('unknown pressure shape function/element type')
end


if nnodel==3 || nnodel==7
    [IP_X IP_w] = ip_triangle(nip);
    [N dNdu]    = shp_deriv_triangles(IP_X, nnodel);
elseif nnodel==4 || nnodel==9
    [IP_X IP_w] = ip_quad(nip);
    [N dNdu]    = shp_deriv_quad(IP_X, nnodel);
elseif nnodel==8 || nnodel==27
    [IP_X IP_w] = ip_3d(nip);
    [   N dNdu] = shp_deriv_3d(IP_X, nnodel);
    
end




%==================================================================
% i) BLOCK LOOP - MATRIX COMPUTATION
%==================================================================
il          = 1;
iu          = nelblo;
minDetJ     = realmax;
for ib = 1:nblo
    %==============================================================
    % ii) FETCH DATA OF ELEMENTS IN BLOCK
    %==============================================================
    
    ECOORD_x = reshape( GCOORD(1,ELEM2NODE(:,il:iu)), nnodel, nelblo);
    ECOORD_y = reshape( GCOORD(2,ELEM2NODE(:,il:iu)), nnodel, nelblo);
    if MESH.ndim==2;
        
    end
    
    for ip=1:nip
        
        %==========================================================
        % v) CALCULATE JACOBIAN, ITS DETERMINANT AND INVERSE
        %==========================================================
        dNdui   =     dNdu{ip};
        if MESH.ndim==2;
            Jx          = ECOORD_x'*dNdui';
            Jy          = ECOORD_y'*dNdui';
            detJ        = Jx(:,1).*Jy(:,2) - Jx(:,2).*Jy(:,1);
            
        else
            error('Not yet implemented in 3D')
            
        end
        minDetJ     = min([minDetJ, min(detJ)]);
        
        
    end
    
    
    il  = il+nelblo;
    if(ib==nblo-1)
        nelblo 	    = nel-iu;
        
    end
    iu  = iu+nelblo;
end


end

function [GRID_DATA] = VisualizeFields(MESH, PARTICLES, NUMERICS, CHAR, NzGrid, BC)
%
% Creats a 2D map with compositional fields from PARTICLES.
%
% Boris Kaus, October 2008
%
% $Id: VisualizeFields.m 3918 2008-12-11 10:01:39Z boris $

GCOORD = MESH.NODES;

RegularGridNumber = MESH.RegularGridNumber;


% Create 2D grid ----------------------------------------------------------
tic 
MinX                    = 	min(GCOORD(1,:));
MinZ                    =	min(GCOORD(2,:));
MaxX                    =  	max(GCOORD(1,:));
MaxZ                    =   max(GCOORD(2,:)) + 0.02*(max(GCOORD(2,:))-min(GCOORD(2,:)));

if length(NzGrid)==1
    NxGrid              = 	round((MaxX-MinX)/(MaxZ-MinZ)*NzGrid);
else
   NxGrid               =   NzGrid(1);
   NzGrid               =   NzGrid(2);
end

while length(MinX:(MaxX-MinX)/(NxGrid-1):MaxX) ~= NxGrid
    NxGrid              =   NxGrid-1;                       % workaround that occurs due to roundoff errors in very rare cases  
end

DxGrid                  = 	(MaxX-MinX)/(NxGrid-1);
DzGrid                  = 	(MaxZ-MinZ)/(NzGrid-1);
[Xgrid,Zgrid]           =   meshgrid(MinX:(MaxX-MinX)/(NxGrid-1):MaxX, MinZ:DzGrid:MaxZ);
Index                   =   zeros(NzGrid, NxGrid);

% Filter PARTICLES & remove deactivated ones ------------------------------
ind                     =   find(isnan(PARTICLES.elem) | isnan(PARTICLES.x) | isnan(PARTICLES.z) | isinf(PARTICLES.z) | isinf(PARTICLES.x) );
PARTICLES.elem(ind)  	=   [];
PARTICLES.x(ind)        =   [];
PARTICLES.z(ind)        =   [];
PARTICLES.HistVar.T(ind)        =   [];
if isfield(PARTICLES.CompVar,'Rho')
    PARTICLES.CompVar.Rho(ind) 	=   [];     % for backwards compatibility
end
PARTICLES.phases(ind)   =   [];
if isfield(PARTICLES.CompVar,'MeltFraction_PhaseDiagram')
    PARTICLES.CompVar.MeltFraction_PhaseDiagram(ind) =   [];
end



% extend PARTICLES in periodic case
switch lower(BC.Stokes.Left)
    case 'periodic'
        % in periodic cases we have to do some more work
        ind                     =   find(GCOORD(2,:)==MinZ);
        W                       =   max(GCOORD(1,ind))-min(GCOORD(1,ind));                  % Width of domain (important for cases that are periodic in the horizontal direction)
        
        % Expand the PARTICLES to the left & right
        PARTICLES.elem              =   [PARTICLES.elem, PARTICLES.elem, PARTICLES.elem];
        PARTICLES.x                 =   [PARTICLES.x-W,  PARTICLES.x,    PARTICLES.x+W ];
        PARTICLES.z                 =   [PARTICLES.z,    PARTICLES.z,    PARTICLES.z   ];
        PARTICLES.HistVar.T         =   [PARTICLES.HistVar.T,    PARTICLES.HistVar.T,    PARTICLES.HistVar.T   ];
        if isfield(PARTICLES.CompVar,'Rho')
            PARTICLES.CompVar.Rho   =   [PARTICLES.CompVar.Rho,    PARTICLES.CompVar.Rho,    PARTICLES.CompVar.Rho   ];
        end
        PARTICLES.phases            =   [PARTICLES.phases,    PARTICLES.phases,    PARTICLES.phases   ];
        
        if isfield(PARTICLES.CompVar,'MeltFraction_PhaseDiagram')
            PARTICLES.CompVar.MeltFraction_PhaseDiagram  =   [PARTICLES.CompVar.MeltFraction_PhaseDiagram,    PARTICLES.CompVar.MeltFraction_PhaseDiagram,    PARTICLES.CompVar.MeltFraction_PhaseDiagram   ];
        end
        
        % Remove PARTICLES outside domain
        PartX_int                           =       round((PARTICLES.x-MinX)/DxGrid)+1;
        PartZ_int                           =       round((PARTICLES.z-MinZ)/DzGrid)+1;
        PartX_int(isinf(PartX_int))         =       NaN;
        PartZ_int(isinf(PartX_int))         =       NaN;
        PartX_int(PartX_int>NxGrid)         =       NaN;
        PartZ_int(PartZ_int>NzGrid)         =       NaN;
        PartX_int(PartX_int<1)              =       NaN;
        PartZ_int(PartZ_int<1)              =       NaN;
        ind                                 =       find(isnan(PartX_int) | isnan(PartZ_int));
        
        PARTICLES.elem(ind)  	=   [];
        PARTICLES.x(ind)        =   [];
        PARTICLES.z(ind)        =   [];
        PARTICLES.HistVar.T(ind)        =   [];
        if isfield(PARTICLES.CompVar,'Rho')
            PARTICLES.CompVar.Rho(ind) 	=   [];     % for backwards compatibility
        end
        PARTICLES.phases(ind)   =   [];
        if isfield(PARTICLES.CompVar,'Melt')
            PARTICLES.CompVar.MeltFraction_PhaseDiagram(ind) =   [];
        end

        
        
end


% Map PARTICLES to 2D grid-------------------------------------------------
PartX_int                           =       round((PARTICLES.x-MinX)/DxGrid)+1;
PartZ_int                           =       round((PARTICLES.z-MinZ)/DzGrid)+1;
PartX_int(isinf(PartX_int))         =       NaN;
PartZ_int(isinf(PartX_int))         =       NaN;
PartX_int(PartX_int>NxGrid)         =       NaN;
PartZ_int(PartZ_int>NzGrid)         =       NaN;
PartX_int(PartX_int<1)              =       NaN;
PartZ_int(PartZ_int<1)              =       NaN;
ind                                 =       find(~isnan(PartX_int) | ~isnan(PartZ_int));
PartInd                             =       ones(size(PartX_int))*NaN;
PartInd(ind)                        =       sub2ind([NzGrid NxGrid], PartZ_int(ind) , PartX_int(ind) );
NumberArray                         =       1:length(PartInd);    
Index(PartInd(~isnan(PartInd)))     =       NumberArray(~isnan(PartInd));




disp(['Finished putting PARTICLES on grid ...', num2str(toc),'s'])


% Fill empty holes --------------------------------------------------------
tic
TestPoints              =   [Xgrid(find(Index==0)), Zgrid(find(Index==0))];
ReferencePts            =   [];
ReferencePts            =   [PARTICLES.x(:), PARTICLES.z(:)];

switch NUMERICS.NearestNeighborhoodAlgorithm
    case 'kdtree_mex'
        tree                    =   kdtree(ReferencePts);                           % Generate tree
        [ClosestPtIndex,pntval] =   kdtree_closestpoint(tree,TestPoints);           % Find closest point
        
    case 'knnsearch'
        [ClosestPtIndex]        =   knnsearch(ReferencePts,TestPoints);
    otherwise
        error('Unknown NearestNeighborhoodAlgorithm')
end
Index(find(Index==0))   =   ClosestPtIndex;                                 % Closest index
disp(['Finished filling empty holes      ... ', num2str(toc),'s'])


% Delete points that are above the free surface----------------------------
tic
X                       =   GCOORD(1,:);
Z                       =   GCOORD(2,:);
x2d_temp                =   X(RegularGridNumber);
z2d_temp                =   Z(RegularGridNumber);
Topo_x                  =   Xgrid(1,:);
Topo_z                  =   interp1(x2d_temp(end,:), z2d_temp(end,:), Xgrid(1,:),'linear','extrap');
Topo_z_index            =   round((Topo_z-MinZ)/DzGrid)+1;
Topo_x_index            =   1:NxGrid;

Topo_idx                =   [Topo_x_index];
Topo_idz                =   [Topo_z_index];
while min(Topo_z_index)<NzGrid
    Topo_z_index            =   Topo_z_index+1;
    Topo_idx                =   [Topo_idx; Topo_x_index; ];
    Topo_idz                =   [Topo_idz; Topo_z_index; ];
end
Topo_idz(Topo_idz>NzGrid) = NzGrid;
Topo_index              =   sub2ind([NzGrid NxGrid], Topo_idz(:), Topo_idx(:));
disp(['Finished filling empty holes      ... ', num2str(toc),'s'])


% Create phase grids, Temperature grids etc;
Phases                  =   zeros(size(Xgrid));

% Get the most abundant phase @ a given point
Phases                  =   double(PARTICLES.phases(Index));

% Take the most abundant phase:
ind                     =   find(~isnan(PartInd));
A                       =	sparse2(PartInd(ind),PARTICLES.phases(ind),ones(length(PartInd(ind)),1,'single'),prod(size(Phases)),max(PARTICLES.phases));
[dum,Phase_vec]         =   max(full(A),[],2);
ind                     =   find(Phase_vec>0 & sum(A,2)>0);
Phases(ind)             =   Phase_vec(ind);
Phases(Topo_index)      =   NaN;

Temperature             =   zeros(size(Xgrid));
Temperature             =   double(PARTICLES.HistVar.T(Index));
Temperature(Topo_index) =   NaN;

if isfield(PARTICLES,'rho')
    Density                 =   zeros(size(Xgrid));
    Density                 =   double(PARTICLES.CompVar.Rho(Index));
    Density(Topo_index)     =   NaN;
    GRID_DATA.Density       =   Density;
end

if isfield(PARTICLES.CompVar,'MeltFraction_PhaseDiagram')
    Melt             	=   zeros(size(Xgrid));
    Melt                =   double(PARTICLES.CompVar.MeltFraction_PhaseDiagram(Index));
    Melt(Topo_index) 	=   NaN;
    GRID_DATA.Melt      =   Melt;
end

PhasesPatch.Phase    =   Phases(:)';
if isfield(PARTICLES.CompVar,'MeltFraction_PhaseDiagram')
    PhasesPatch.Melt =   Melt(:)';
else
    PhasesPatch.Melt =   zeros(size(Phases(:)'));
end


% Save data in structure---------------------------------------------------
GRID_DATA.Xvec          =   single(Xgrid(1,:)*CHAR.Length/1e3);
GRID_DATA.Zvec          =   single(Zgrid(:,1)*CHAR.Length/1e3);
GRID_DATA.DxGrid        =   single(DxGrid*CHAR.Length/1e3);
GRID_DATA.DzGrid        =   single(DzGrid*CHAR.Length/1e3);
GRID_DATA.Phases        =   single(Phases);
if isfield(GRID_DATA,'Melt')
    GRID_DATA.Melt          =   uint8(GRID_DATA.Melt);
end
GRID_DATA.Temperature   =   single(Temperature*CHAR.Temperature-273);


% CONSTRUCT PATCHES FOR VISUALIZATION
% % PhasesPatch.X        =   [Xgrid(:)'-DxGrid/2; Xgrid(:)'+DxGrid/2;  Xgrid(:)'+DxGrid/2; Xgrid(:)'-DxGrid/2]*l_c/1e3;
% PhasesPatch.Z        =   [Zgrid(:)'-DzGrid/2; Zgrid(:)'-DxGrid/2;  Zgrid(:)'+DzGrid/2; Zgrid(:)'+DxGrid/2]*l_c/1e3;
% PhasesPatch.X           =   single(PhasesPatch.X);
% PhasesPatch.Z           =   single(PhasesPatch.Z);
% PhasesPatch.Phase       =   uint32(PhasesPatch.Phase);
% PhasesPatch.Melt        =   uint8(PhasesPatch.Melt);

% % Visualize (for debugging)
% % figure(1), clf
% % 
% ind = find(PhasesPatch.Phase==1);
% h=patch(PhasesPatch.X(:,ind), PhasesPatch.Z(:,ind),'r'); set(h,'edgecolor','none')
% 
% ind = find(PhasesPatch.Phase==2);
% h=patch(PhasesPatch.X(:,ind), PhasesPatch.Z(:,ind),'y'); set(h,'edgecolor','none')
% 
% ind = find(PhasesPatch.Phase==3);
% h=patch(PhasesPatch.X(:,ind), PhasesPatch.Z(:,ind),'b'); set(h,'edgecolor','none')
% 
% ind = find(PhasesPatch.Phase==4);
% h=patch(PhasesPatch.X(:,ind), PhasesPatch.Z(:,ind),[.8 .8 .8]); set(h,'edgecolor','none')
% 
% ind = find(PhasesPatch.Phase==5);
% h=patch(PhasesPatch.X(:,ind), PhasesPatch.Z(:,ind),'g'); set(h,'edgecolor','none')
% 


function [Data_2D, MAPDATA] = MapParticlesToRegularGrid(MESH,PARTICLES,Part_data, MAPDATA, MovingAverageWindowSize,FindElements)
% This maps particles to a regular grid that covers the computational
% domain
%
% [Data_2D, MAPDATA] = MapParticlesToRegularGrid(MESH,PARTICLES,Part_data, MAPDATA, FindElements)


% Extract necessary info
NxGrid                      =   MAPDATA.NxGrid;
NzGrid                      =   MAPDATA.NzGrid;

if size(Part_data,1)>size(Part_data,2)
    Part_data               =   Part_data';     % happens sometimes
end

% if ~isfield(MAPDATA,'Xgrid')

% Boundaries of the computational mesh
MinX                        = 	min(MESH.NODES(1,:));
MinZ                        =	min(MESH.NODES(2,:));
MaxX                        =  	max(MESH.NODES(1,:));
MaxZ                        =   max(MESH.NODES(2,:)); % + 0.02*(max(MESH.NODES(2,:))-min(MESH.NODES(2,:)));

DxGrid                      = 	(MaxX-MinX)/(NxGrid-1);
DzGrid                      = 	(MaxZ-MinZ)/(NzGrid-1);

% Create 2D grid
[MAPDATA.Xgrid,MAPDATA.Zgrid] =   meshgrid(MinX:DxGrid:MaxX, MinZ:DzGrid:MaxZ);

% Create indices
[ind_i_reg, ind_j_reg]      =   meshgrid(1:size(MAPDATA.Xgrid,2), 1:size(MAPDATA.Xgrid,1) );
ind_i_reg                   =   ind_i_reg(:);
ind_j_reg                   =   ind_j_reg(:);
MAPDATA.ind_i_reg           =   ind_i_reg;
MAPDATA.ind_j_reg           =   ind_j_reg;

% end

% 2) Map particles to 2D grid -------------------------------------
if ~isfield(MAPDATA,'Weight_vec')
    %     ALL THESE ROUTINES, ONLY NEED TO BE DONE ONCE PER TIMESTEP
    
    
    fac_x           =   MovingAverageWindowSize;
    fac_z           =   MovingAverageWindowSize;
    PartX_int_vec   =   [];
    PartZ_int_vec   =   [];
    Weight_vec      =   [];
    Part_active     =   [];
    
    % We use a 'moving average' area to interpolate properties from particles->grid.
    % This ensures that we have sufficient particles to average over.
    % The size of the area is [fac_x*dx_fine by fac_y*dy_fine]
    %
    NumberSweeps = fac_x*fac_z;
    for ix = 1:fac_x;
        for iz = 1:fac_z;
            
            
            MinX = MAPDATA.Xgrid(iz,ix);
            MinZ = MAPDATA.Zgrid(iz,ix);
            
            
            % This is done with an integer operation
            dx                          =   fac_x*DxGrid;
            dz                          =   fac_z*DzGrid;
            
            
            
            PartX_in                   = 	round((PARTICLES.x-MinX)/dx)+1;
            PartZ_in                   =	round((PARTICLES.z-MinZ)/dz)+1;
            
            % center coordinates of integer points
            Xcoord                      =   (PartX_in-1)*dx + MinX;
            Zcoord                      =   (PartZ_in-1)*dz + MinZ;
            
            Dist_X                      =  	(PARTICLES.x-Xcoord); % normalized distance in x-direction
            Dist_Z                  	=  	(PARTICLES.z-Zcoord); % normalized distance in z-direction
            
            Dist_X                      = 	abs(Dist_X)*2;
            Dist_Z                  	=  	abs(Dist_Z)*2;
            Distance                    =  	sqrt(Dist_X.^2 + Dist_Z.^2)/sqrt((dx)^2 + (dz)^2);
            
            % save memory & computations
            PartX_int                   =   ((PartX_in-1)*fac_x)+ix;
            PartZ_int                   =   ((PartZ_in-1)*fac_z)+iz;
            
            
            % Correct for boundaries
            PartX_int(isinf(PartX_int)) 	= 	NaN;
            PartZ_int(isinf(PartX_int))     = 	NaN;
            PartX_int(PartX_int>NxGrid)     = 	NaN;
            PartZ_int(PartZ_int>NzGrid)     = 	NaN;
            PartX_int(PartX_int<ix)         =  	NaN;
            PartZ_int(PartZ_int<iz)         =  	NaN;
            
            % Remove data points that are NaN
            if size(Part_data,1)==1
                ind_nonactive = find(isnan(PartX_int) | isnan(PartZ_int));
            else
                ind_nonactive = find(isnan(PartX_int(:)) | isnan(PartZ_int(:)));
            end
            active                      =   true(size(Part_data));
            active(ind_nonactive)       =   false;
            
            Weight                          =	(1-Distance);
            
            % remove NaN's to start with
            if ~isempty(ind_nonactive)
                Weight(ind_nonactive)       = [];
                PartX_int(ind_nonactive)    = [];
                PartZ_int(ind_nonactive)    = [];
            end
            
            
            
            PartX_int_vec   = [PartX_int_vec;   PartX_int(:)];
            PartZ_int_vec   = [PartZ_int_vec;   PartZ_int(:)];
            Weight_vec      = [Weight_vec;      Weight(:)];
            Part_active     = [Part_active;     active(:)];
            
        end
    end
    
    PartX_int_vec                   =   uint32(PartX_int_vec);
    PartZ_int_vec                   =   uint32(PartZ_int_vec);
    
    ind_activeParticles             =   find(Part_active==true);
    
    MAPDATA.ind_activeParticles     =   ind_activeParticles;      % points with NaN
    MAPDATA.Weight_vec              =   Weight_vec;
    MAPDATA.PartX_int_vec           =   PartX_int_vec;
    MAPDATA.PartZ_int_vec           =   PartZ_int_vec;
    MAPDATA.Part_active             =   Part_active;
    MAPDATA.NumberSweeps            =   NumberSweeps;
    
else
    
    % No need to recompute this
    ind_activeParticles     	= 	MAPDATA.ind_activeParticles;
    Weight_vec              	= 	MAPDATA.Weight_vec;
    PartX_int_vec               =	MAPDATA.PartX_int_vec;
    PartZ_int_vec               = 	MAPDATA.PartZ_int_vec;
    Part_active                 =   MAPDATA.Part_active;
    NumberSweeps                =   MAPDATA.NumberSweeps;
    
    
end

Part_data_vec   =   repmat(Part_data(:),[NumberSweeps, 1]);
Part_data_vec   =   Part_data_vec(ind_activeParticles);


% remove datapoints that are NaN
if any(isnan(Part_data_vec))
    % This doesn't happen all that often
    id                              =   find(isnan(Part_data_vec));
    if ~isempty(id)
        % remove those points
        Part_data_vec(id)   =   [];
        Weight_vec(id)      =   [];
        PartX_int_vec(id)   =   [];
        PartZ_int_vec(id)   =   [];
    end
    
    % Recreate Weight 2D matrix in this case
    [Weight_2D]                 =       sparse2(PartZ_int_vec,PartX_int_vec,Weight_vec,                       NzGrid, NxGrid);
    
else
    if isfield(MAPDATA,'Weight_2D')
        Weight_2D               =       MAPDATA.Weight_2D ;
    else
        try
            [Weight_2D]             =       sparse2(PartZ_int_vec,PartX_int_vec,Weight_vec,                       NzGrid, NxGrid);
        catch
            [Weight_2D]             =       sparse (double(PartZ_int_vec),double(PartX_int_vec),Weight_vec,       NzGrid, NxGrid);
        end
        
        MAPDATA.Weight_2D       =       Weight_2D;      % no need to compute this again
    end
    
end

% Map the particles to a regular grid
try
    Data_2D                     =   sparse2(PartZ_int_vec,PartX_int_vec,Weight_vec(:).*Part_data_vec(:),  NzGrid, NxGrid);
catch
    Data_2D                     =   sparse (double(PartZ_int_vec),double(PartX_int_vec),Weight_vec(:).*Part_data_vec(:),  NzGrid, NxGrid);
end

Data_2D                     =   full(Data_2D./Weight_2D);


if ~isfield(MAPDATA,'Elem_2D') & FindElements
    % find in which element every point is located.
    % WE CAN POTENTIALLY SPEED THIS UP BY USING KDTREE
    
    X                       =   MESH.NODES(1,:);
    Z                       =   MESH.NODES(2,:);
    Xc                      =   mean(X(MESH.ELEMS));
    Zc                      =   mean(Z(MESH.ELEMS));
    
    switch MESH.element_type.velocity
        case {'Q1','Q2'}
            Elem_Number             =   MESH.RegularElementNumber';
        case {'T1','T2'}
            Elem_Number            =   [1:size(MESH.ELEMS,2)];
            
    end
    
    try
        ReferencePts        =   [Xc(:) Zc(:)];
        tree                =   kdtree(ReferencePts);
        TestPoints          =   [MAPDATA.Xgrid(:), MAPDATA.Zgrid(:)];
        [ClosestPtIndex]  	=   kdtree_closestpoint(tree,TestPoints);
        Elem_2D             =   zeros(size( MAPDATA.Xgrid));
        Elem_2D(1:prod(size(Elem_2D)))             =   Elem_Number(ClosestPtIndex);
    catch
        Elem_2D                 =   griddata(Xc(:),Zc(:), Elem_Number(:), MAPDATA.Xgrid, MAPDATA.Zgrid,'nearest');
    end
    
    
    MAPDATA.Elem_2D         =   uint32(Elem_2D);
end

function  [ rho ]	= 	ComputeFluidDensity(INTP_PROPS,  Density)
% ComputeFluidDensity -  updates the permeability

% $Id: ComputeHeatCapacity.m 4919 2013-11-07 19:22:05Z lkausb $


Type        =   fieldnames(Density);
if size(Type,1) > 1
    error('You can currently only have one Fluid Density type activated simultaneously')
end

switch Type{1}
    case 'Constant'
        Density 	=       Density.Constant.Rho;
        
        rho         =       ones(size(INTP_PROPS.X))*Density;
        
    otherwise
        error('Unknown Fluid Density law')
end

end

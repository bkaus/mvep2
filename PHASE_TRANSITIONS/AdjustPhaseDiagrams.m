function [rho_Melt,rho_withoutMelt, Error, addRho] = AdjustPhaseDiagrams(rho, Melt, P_Pa, T_K, addRho)
% This takes the 'old' phase diagram structure and creates a new structure
% out of it, where we need information on 'rho_withoutMelt' and 'rho_melt'
% rather than just 'Melt' and 'rho'

% We automatically adjust 'rho_melt' to find the optimum value

% load(fname)

if exist('Melt')==0
    rho_Melt         =   [];
    rho_withoutMelt  = [];
    Error            = [];
    warning('no Melt present in diagram')
    return;
    
end


if max(Melt(:))==0
    rho_Melt         =   [];
    rho_withoutMelt  = [];
    Error            = [];
    warning('no Melt present in diagram')
    return
else
    % Melt is present
    
    % First, analyze all areas with pure melt (for melt density estimation)
    rho_Melt        =  	rho;
    ind             =   find(Melt<1);
    rho_Melt(ind)   =   NaN;
    if all(isnan(rho_Melt(:)))
        % all elements are NaN - no point with 100% melt available
        %
        % in this case we have to reconstruct density in a more complicated manner:
        
        % rho_tot = rho_s*(1-phi) + rho_f*phi,
        %       because now both rho_s and rho_f are unknown
        %
        % To solve, we make a few simplifying assumptions:
        %   a)  we assume that rho_s is a smooth functiom
        %   b)  we assume that it is computed as
        %           rho_s = rho_tot + phi*addRho
        %
        
        [addRho     ]               =   fminbnd(@(x1) FindSmooth_SolidDensity(x1,rho,Melt),1, 2500);
        [~, Error, rho_withoutMelt] =   ComputeError(addRho, rho,rho_Melt,Melt);
        rho_Melt =  (rho-rho_withoutMelt.*(1-Melt))./Melt;
    end
    
    
    % (linear) extrapolate melt density over the whole domain
    [T,P]   =   meshgrid(P_Pa,T_K);
    ind     =   find(isnan(rho_Melt));
    for ii=1:size(T,2)
        ind = find(isnan(rho_Melt(:,ii)));
        ind_1 = find(~isnan(rho_Melt(:,ii)));
        if ~isempty(ind)
            if length(ind_1)>2
                ind_1 = ind_1(2:end);
                ind = 1:ind_1(1);
                rho_Melt(ind,ii)       = interp1(P(ind_1,ii),rho_Melt(ind_1, ii),P(ind,ii),'linear','extrap');
            end
        end
    end
    
    for ii=1:size(T,1)
        ind = find(isnan(rho_Melt(ii,:)));
        ind_1 = find(~isnan(rho_Melt(ii,:)));
        if ~isempty(ind)
            if length(ind_1)>2
                rho_Melt(ii,ind)       = interp1(T(ii,ind_1),rho_Melt(ii,ind_1),T(ii,ind),'linear','extrap');
            end
        end
    end
    
    
    rho_Melt(isnan(rho_Melt))=min(rho_Melt(:));
    
    
    % Automatically find optimum solid density to minimize error
    if isempty(addRho)
        options                     = optimset('Display','iter');
        
        addRho                     = fminbnd(@(x1) ComputeError(x1,rho,rho_Melt,Melt),1, 2500, options);
        disp(['Optimal addRho = ',num2str(addRho)]);
        
    end
    [~, Error, rho_withoutMelt] = ComputeError(addRho, rho,rho_Melt,Melt);
    
    
    %     if display
    %
    figure(111),clf
    subplot(321)
    pcolor(T_K-273,P_Pa/1e8,rho'), shading flat, colorbar, title('rho'), colormap(jet)
    ylabel('P [kbar]')
    
    subplot(322)
    pcolor(T_K-273,P_Pa/1e8, [rho_withoutMelt.*(1-Melt) + rho_Melt.*Melt]'), shading flat, colorbar, colormap(jet)
    title('rho\_withoutMelt.*(1-Melt) + rho\_Melt.*Melt')
    
    subplot(323)
    pcolor(T_K-273,P_Pa/1e8,rho_withoutMelt'), shading flat, colorbar, title('rho w/out melt'), colormap(jet)
    ylabel('P [kbar]')
    
    subplot(324)
    pcolor(T_K-273,P_Pa/1e8,rho_Melt'), shading flat, colorbar, title('rho\_Melt'), colormap(jet)
    
    
    subplot(325)
    pcolor(T_K-273,P_Pa/1e8,Error'), shading flat, colorbar, title('rho - predicted rho'), colormap(jet)
    ylabel('P [kbar]')
    xlabel('T [Celcius]')
    
    
    subplot(326)
    pcolor(T_K-273,P_Pa/1e8,Melt'), shading flat, colorbar, title('Melt'), colormap(jet)
    xlabel('T [Celcius]')
    %     end
end

%--------------------------------------------------------------------------
function [Err, Error, rho_withoutMelt] = ComputeError(addRho, rho,rho_Melt,Melt)


rho_withoutMelt =   rho+Melt.*addRho;

rho_s           =   rho_withoutMelt.*(1-Melt) + rho_Melt.*Melt;

Error           =   rho_s-rho;
Err             =   norm(Error);


%--------------------------------------------------------------------------
function [Err, rho_withoutMelt] = FindSmooth_SolidDensity(addRho,rho, Melt)


rho_withoutMelt =       rho+Melt.*addRho;

Err            =       norm(del2(rho_withoutMelt));

% Since rho = rho_withoutMelt*(1-Melt) + rho_Melt*Melt, we can also compute
%



function surface_data = ExtractSurfaceDataFromMesh(MESH,CHAR,surface_data,filteropts)
%
% surface_data = ExtractSurfaceDataFromMesh(MESH,CHAR,surface_data)
% 
% Extract surface data of MESH
% =========================================================================

if ~isempty(MESH)
    Z                  = MESH.NODES(2,:);
    X                  = MESH.NODES(1,:);
    VELX               = MESH.VEL(1,:);
    VELZ               = MESH.VEL(2,:);
    Xgrid              = X(MESH.RegularGridNumber);
    Zgrid              = Z(MESH.RegularGridNumber);
    VXgrid             = VELX(MESH.RegularGridNumber);
    VZgrid             = VELZ(MESH.RegularGridNumber);    
    
    % output structure 
    surface_data.velx  = VXgrid(end,:)*CHAR.Velocity*CHAR.SecYear*100; % [cm/year]
    surface_data.velz  = VZgrid(end,:)*CHAR.Velocity*CHAR.SecYear*100; % [cm/year]    
    surface_data.topo  = Zgrid(end,:) *CHAR.Length;                    % [m]  
    surface_data.x     = Xgrid(end,:) *CHAR.Length;                    % [m]   
end

if isfield(surface_data,'velx')
    [surface_data.xreg,surface_data.velx_filt] = filter_surface_data(surface_data.x,surface_data.velx,filteropts);
end
if isfield(surface_data,'velz')
    [~                ,surface_data.velz_filt] = filter_surface_data(surface_data.x,surface_data.velz,filteropts);
end
if isfield(surface_data,'topo')
    [~                ,surface_data.topo_filt] = filter_surface_data(surface_data.x,surface_data.topo,filteropts);
end
if isfield(surface_data,'boug')
    [surface_data.xregboug       ,surface_data.boug_filt] = filter_surface_data(surface_data.survey_x,surface_data.boug,filteropts);
end



end % END OF FUNCTION

% =========================================================================
% sub functions 
% =========================================================================
function [xreg,yreg_filt]=filter_surface_data(x,y,filteropts)


type='butter_zerophase';
if isfield(filteropts,type)
    filteropts = filteropts_adddefaults(filteropts,type);
    xreg  = linspace(min(x),max(x),filteropts.(type).Nreg);
    yreg  = interp1(x,y,xreg);
    filteropts.(type).dxreg = mean(xreg(2:end)-xreg(1:end-1));
    yreg_filt = butter_zerophase(yreg,filteropts.(type));
end


% ... others

end

%--------------------------------------------------------------------------
function filteropts = filteropts_adddefaults(filteropts,type)

if ~isfield(filteropts.(type),'Nreg')
    filteropts.(type).Nreg = 250;
end

if ~isfield(filteropts.(type),'ord')
    filteropts.(type).ord = 2;
end

if ~isfield(filteropts.(type),'type')
    filteropts.(type).type = 'low';
end

if ~isfield(filteropts.(type),'lambda')
    filteropts.(type).lambda = 30e3;  % 50 km
end
end

%--------------------------------------------------------------------------
% butterworth zero-phase time/spatial domain (non-fft) filter
%
% opts.ord  = 2 or 4 : 2nd or 4th order are standard
% opts.type = 'low'   , 'high'   , 'bandpass'
% opts.lambda= [low_cf], [high_cf], [low_cf, high_cf]  
%
function yreg_filt=butter_zerophase(yreg,opts) 

[b,a] = butter(opts.ord, (1./opts.lambda) .* (2*opts.dxreg),opts.type);
yreg_filt=filtfilt(b,a,yreg);

end
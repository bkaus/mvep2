function [ NUMERICS,opts ] = SetDefaultInversionParameters(varargin)
%  Sets default inversion setup parameters
%
% [ NUMERICS,opts ] = SetDefaultInversionParameters(NUMERICS,opts)
%
% $Id: SetDefaultInversionParameters.m 4958 2013-11-11 14:02:59Z lkausb $
% =========================================================================


if nargin>0
    NUMERICS = varargin{1};
    opts     = varargin{2};
else
    NUMERICS = [];
    opts     = [];
end


% In any case Perform Stokes by default
opts.PerformStokes                                = true;


% Default options & error checking
if opts.PerformInversion
    if(~exist([opts.ReferenceDataName '.mat'],'file'))
        error(['The reference file ' opts.ReferenceDataName '.mat  does not exist']);
    end
    
    % output file name for forward models
    opts.InvOutName       =  ['InvMod_' num2str(opts.InvMpiID)];
    
    % no plots
    if ~isfield(opts,'CreatePlots')
        opts.CreatePlots                              =   false;
    end
    
    % timesteps to be performed
    NUMERICS.time_end                             =   6;
    
    % Don't save any output files
    NUMERICS.SaveOutput.NumberTimestepsToSave     = 1e9;
    NUMERICS.Breakpoints.SaveBreakpoint           = false;
    NUMERICS.Breakpoints.Restart                  = false;
    
        
    % Don't compute reference data
    opts.ComputeReferenceData                     = false;
    
    
    % In case of a gravity only inversion
    if opts.PerformGravityOnly
       opts.PerformStokes                         = false;
       opts.PerformGravityTest                    = false;
    end
    
    disp('=== Perform inversion ===');
    
end
    
   

    
    
    
    


end % END OF FUNCTION



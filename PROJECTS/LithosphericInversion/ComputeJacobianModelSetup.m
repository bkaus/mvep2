% This routine computes the Jacobian, or how much the misfit changes if I
% perturb one parameter by a factor eps .
%
% This is a relatively fast way 


eps         =   0.1;


% True parameters and variation (eps), used
P0          =   [1000,14.3103,14.4568,3.5000,3.5000,480, 530, 15,2,30,5,20,3100,3250,3200  ]; % "true parameters"
% Variation   =   [100 ,1      ,1      ,0.5,   0.5,   100, 100, 5, 5,5, 5, 5,  50, 25,  5    ]; % typical variation of those parameters


% TRUE MODEL RESULTS 50,   
P=P0;
misfit_True = LithSubInv(0,1,P(1),P(2),P(3),P(4),P(5),P(6),P(7),P(8),P(9),P(10),P(11),P(12),P(13),P(14),P(15));


ParameterNames = {'\sigma_{plast,max}'}


%matlabpool(4)


for i=1:length(P)
    P           =   P0;
%     eps         =	Variation(i);
    P(i)        =   P(i)*(1+eps);    % perturb parameter
    
    
    disp(['Computing results for model ',num2str(i)])
    misfit(i)   =   LithSubInv(0,1,P(1),P(2),P(3),P(4),P(5),P(6),P(7),P(8),P(9),P(10),P(11),P(12),P(13),P(14),P(15));
    
    Jacobian(i) =   (misfit(i)-misfit_True)/eps;
    
end

fntsize = 20;

figure(1), clf
bar(log10(Jacobian))
xlabel('Parameter number','fontsize',fntsize)
ylabel(' (\Delta R)/ (\Delta eps) ','fontsize',fntsize)
title('Sensitivity of inversion setup ','fontsize',fntsize)
set(gca,'Fontsize',fntsize)

% Models salt tectonics in the presence of fast erosion and deposition of material 

% $Id$

% Add paths & subdirectories
AddDirectories;

clear

tic

if ismac
    CreatePlots         =   logical(1);
else
    CreatePlots         =   logical(0);
end

factor              =   1;
SecYear             =   3600*24*365.25;
NUMERICS.Restart    =   logical(0);
NUMERICS.dt_max     =   100e3*SecYear;

%% Create mesh
opts.element_type   =   'quad4';
mesh_input.x_min    =   -2e3;
mesh_input.x_max    =    2e3;
mesh_input.z_min    =   -4e3;
mesh_input.z_max    =    0e3;
% mesh_input.FixedAverageTopography = 0;

opts.nx             =   100*factor;
opts.nz             =   300*factor;
dt                  =   2000*SecYear;       % initial dt

% Parameters that will be changed during the simulations
Mu_Background           =   1e16; % Pa/s
Mu_Overburden           =   1e23;
Mu_Overburden2          =   1e23;
Mu_Zechstein            =   1e17;
Mu_Anhydrit             =   5e20;


% Parameters that are fixed:
e_bg                    =   0;      % background strainrate: positive=extension, negative = compression (e.g. -1e-15)
SedVel_cmyear           =   0.05; %cm/year
FrictionAngle           =   30;
Cohesion                =   20e6;
MaxYield                =   1000e6;

Rho_Background          =   1000; % kg/m3
Rho_Overburden          =   2600;
Rho_Overburden2         =   2600;
Rho_Zechstein           =   2200;
Rho_Anhydrit            =   2900;



%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values

%----------------------------------
%Free-Air/Sticky water
MATERIAL_PROPS      =  [];
Phase            	=  1;

%Viscous
Type                =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu                          =    Mu_Background;

Type              	=   'Constant';
MATERIAL_PROPS(Phase).Density.(Type).Rho                           =    Rho_Background;
%----------------------------------

%----------------------------------
%Overburden #1
Phase            	=  2;

%Viscous
Type                =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu                          =    Mu_Overburden;

%Density
Type              	=   'Constant';
MATERIAL_PROPS(Phase).Density.(Type).Rho                           =    Rho_Overburden;

% %Plasticity
% Type                =   'DruckerPrager';                                         	% Plasticity Model
% MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   FrictionAngle;       % Friction Angle
% MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   Cohesion;         	% Cohesion
%

%----------------------------------
%Overburden #2

Phase            	=  3;

%Viscous
Type                =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu                          = Mu_Overburden2;

%Density
Type              	=   'Constant';
MATERIAL_PROPS(Phase).Density.(Type).Rho                           =   Rho_Overburden2;

% %Plasticity
% Type                =   'DruckerPrager';                                        	% Plasticity Model
% MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   FrictionAngle;       % Friction Angle
% MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   Cohesion;         	% Cohesion
%----------------------------------------


%----------------------------------------
%Zechsteinsalt

Phase               =   4;

% Density
Type              	=   'Constant';
MATERIAL_PROPS(Phase).Density.(Type).Rho                           =  Rho_Zechstein;

%Viscous
Type              	=   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu                           =  Mu_Zechstein;

%
%----------------------------------------

%----------------------------------
%Anhydritlayer

Phase            	=  5;

%Viscous
Type                =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu                       = Mu_Anhydrit;

%Density
Type              	=   'Constant';
MATERIAL_PROPS(Phase).Density.(Type).Rho                        = Rho_Anhydrit;

% %Plasticity
% Type                =   'DruckerPrager';                                           	% Plasticity Model
% MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle   =   FrictionAngle;       	% Friction angle
% MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion        =   Cohesion;               %
%----------------------------------

%----------------------------------
%Overburden #3

Phase            	=  6;

%Viscous
Type                =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu                          = Mu_Overburden2;

%Density
Type              	=   'Constant';
MATERIAL_PROPS(Phase).Density.(Type).Rho                           =   Rho_Overburden2;

% %Plasticity
% Type                =   'DruckerPrager';                                        	% Plasticity Model
% MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   FrictionAngle;       % Friction Angle
% MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   Cohesion;         	% Cohesion
%----------------------------------------


%----------------------------------

% Gravitational acceleration
MATERIAL_PROPS(1).Gravity.Value                 =   10;

% Add material properties that were not defined here, but which MILAMIN_VEP needs to know
MATERIAL_PROPS                                 =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required


%% Numerical parameters
NUMERICS.Viscosity.LowerCutoff                  =   1e15;
NUMERICS.Viscosity.UpperCutoff                  =   1e25;
NUMERICS.Plasticity.MaximumYieldStress          =   MaxYield;
NUMERICS                                        =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required



%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{1};
BC.Stokes.Left          =   Bound{3};
BC.Stokes.Right         =   Bound{3};
BC.Stokes.Top           =   Bound{3};
BC.Stokes.BG_strrate    =   e_bg;

% Thermal boundary condition
%                                       1              2           3
BoundThermal          =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   20 + 273;
BC.Energy.Value.Bottom=   320 + 273;                    % T at bottom of mantle


%% Create initial Particles
numz                        =   1200*factor;
numx                        =   round((mesh_input.x_max-mesh_input.x_min)/(mesh_input.z_max-mesh_input.z_min))*numz;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (mesh_input.z_max-mesh_input.z_min)/numz;
[PARTICLES.x,PARTICLES.z]   =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,mesh_input.z_min:dz:mesh_input.z_max);
PARTICLES.x                 =   PARTICLES.x(:);
PARTICLES.z                 =   PARTICLES.z(:);
PARTICLES.x                 =   PARTICLES.x + 1*[rand(size(PARTICLES.x))-0.5]*dx;
PARTICLES.z                 =   PARTICLES.z + 1*[rand(size(PARTICLES.x))-0.5]*dz;

% add refined particles
%numz                        =   1000*factor;
%numx                        =   round((mesh_input.x_max-mesh_input.x_min)/(mesh_input.z_max-mesh_input.z_min))*numz;
%dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
%dz                          =   (mesh_input.z_max-0.4*mesh_input.z_min)/numz;
%[PARTICLES1.x,PARTICLES1.z] =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,0.4*mesh_input.z_min:dz:mesh_input.z_max);
%PARTICLES1.x            	=   PARTICLES1.x(:);
%PARTICLES1.z            	=   PARTICLES1.z(:);
%PARTICLES1.x              	=   PARTICLES1.x + 1*[rand(size(PARTICLES1.x))-0.5]*dx;
%PARTICLES1.z            	=   PARTICLES1.z + 1*[rand(size(PARTICLES1.x))-0.5]*dz;

%PARTICLES.x                 =   [PARTICLES.x; PARTICLES1.x ];
%PARTICLES.z                 =   [PARTICLES.z; PARTICLES1.z ];

PARTICLES.phases            =   ones(size(PARTICLES.x));
PARTICLES.HistVar.T         =   293.15 + 0.03*abs(PARTICLES.z);        % Kelvin                                       

%% Set phases
PARTICLES.phases            = ones(size(PARTICLES.x))*4;       % set everything to be salt


% Free-Air/Water
ind                         =   find(PARTICLES.z>-3*1e3);
PARTICLES.phases(ind)       =   1;

%Overburden - left 
ind                         = find(PARTICLES.z<= -3*1e3 & PARTICLES.z> -3.3*1e3 & PARTICLES.x>= -2*1e3 & PARTICLES.x<= -0.5*1e3);
PARTICLES.phases(ind)       = 2;

%Overburden - right
ind                         = find(PARTICLES.z<= -3*1e3 & PARTICLES.z> -3.3*1e3 & PARTICLES.x>= 0.5*1e3 & PARTICLES.x<= 2*1e3);
PARTICLES.phases(ind)       = 2;

% Anhydrite Layer 1
ind                         = find(PARTICLES.z<= -3.7*1e3 & PARTICLES.z>= -3.75*1e3);
PARTICLES.phases(ind)       = 5;


%% Specify sedimentation parameters


SedVel                      =   SedVel_cmyear*1e-2/SecYear ; %m/s
InitialErosionLevel         =   -3e3; % in m

%     
NUMERICS.dt_max                                                     =   10e3*SecYear;
NUMERICS.ErosionSedimentation.Method                                =   'FastErosionWithSedimentation_Particles';
NUMERICS.ErosionSedimentation.Sedimentation_Velocity                =   SedVel;                          % m/s
NUMERICS.ErosionSedimentation.ElevationAboveWhichToApplyErosion  	=   InitialErosionLevel;            % in m
NUMERICS.ErosionSedimentation.AirPhase                              =   1;                              % phase of air/water
NUMERICS.ErosionSedimentation.SedimentPhases                     	=   [2 3 6];                          % phases of the sediments
NUMERICS.ErosionSedimentation.SedimentationTimeInterval_years     	=   50e3;                          % for how long do we sediment a specific sediment number (in years)?
NUMERICS.ErosionSedimentation.InfinitelyFastErosion                 =   logical(0);




%% Non-dimensionalize input parameters
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]         =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,[]);


dt                  =   dt/CHAR.Time;
time                =    0;

%% Load breakpoint file if required
Load_BreakpointFile


%% Plot initial particles
if 1==1 & CreatePlots
    Style	= {'*r', '+g', '*y', '.b', '*k', '.m', 'd'};
    %     if max(Point_id)>1
    %         for i=1:max(Point_id)
    %             Ind	= find(Point_id==i);
    %             a = mod(i,size(Style))+1;
    %             plot(GCOORD(1,Ind), GCOORD(2,Ind), Style{a(2)});
    %         end
    %     end
    
    % Create mesh
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);
    
    figure(1), clf, hold on
    PlotMesh(MESH,[],CHAR.Length/1e3,'r.-'),
    
    for i=1:max(PARTICLES.phases)
        ind = find(PARTICLES.phases==i );
        plot(PARTICLES.x(ind)*CHAR.Length/1e3,PARTICLES.z(ind)*CHAR.Length/1e3,Style{i})
    end
    
end

for itime=NUMERICS.time_start:1000
    start_cpu = cputime;
    
    %% Create mesh
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);
    
    %% Compute Stokes & Energy solution
    [PARTICLES, MESH, INTP_PROPS] 	=   StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    
    %% Advect particles and mesh - mesh is recreated every timestep
    PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
    MESH.NODES      =   MESH.NODES  + MESH.VEL*dt;
    time            =   time+dt;
        
    %% Apply sedimentation and fast erosion
    [ MESH, PARTICLES, NUMERICS]= Apply_ErosionSedimentation(MESH,NUMERICS,dt, PARTICLES, time);
    
  
    %% Compute new timestep
    dt                          =   dt*1.25;
    dx_min                      =   min(MESH.dx_min, MESH.dz_min);
    CourantFactor               =   2;
    dt_courant                  =   CourantFactor*dx_min/max(abs(MESH.VEL(:)));
    dt                          =   min([dt NUMERICS.dt_max dt_courant]);                                  % ensure it is not larger than a given maximum
  
    %% Plot results
    if mod(itime,5)==0 & CreatePlots
        
        % Particles and velocities
        figure(1), clf, hold on
        PlotMesh(MESH,[],CHAR.Length/1e3,'r.-'),
        ind = find(PARTICLES.phases==1); % air
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'ro')
        ind = find(PARTICLES.phases==2);% overb 1
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'go')
        ind = find(PARTICLES.phases==3);% overb 2
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'bo')
        ind = find(PARTICLES.phases==4);% salt
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'mo')
        ind = find(PARTICLES.phases==5);% anhydrite
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'y.')
        
        %         quiver(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(1,:),MESH.VEL(2,:))
        axis equal, axis tight
        title(['Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),'Myrs'])
        drawnow
        
        
        % Temperature
        figure(2), clf
        PlotMesh(MESH,MESH.TEMP*CHAR.Temperature-273, CHAR.Length/1e3), axis equal, axis tight, colorbar; shading flat
        axis equal, axis tight, colorbar, title('Temperature [C]')
        
        figure(3), clf
        PlotMesh(MESH,log10(MESH.CompVar.Mu_Eff*CHAR.Viscosity), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('log10(effective viscosity)')
        
        figure(4), clf
        PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('log10(e2nd [1/s])')
        
        figure(5), clf
        PlotMesh(MESH,(MESH.CompVar.T2nd*(CHAR.Stress/1e6)), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('(T2nd [MPa])')
        
        figure(7), clf
        PlotMesh(MESH,(MESH.CompVar.Rho*(CHAR.Density)), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('Density [kg/m^3]')
        
        
        figure(8), clf
        X=MESH.NODES(1,:);Z=MESH.NODES(2,:);
        X2d = X(MESH.RegularGridNumber)*CHAR.Length;
        Z2d = Z(MESH.RegularGridNumber)*CHAR.Length;
        plot(X2d(end,:)/1e3,Z2d(end,:))
        xlabel('Width [km]')
        ylabel('Topography [m]')
        drawnow
        
        Z   = MESH.NODES(2,:);
        Z2d = Z(MESH.RegularGridNumber)*CHAR.Length/1e3;
        T2nd = MESH.CompVar.T2nd(MESH.RegularGridNumber)*CHAR.Stress/1e6;
        
        figure(3), drawnow
        
        
        
    end
    
    % Create breakpoint file if required
    Save_BreakpointFile;
    
    % Save filename if required
    if mod(itime,20)==0
        SaveFile(itime, PARTICLES, NUMERICS,MESH,INTP_PROPS,CHAR,BC, time);
    end
    
    
    end_cpu         = cputime;
    time_timestep    = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3),' kyrs; dt=',num2str(dt*CHAR.Time/CHAR.SecYear/1e3),' kyrs'])
    disp(' ')
    
end

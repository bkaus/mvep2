function GEOMOD_PROPS = Compute_Geomod2008_PROPERTIES(MESH, INTP_PROPS, CHAR)
%
% 
% Computes the various parameters that are required for the benchmark

X                   =   MESH.NODES(1,:)*CHAR.Length;
Z                   =   MESH.NODES(2,:)*CHAR.Length;

x2d                 =   X(MESH.RegularGridNumber)*1e2;
z2d                 =   Z(MESH.RegularGridNumber)*1e2;

HorizontalMotion_cm = (35-x2d(end,end)); % horizontal motion in cm's
disp(['Horizontal motion = ',num2str(HorizontalMotion_cm),' cm'])

GEOMOD_PROPS.HorizontalMotion_cm = HorizontalMotion_cm;


% % Dissipation of energy
Sxx = -INTP_PROPS.Pressure + INTP_PROPS.Stress.Txx;
Syy = -INTP_PROPS.Pressure + INTP_PROPS.Stress.Tyy;
Sxy =                  INTP_PROPS.Stress.Txy;
% Diss        = mean(Sxx.*INTP_PROPS.Exx(:,1:4) + Syy.*STRAINRATE_INTP.Eyy(:,1:4) + 2*Sxy.*STRAINRATE_INTP.Exy(:,1:4),2);
% Dissipation = 1/2*sum(Diss.*AREA(:,1));                                 % total dissipation
% 
% % Gravitational work
% GravWork = sum(mean(INTP_PROPS.RhoG.*VELOCITY_INTP.Vy,2).*AREA(:,1));
% 
% % RMS velocity ala Susanne's definition
% Vrms     =  sqrt( sum(Vx2d(:).^2 + Vz2d(:).^2)/(nx*nz)  );
% 
% 
% % Store time-dependent properties:
% HorMotionTime(itime)    =   HorizontalMotion_cm;
% ForceTime(itime)        =   Force;
% DissTime(itime)         =   Dissipation;
% GravWorkTime(itime)     =   GravWork;
% VrmsTime(itime)         =   Vrms;

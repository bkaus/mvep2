function [mesh_input,PARTICLES, MESH] = AdaptMesh_ToFoil_GEOMOD(MESH,BC,PARTICLES, mesh_input, opts, FoilPhase)
% Modifies mesh input for the geomod setup such that the foil is taken into
% account @ side and bottom boundaries
%

% Add side height
if ~isempty(MESH)
    X                           =   MESH.NODES(1,:);
    Z                           =   MESH.NODES(2,:);
    x2d                         =   X(MESH.RegularGridNumber);
    z2d                         =   Z(MESH.RegularGridNumber);
    
    mesh_input.x_min            =   x2d(1,1);
    mesh_input.x_max            =   x2d(end,end);
    mesh_input.z_min            =   z2d(1,1);
    mesh_input.z_max            =   max(z2d(:));
    
    Topo_x                      =   x2d(end,:);
    Topo_z                      =   z2d(end,:);
    
    ind                         =   find(Topo_x>x2d(end,end)-BC.Stokes.ConstantVelocity.GeomodFoilBackThickness);
    factor                      =   (ind-ind(1)+1)./(length(ind));
    
    Topo_z(ind)                 =   max(Topo_z(1:ind(1)-1)) + factor.*BC.Stokes.ConstantVelocity.Added_Side_Height;
    z2d(end,:)                  =   Topo_z;
   
    mesh_input.Top_x            =   Topo_x;
    mesh_input.Top_z            =   Topo_z;
    
    MESH.NODES(1,:)             =   x2d(MESH.RegularGridNumber(:));
    MESH.NODES(2,:)             =   z2d(MESH.RegularGridNumber(:));
    

end

% Adapt x_vec to foil
x_vec       =   0:1/(opts.nx-1):1;
x_vec       =   x_vec*(mesh_input.x_max-mesh_input.x_min) + mesh_input.x_min;

x_vec_back  =   max(x_vec)-BC.Stokes.ConstantVelocity.GeomodFoilBackThickness;
[dum,ind]   =   min(abs(x_vec-x_vec_back));
x_vec(ind)  =   x_vec_back;

% Adapt z_vec to foil
z_vec       =   mesh_input.z_min:(mesh_input.z_max-mesh_input.z_min)/(opts.nz-1):mesh_input.z_max;
z_vec       =   z_vec.^1.0;
z_vec_old   =   z_vec;

z_vec       = z_vec(1:2:end);
[dum,ind]   =   min(abs(z_vec-BC.Stokes.ConstantVelocity.GeomodFoilThickness));
if ind==1
    ind=2;
end

z_vec(ind)              =   BC.Stokes.ConstantVelocity.GeomodFoilThickness;
z_vec_old(1:2:end)      =   z_vec;
z_vec_old(2:2:end-1)    =   (z_vec_old(1:2:end-2)+z_vec_old(3:2:end))/2;
z_vec                   =   z_vec_old;


indTopOfFoil            =   find(z_vec==BC.Stokes.ConstantVelocity.GeomodFoilThickness);

% Store data
mesh_input.x_vec        =   x_vec;
mesh_input.z_vec        =   z_vec;




% Adapt PARTICLES to foil

% sand particles below
ind = find(PARTICLES.z<BC.Stokes.ConstantVelocity.GeomodFoilThickness & PARTICLES.phases ~= FoilPhase);
PARTICLES.phases(ind) = FoilPhase;

% foil particles above = set to 1
ind = find(PARTICLES.z>=BC.Stokes.ConstantVelocity.GeomodFoilThickness & PARTICLES.phases == FoilPhase);
PARTICLES.phases(ind) = 1;

% sand particles to right of foil
ind = find(PARTICLES.x>x_vec_back & PARTICLES.phases ~= FoilPhase);
PARTICLES.phases(ind) = FoilPhase;

% foil particles to left of foil
ind = find(PARTICLES.x<=x_vec_back & PARTICLES.z>=BC.Stokes.ConstantVelocity.GeomodFoilThickness & PARTICLES.phases == FoilPhase);
PARTICLES.phases(ind) = 1;



MESH=[];

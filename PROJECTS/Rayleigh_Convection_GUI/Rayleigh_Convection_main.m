function [ SUCCESS ] = Rayleigh_Convection_main( handles,SUCCESS )
AddDirectories;
tic



Directory = (get(handles.SimulationName,'String'));
home_directory = pwd;
mkdir(Directory)

value   =   get(handles.TimestepsToSave,'Value');
String  =   get(handles.TimestepsToSave,'String');
NoSave        =   str2num(String{value});    % aspect_ratio

%% Create mesh
opts.element_type   =   'quad4';

value   =   get(handles.aspect_ratio,'Value');
String  =   get(handles.aspect_ratio,'String');
aspect_ratio        =   str2num(String{value});    % aspect_ratio

mesh_input.z_min    =   0;
mesh_input.z_max    =   1;
mesh_input.x_min    =   0;
mesh_input.x_max    =   mesh_input.z_max * aspect_ratio;

value   =   get(handles.no_nodes,'Value');
String  =   get(handles.no_nodes,'String');
opts.nx        =   str2num(String{value});    % aspect_ratio

opts.nz = opts.nx*aspect_ratio;

%% Create Particles & set phases
n_markers           =   (opts.nx*6)*(opts.nz*6);    % include 30 particles in every cell
[X, Z]              =   meshgrid(linspace(0,1,ceil(sqrt(n_markers))));
dx=diff(unique(X)); dx=dx(1);
dz=diff(unique(Z)); dz=dz(1);
PARTICLES.x         =   X(:)' +  1*(rand(size(X(:)'))-0.5)*dx;
PARTICLES.z         =   Z(:)' +  1*(rand(size(X(:)'))-0.5)*dz;
PARTICLES.x(PARTICLES.x<0) = 0;
PARTICLES.x(PARTICLES.x>1) = 1;
PARTICLES.z(PARTICLES.z<0) = 0;
PARTICLES.z(PARTICLES.z>1) = 1;



PARTICLES.phases    =   ones(size(PARTICLES.x),'uint32');
PARTICLES.HistVar.T =   (1-PARTICLES.z)*1+0.0 +  rand(size(PARTICLES.z))*0.01;                        % temperature

PARTICLES.HistVar.Txx =   0* PARTICLES.HistVar.T;
PARTICLES.HistVar.Tyy =   0* PARTICLES.HistVar.T;
PARTICLES.HistVar.Txy =   0* PARTICLES.HistVar.T;


%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values

% Upper layer
Phase                                       =   1;

value   =   get(handles.visc_type,'Value');
String  =   get(handles.visc_type,'String');
Type        =   (String{value});    % aspect_ratio

switch Type
    case 'Constant'
        % Isoviscous case
        MATERIAL_PROPS(Phase).Viscosity.(Type).Mu 	=   1; 

    case 'TemperatureDependent'
        value   =   get(handles.ref_visc,'Value');
        String  =   get(handles.ref_visc,'String');
        MATERIAL_PROPS(Phase).Viscosity.(Type).eta0        =   str2num(String{value});    % aspect_rati
        
        MATERIAL_PROPS(Phase).Viscosity.(Type).A    =   5;
end

%


Type                                        =   'RayleighConvection';       % type of density law we can employ
MATERIAL_PROPS(Phase).Density.(Type).Rho 	=   1;                          %
String  =   get(handles.edit1,'String');
MATERIAL_PROPS(Phase).Density.(Type).Ra        =   str2num(String);    % aspect_rati

if MATERIAL_PROPS(Phase).Density.(Type).Ra >5e6
    MATERIAL_PROPS(Phase).Density.(Type).Ra = 5e6;
end


MATERIAL_PROPS(Phase).Conductivity.Constant.k   	=   1;                       % conductivity
MATERIAL_PROPS(Phase).HeatCapacity.Constant.Cp      =   1;                   	% heat capacity



% Add material properties that were not defined here, but which MILAMIN_VEP
% needs to know
MATERIAL_PROPS                          =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required

MATERIAL_PROPS(1).Gravity.Value = 1;

t_diff_interpolate = (dx^2)/(MATERIAL_PROPS(1).Conductivity.Constant.k/(MATERIAL_PROPS(1).Density.RayleighConvection.Rho*MATERIAL_PROPS(1).HeatCapacity.Constant.Cp));
co = 1;
% time after which the whole system gets interpolated (not only incremental)

%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress','Periodic'};
BC.Stokes.Bottom        =   Bound{2};
BC.Stokes.Left          =   Bound{2};
BC.Stokes.Right         =   Bound{2};
BC.Stokes.Top           =   Bound{2};
BC.Stokes.BG_strrate    =   0;

% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   0;
BC.Energy.Value.Bottom=   1;                    % T at bottom of mantle



NUMERICS.Viscosity.LowerCutoff = 1e-10;
NUMERICS.Viscosity.UpperCutoff = 1e10;
NUMERICS.Nonlinear.MinNumberIterations = 0;
NUMERICS.Nonlinear.MaxNumberIterations = 1;
NUMERICS.Nonlinear.Tolerance = 1;
NUMERICS.Timestepping.Method = 'Euler';

NUMERICS                =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required

%% Non-dimensionalize
% since we run in ND units, set them all to 1
CHAR.Length             =   1;
CHAR.Time               =   1;
CHAR.Viscosity          =   1;
CHAR.Temperature     	=   1;

[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,CHAR);


%% Create mesh
%%%%%%%%%%%%%%%% CHANGED WITH RESPECT TO THE END GUI %%%%%%%%%%%%%%%%%
% refine the upper boundary
refine_x                =   [0    0.5   1];
refine_z                =   [0.15  1     0.15];
z                       =   GenerateGrid_1D(refine_x , refine_z, opts.nz);

mesh_input.z_vec        =   z;

%%%%%%%%%%%%%%%% CHANGED WITH RESPECT TO THE END GUI %%%%%%%%%%%%%%%%%
[MESH]                      =   CreateMesh(opts, mesh_input);


figure(3)
PlotMesh(MESH);

dt                  =   1e-6;

time               =    0;
value   =   get(handles.max_time,'Value');
String  =   get(handles.max_time,'String');
time_max        =   str2num(String{value});    

itime = 0;

Pos = get(handles.axes2,'Position');
axe_scaling(1:3) = Pos(1:3);
axe_scaling(4) = Pos(4)/aspect_ratio;
set(handles.axes2,'Position',axe_scaling)

if aspect_ratio == 1
    shift =0;
elseif aspect_ratio == 2
    shift = axe_scaling(4)*((aspect_ratio)/(2*aspect_ratio));
elseif aspect_ratio == 3
    shift = axe_scaling(4);
elseif aspect_ratio == 4
    shift = axe_scaling(4)*((3*aspect_ratio)/(2*aspect_ratio));
end

axe_scaling(2) = axe_scaling(2) + shift;
axe_scaling(3) = axe_scaling(3)/aspect_ratio;

set(handles.axes2,'Position',axe_scaling)
set(handles.Stop,'Value',1)


while time<=time_max && SUCCESS == 1
    
    if get(handles.Stop,'Value') == 0
        SUCCESS = 0;
        break
    end
    
    start_cpu = cputime;
    itime = itime + 1;
    
    %% Compute Stokes (& Energy solution if required)
    [PARTICLES, MESH, INTP_PROPS] 	 = StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    if get(handles.Stop,'Value') == 0
        SUCCESS = 0;
        break
    end
    
    %% Advect particles [MESH is Eulerian in this simulation]
    PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
    time            =   time+dt;
    
    
    
    %% Compute new timestep
    if itime > 20
    X               =   MESH.NODES(1,:);
    Z               =   MESH.NODES(2,:);
    dx              =   mean(sqrt(diff(X(MESH.ELEMS(1:2,:))).^2 + diff(Z(MESH.ELEMS(1:2,:))).^2));
    
    Vz              =   max(PARTICLES.Vz);
    CFL             =   0.25;
    dt              =   CFL*dx/Vz;
    
    if dt>1e-2
        dt=1e-2;
    end
    end
    
    if time>co*t_diff_interpolate
        NUMERICS.InterpolationMethod.InterpolateOnlyIncrements = logical(0);
        co = co+1;
    else
        NUMERICS.InterpolationMethod.InterpolateOnlyIncrements = logical(1);
    end
    
     % Store some data
    dx = max(diff(MESH.NODES(1,MESH.ELEMS(:,1))));
    dz = max(diff(MESH.NODES(2,MESH.ELEMS(:,1))));
    
    
    Vx = MESH.VEL(1,:);
    Vz = MESH.VEL(2,:);
    Vx_mean = mean(Vx(MESH.ELEMS),1);
    Vz_mean = mean(Vz(MESH.ELEMS),1);
    Vrms(itime)     =   sqrt(sum((Vx_mean.^2 + Vz_mean.^2)*dx*dz))/1;
    Time_vec(itime) = time;
    
    T = MESH.TEMP(1,:);
    T = T(MESH.RegularGridNumber);
    T1 = T(end,:);
    T2 = T(end-1,:);
    T3 = T(1,:);
    
    for i = 1:1:opts.nx
        T_differ_top = (T1(1,i) - T2(1,i))/dz;
    end
    
    T_differ_top = (mean(T_differ_top));
    T_differ_bot = (mean(T3));
    
    NN(itime)   = -mesh_input.z_max * ((T_differ_top)/(T_differ_bot));                                 % Nusselt number
   

    
    
    %% Plot results
    %%%%%%%%%%%%%%%%% CHANGED %%%%%%%%%%%%%%%%%%%%%%%%%
    if mod(itime,10)==0
        %%%%%%%%%%%%%%%%% CHANGED %%%%%%%%%%%%%%%%%%%%%%%%%
        
        axes(handles.axes1),cla
        PlotMesh(MESH,MESH.TEMP); axis equal, axis tight, colorbar, shading interp;
        hold on
        ind = find(PARTICLES.phases==2);
        plot(PARTICLES.x(ind), PARTICLES.z(ind),'o')
        hold on
        quiver(MESH.NODES(1,1:aspect_ratio:end),MESH.NODES(2,1:aspect_ratio:end),MESH.VEL(1,1:aspect_ratio:end),MESH.VEL(2,1:aspect_ratio:end),'k')
        xlabel('Width')
        ylabel('Depth')
        axis equal, axis tight
        title(['Temperature at time=',num2str(time)])
        
        axes(handles.axes2),cla
        T_field = mean(MESH.TEMP(MESH.RegularGridNumber),2);
        Z = MESH.NODES(2,:);
        Z = Z(MESH.RegularGridNumber);
        Z = mean(Z,2);
        plot(T_field,Z,'r-','Linewidth',5)
        xlabel('Temperature')
        ylabel('Depth')
        drawnow
        
        axes(handles.axes3),cla
        text(0,0.5,(['Finished Timestep ',num2str(itime),' with dt = ',num2str(dt)]))
        title(['Simulation'])
        
    if get(handles.Stop,'Value') == 0
        SUCCESS = 0;
        break
    end
          
    end
    
    if mod(itime,NoSave) == 0
        FiguresVisible = 'off';
        
        h1 = figure(1); clf, hold on
        set(h1,'Visible',FiguresVisible);
        
        subplot(1,2,2)
        PlotMesh(MESH,MESH.TEMP); axis equal, axis tight, colorbar, shading interp;
        hold on
        ind = find(PARTICLES.phases==2);
        plot(PARTICLES.x(ind), PARTICLES.z(ind),'o')
        hold on
        quiver(MESH.NODES(1,1:aspect_ratio:end),MESH.NODES(2,1:aspect_ratio:end),MESH.VEL(1,1:aspect_ratio:end),MESH.VEL(2,1:aspect_ratio:end),'k')
        xlabel('Width')
        ylabel('Depth')
        axis equal, axis tight
        if aspect_ratio == 1
            set(gca,'Position',[ 0.4703    0.1    0.3347*(1.1)    0.8150*(1.1)]);
        elseif aspect_ratio == 2
            set(gca,'Position',[0.4703   0.1    0.3347*(1.2)    0.8150*(1.2)]);
        elseif aspect_ratio == 3
            set(gca,'Position',[ 0.4703   0.1    0.3347*(1.2)    0.8150*(1.2)]);
        elseif aspect_ratio == 4
            set(gca,'Position',[ 0.4703   0.1    0.3347*(1.2)    0.8150*(1.2)]);
        end
        
        subplot(1,2,1)
        T_field = mean(MESH.TEMP(MESH.RegularGridNumber),2);
        Z = MESH.NODES(2,:);
        Z = Z(MESH.RegularGridNumber);
        Z = mean(Z,2);
        plot(T_field,Z,'r-','Linewidth',5),box on;
        xlabel('Temperature')
        ylabel('Depth')
        if aspect_ratio == 1
            set(gca,'Position',[0.18  0.29     0.2092    0.2812*1.8]);
        elseif aspect_ratio == 2
            set(gca,'Position',[0.2  0.45    0.2092/2    0.2812]);
        elseif aspect_ratio == 3
            set(gca,'Position',[ 0.2     0.51    0.2092/2    0.2812/1.7]);
        elseif aspect_ratio == 4
            set(gca,'Position',[ 0.2     0.53    0.2092/2    0.2812/2]);
        end
        
        set(gcf,'Color',[255 255 255]./255);
        fname = ([num2str(Directory),'_','Temperature','_',num2str(100000+itime)]);
        cd(Directory)
        print(h1,'-dpng',fname)
        cd(home_directory)
        
        
        h2 = figure(2); clf, hold on
        set(h2,'Visible',FiguresVisible);
        X = MESH.NODES(1,:);
        X = X(MESH.RegularGridNumber);
        X = X(1,:);
        
        subplot(2,1,1)
        plot(Time_vec,NN,'r-','Linewidth',5),box on;
        xlabel('Time')
        ylabel('Nusselt Number')
        
        subplot(2,1,2)
        plot(Time_vec,Vrms,'r-','Linewidth',5),box on;
        xlabel('Time')
        ylabel('Velocity root mean square')
        
        set(h2,'Visible',FiguresVisible);
        set(gcf,'Color',[255 255 255]./255);
        fname = ([num2str(Directory),'_','VRMS_and_Nusselt','_',num2str(100000+itime)]);
        cd(Directory)
        print(h2,'-dpng',fname)
        cd(home_directory)
        
        
        axes(handles.axes3),cla
        text(0,0.5,(['Saved pictures in folder: ',Directory]))
        title(['Simulation'])
       
        
    if get(handles.Stop,'Value') == 0
        SUCCESS = 0;
        break
    end
    end
end

cd(home_directory)
% Set back the axes to the original size
if aspect_ratio == 1
    shift = 0;
elseif aspect_ratio == 2
    shift = axe_scaling(4)*((aspect_ratio)/(2*aspect_ratio));
elseif aspect_ratio == 3
    shift = axe_scaling(4);
elseif aspect_ratio == 4
    shift = axe_scaling(4)*((3*aspect_ratio)/(2*aspect_ratio));
end
axe_scaling(2) = axe_scaling(2) - shift;
axe_scaling(3) = axe_scaling(3)*aspect_ratio;
axe_scaling(4) = axe_scaling(4)*aspect_ratio;
set(handles.axes2,'Position',axe_scaling)


if time>=time_max
    SUCCESS = 1;
end

end


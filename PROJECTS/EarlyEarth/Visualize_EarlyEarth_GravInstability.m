function [] =  Visualize_EarlyEarth_GravInstability(varargin)

% -------------------------------------------------------------------------
% Creates a visualization of:
% 1. Composition
% 2. Strain rate
% 3. Viscosity
% 4. Density
% 5. Stress
% 6. Temperature
% 7. Topography
%
% If matlab is executed interactively (which is checked by CreatePlots) the
% figures are stored with temperatures on top and a composition plot above
% every parameter plot as the function 'freezecolors' then works.
%
% If matlab is executed non_interactively only the velocity is shown on top
% of the composition and only the parameter plot is shown.
%
% -------------------------------------------------------------------------

[v,d] = version;   % check the matlab version to decide options for the plotting

% sort input arguments
if nargin == 1
    PARTICLES       = varargin{1};
elseif nargin == 9
    PARTICLES       = varargin{1};
    CHAR            = varargin{2};
    NUMERICS        = varargin{3};
    BC              = varargin{4};
    MESH            = varargin{5};
    time            = varargin{6};
    itime           = varargin{7};
    CreatePlots     = varargin{8};
    SavePlots       = varargin{9};
else
    error('You missed some input parameters for the visualization')
end

% Check if non-interactive or interactive mode is applied:


% Check for matlab version and mode to decide which filetyp is the best:
% 2014b = PNG recommended (40kb) ; PDF (>30Mb)
% 2013a = PNG low resolution (40kb) ; PDF recommended (~5Mb) (strongly depends on your numerical resolution, might get very large)
if strcmp(v,'8.1.0.604 (R2013a)') & CreatePlots;                 % old matlab interactive --> png
    FileType = '-dpng';
elseif strcmp(v,'8.1.0.604 (R2013a)') & ~CreatePlots;            % old matlab non-interactive --> pdf
    FileType = '-dpdf';
    FileType = '-dpng';
    
elseif strcmp(v,'8.4.0.150421 (R2014b)')                         % new matlab --> always png
    % matlab 2014 on SITH/GAIA. Start it with -singleCompThread -softwareopengl !
    FileType = '-dpng';
else
    FileType = '-dpng';
end

% If you want do define the FileType by hand just uncomment this line:
% FileType = '-dpng';

if CreatePlots
    FiguresVisible = 'on';   % must be invisible to make non-interactive visualization
else
    FiguresVisible = 'off';   % must be invisible to make non-interactive visualization
end

if 1==1 & SavePlots & strcmp(v,'8.1.0.604 (R2013a)')            % old matlab can handle renderer zbuffer
    renderer = '-zbuffer';
    renderer = '-zbuffer';
    
elseif 1==1 & SavePlots & strcmp(v,'8.4.0.150421 (R2014b)')     % new matlab can only handle openGL
    renderer = '-opengl';   % choose a renderer (painters is the only one that works non-interactive)
    renderer = '-painters';
else
    renderer = '-painters';
end
res = '-r300';            % -r300 for standard pictures and -r600 for publications (interactive: reoslution is only recognized if FileType = -dpdf)



%----------------------------------------------------------------------
% SOME COMPUTATIONS
%----------------------------------------------------------------------
X           =   MESH.NODES(1,:);
Z           =   MESH.NODES(2,:);
VELX     	=   MESH.VEL(1,:);
VELZ    	=   MESH.VEL(2,:);

X2d         =   X(MESH.RegularGridNumber)*CHAR.Length/1e3;
Z2d         =   Z(MESH.RegularGridNumber)*CHAR.Length/1e3;

Vx_2d     	=   VELX(MESH.RegularGridNumber)*CHAR.Velocity*CHAR.cm_Year;
Vz_2d     	=   VELZ(MESH.RegularGridNumber)*CHAR.Velocity*CHAR.cm_Year;
Vel_2d      =   sqrt(Vx_2d.^2 + Vz_2d.^2);



Topo_x      =   X2d(end,:);
Topo_z      =   Z2d(end,:);
x_min       =   min(X2d(:));
x_max       =   max(X2d(:));
z_min       =   min(Z2d(:));
z_max       =   max(Z2d(:));

% Interpolate Velocity to a lower resolution grid
num_lowx    =   50;
num_lowz    =   fix((z_max-z_min)/(x_max-x_min)*num_lowx);
dz_low      =   (z_max-z_min)/num_lowz;
[Xlow,Zlow] =   meshgrid(x_min:(x_max-x_min)/num_lowx:x_max, z_min+dz_low:dz_low:z_max);

Vxlow       =   griddata(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(1,:), Xlow,Zlow);
Vzlow       =   griddata(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(2,:), Xlow,Zlow);

% extrapolate particles to grid
NzGrid      = NUMERICS.SaveOutput.NzGrid; % size of grid on which to interpolate PARTICLES
[GRID_DATA] = VisualizeFields(MESH, PARTICLES, NUMERICS, CHAR, NzGrid, BC);



%==================================================================
% Create colormaps etc. for this setup
% We can specify the RGB colors for each phase here
%
CompositionalPhases     =    double(GRID_DATA.Phases*0);            %

% Crust 1
CompositionalPhases(GRID_DATA.Phases==1 ) = 1;
PhaseColor(1,:)         =       [ 255 222 173]./255;    

% Crust 2
CompositionalPhases(GRID_DATA.Phases==2 ) = 2;
PhaseColor(2,:)         =       [210 219 244]./255;

% Asthenosphere
CompositionalPhases(GRID_DATA.Phases==3 ) = 3;
PhaseColor(3,:)         =       [205 82 59]./255;

% Mantle Lithosphere
CompositionalPhases(GRID_DATA.Phases==4 ) = 4;
PhaseColor(4,:)         =       [159 60 62]./255;

% PARTIALLY MOLTEN MANTLE
CompositionalPhases(GRID_DATA.Phases==5 ) = 5;
PhaseColor(5,:)         =       [240 245 88]./255;

% Depleted mantle
CompositionalPhases(GRID_DATA.Phases==6 ) = 6;
PhaseColor(6,:)         =       [35 67 102]./255;

% New Crust
CompositionalPhases(GRID_DATA.Phases==7 ) = 7;
PhaseColor(7,:)         =       [93 188 94]./255;

% Newly added igneous rocks 
CompositionalPhases(GRID_DATA.Phases==8 ) = 8;
PhaseColor(8,:)         =       [255 127 0]./255;

if max(CompositionalPhases(:))<size(PhaseColor,1)-1
    PhaseColor = PhaseColor(1:max(CompositionalPhases(:)),:);
end
PhaseColorMap = PhaseColor;
% [PhaseColorMap]  = CreatePhasesColormap(PhaseColor);

maxPhase                =  length(PhaseColor);


% Color of the arrows
% color_arrows = [200 200 200]/255;
color_arrows = [50 50 50]/255;


%==================================================================


%--------------------------------------------------------------------------
% CREATE THE PLOT WITH COMPOSITION
%--------------------------------------------------------------------------
h=figure(1); clf
set(h,'Visible',FiguresVisible);
set(h,'Renderer','painters');

hold on
subplot('position',[0.08 0.4 0.8 .45])
h=pcolor(GRID_DATA.Xvec(1:3:end),GRID_DATA.Zvec(1:3:end),CompositionalPhases(1:3:end,1:3:end));      % plot every third value to save space and gain speed in the pdf files (does not the decrease the resolution)
shading flat; colormap(PhaseColorMap);caxis([1 length(PhaseColorMap)]);
axis equal tight
set(gca,'TickDir','out')
hold on
h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
freezeColors
hold on

% if 1==1 & CreatePlots
%     freezeColors
%
% 
Temperature_positive = GRID_DATA.Temperature;
for i=1:1
    dT2 = del2(Temperature_positive);
    dT2(1  ,:)=0;
    dT2(end,:)=0;
    
    Temperature_positive= Temperature_positive + 0.25*dT2;
end



ind = find(Temperature_positive<0);
Temperature_positive(ind) = 0;
[c,hcontour]= contour(GRID_DATA.Xvec,GRID_DATA.Zvec,Temperature_positive,[100:200:1300],'w');
freezeColors
% set(h,'Linewidth',0.00005)

%
%     axis equal
%     axis tight
%
%     freezeColors
% end

% drawnow
title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
xlabel('Width [km]')
ylabel('Depth [km]')
box on

subplot('position',[0.1 0.1 0.8 0.1]);
% plot composition citation
CompositionName = {'C1','C2','Asth','ML','PM','DM','Volcanic','Ingeous'};
for i=1:length(PhaseColor)
    xcolor=[i;i+1;i+1;i];
    ycolor=[1;1;2;2];
    fill(xcolor,ycolor,PhaseColor(i,:));
    freezeColors
    yp=1.4;xp=i+.2;
    text(xp,yp,CompositionName{i});
    axis off;hold on
end
freezeColors        
h=figure(1);
set(h,'Visible',FiguresVisible);
fname = ['Composition',num2str(itime+1e6)];
if SavePlots
    print(h,FileType,renderer,res,fname)
end


% delete(hcontour)
% set(gca,'YLim',[-300 0])
% fname = ['Composition_UpperMantle',num2str(itime+1e6)];
% if SavePlots
%     print(h,FileType,renderer,res,fname)
% end



%--------------------------------------------------------------------------
% COMPOSITION AND STRAIN RATE
%--------------------------------------------------------------------------
h=figure(2); clf
set(h,'Visible',FiguresVisible);

% if 1==1 %& CreatePlots
%     ax=subplot(211)
%     h = pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
%     shading flat; colormap(ax,PhaseColorMap);
% 
%     hold on
%     h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
%     axis equal
%     axis tight
%     set(gca,'YLim',[-300 0])
%     
%     title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
%     xlabel('Width [km]')
%     ylabel('Depth [km]')
%     box on
% %     freezeColors
% 
%     ax=subplot(212)
% end

h = PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)),CHAR.Length/1e3,'-');
shading interp; h=colorbar('vert');
colormap(jet)

axis equal
axis tight
xlabel(h,'log10(e_{II}) [1/s]')
title('Strainrate')
set(gca,'YLim',[-300 0])
h=figure(2);
set(h,'Visible',FiguresVisible);
fname = ['Strainrate',num2str(itime+1e6)];
if SavePlots
    print(h,FileType,renderer,res,fname)
end

%--------------------------------------------------------------------------
% COMPOSITION AND EFFECTIVE VISCOSITY
%--------------------------------------------------------------------------
h=figure(3); clf
set(h,'Visible',FiguresVisible);

% if 1==1 & CreatePlots
%     subplot(211)
%     h = pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
%     shading flat; colormap(PhaseColorMap);
%
%     hold on
%     h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
%
%     axis equal
%     axis tight
%     drawnow
%     title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
%     xlabel('Width [km]')
%     ylabel('Depth [km]')
%     box on
%     freezeColors
%
%     subplot(212)
% end

h = PlotMesh(MESH,log10(MESH.CompVar.Mu_Eff*(CHAR.Viscosity)),CHAR.Length/1e3,'-');
shading flat;
colormap(jet)
axis equal
axis tight
set(gca,'YLim',[-300 0])
caxis([18 24])
h=colorbar('horiz');
xlabel(h,'log10(\eta) [Pa s]')
title('Effective viscosity')

h=figure(3);
set(h,'Visible',FiguresVisible);

fname = ['Viscosity',num2str(itime+1e6)];
if SavePlots
    print(h,FileType,renderer,res,fname)
end

%--------------------------------------------------------------------------
% COMPOSITION AND DENSITY
%--------------------------------------------------------------------------
h=figure(4); clf
set(h,'Visible',FiguresVisible);
%
% if 1==1 & CreatePlots
%     subplot(211)
%     h = pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
%     shading flat; colormap(PhaseColorMap);
%
%     hold on
%     h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
%
%     axis equal
%     axis tight
%     drawnow
%     title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
%     xlabel('Width [km]')
%     ylabel('Depth [km]')
%     box on
%     freezeColors
%
%     subplot(212)
% end
axis equal
axis tight
h = PlotMesh(MESH,(MESH.CompVar.Rho*(CHAR.Density)),CHAR.Length/1e3,'-');
shading flat;
colormap(jet)
caxis([2900 3400])
h=colorbar('horiz');
xlabel(h,'Density [kg m^{-3}]')
title('Density')
axis equal
axis tight
set(gca,'YLim',[-300 0])
h=figure(4);
set(h,'Visible',FiguresVisible);
set(h,'Renderer','painters');

fname = ['Density',num2str(itime+1e6)];
if SavePlots
    print(h,FileType,renderer,res,fname)
end

%--------------------------------------------------------------------------
% COMPOSITION AND STRESS
%--------------------------------------------------------------------------
h=figure(5); clf
set(h,'Visible',FiguresVisible);

% if 1==1 & CreatePlots
%     subplot(211)
%     h = pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
%     shading flat; colormap(PhaseColorMap);
%
%     hold on
%     h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
%
%     axis equal
%     axis tight
%     drawnow
%     title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
%     xlabel('Width [km]')
%     ylabel('Depth [km]')
%     box on
%     freezeColors
%
%
%     subplot(212)
% end

h = PlotMesh(MESH,(MESH.CompVar.T2nd*(CHAR.Stress/1e6)),CHAR.Length/1e3,'-');
shading flat;
colormap(jet)
axis equal
axis tight
h=colorbar('horiz');
xlabel(h,'Stress [MPa]')
title('Stress')
set(gca,'YLim',[-300 0])

h=figure(5);
set(h,'Visible',FiguresVisible);
fname = ['Stress',num2str(itime+1e6)];
if SavePlots
    print(h,FileType,renderer,res,fname)
end

%--------------------------------------------------------------------------
% TEMPERATURE
%--------------------------------------------------------------------------
h=figure(6); clf
set(h,'Visible',FiguresVisible);

h = PlotMesh(MESH,(MESH.TEMP*CHAR.Temperature-273),CHAR.Length/1e3,'-');
shading flat;
colormap(jet)
axis equal
axis tight
set(gca,'YLim',[-300 0])
h=colorbar('horiz');
xlabel(h,'T Celcius')
title('Temperature')

h=figure(6);
set(h,'Visible',FiguresVisible);
fname = ['Temperature',num2str(itime+1e6)];
if SavePlots
    print(h,FileType,renderer,res,fname)
end


%--------------------------------------------------------------------------
% EXTRACTED MELT
%--------------------------------------------------------------------------
if isfield(MESH.CompVar,'ExtractedMelt')
    h=figure(7); clf
    set(h,'Visible',FiguresVisible);
    
    h = PlotMesh(MESH,(MESH.CompVar.ExtractedMelt),CHAR.Length/1e3,'-');
    shading flat;
    colormap(jet)
    caxis([0 0.5])
    axis equal
    axis tight
    set(gca,'YLim',[-300 0])
    h=colorbar('horiz');
    % xlabel(h,'%')
    title('Total extracted Melt Fraction')
    
    h=figure(7);
    set(h,'Visible',FiguresVisible);
    fname = ['ExtractedMeltFraction',num2str(itime+1e6)];
    if SavePlots
        print(h,FileType,renderer,res,fname)
    end
end

%--------------------------------------------------------------------------
%  MELT FRACTION
%--------------------------------------------------------------------------
if isfield(MESH.CompVar,'MeltFraction')
    h=figure(8); clf
    set(h,'Visible',FiguresVisible);
    
    h = PlotMesh(MESH,(MESH.CompVar.MeltFraction),CHAR.Length/1e3,'-');
    shading flat;
    colormap(jet)
    axis equal
    axis tight
    set(gca,'YLim',[-300 0])
    h=colorbar('horiz');
    % xlabel(h,'%')
    title('Melt Fraction')
    
    h=figure(8);
    set(h,'Visible',FiguresVisible);
    fname = ['MeltFraction',num2str(itime+1e6)];
    if SavePlots
        print(h,FileType,renderer,res,fname)
    end
    
end

%------------------------------------------------------------------
% Plot topography
%------------------------------------------------------------------
h=figure(9); clf
set(h,'Visible',FiguresVisible);

% if 1==1 & CreatePlots
%     subplot(211)
%     h = pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
%     shading flat; colormap(PhaseColorMap);
%
%     hold on
%     h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
%
%     axis equal
%     axis tight
%     drawnow
%     title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
%     xlabel('Width [km]')
%     ylabel('Depth [km]')
%     box on
%     freezeColors
%
%
%     subplot(212)
% end

h = plot(Topo_x,Topo_z,'k');       % surface topo
colormap(jet)
xlabel('Width [km]')
ylabel('Topography [km]')
title(['Topography; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])

h=figure(9);
set(h,'Visible',FiguresVisible);
fname = ['Topography',num2str(itime+1e6)];
if SavePlots
    print(h,FileType,renderer,res,fname)
end

% --------------------- End of normal script ---------------------------- %
% --------------------- End of normal script ---------------------------- %


% Perform the early earth simulations as described in Johnson et al. Nature Geoscience (2014)

% Add paths & subdirectories
AddDirectories;

clear

tic

%==========================================================================
% PARAMETERS THAT CAN BE CHANGED
%=======================================================1=================
NUMERICS.ShowTimingInformation  =   logical(1);
SavePlots                	=   logical(1);
if ismac
    CreatePlots             =   logical(1);
else
    CreatePlots         	=   logical(0);
end

SecYear                     =   3600*24*365.25;

% Input parameters
factor                      =   2;                 % numerical resolution

dt                          =   1e3*SecYear;                                % Initial dt
BG_StrRate                  =   1e-17;                                      % Background extension rate


TempSurface                 =   20;                                         % Surface temperature
TempMoho                    =   1000;


ThicknessLithosphere        =   100e3;                                      % Thickness of Lithosphere
TempMantlePotential     	=   1600;                                       % Asthenospheric temperature in Celcius
CrustalComposition          =   {'Hydrous','Anhydrous'};
CrustalComposition          =   CrustalComposition{1}


switch CrustalComposition
    case 'Hydrous'
        ThicknessCrust  	=   45e3;
    case 'Anhydrous'
        ThicknessCrust    	=   30e3;
end

Geotherm                    =   {'Linear 2stage', 'Cooling age'};           % Which geotherm to use?
Geotherm                    =   Geotherm{1}
ThermalAgeMyrs              =   20;                                         % Thermal age in case we use a cooling age [Myrs]

RheologyType                =   {'Wet','Dry'};
RheologyType                =   RheologyType{1};

Rheology                    =   {'Linear Viscous','T-dependent'};           % Which rheology do we use?
Rheology                    =   Rheology{2}

% In case of T-dependent viscosity:
PrefactorCrustViscosity     =   1;                                          % by how much should we lower or increase this viscoity?
PrefactorMantleViscosity  	=   1;

AdiabaticGradientMantle     =   0.3/1e3;                                    % Gradient in mantle
TempMantle                  =   TempMantlePotential + ThicknessLithosphere*AdiabaticGradientMantle;     % temp at LAB

% Store input parameters, for visualization
INPUT_PARAM.TempSurface             =   TempSurface;
INPUT_PARAM.TempMoho                =   TempMoho;
INPUT_PARAM.TempMantlePotential  	=   TempMantlePotential;
INPUT_PARAM.ThicknessCrust          =   ThicknessCrust;
INPUT_PARAM.ThicknessLithosphere    =   ThicknessLithosphere;
INPUT_PARAM.BG_StrRate              =   BG_StrRate;
INPUT_PARAM.factor                  =   factor;
INPUT_PARAM.CrustalComposition      =   CrustalComposition;
%
%=========================================================================


% -------------------------------------------------------------------------
% Define melt extraction parameters for 'normal' mantle & asthenosphere
MeltExtractionMantle.Type                             =   'ExtractAndFormCrust';
MeltExtractionMantle.M_min                            =   0.005;                  %   Fraction of melt that cannot be extracted from rock
MeltExtractionMantle.M_critical                       =   0.05;
MeltExtractionMantle.MaximumExtractableMeltFraction   =   0.5;                    %   Maximum amount of melt we can take out of any volume of rock
MeltExtractionMantle.IntrusiveRockRatio               =   0.0;                    %   How much of the total injected mass is as intrusive rocks?
MeltExtractionMantle.IntrusiveRockPhase               =   8;                      %   Phase number of the igneous rock
MeltExtractionMantle.IntrusiveTemperature_Celcius     =   600;                    %   Temperature of intrusive rock
MeltExtractionMantle.ExtrusiveRockPhase               =   7;                      %   Phase number of the volcanic rock
MeltExtractionMantle.ExtrusiveTemperature_Celcius     =   200;                    %   Temperature of extrusive rock
MeltExtractionMantle.CrustalRockPhases                =   [1 2 7 8];              %   We need to tell the code which phases are crust (so we create intrusive rocks in the crust).
MeltExtractionMantle.MeltWeakeningPlasticityFactor    =   0.01;                   %   Melt weakening of plasticity parameters



%% Create mesh
mesh_input.x_min    =   -500e3;
mesh_input.x_max    =    500e3;
mesh_input.z_min    =   -660e3;
mesh_input.z_max    =   0;
opts.nx             =   128*factor+1;
opts.nz             =   128*factor+1;
NUMERICS.dt_max                         =   50e3*SecYear;
NUMERICS.Nonlinear.Tolerance            =   1e-2;
NUMERICS.Nonlinear.MaxNumberIterations  =   20;

%% Set material properties for every phase
%----------------------------------
% Primitive Crust  (Crust 1)
Phase            	=   1;
[MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep([], Phase, 'Diabase - Huismans et al (2001)');   % to compare with solution below

% 'Old' viscosity, used in old simulations
MATERIAL_PROPS(Phase).Viscosity.ParameterizedDislocationCreep.mu0            =   1.5676e+15;
MATERIAL_PROPS(Phase).Viscosity.ParameterizedDislocationCreep.n              =  3.05;
MATERIAL_PROPS(Phase).Viscosity.ParameterizedDislocationCreep.Q              =  1.0884e+04;
MATERIAL_PROPS(Phase).Viscosity.ParameterizedDislocationCreep.e0             =  1e-15;
MATERIAL_PROPS(Phase).Viscosity.ParameterizedDislocationCreep.T0             =  873;


Type                =   'PhaseDiagram';
switch CrustalComposition
    case 'Hydrous'
        MATERIAL_PROPS(Phase).Density.(Type).Name      =   'EarlyEarth_Crust_MgO23PT_1';   % name of
    case 'Anhydrous'
        MATERIAL_PROPS(Phase).Density.(Type).Name      =   'EarlyEarth_Crust_Anhydrous_MgO23dryPT_1';   % name of
end
MATERIAL_PROPS(Phase).Density.(Type).PhaseRename       =   [1 2]; % you can change the phase number of a PARTICLE based on where it is in the phase diagram

Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   30;              % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   20e6;         	% Cohesion

%
%----------------------------------

%----------------------------------
% Primitive Crust  (Crust 2) - negatively buoyant part of crust; same properties as phase 1,
% but different color
Phase               =   2;
[MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep(MATERIAL_PROPS, Phase, 'Diabase - Huismans et al (2001)');   % to compare with solution below

Type                =   'PhaseDiagram';
switch CrustalComposition
    case 'Hydrous'
        MATERIAL_PROPS(Phase).Density.(Type).Name       =   'EarlyEarth_Crust_MgO23PT_1';   % name of
    case 'Anhydrous'
        MATERIAL_PROPS(Phase).Density.(Type).Name       =   'EarlyEarth_Crust_Anhydrous_MgO23dryPT_1';   % name of
end
MATERIAL_PROPS(Phase).Density.(Type).PhaseRename        =   [1 2]; % you can change the phase number of a PARTICLE based on where it is in the phase diagram

Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   30;              % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   20e6;         	% Cohesion
%----------------------------------

%----------------------------------
% Asthenosphere [fertile] Mantle, which can melt and form new crust
Phase                       =   3;
switch RheologyType
    case 'Wet'
        [MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003 (constant C_OH)');
        [MATERIAL_PROPS] 	=   Add_DiffusionCreep(MATERIAL_PROPS, Phase,   'Wet Olivine diffusion creep- Hirth and Kohlstedt 2003 constant C_OH');
    case 'Dry'
        [MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Dry Olivine dislocation creep- Hirth and Kohlstedt 2003');
        [MATERIAL_PROPS]  	=   Add_DiffusionCreep(MATERIAL_PROPS, Phase,  'Dry Olivine diffusion creep- Hirth and Kohlstedt 2003');
end
MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.V = 0;
MATERIAL_PROPS(Phase).Viscosity.DiffusionCreep.V   = 0;
MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.F2 = 1/2;
MATERIAL_PROPS(Phase).Viscosity.DiffusionCreep.F2   = 1/2;

Type                                             =   'PhaseDiagram';
MATERIAL_PROPS(Phase).Density.(Type).Name        =   'EarlyEarth_Asthenosphere_WithMelt_KatzParameterization';
MATERIAL_PROPS(Phase).Density.(Type).PhaseRename =   [3 5 5];

% Define the melt extraction algorithm and various parameters
MATERIAL_PROPS(Phase).MeltExtraction            =   MeltExtractionMantle;

Type                =   'DruckerPrager';                                                    % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   30;              % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   20e6;         	% Cohesion
%
%----------------------------------

%----------------------------------
% Mantle Lithosphere, which has a slightly different composition than the
% asthenosphere, and can melt & create new crust
Phase                       =   4;
switch RheologyType
    case 'Wet'
        [MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003 (constant C_OH)');
        [MATERIAL_PROPS]     =  Add_DiffusionCreep(MATERIAL_PROPS, Phase,   'Wet Olivine diffusion creep- Hirth and Kohlstedt 2003 constant C_OH');
        
    case 'Dry'
        [MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Dry Olivine dislocation creep- Hirth and Kohlstedt 2003');
        [MATERIAL_PROPS]     =  Add_DiffusionCreep(MATERIAL_PROPS, Phase,  'Dry Olivine diffusion creep- Hirth and Kohlstedt 2003');
end

MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.V  =   0;
MATERIAL_PROPS(Phase).Viscosity.DiffusionCreep.V   =    0;
MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.F2 =   1/2;
MATERIAL_PROPS(Phase).Viscosity.DiffusionCreep.F2   =   1/2;
Type                                                =   'PhaseDiagram';
MATERIAL_PROPS(Phase).Density.(Type).Name           =   'EarlyEarth_MantleLithosphere_WithMelt';
MATERIAL_PROPS(Phase).Density.(Type).PhaseRename    =   [4 5 6]; % can be molten
MATERIAL_PROPS(Phase).MeltExtraction                =   MeltExtractionMantle;

Type                                                       =   'DruckerPrager';   	% Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   30;                  % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   20e6;                % Cohesion
%
%----------------------------------

%----------------------------------
% Partially molten mantle
Phase                                               =   5;
MuPartialMelt                                       =   1e18;
Type                                                =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu           =   MuPartialMelt;                           % parameter

Type                                                =   'PhaseDiagram';
MATERIAL_PROPS(Phase).Density.(Type).Name           =   'EarlyEarth_Asthenosphere_WithMelt_KatzParameterization';
MATERIAL_PROPS(Phase).Density.(Type).PhaseRename    =   [3 5 5];

MATERIAL_PROPS(Phase).MeltExtraction                =   MeltExtractionMantle;

%
%----------------------------------

%----------------------------------
% Depleted mantle [cannot melt]
Phase                       =   6;
switch RheologyType
    case 'Wet'
        [MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003 (constant C_OH)');
        [MATERIAL_PROPS]     =  Add_DiffusionCreep(MATERIAL_PROPS, Phase,   'Wet Olivine diffusion creep- Hirth and Kohlstedt 2003 constant C_OH');
        
    case 'Dry'
        [MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Dry Olivine dislocation creep- Hirth and Kohlstedt 2003');
        [MATERIAL_PROPS]     =  Add_DiffusionCreep(MATERIAL_PROPS, Phase,  'Dry Olivine diffusion creep- Hirth and Kohlstedt 2003');
end

MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.V = 0;
MATERIAL_PROPS(Phase).Viscosity.DiffusionCreep.V   = 0;
MATERIAL_PROPS(Phase).Viscosity.DislocationCreep.F2 = 1/2;
MATERIAL_PROPS(Phase).Viscosity.DiffusionCreep.F2   = 1/2;

Type                                                =   'PhaseDiagram';
MATERIAL_PROPS(Phase).Density.(Type).Name           =   'EarlyEarth_MantleLithosphere_NoMelt';
MATERIAL_PROPS(Phase).Density.(Type).PhaseRename 	=   [6]; % always stays this crustal block

Type                                                       =   'DruckerPrager';   	% Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   30;                  % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   20e6;                % Cohesion

%
%----------------------------------


%----------------------------------
% New mafic crust, formed by partial melting of mantle (volcanic rocks)
Phase                           =   7;
[MATERIAL_PROPS]                =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Diabase - Huismans et al (2001)');   % to compare with solution below

Type                                                =   'PhaseDiagram';
MATERIAL_PROPS(Phase).Density.(Type).Name           =   'EarlyEarth_Crust_MgO23PT_1';    	% Name of phase diagram
MATERIAL_PROPS(Phase).Density.(Type).PhaseRename    =   [7 7]; % always stays this crustal material

Type                                                       =   'DruckerPrager';   	% Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   30;                  % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   20e6;                % Cohesion

%
%----------------------------------

%----------------------------------
% New mafic intrusions, formed by partial melting of mantle (and creation of igneous rocks)
Phase                           =   8;
[MATERIAL_PROPS]                =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Diabase - Huismans et al (2001)');   % to compare with solution below

Type                                                =   'PhaseDiagram';
MATERIAL_PROPS(Phase).Density.(Type).Name           =   'EarlyEarth_Crust_MgO23PT_1';    	% Name of phase diagram
MATERIAL_PROPS(Phase).Density.(Type).PhaseRename    =   [8 8]; % always stays this crustal material

Type                                                       =   'DruckerPrager';   	% Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   30;                  % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   20e6;                % Cohesion
%
%----------------------------------


% %----------------------------------
% % NOT USED CURRENTLY IN THESE SIMULATIONS !!
% Phase           	=   4;
% [MATERIAL_PROPS]    =   Add_ParameterizedDislocationCreep(MATERIAL_PROPS, Phase, 'Diabase - Huismans et al (2001)');   % to compare with solution below
%
% Type                                             =   'PhaseDiagram';
% switch CrustalComposition
%     case 'Hydrous'
%         MATERIAL_PROPS(Phase).Density.(Type).Name        =   'EarlyEarth_Crust_MgO23PT_1';
%     case 'Anhydrous'
%         MATERIAL_PROPS(Phase).Density.(Type).Name        =   'EarlyEarth_Crust_Anhydrous_MgO23dryPT_1';
% end
% MATERIAL_PROPS(Phase).Density.(Type).PhaseRename        =   [4 4]; % always stays this crustal block
% %
% %----------------------------------


% Add material properties that were not defined here, but which MILAMIN_VEP needs to know
MATERIAL_PROPS                                  =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required

%% Numerics
NUMERICS.Viscosity.LowerCutoff  =   1e18;
NUMERICS.Viscosity.UpperCutoff  =   1e24;
NUMERICS                        =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required




%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{1};
BC.Stokes.Left          =   Bound{2};
BC.Stokes.Right         =   Bound{2};
BC.Stokes.Top           =   Bound{2};
BC.Stokes.BG_strrate    =   0;

% Thermal boundary condition
%                                        1              2           3
BoundThermal                =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top               =   BoundThermal{2};
BC.Energy.Bottom            =   BoundThermal{2};
BC.Energy.Left              =   BoundThermal{1};
BC.Energy.Right             =   BoundThermal{1};
BC.Energy.Value.Top         =   TempSurface + 273;
BC.Energy.Value.Bottom      =   TempMantlePotential + AdiabaticGradientMantle*(mesh_input.z_max-mesh_input.z_min) + 273;                    % T at bottom of mantle


%% Create Particles
part_fac                    =   4;
numx                        =   400*part_fac;
numz                        =   200*part_fac;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (mesh_input.z_max-mesh_input.z_min)/numz;
[PARTICLES.x,PARTICLES.z]   =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,mesh_input.z_min/2:dz:mesh_input.z_max);
PARTICLES.x                 =   PARTICLES.x(:);
PARTICLES.z                 =   PARTICLES.z(:);
PARTICLES.x                 =   PARTICLES.x + 1*[rand(size(PARTICLES.x))-0.5]*dx;
PARTICLES.z                 =   PARTICLES.z + 1*[rand(size(PARTICLES.x))-0.5]*dz;

part_fac                    =   2;
numx                        =   400*part_fac;
numz                        =   200*part_fac;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (mesh_input.z_max-mesh_input.z_min)/numz;
[Particles1.x,Particles1.z]	=   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,mesh_input.z_min:dz:mesh_input.z_min/2);
Particles1.x                =   Particles1.x(:);
Particles1.z                =   Particles1.z(:);
Particles1.x                =   Particles1.x + 1*[rand(size(Particles1.x))-0.5]*dx;
Particles1.z                =   Particles1.z + 1*[rand(size(Particles1.x))-0.5]*dz;

PARTICLES.x                 = [PARTICLES.x ; Particles1.x(:)]';
PARTICLES.z                 = [PARTICLES.z ; Particles1.z(:)]';

PARTICLES.phases                             =   ones(size(PARTICLES.x));
PARTICLES.HistVar.T                          =   zeros(size(PARTICLES.x));
PARTICLES.CompVar.MeltFraction_PhaseDiagram  =   zeros(size(PARTICLES.x));           % Melt fraction from phase diagram


%% Create initial geometry and temperature structure
% Set initial temperature structure, which has two straight parts
ind                         =   find(PARTICLES.z>-ThicknessCrust);
T                           =   -(PARTICLES.z(ind)-0).*(TempMoho-TempSurface)/(ThicknessCrust-0) + TempSurface;
PARTICLES.HistVar.T(ind)    =   T;  % Temp crust

ind                         =   find(PARTICLES.z<=-ThicknessCrust & PARTICLES.z>-ThicknessLithosphere);
T                           = 	-(PARTICLES.z(ind)+ThicknessCrust).*(TempMantle-TempMoho)/(ThicknessLithosphere-ThicknessCrust) + TempMoho;
PARTICLES.HistVar.T(ind)  	=   T;  % Temp lower lithosphere

% % Adiabatic T-gradient in Mantle
ind                         =   find(PARTICLES.z<=-ThicknessLithosphere);
PARTICLES.HistVar.T(ind)  	=   (mesh_input.z_max-PARTICLES.z(ind))*AdiabaticGradientMantle + TempMantlePotential;  % Temp asthenosphere
%

% ADD RANDOM NOISE TO INITIATE CONVECTION
ind              = find(PARTICLES.z<mesh_input.z_min*0.2);
PARTICLES.HistVar.T(ind) = PARTICLES.HistVar.T(ind) + (rand(size(ind))-0.5)*10;

% Conversion to kelvin and scaling
PARTICLES.HistVar.T                 =   PARTICLES.HistVar.T+273;  % in K

% Set particles to phase 2
ind                         =   find(PARTICLES.z< -ThicknessCrust );
PARTICLES.phases(ind)       =   1;


% Set particles to phase 3
ind                         =   find( PARTICLES.z <    -ThicknessCrust);
PARTICLES.phases(ind)       =   3;


% Set Mantle Lithosphere
ind = find( ((PARTICLES.HistVar.T-273) <    TempMantle) & PARTICLES.phases==3);
PARTICLES.phases(ind)       =   4;


% % Block in center with higher melt fraction
% ind                                  =  find(PARTICLES.z<=-ThicknessLithosphere & abs(PARTICLES.x<100e3) & PARTICLES.z>-2*ThicknessLithosphere);
% PARTICLES.HistVar.MeltFraction(ind)  =  0.1;


% Create a vertically refined mesh
z_trans                 =   0.5;
dz_top                  =   (1-z_trans)/fix(3/5*opts.nz);
dz_bot                  =   (z_trans)/(fix(2/5*opts.nz));
z_vec                   =   [0:dz_bot:z_trans-dz_bot, z_trans:dz_top:1];
mesh_input.z_vec        =   z_vec;
opts.nz                 =   length(z_vec);  % just to be sure


%% Non-dimensionalize input parameters
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES,  NUMERICS, []);


%% Load breakpoint file if required
MELT_EXTRACTION = [];
Load_BreakpointFile

if ismac
    CreatePlots=logical(1);
end

%% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input);



%% Plot initial particles
if 1==0 & CreatePlots
    Style	= {'*r', '.g', '*y', '.b', '*k', '.m', 'd'};
    %     if max(Point_id)>1
    %         for i=1:max(Point_id)
    %             Ind	= find(Point_id==i);
    %             a = mod(i,size(Style))+1;
    %             plot(GCOORD(1,Ind), GCOORD(2,Ind), Style{a(2)});
    %         end
    %     end
    
    figure(1), clf, hold on
    PlotMesh(MESH,[],CHAR.Length/1e3,'r.-');
    
    for i=1:max(PARTICLES.phases)
        ind = find(PARTICLES.phases==i );
        plot(PARTICLES.x(ind)*CHAR.Length/1e3,PARTICLES.z(ind)*CHAR.Length/1e3,Style{i})
    end
    
end


%% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input, MESH);

dt                  =   dt/CHAR.Time;
time                =    0;
for itime=NUMERICS.time_start:1e4
    start_cpu = cputime;
    
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);

    
    %% Compute melt extraction if wished
    if itime>1
        
        % Compute the melt fraction @
        PARTICLES = ComputeMeltFractionFromPhaseDiagrams(PARTICLES, MATERIAL_PROPS, CHAR);
        
        cpu1=cputime;
        [PARTICLES, MESH, MELT_EXTRACTION]	=   ParameterizedMeltExtraction(MESH, BC, PARTICLES, MATERIAL_PROPS, NUMERICS, CHAR, MELT_EXTRACTION);           % Melt extraction and creation of new crust
        disp(['Melt extraction took ',num2str(cputime-cpu1),'s'])
    end
    
    %% Compute Stokes & Energy solution
    cpu1=cputime;
    [PARTICLES, MESH, INTP_PROPS, NUMERICS, MATERIAL_PROPS, dt] 	=   StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    disp(['Stokes/Energy solver took ',num2str(cputime-cpu1),'s'])
    
    %% Advect particles [MESH is Eulerian in this simulation]
    PARTICLES.x         =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z         =   PARTICLES.z + PARTICLES.Vz*dt;
    time                =   time+dt;
    
    % Change phases of particles, depending on melt content (mainly for
    % visualization)_
    if isfield(PARTICLES,'MeltExtraction')
        for iphase=1:length(MATERIAL_PROPS)
            
            if ~isempty(MATERIAL_PROPS(iphase).MeltExtraction)
                indPhase                        =   find(PARTICLES.phases==iphase);     % current phases
                
                % Mantle becomes depleted if sufficient melt has been extracted
                id                              =   find( ismember(PARTICLES.phases(indPhase),[3 4 5])    &  (PARTICLES.MeltExtraction.ExtractedMelt(indPhase)>=MATERIAL_PROPS(iphase).MeltExtraction.MaximumExtractableMeltFraction) );
                PARTICLES.phases(indPhase(id))	=   6;
                
                
                % Mantle that is molten gets a different color
                id                              =   find( ismember(PARTICLES.phases(indPhase),[3 4 ])      &  (PARTICLES.MeltExtraction.MeltFraction(indPhase)>0.001) );
                PARTICLES.phases(indPhase(id))	=   5;
                
                % Mantle that is no longer molten
                id                              =   find( ismember(PARTICLES.phases(indPhase),[5])          &  (PARTICLES.MeltExtraction.MeltFraction(indPhase)<0.001) );
                PARTICLES.phases(indPhase(id))	=   3;
                
            end
        end
        
    end
    
    
    %% Compute new timestep
    dt                          =   dt*1.25;
    dt_courant                  =   CourantTimestep(MESH, 0.5);
    dt                          =   min([dt NUMERICS.dt_max dt_courant]);                                  % ensure it is not larger than a given maximum
    
    %% Create figures & save them to disk if wished
    if mod(itime,20)==0   %& ~ismac  % Save plots if wanted
        cpu1=cputime;
        Visualize_EarlyEarth_GravInstability(PARTICLES,CHAR,NUMERICS,BC,MESH,time,itime,CreatePlots,SavePlots);
        disp(['Creating/saving plots took ',num2str(cputime-cpu1),'s'])
    end
    
    %% Plot results
    if mod(itime,1)==0 & CreatePlots & 1==0
        if isfield(MESH,'TEMP')
            
            % Temperature
            figure(2), clf
            PlotMesh(MESH,MESH.TEMP*CHAR.Temperature-273, CHAR.Length/1e3); axis equal, axis tight, colorbar; shading flat
            axis equal, axis tight, colorbar, title('Temperature [C]')
            
            figure(3), clf
            PlotMesh(MESH,log10(MESH.CompVar.Mu_Eff*CHAR.Viscosity), CHAR.Length/1e3); shading interp
            axis equal, axis tight, colorbar, title('log10(effective viscosity)')
            
            figure(4), clf
            PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)), CHAR.Length/1e3); shading interp
            axis equal, axis tight, colorbar, title('log10(e2nd [1/s])')
            
            figure(5), clf
            PlotMesh(MESH,(MESH.CompVar.T2nd*(CHAR.Stress/1e6)), CHAR.Length/1e3); shading interp
            axis equal, axis tight, colorbar, title('(T2nd [MPa])')
            
            figure(6), clf
            PlotMesh(MESH,(MESH.CompVar.Pressure*(CHAR.Stress)/1e6), CHAR.Length/1e3); shading interp
            axis equal, axis tight, colorbar, title('Pressure [MPa]')
            
            figure(7), clf
            PlotMesh(MESH,(MESH.CompVar.Rho*(CHAR.Density)), CHAR.Length/1e3); shading interp
            axis equal, axis tight, colorbar, title('Density [kg/m^3]')
            caxis([2900 3400]), axis([-500 500 -300 0])
            
            figure(8), clf
            if isfield(MESH.CompVar, 'MeltFraction')
                subplot(211)
                PlotMesh(MESH,(MESH.CompVar.MeltFraction), CHAR.Length/1e3); shading interp
                axis equal, axis tight, colorbar, title('MeltFraction []')
            end
            if isfield(PARTICLES,'MeltExtraction')
                subplot(212)
                plotclr(PARTICLES.x,PARTICLES.z,PARTICLES.MeltExtraction.MeltFraction)
                axis equal, axis tight, title('MeltFraction on Particles []')
            end
            figure(9), clf
            %             subplot(211)
            %             PlotMesh(MESH,(MESH.HistVar.MeltFraction_PhaseDiagram), CHAR.Length/1e3); shading interp
            %             axis equal, axis tight, colorbar, title('MeltFraction\_PhaseDiagram []')
            %
            if isfield(PARTICLES,'MeltExtraction')
                subplot(212)
                plotclr(PARTICLES.x,PARTICLES.z,PARTICLES.CompVar.MeltFraction_PhaseDiagram)
                axis equal, axis tight, title('MeltFraction\_PhaseDiagram on Particles []')
            end
            
            if isfield(PARTICLES,'MeltExtraction')
                
                if isfield(PARTICLES.MeltExtraction,'ExtractedMelt')
                    figure(10), clf
                    plotclr(PARTICLES.x,PARTICLES.z,PARTICLES.MeltExtraction.ExtractedMelt)
                    axis equal, axis tight, title('Extracted Melt on Particles []')
                end
                
                if isfield(PARTICLES.MeltExtraction,'MeltExtractEvents')
                    figure(11), clf
                    plotclr(PARTICLES.x,PARTICLES.z,PARTICLES.MeltExtraction.MeltExtractEvents)
                    axis equal, axis tight, title('MeltExtractEvents on Particles []')
                end
            end
            %             if isfield(MESH.HistVar,'MeltFraction_CurrentlyExtracted')
            %                 figure(11), clf
            %                 PlotMesh(MESH,(MESH.HistVar.MeltFraction_CurrentlyExtracted), CHAR.Length/1e3), shading interp
            %                 axis equal, axis tight, colorbar, title('MeltFraction\_CurrentlyExtracted []')
            %             end
            %
            
            
            drawnow
        end
        
        
        [GRID_DATA] = VisualizeFields(MESH, PARTICLES, NUMERICS, CHAR, 1001, BC);
        
        figure(1), clf, hold on
        %
        %                 ind = find(PARTICLES.phases==1); % pos. buoyant  crust
        %                 plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'ro')
        %                 ind = find(PARTICLES.phases==2);% neg. buoyant  crust
        %                 plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'go')
        %                 ind = find(PARTICLES.phases==3);% mantle
        %                 plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'bo')
        %                 ind = find(PARTICLES.phases==4);% mantle lithosphere
        %                 plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'ko')
        %                 ind = find(PARTICLES.phases==5);%mantle, partially molten
        %                 plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'yo')
        %
        %
        %                 ind = find(PARTICLES.phases==6);% mantle, depleted
        %                 plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'c*')
        %
        %                 ind = find(PARTICLES.phases==7);% NEW CRUST
        %                 plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'k*')
        
        pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,double(GRID_DATA.Phases)); shading flat
        hold on
        
        quiver(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(1,:),MESH.VEL(2,:))
        axis equal, axis tight,
        title(['Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3),'kyrs'])
        
        %         axis([-500 500 -300 0])
        % caxis([1 7]), colorbar
        drawnow
        
    end
    
    
    
    % Create breakpoint file if required
    %     NUMERICS.Breakpoints.NumberTimestepsToSave = 500;
    Save_BreakpointFile;
    
    % Save filename if required
    if mod(itime,100)==0
        SaveFile(itime, PARTICLES, NUMERICS,MESH,INTP_PROPS,CHAR,BC, time);
    end
    
    
    end_cpu         = cputime;
    time_timestep    = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs'])
    disp(' ')
    
end

function [] = SubmitSystematicRuns()
%
% This script creates a directory name, copies a setup file into that
% directory and adjusts the parameters in that setup file.

% MATLAB filename in which to change the parameters (without .m)
MATLAB_File                             =       'greenstonesetup';

% Indicate which parameters you want to vary - these parameters must be
% listed in the main code in a section that starts with:
% % ------------------------------
% % VARY PARAMETER START
% %
% %
% %
% %
% % VARY PARAMETER END
% % ------------------------------
PARAMETER_VARIATION(1).Name             =    'GeothermalGradient_K_per_KM';
PARAMETER_VARIATION(1).NameDirectory    =    'Geotherm';
PARAMETER_VARIATION(1).Range            =    [15 25 35];

PARAMETER_VARIATION(2).Name             =    'T_mantle_Celcius'
PARAMETER_VARIATION(2).NameDirectory    =    'Tm';
PARAMETER_VARIATION(2).Range            =    [1400 1500 1600 1700];

PARAMETER_VARIATION(3).Name             =    'e_bg'
PARAMETER_VARIATION(3).Range            =    [-1e-15 0 1e-15 1e-16];
PARAMETER_VARIATION(3).NameDirectory    =    'Ebg';


PARAMETER_VARIATION(4).Name             =    'FrictionAngle_Greenstone';
PARAMETER_VARIATION(4).NameDirectory    =    'Phi';
PARAMETER_VARIATION(4).Range            =    [ 5 15 30];


counterVec             =   GetPermutations(PARAMETER_VARIATION);               % compute all possible permutations of the input parameters

for iModel=1:size(counterVec,1)
    counter             =   counterVec(iModel,:);
    
    % Directory name
    CurDir              =   pwd;
    Dirname            	=   CreateDirectoryName(PARAMETER_VARIATION, counter)
   
    % Create directory with this name
    mkdir(Dirname);
    
    
    % Copy required files over to the directory
    system(['cp ', MATLAB_File,'.m ./',Dirname]);        % Copy matlab file over
    system(['cp AddDirectories.m ./',Dirname]);          % Copy AddDirectories.m over
    
    % maybe copy job submission scripts at this stage
    
    
    
    
    % Modify the relevant lines in the matlab script
    cd(Dirname);
    ModifyMatlabScript(PARAMETER_VARIATION, counter, MATLAB_File);      % change the relevant lines in the matlab file
    
    
    
    % Automatically submit a job..
    
    
    cd(CurDir);      % return to previous directory
    
    
    
    
    
    
    
end


%--------------------------------------------------------------------------
function counter = GetPermutations(PARAMETER_VARIATION)
% Computes a vector will all possible permutations of the input parameters

for i=1:length(PARAMETER_VARIATION);
    a{i} = 1:length(PARAMETER_VARIATION(i).Range);
end



% method found online
N       = numel(a);
v       = cell(N,1);
[v{:}]  = ndgrid(a{:});
counter = reshape(cat(N+1,v{:}),[],N);






%--------------------------------------------------------------------------
function dirname = CreateDirectoryName(PARAMETER_VARIATION, counter)
% Create appropriate directory-name

NumberParameters                        = length(PARAMETER_VARIATION);

dirname = [];
for Parameter =1:NumberParameters
    str = [PARAMETER_VARIATION(Parameter).NameDirectory,num2str(PARAMETER_VARIATION(Parameter).Range(counter(Parameter))),'_'];
    dirname = [dirname, str];
end
dirname(strfind(dirname,'-'))='_';  % remove minus signs and replace them with an underscore
dirname = dirname(1:end-1);





%--------------------------------------------------------------------------
function ModifyMatlabScript(PARAMETER_VARIATION, counter, MATLAB_File)
% Modifies the relevant lines within the matlab script with different
% parameters

fid         =   fopen([MATLAB_File,'.m']);
fout        =   fopen('temp.m','w');            % temporary file (deleted later)

% Go to beginning of section where parameter variations are listed
line = [];
while isempty(strfind(line,'VARY PARAMETER START'))
    line = fgetl(fid);
    
    fprintf(fout,'%s\n',line);       % write line to output file
    
    if feof(fid)
        error('Reached end of file before finding "VARY PARAMETER START"')
    end
end



% Replace relevant lines
while isempty(strfind(line,'VARY PARAMETER END'))
    line = fgetl(fid);
    
    % check if any of the parameters are there
    for i=1:length(PARAMETER_VARIATION);
        if ~isempty(strfind(line,PARAMETER_VARIATION(i).Name))
            % replace line
            line = [PARAMETER_VARIATION(i).Name,'=',num2str(PARAMETER_VARIATION(i).Range(counter(i))),';'];
            
        end
    end
    
    if feof(fid)
        error('Reached end of file before finding "VARY PARAMETER END"')
    end
    
    % write line to output file
    disp(line)
    fprintf(fout,'%s\n',line);
    
    
end


% Replace pther lines
while ~feof(fid)
    line = fgetl(fid);
    fprintf(fout,'%s\n',line);       % write line to output file
end

fclose(fid);
fclose(fout);

% copy the temporary file over the original file
system(['cp temp.m ',MATLAB_File,'.m']);
system('rm temp.m');





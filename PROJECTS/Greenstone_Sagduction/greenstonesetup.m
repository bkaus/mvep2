% Model of sagducted greenstones

% $Id$


% Add paths & subdirectories
AddDirectories;

clear

tic

% ------------------------------
% VARY PARAMETER START
%
% Parameters to vary:
GeothermalGradient_K_per_KM   	=   35;               % geothermal gradient in Kelvin/km                                        [15 25 35] 
T_mantle_Celcius                =   1600;             % Mantle Temperature                                                      [1400 1500 1600 1700]
e_bg                            =   1e-15;         	  % background strainrate [1/s] positive = extension, negative compression  [-1e-15 0 1e-15 1e-16]
FrictionAngle_Greenstone        =   5;                %                                                                         [ 5 15 30]
% 
% VARY PARAMETER END
% ------------------------------



if ismac
    CreatePlots         =   logical(1);
else
    CreatePlots         =   logical(0);
end

factor              =   1;
SecYear             =   3600*24*365.25;
NUMERICS.Restart    =   logical(1);
NUMERICS.dt_max     =   5e6*SecYear;

%% Create mesh
opts.element_type   =   'quad4'
mesh_input.x_min    =   -300e3;
mesh_input.x_max    =    300e3;
mesh_input.z_min    =   -200e3;
mesh_input.z_max    =   0;
mesh_input.FixedAverageTopography = 0;
opts.nx             =   301*factor;
opts.nz             =   101*factor;



% Parameters that are fixed:
dt                              =   2000*SecYear;       % initial dt
FrictionAngle_TTG               =   15;
=Rho_CFB1                       =   2840;
Rho_CFB2                        =   3050;
Rho_CFB3                        =   3135;
Rho_CFB4                        =   3200;
Rho_FEL                         =   2720;
Rho_MANTLE                      =   3310;




%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values

%----------------------------------
% Mantle - does have depth-dependent properties!
Phase               =   1;

% Viscosity
[MATERIAL_PROPS]    =   Add_DislocationCreep([], Phase, 'Dry Olivine dislocation creep- Hirth and Kohlstedt 2003');   % to compare with solution below

% Density
Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                   =   Rho_MANTLE;

% Plasticity
Type                =   'DruckerPrager';
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   30;     	% Friction angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   40e6;                % Cohesion
%
%----------------------------------------

%----------------------------------
% Felsic Basement TTG
Phase            	=   2;

% Viscosity
[MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Granite - Huismans et al (2001)');   % to compare with solution below


% Density
Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                    =   Rho_FEL;

%Plasticity
Type                =   'DruckerPrager';                                            % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   FrictionAngle_TTG;                  % Friction Angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   10e6;         	% Cohesion

%----------------------------------


%----------------------------------
% Greenstones CFB layer 1
Phase            	=   3;
% Viscosity

[MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Granite - Huismans et al (2001)');   % to compare with solution below



Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                   =   Rho_CFB1;

Type                =   'DruckerPrager';                                            % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   FrictionAngle_Greenstone;                   % Friction angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   10e6;                % Cohesion
%----------------------------------

% Greenstones CFB layer 2
Phase              =   4;

% Viscosity
[MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Mafic Granulite - Ranalli 1995');   % to compare with solution below

% Density
Type               =   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                    =   Rho_CFB2;

% Plasticity
Type               =   'DruckerPrager';
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   FrictionAngle_Greenstone;
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   10e6;
% %----------------------------------
% 
% Greenstones CFB layer 3
Phase               =   5;

% Viscosity
[MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Mafic Granulite - Ranalli 1995');   % to compare with solution below

% Density
Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                    =   Rho_CFB3;

% Plasticity
Type                =   'DruckerPrager';                                            % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   FrictionAngle_Greenstone;                   % Friction angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   10e6;                % Cohesion
%----------------------------------

% Greenstones CFB layer 4
Phase               =   6;

% Viscosity
[MATERIAL_PROPS]    =   Add_DislocationCreep(MATERIAL_PROPS, Phase, 'Mafic Granulite - Ranalli 1995');   % to compare with solution below

% Density
Type              	=   'TemperatureDependent';
MATERIAL_PROPS(Phase).Density.(Type).Rho0                           =   Rho_CFB4;

% Plasticity
Type                =   'DruckerPrager';                                            % Plasticity Model
MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle      =   FrictionAngle_Greenstone;                   % Friction angle
MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion           =   10e6;                % Cohesion
%----------------------------------



% Gravitational acceleration
MATERIAL_PROPS(1).Gravity.Value                 =   10;

% Add material properties that were not defined here, but which MILAMIN_VEP needs to know
MATERIAL_PROPS                                 =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required


%% Numerical parameters
NUMERICS.Viscosity.LowerCutoff                  =   1e18;
NUMERICS.Viscosity.UpperCutoff                  =   1e25;
NUMERICS.Plasticity.MaximumYieldStress          =   1000e6;
NUMERICS                                        =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required



%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{3};
BC.Stokes.Left          =   Bound{3};
BC.Stokes.Right         =   Bound{3};
BC.Stokes.Top           =   Bound{4};
BC.Stokes.BG_strrate    =   e_bg;

% Thermal boundary condition
%                                        1              2          
BoundThermal                    =   {'Zero flux', 'Isothermal'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   20 + 273;
BC.Energy.Value.Bottom=   T_mantle_Celcius+273;                    % T at bottom of mantle


%% Create initial Particles
numz                        =   400*factor;
numx                        =   round((mesh_input.x_max-mesh_input.x_min)/(mesh_input.z_max-mesh_input.z_min))*numz;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (mesh_input.z_max-mesh_input.z_min)/numz;
[PARTICLES.x,PARTICLES.z]   =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,mesh_input.z_min:dz:mesh_input.z_max);
PARTICLES.x                 =   PARTICLES.x(:);
PARTICLES.z                 =   PARTICLES.z(:);
PARTICLES.x                 =   PARTICLES.x + 1*(rand(size(PARTICLES.x))-0.5)*dx;
PARTICLES.z                 =   PARTICLES.z + 1*(rand(size(PARTICLES.x))-0.5)*dz;

% add refined particles
numz                        =   500*factor;
numx                        =   round((mesh_input.x_max-mesh_input.x_min)/(mesh_input.z_max-mesh_input.z_min))*numz;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (mesh_input.z_max-0.4*mesh_input.z_min)/numz;
[PARTICLES1.x,PARTICLES1.z] =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,0.4*mesh_input.z_min:dz:mesh_input.z_max);
PARTICLES1.x            	=   PARTICLES1.x(:);
PARTICLES1.z            	=   PARTICLES1.z(:);
PARTICLES1.x              	=   PARTICLES1.x + 1*[rand(size(PARTICLES1.x))-0.5]*dx;
PARTICLES1.z            	=   PARTICLES1.z + 1*[rand(size(PARTICLES1.x))-0.5]*dz;

PARTICLES.x                 =   [PARTICLES.x; PARTICLES1.x ];
PARTICLES.z                 =   [PARTICLES.z; PARTICLES1.z ];


PARTICLES.phases            =   ones(size(PARTICLES.x));
PARTICLES.HistVar.T         =   zeros(size(PARTICLES.x));


% Set initial temperature according to half-space cooling profile
PARTICLES.HistVar.T         =   20 - GeothermalGradient_K_per_KM/1000*PARTICLES.z;           % set linear gradient of temperature in Celcius
ind                         =   find(PARTICLES.HistVar.T>T_mantle_Celcius);
PARTICLES.HistVar.T(ind)    =   T_mantle_Celcius;
PARTICLES.HistVar.T         =   PARTICLES.HistVar.T +273;                                   % in K


% Set phases

% CFB layer 1
ind = find(PARTICLES.z>-2e3 & PARTICLES.z<=2e3 & PARTICLES.x>=-300e3 & PARTICLES.x<=300e3);
PARTICLES.phases(ind) = 3;

% CFB layer 2
ind = find(PARTICLES.z>-5e3 & PARTICLES.z<=-2e3 & PARTICLES.x>=-300e3 & PARTICLES.x<=300e3);
PARTICLES.phases(ind) = 4;

% CFB layer 3
ind = find(PARTICLES.z>-10e3 & PARTICLES.z<=-5e3 & PARTICLES.x>=-300e3 & PARTICLES.x <=300e3);
PARTICLES.phases(ind) = 5;


% CFB layer 4
ind =find(PARTICLES.z>-15e3 & PARTICLES.z<=-10e3 & PARTICLES.x>=-300e3 & PARTICLES.x <=300e3);
PARTICLES.phases(ind) = 6;

% Felsic Basement TTG
ind = find(PARTICLES.z<=-15e3 & PARTICLES.z>-45e3 & PARTICLES.x>=-300e3 & PARTICLES.x <=300e3) ;
PARTICLES.phases(ind) = 2;

% Mantle
ind = find(PARTICLES.z<=-45e3 & PARTICLES.z>=-200 & PARTICLES.x>=-300e3 & PARTICLES.x <=300e3);
PARTICLES.phases(ind) = 1;


%% Non-dimensionalize input parameters
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,[]);


dt                  =   dt/CHAR.Time;
time                =    0;

%% Load breakpoint file if required
Load_BreakpointFile

% Generate a z-grid with low res. @ bottom and higher @ lithosphere
refine_x                =   [0 .5    1];
refine_z                =   [1 .15  .15];
z                       =   GenerateGrid_1D(refine_x , refine_z, opts.nz);

mesh_input.z_vec        =   z;

refine_x                =   [0 .2  .8 1];
refine_z                =   [1 .1  .1 1];
x                       =   GenerateGrid_1D(refine_x , refine_z, opts.nx);
mesh_input.x_vec        =   x;



%% Plot initial particles
if 1==1 & CreatePlots
    Style	= {'*r', '+g', '*y', '.b', '*k', '.m', 'd'};
    %     if max(Point_id)>1
    %         for i=1:max(Point_id)
    %             Ind	= find(Point_id==i);
    %             a = mod(i,size(Style))+1;
    %             plot(GCOORD(1,Ind), GCOORD(2,Ind), Style{a(2)});
    %         end
    %     end
    
    % Create mesh
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);
    
    figure(1), clf, hold on
    PlotMesh(MESH,[],CHAR.Length/1e3,'r.-'),
    
    for i=1:max(PARTICLES.phases)
        ind = find(PARTICLES.phases==i );
        plot(PARTICLES.x(ind)*CHAR.Length/1e3,PARTICLES.z(ind)*CHAR.Length/1e3,Style{i})
    end
    
end

for itime=NUMERICS.time_start:1000
    start_cpu = cputime;
    
    %% Create mesh
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);
    
    %% Compute Stokes & Energy solution
    [PARTICLES, MESH, INTP_PROPS] 	=   StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    
    %% Advect particles and mesh - mesh is recreated every timestep
    PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
    MESH.NODES      =   MESH.NODES  + MESH.VEL*dt;
    
    time            =   time+dt;
    
    %% Compute new timestep
    dt                          =   dt*1.25;
    dx_min                      =   min(MESH.dx_min, MESH.dz_min);
    CourantFactor               =   2;
    MaximumSurfaceMotionMeters  =   25;    % maximum change in surface topography per timestep
    dt_courant                  =   CourantFactor*dx_min/max(abs(MESH.VEL(:)));
    dt                          =   min([dt NUMERICS.dt_max dt_courant]);                                  % ensure it is not larger than a given maximum
    
    
    %% Plot results
    if mod(itime,5)==0 & CreatePlots
        
        % Particles and velocities
        figure(1), clf, hold on
        PlotMesh(MESH,[],CHAR.Length/1e3,'r.-'),
        ind = find(PARTICLES.phases==1); % pos. buoyant  crust
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'ro')
        ind = find(PARTICLES.phases==2);% neg. buoyant  crust
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'go')
        ind = find(PARTICLES.phases==3);% mantle
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'bo')
        ind = find(PARTICLES.phases==4);% mantle lithosphere
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'mo')
        ind = find(PARTICLES.phases==5);% mantle, partially molten
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'y.')
        
        %         quiver(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(1,:),MESH.VEL(2,:))
        axis equal, axis tight
        title(['Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),'Myrs'])
        drawnow
        
        
        % Temperature
        figure(2), clf
        PlotMesh(MESH,MESH.TEMP*CHAR.Temperature-273, CHAR.Length/1e3), axis equal, axis tight, colorbar; shading flat
        axis equal, axis tight, colorbar, title('Temperature [C]')
        
        figure(3), clf
        PlotMesh(MESH,log10(MESH.CompVar.Mu_Eff*CHAR.Viscosity), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('log10(effective viscosity)')
        
        figure(4), clf
        PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('log10(e2nd [1/s])')
        
        figure(5), clf
        PlotMesh(MESH,(MESH.CompVar.T2nd*(CHAR.Stress/1e6)), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('(T2nd [MPa])')
        
        figure(7), clf
        PlotMesh(MESH,(MESH.CompVar.Rho*(CHAR.Density)), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('Density [kg/m^3]')
        
        
        figure(8), clf
        X=MESH.NODES(1,:);Z=MESH.NODES(2,:);
        X2d = X(MESH.RegularGridNumber)*CHAR.Length;
        Z2d = Z(MESH.RegularGridNumber)*CHAR.Length;
        plot(X2d(end,:)/1e3,Z2d(end,:))
        xlabel('Width [km]')
        ylabel('Topography [m]')
        drawnow
        
        Z   = MESH.NODES(2,:);
        Z2d = Z(MESH.RegularGridNumber)*CHAR.Length/1e3;
        T2nd = MESH.CompVar.T2nd(MESH.RegularGridNumber)*CHAR.Stress/1e6;
        
        
        figure(9), clf
        PlotMesh(MESH,(MESH.VEL(2,:)*(CHAR.Velocity*CHAR.cm_Year)), CHAR.Length/1e3), shading interp
        colorbar, title('(Vz [cm/yr])')
        xlabel('Width [km]')
        ylabel('Topography [m]')
        axis equal, axis tight
        drawnow
        
        
        
        figure(3), drawnow
        
        
        
    end
    
    % Create breakpoint file if required
    Save_BreakpointFile;
    
    % Save filename if required
    if mod(itime,20)==0
        SaveFile(itime, PARTICLES, NUMERICS,MESH,INTP_PROPS,CHAR,BC, time);
    end
    
    
    end_cpu         = cputime;
    time_timestep    = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs; dt=',num2str(dt*CHAR.Time/CHAR.SecYear/1e3),' kyrs'])
    disp(' ')
    
end

%% Visualize_subduction_Li
%
% Creates a visualization of the subduction_Li simulation (after Li(2010).


% Add the necessary directories to the path
% addpath(genpath('../mutils-0.4'));
addpath(genpath('../SOURCE_CODE/'))



% Specify the directory in which you want to visualize the models
Directory = './subduction_Li_correct_VisualizeTest'; % current directory
Directory = pwd;

%
FiguresVisible = 'on';
if isunix & ~ismac
    FiguresVisible = 'off'; % faster visualization (but not interactive)
end

%renderer = '-opengl';
renderer = '-zbuffer';



for num=10:1:4000
    num
    
    
    %----------------------------------------------------------------------
    % LOAD FILE FROM DIRECTORY (IF PRESENT)
    %----------------------------------------------------------------------
    % Load file
    curdir          = pwd;
    cd(Directory)
    fname       = ['Output','_', num2str(num+1000000),'.mat'];
    if exist(fname,'file')
        load_file=logical(1);
        load(fname)
    else
        load_file=logical(0);
    end
    
    cd(curdir)
    
    
    if load_file
        %----------------------------------------------------------------------
        % SOME COMPUTATIONS
        %----------------------------------------------------------------------
        X           =   MESH.NODES(1,:);
        Z           =   MESH.NODES(2,:);
        VELX     	=   MESH.VEL(1,:);
        VELZ    	=   MESH.VEL(2,:);
        
        X2d         =   X(MESH.RegularGridNumber)*CHAR.Length/1e3;
        Z2d         =   Z(MESH.RegularGridNumber)*CHAR.Length/1e3;
        
        Vx_2d     	=   VELX(MESH.RegularGridNumber)*CHAR.Velocity*CHAR.cm_Year;
        Vz_2d     	=   VELZ(MESH.RegularGridNumber)*CHAR.Velocity*CHAR.cm_Year;
        Vel_2d      =   sqrt(Vx_2d.^2 + Vz_2d.^2);
        
        
        
        Topo_x      =   X2d(end,:);
        Topo_z      =   Z2d(end,:);
        x_min       =   min(X2d(:));
        x_max       =   max(X2d(:));
        z_min       =   min(Z2d(:));
        z_max       =   max(Z2d(:));
        
        % Interpolate Velocity to a lower resolution grid
        num_lowx    =   50;
        num_lowz    =   fix((z_max-z_min)/(x_max-x_min)*num_lowx);
        dz_low      =   (z_max-z_min)/num_lowz;
        [Xlow,Zlow] =   meshgrid(x_min:(x_max-x_min)/num_lowx:x_max, z_min+dz_low:dz_low:z_max);
        
        Vxlow       =   griddata(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(1,:), Xlow,Zlow);
        Vzlow       =   griddata(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(2,:), Xlow,Zlow);
        
        
        %==================================================================
        % Create colormaps etc. for this setup
        % We can specify the RGB colors for each phase here
        %
        CompositionalPhases     =    double(GRID_DATA.Phases*0);            %
        
        
        % Mantle
        CompositionalPhases(GRID_DATA.Phases==1 ) = 1;
        PhaseColor(1,:)         =      [96 89 166]./255   ;
        
        % CuC
        CompositionalPhases(GRID_DATA.Phases==2 ) = 2;
        PhaseColor(2,:)         =      [150 49 120 ]./255   ;
        
        % CuC_Visualize
        CompositionalPhases(GRID_DATA.Phases==3 ) = 3;
        PhaseColor(3,:)         =      [156 169 174 ]./255   ;
        
        % ClC_S
        CompositionalPhases(GRID_DATA.Phases==4 ) = 4;
        PhaseColor(4,:)         =    [ 157 176 69]./255;
        
        % ClC_S_Visualize
        CompositionalPhases(GRID_DATA.Phases==5 ) = 5;
        PhaseColor(5,:)         =    [ 0 161 77]./255;
        
        %Lithosphere
        CompositionalPhases(GRID_DATA.Phases==7 ) = 7;
        PhaseColor(7,:)         =      [52 55 147]./255   ;
        
        %Weak Channel 1
        CompositionalPhases(GRID_DATA.Phases==10 ) = 10;
        PhaseColor(10,:)         =      [0 174 230]./255   ;
        
        %Weak Channel 2
        CompositionalPhases(GRID_DATA.Phases==11 ) = 11;
        PhaseColor(11,:)         =      [133 186 229]./255   ;
        
        %Sediments
        CompositionalPhases(GRID_DATA.Phases==8 ) = 8;
        PhaseColor(8,:)         =      [242 146 54]./255   ;
        
        %Sediments_Visualize
        CompositionalPhases(GRID_DATA.Phases==9 ) = 9;
        PhaseColor(9,:)         =      [177 114 51]./255   ;
        
        
        
        
        if max(CompositionalPhases(:))<size(PhaseColor,1)-1
            PhaseColor = PhaseColor(1:max(CompositionalPhases(:)),:);
        end
        [PhaseColorMap]  = CreatePhasesColormap(PhaseColor);
        
        maxPhase                =  length(PhaseColor);
        
        
        % Color of the arrows
        color_arrows = [200 200 200]/255;
        
        %==================================================================
        
        
        %--------------------------------------------------------------------------
        % CREATE THE PLOT WITH COMPOSITION
        %--------------------------------------------------------------------------
        h=figure(1); clf
        set(h,'Visible',FiguresVisible);
        %         pcolor(X2d,Z2d,Vel_2d); shading interp
        %         colormap(jet)
        hc=colorbar
        ylabel(hc,'Velocity [cm/yr]')
        
        
        CompositionalPhases1=CompositionalPhases;
        %         CompositionalPhases1(CompositionalPhases==1)=NaN;
        freezeColors
        
        hold on
        
        
        % Add melt fraction on top
        h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
        
        hold on
        
        h=pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
        shading interp; colormap(PhaseColorMap);
        
        caxis([1 maxPhase])
        %  set(h,'FaceAlpha',0.5)      % make blocks transparent - requires openGL rendered
        
        axis equal
        hold on
        
        plot(Topo_x,Topo_z,'k')       % surface topo
        freezeColors
        
        % Add temperature on top
        
        Temperature_positive = GRID_DATA.Temperature;
        ind = find(Temperature_positive<0);
        Temperature_positive(ind) = 0;

        
        [c,h]= contour(GRID_DATA.Xvec,GRID_DATA.Zvec,Temperature_positive,[100:200:1300],'w');
        set(h,'Linewidth',0.0005)
        
        axis equal
        axis tight
        
        freezeColors
        colormap(jet)
        
        drawnow
        title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
        xlabel('Width [km]')
        ylabel('Depth [km]')
        box on
        
        
        %--------------------------------------------------------------------------
        % COMPOSITION AND STRAIN RATE
        %--------------------------------------------------------------------------
        h=figure(2); clf
        set(h,'Visible',FiguresVisible);
        subplot(211)
        pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
        shading interp; colormap(PhaseColorMap);
        
        caxis([1 maxPhase])
        axis equal
        hold on
        
        plot(Topo_x,Topo_z,'k')       % surface topo
        freezeColors
        hc= colorbar('horiz');
        
        % Add temperature on top
        
        % Add melt fraction on top
        h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
        
        axis equal
        axis tight
        drawnow
        title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
        xlabel('Width [km]')
        ylabel('Depth [km]')
        box on
        
        subplot(212)
        PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)),CHAR.Length/1e3,'-')
        shading interp; h=colorbar('horiz');
        axis equal, axis tight
        colormap(jet)
        xlabel(h,'log10(e_{II}) [1/s]')
        title('Strainrate')
        
        
        set(hc,'Visible','off')
        
        
        %--------------------------------------------------------------------------
        % COMPOSITION AND EFFECTIVE VISCOSITY
        %--------------------------------------------------------------------------
        h=figure(3); clf
        set(h,'Visible',FiguresVisible);
        subplot(211)
        pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
        shading interp; colormap(PhaseColorMap);
        
        caxis([1 maxPhase])
        axis equal
        hold on
        
        plot(Topo_x,Topo_z,'k')       % surface topo
        freezeColors
        hc= colorbar('horiz');
        
        % Add temperature on top
        
        % Add melt fraction on top
        h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
        
        axis equal
        axis tight
        drawnow
        title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
        xlabel('Width [km]')
        ylabel('Depth [km]')
        box on
        
        subplot(212)
        PlotMesh(MESH,log10(MESH.CompVar.Mu_Eff*(CHAR.Viscosity)),CHAR.Length/1e3,'-')
        shading interp;
        axis equal, axis tight
        colormap(jet)
        
        caxis([18 25])
        h=colorbar('horiz');
        xlabel(h,'log10(\eta) [Pa s]')
        title('Effective viscosity')
        
        
        set(hc,'Visible','off')
        
        
        
        %--------------------------------------------------------------------------
        % COMPOSITION AND DENSITY
        %--------------------------------------------------------------------------
        h=figure(4); clf
        set(h,'Visible',FiguresVisible);
        subplot(211)
        pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
        shading interp; colormap(PhaseColorMap);
        
        caxis([1 maxPhase])
        axis equal
        hold on
        
        plot(Topo_x,Topo_z,'k')       % surface topo
        freezeColors
        hc= colorbar('horiz');
        
        % Add temperature on top
        
        % Add melt fraction on top
        h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
        
        axis equal
        axis tight
        drawnow
        title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
        xlabel('Width [km]')
        ylabel('Depth [km]')
        box on
        
        subplot(212)
        PlotMesh(MESH,(MESH.CompVar.Rho*(CHAR.Density)),CHAR.Length/1e3,'-')
        shading interp;
        axis equal, axis tight
        colormap(jet)
        
        h=colorbar('horiz');
        xlabel(h,'Density [kg m^{-3}]')
        title('Density')
        
        
        set(hc,'Visible','off')
        
        
        
        %--------------------------------------------------------------------------
        % COMPOSITION AND STRESS
        %--------------------------------------------------------------------------
        h=figure(5); clf
        set(h,'Visible',FiguresVisible);
        subplot(211)
        pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
        shading interp; colormap(PhaseColorMap);
        
        caxis([1 maxPhase])
        axis equal
        hold on
        
        plot(Topo_x,Topo_z,'k')       % surface topo
        freezeColors
        hc= colorbar('horiz');
        
        % Add temperature on top
        
        % Add melt fraction on top
        h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
        
        axis equal
        axis tight
        drawnow
        title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
        xlabel('Width [km]')
        ylabel('Depth [km]')
        box on
        set(hc,'Visible','off')
        
        
        subplot(212)
        PlotMesh(MESH,(MESH.CompVar.T2nd*(CHAR.Stress/1e6)),CHAR.Length/1e3,'-')
        shading interp;
        axis equal, axis tight
        colormap(jet)
        
        h=colorbar('horiz');
        xlabel(h,'Stress [MPa]')
        title('Stress')
        
        
        %--------------------------------------------------------------------------
        % COMPOSITION AND TEMP
        %--------------------------------------------------------------------------
        h=figure(7); clf
        set(h,'Visible',FiguresVisible);
        subplot(211)
        pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
        shading interp; colormap(PhaseColorMap);
        
        caxis([1 maxPhase])
        axis equal
        hold on
        
        plot(Topo_x,Topo_z,'k')       % surface topo
        freezeColors
        hc= colorbar('horiz');
        
        % Add temperature on top
        
        % Add melt fraction on top
        h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
        
        axis equal
        axis tight
        drawnow
        title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
        xlabel('Width [km]')
        ylabel('Depth [km]')
        box on
        set(hc,'Visible','off')
        
        
        subplot(212)
        PlotMesh(MESH,(MESH.TEMP*CHAR.Temperature-273),CHAR.Length/1e3,'-')
        shading interp;
        axis equal, axis tight
        colormap(jet)
        
        h=colorbar('horiz');
        xlabel(h,'T Celcius')
        title('Temperature')
        
        
        
        %------------------------------------------------------------------
        % Plot topography
        %------------------------------------------------------------------
        h=figure(6); clf
        set(h,'Visible',FiguresVisible);
        
        
        set(h,'Visible',FiguresVisible);
        %             subplot('position',[0.08 0.12 0.4 .8])
        subplot(211)
        pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
        shading interp; colormap(PhaseColorMap);
        caxis([1 maxPhase])
        axis equal
        axis tight
        
        
        hold on
        plot(Topo_x,Topo_z,'k')       % surface topo
        freezeColors
        
        
        % Add temperature on top
        
        if num==1
            clabel(c,h,'labelspacing',200)
        end
        drawnow
        title(['Composition and Melt Fraction; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
        xlabel('Width [km]')
        ylabel('Depth [km]')
        box on
        
        
        % Add velocity
        h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
        
        
        
        
        %             subplot('position',[0.50 0.12 0.4 .8])
        subplot(212)
        plot(Topo_x,Topo_z,'k')       % surface topo
        colormap(jet)
        xlabel('Width [km]')
        ylabel('Topography [km]')
        title(['Topography; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%3.1f'),' kyrs'])
        
        
           
        %--------------------------------------------------------------------------
        % ZOOM-IN ON COMPOSITION
        %--------------------------------------------------------------------------
        h=figure(8); clf
        set(h,'Visible',FiguresVisible);
        %         pcolor(X2d,Z2d,Vel_2d); shading interp
        %         colormap(jet)
        
        
        CompositionalPhases1=CompositionalPhases;
        %         CompositionalPhases1(CompositionalPhases==1)=NaN;

        
        hold on
        
        
        % Add melt fraction on top
        h= quiver(Xlow,Zlow,Vxlow,Vzlow,'Color',color_arrows);
        
        hold on
        
        h=pcolor(GRID_DATA.Xvec,GRID_DATA.Zvec,CompositionalPhases);
        shading interp; colormap(PhaseColorMap);
        
        caxis([1 maxPhase])
        %  set(h,'FaceAlpha',0.5)      % make blocks transparent - requires openGL rendered
        
        axis equal
        hold on
        
        plot(Topo_x,Topo_z,'k')       % surface topo

        
        % Add temperature on top
        Temp_2d = MESH.TEMP(MESH.RegularGridNumber)*CHAR.Temperature-273;
        [c,h]   = contour(X2d,Z2d,Temp_2d,[100:200:1300],'w');
        set(h,'Linewidth',0.0005)
        
        axis equal
        axis tight
        
       
        drawnow
        title(['Composition; Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6,'%3.1f'),' Myrs'])
        xlabel('Width [km]')
        ylabel('Depth [km]')
        box on
        axis([1650 2200 -200 20])
        
        
        
        
        if 1==1
            %------------------------------------------------------------------
            % SAVE PICTURES TO DISK
            %------------------------------------------------------------------
            cd(Directory)
            %             res = '-r600';     % high resolution (for publications)
            %             res = '-r300';     % high resolution (for publications)
            res = '-r300';      %
            
            
            h=figure(1);
            set(h,'Visible',FiguresVisible);
            
            fname = ['Composition',num2str(num+1e6),'.jpg'];
            print('-dpng',renderer,res,fname)
            
            
            
            %             export_fig(fname,res)
            
            h=figure(2);
            set(h,'Visible',FiguresVisible);
            fname = ['CompositionAndStrainrate',num2str(num+1e6),'.jpg'];
            %             print('-djpeg','-zbuffer',res,fname)
            %             export_fig(fname,res)
            %             print( gcf, fname,'-djpeg', res )
            print('-dpng',renderer,res,fname)
            
            
            
            h=figure(3);
            set(h,'Visible',FiguresVisible);set(h,'Renderer','zbuffer');
            fname = ['CompositionAndViscosity',num2str(num+1e6),'.jpg'];
            %             print('-djpeg','-zbuffer',res,fname)
            print('-dpng',renderer,res,fname)
            
            h=figure(4);
            set(h,'Visible',FiguresVisible);set(h,'Renderer','zbuffer');
            fname = ['CompositionAndDensity',num2str(num+1e6),'.jpg'];
            print('-dpng',renderer,res,fname)
            
            h=figure(5);
            set(h,'Visible',FiguresVisible);set(h,'Renderer','zbuffer');
            fname = ['CompositionAndStress',num2str(num+1e6),'.jpg'];
            print('-dpng',renderer,res,fname)
            
            h=figure(6);
            set(h,'Visible',FiguresVisible);set(h,'Renderer','zbuffer');
            fname = ['CompositionAndTopography',num2str(num+1e6),'.jpg'];
            print('-dpng',renderer,res,fname)
            
            h=figure(7);
            set(h,'Visible',FiguresVisible);set(h,'Renderer','zbuffer');
            fname = ['CompositionAndTemperature',num2str(num+1e6),'.jpg'];
            print('-dpng',renderer,res,fname)
            
            h=figure(8);
            set(h,'Visible',FiguresVisible);set(h,'Renderer','zbuffer');
            fname = ['CompositionZoom',num2str(num+1e6),'.jpg'];
            print('-dpng',renderer,res,fname)
            
        end
        
        
        
        cd(curdir)
        
        
    end
    
    
    %    pause
end
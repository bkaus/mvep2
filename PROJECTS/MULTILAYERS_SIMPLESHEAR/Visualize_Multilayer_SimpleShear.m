% Visualize_Multilayer_SimpleShear
%



% Add the necessary directories to the path
% addpath(genpath('../mutils-0.4'));
addpath(genpath('../../SOURCE_CODE/'))



% Specify the directory in which you want to visualize the models
Directory = './TEST1'; % current directory


%
FiguresVisible = 'on';
if isunix & ~ismac
    FiguresVisible = 'off'; % faster visualization (but not interactive)
end


%renderer = 'opengl';
renderer = 'zbuffer';



for num=1:1:4000
% for num=1:30
    num
    
    
    %----------------------------------------------------------------------
    % LOAD FILE FROM DIRECTORY (IF PRESENT)
    %----------------------------------------------------------------------
    % Load file
    curdir          = pwd;
    cd(Directory)
    fname       = ['Output','_', num2str(num+1000000),'.mat'];
    if exist(fname,'file')
        load_file=logical(1);
        load(fname)
    else
        load_file=logical(0);
    end
    
    cd(curdir)
    
    
    if load_file
        %----------------------------------------------------------------------
        % SOME COMPUTATIONS
        %----------------------------------------------------------------------
        X           =   MESH.NODES(1,:);
        Z           =   MESH.NODES(2,:);
        VELX     	=   MESH.VEL(1,:);
        VELZ    	=   MESH.VEL(2,:);
        
        X2d         =   X(MESH.RegularGridNumber)*CHAR.Length/1e3;
        Z2d         =   Z(MESH.RegularGridNumber)*CHAR.Length/1e3;
        
        Vx_2d     	=   VELX(MESH.RegularGridNumber)*CHAR.Velocity*CHAR.cm_Year;
        Vz_2d     	=   VELZ(MESH.RegularGridNumber)*CHAR.Velocity*CHAR.cm_Year;
        Vel_2d      =   sqrt(Vx_2d.^2 + Vz_2d.^2);
        
        
        
        Topo_x      =   X2d(end,:);
        Topo_z      =   Z2d(end,:);
        x_min       =   min(X2d(:));
        x_max       =   max(X2d(:));
        z_min       =   min(Z2d(:));
        z_max       =   max(Z2d(:));
        
        % Interpolate Velocity to a lower resolution grid
        num_lowx    =   50;
        num_lowz    =   fix((z_max-z_min)/(x_max-x_min)*num_lowx);
        [Xlow,Zlow] =   meshgrid(x_min:(x_max-x_min)/num_lowx:x_max, z_min:(z_max-z_min)/num_lowz:z_max);
        
        Vxlow       =   griddata(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(1,:), Xlow,Zlow);
        Vzlow       =   griddata(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(2,:), Xlow,Zlow);
        
        
        %==================================================================
        % Create colormaps etc. for this setup
        % We can specify the RGB colors for each phase here
        %
        CompositionalPhases     =    double(GRID_DATA.Phases*0);            %
        
        % MATRIX
        CompositionalPhases(GRID_DATA.Phases==1 ) = 1;
        PhaseColor(1,:)         =      [220 220 220]./255   ;
        
        % LAYERS
        CompositionalPhases(GRID_DATA.Phases==2 ) = 2;
        PhaseColor(2,:)         =    [ 10 10 10]./255;
        
        CompositionalPhases(GRID_DATA.Phases==3 ) = 3;
        PhaseColor(3,:)         =      [17 39 63]./255   ;
        
        if max(CompositionalPhases(:))<size(PhaseColor,1)-1
            PhaseColor = PhaseColor(1:max(CompositionalPhases(:)),:);
        end
        [PhaseColorMap]  = CreatePhasesColormap(PhaseColor);
        
        maxPhase                =  size(PhaseColor,2);
        
        
        % Color of the arrows
        color_arrows = [200 200 200]/255;
        
        %==================================================================
        
        
        %--------------------------------------------------------------------------
        % CREATE THE PLOT WITH COMPOSITION
        %--------------------------------------------------------------------------
        h=figure(1); clf
        h=pcolor(GRID_DATA.Xvec*1e3/1e-6,GRID_DATA.Zvec*1e3/1e-6,CompositionalPhases);
        shading interp; colormap(PhaseColorMap);
        caxis([1 maxPhase])
        
        axis equal
        hold on
        
        drawnow
        title(['Composition; Gamma=',num2str(gamma,'%3.1f'),' '])
        xlabel('Width [\mum]')
        ylabel('Height [\mum]')
        axis equal, axis tight
        box on
        
        
        %--------------------------------------------------------------------------
        % COMPOSITION AND STRAIN RATE
        %--------------------------------------------------------------------------
        h=figure(2); clf
        set(h,'Visible',FiguresVisible);
        subplot(211)
        pcolor(GRID_DATA.Xvec*1e3/1e-6,GRID_DATA.Zvec*1e3/1e-6,CompositionalPhases);
        shading interp; colormap(PhaseColorMap);
        
        caxis([1 maxPhase])
        axis equal, axis tight
        hold on
        
        freezeColors
        hc= colorbar('horiz');
        
        
        axis equal
        axis tight
        drawnow
        title(['Composition; Gamma=',num2str(gamma,'%3.1f'),' '])
        xlabel('Width [\mum]')
        ylabel('Height [\mum]')
        
        
        box on
        
        subplot(212)
        PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)),CHAR.Length/1e-6,'-')
        shading interp; h=colorbar('horiz');
        axis equal, axis tight
        colormap(jet)
        xlabel(h,'log10(e_{II}) [1/s]')
        title('Strainrate')
        
        
        set(hc,'Visible','off')
        
        %--------------------------------------------------------------------------
        % COMPOSITION AND STRAIN RATE
        %--------------------------------------------------------------------------
        h=figure(3); clf
        set(h,'Visible',FiguresVisible);
        subplot(211)
        pcolor(GRID_DATA.Xvec*1e3/1e-6,GRID_DATA.Zvec*1e3/1e-6,CompositionalPhases);
        shading interp; colormap(PhaseColorMap);
        
        caxis([1 maxPhase])
        axis equal, axis tight
        hold on
        
        freezeColors
        hc= colorbar('horiz');
        
        
        axis equal
        axis tight
        drawnow
        title(['Composition; Gamma=',num2str(gamma,'%3.1f'),' '])
        xlabel('Width [\mum]')
        ylabel('Height [\mum]')
        
        
        box on
        
        subplot(212)
        PlotMesh(MESH,log10(MESH.CompVar.Mu_Eff*(CHAR.Viscosity)),CHAR.Length/1e-6,'-')
        shading interp; h=colorbar('horiz');
        axis equal, axis tight
        colormap(jet)
        xlabel(h,'log10(\eta_{eff}) [Pa s]')
        title('Effective Viscosity')
        
        
        set(hc,'Visible','off')
        
        
        
        
        
        
        
        
        if 1==1
            %------------------------------------------------------------------
            % SAVE PICTURES TO DISK
            %------------------------------------------------------------------
            cd(Directory)
            %             res = '-r600';     % high resolution (for publications)
            %             res = '-r300';     % high resolution (for publications)
            res = '-r300';      %
            
            
            h=figure(1);
            set(h,'Visible',FiguresVisible);
            fname = ['Composition',num2str(num+1e6),'.png'];
            print('-dpng','-zbuffer',res,fname)
            
            
            %             export_fig(fname,res)
            h=figure(2);
            set(h,'Visible',FiguresVisible);
            fname = ['CompositionAndStrainrate',num2str(num+1e6),'.png'];
            print('-dpng','-zbuffer',res,fname)
            
            h=figure(3);
            set(h,'Visible',FiguresVisible);set(h,'Renderer','zbuffer');
            fname = ['CompositionAndViscosity',num2str(num+1e6),'.png'];
            print('-dpng','-zbuffer',res,fname)
            %             export_fig(fname,res)
            
            %
            %             h=figure(4);
            %             set(h,'Visible',FiguresVisible);set(h,'Renderer','zbuffer');
            %             fname = ['CompositionAndDensity',num2str(num+1e6),'.png'];
            %             print('-dpng','-zbuffer',res,fname)
            %             %             export_fig(fname,res)
            %
            %             h=figure(5);
            %             set(h,'Visible',FiguresVisible);set(h,'Renderer','zbuffer');
            %             fname = ['CompositionAndStress',num2str(num+1e6),'.png'];
            %             print('-dpng','-zbuffer',res,fname)
            %             %             export_fig(fname,res)
            %
            %             h=figure(6);
            %             set(h,'Visible',FiguresVisible);set(h,'Renderer','zbuffer');
            %             fname = ['CompositionAndTopography',num2str(num+1e6),'.png'];
            %             print('-dpng','-zbuffer',res,fname)
            %             %             export_fig(fname,res)
            
        end
        
        
        
        cd(curdir)
        
        
    end
    
    
    %    pause
end
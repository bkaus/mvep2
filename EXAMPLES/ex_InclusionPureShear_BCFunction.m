% Models a circular inclusion that is subjected to Pure Shear, using an
% unstructured mesh, for which an analytical solution exists

% Add paths & subdirectories
AddDirectories;

clear

tic

if ismac
    CreatePlots         =   logical(1);
else
    CreatePlots         =   logical(0);
end

factor              =   2;
SecYear             =   3600*24*365.25;
NUMERICS.Restart    =   logical(1);
NUMERICS.dt_max     =   250e3*SecYear;
NUMERICS.time_end   =   10;
%UpperBC             =   'Free Slip';
%UpperBC             =   'No Stress';

Rinc = 1;

%% Create mesh
opts.element_type.velocity   =   'T2';
opts.element_type.pressure   =   'P-1';
mesh_input.x_min    =   -2;
mesh_input.x_max    =    2;
mesh_input.z_min    =   -2;
mesh_input.z_max    =    2;
NUMERICS.Nonlinear.Tolerance=1;    % linear viscous so we can switch off nonlinearities
NUMERICS.Plasticity.MaximumYieldStress = 1e20;
 NUMERICS.LinearSolver.PressureShapeFunction='Global'

%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values


%------------------------------
% Matrix
Phase                   =   1;
Type                                            =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu       =   1;                             % parameter

Type              	=   'Constant';
MATERIAL_PROPS(Phase).Density.(Type).Rho        =   1;


%
%----------------------------------

%------------------------------
% Inclusion
Phase            	=   2;
Type                                            =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu       =   1e-3;                             % parameter

Type              	=   'Constant';
MATERIAL_PROPS(Phase).Density.(Type).Rho        =   3300;
%
%----------------------------------

% Gravitational acceleration
MATERIAL_PROPS(1).Gravity.Value                 =   0;

% Add material properties that were not defined here, but which MILAMIN_VEP needs to know
MATERIAL_PROPS                                 =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required


%% Numerical parameters
NUMERICS.Viscosity.LowerCutoff                  =   1e-6;
NUMERICS.Viscosity.UpperCutoff                  =   1e6;
NUMERICS                                        =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required



%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress','velocity function'};
BC.Stokes.Bottom        =   Bound{5};
BC.Stokes.Left          =   Bound{5};
BC.Stokes.Right         =   Bound{5};
BC.Stokes.Top           =   Bound{5};
BC.Stokes.BG_strrate    =   1;

% get analytical values for boundary velocity
BC.Stokes.VelocityFunction.Bottom       =   @(x,z)eval_sol_VI_Vel(x,z,0,BC.Stokes.BG_strrate,MATERIAL_PROPS(1).Viscosity.Constant.Mu,MATERIAL_PROPS(2).Viscosity.Constant.Mu,Rinc);   
BC.Stokes.VelocityFunction.Top          =   @(x,z)eval_sol_VI_Vel(x,z,0,BC.Stokes.BG_strrate,MATERIAL_PROPS(1).Viscosity.Constant.Mu,MATERIAL_PROPS(2).Viscosity.Constant.Mu,Rinc);  
BC.Stokes.VelocityFunction.Left         =   @(x,z)eval_sol_VI_Vel(x,z,0,BC.Stokes.BG_strrate,MATERIAL_PROPS(1).Viscosity.Constant.Mu,MATERIAL_PROPS(2).Viscosity.Constant.Mu,Rinc);   
BC.Stokes.VelocityFunction.Right        =   @(x,z)eval_sol_VI_Vel(x,z,0,BC.Stokes.BG_strrate,MATERIAL_PROPS(1).Viscosity.Constant.Mu,MATERIAL_PROPS(2).Viscosity.Constant.Mu,Rinc); 



% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   1;
BC.Energy.Value.Bottom=   1;                    % T at bottom of mantle


%% Create initial Particles

factor                      =   1;
numz                        =   400*factor;
numx                        =   ceil((mesh_input.x_max-mesh_input.x_min)/(mesh_input.z_max-mesh_input.z_min))*numz;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (mesh_input.z_max-mesh_input.z_min)/numz;
[PARTICLES.x,PARTICLES.z]   =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,mesh_input.z_min:dz:mesh_input.z_max);
PARTICLES.x                 =   PARTICLES.x(:);
PARTICLES.z                 =   PARTICLES.z(:);
PARTICLES.x                 =   PARTICLES.x + 0*[rand(size(PARTICLES.x))-0.5]*dx;
PARTICLES.z                 =   PARTICLES.z + 0*[rand(size(PARTICLES.x))-0.5]*dz;

% Set initial temperature
PARTICLES.HistVar.T         =   PARTICLES.x*0;       % in K
PARTICLES.HistVar.Txx       =   PARTICLES.x*0;
PARTICLES.HistVar.Tyy       =   PARTICLES.x*0;
PARTICLES.HistVar.Txy       =   PARTICLES.x*0;




%% Non-dimensionalize input parameters
CHAR.Length         =       1;
CHAR.Viscosity      =       1;
CHAR.Time           =       1;
CHAR.Temperature 	=       1;
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,CHAR);


% Create contours to build unstructured mesh
opts.element_type.velocity   =   'T2';
opts.element_type.pressure   =   'P-1';
opts.max_tri_area   =   .01;


% Outer box of the computational domain
points              = [ mesh_input.x_min mesh_input.z_min;
    mesh_input.x_max mesh_input.z_min;
    mesh_input.x_max mesh_input.z_max;
    mesh_input.x_min mesh_input.z_max]';
n                   = length(points)
segments            = [[1:n]; [2:n, 1]; ];
segmentmarkers      = [ 1; 2; 3; 4]';


% Inclusion
t                   = [0:.1:2*pi];
points1             = [1*cos(t); -sin(t)*1];
n1                   = length(points1);
segments1           = [[1:n1]; [2:n1, 1]; ]+length(segments)+1;
% segments1           = [[1:n1-1]; [2:n1]; ]+length(segments)+1;

segmentmarkers1     = ones(1,n1)*5;


points                  = [points,          points1];
segmentmarkers          = [segmentmarkers,  segmentmarkers1];
segments                = [segments,        segments1];


% Set triangle input
mesh_input.points         = points;
mesh_input.segments       = uint32(segments);
mesh_input.segmentmarkers = uint32(segmentmarkers);


%% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input, []);



%% ADD PARTICLES such that every triangle has at least one particle
ind                     =   (MESH.ELEMS(7,:));
PARTICLES1.x            =   MESH.NODES(1,ind)';
PARTICLES1.z            =   MESH.NODES(2,ind)';
PARTICLES.x             =   [PARTICLES.x; PARTICLES1.x ];
PARTICLES.z             =   [PARTICLES.z; PARTICLES1.z ];





% Set initial temperature
PARTICLES.HistVar.T         =   PARTICLES.x*0;       % in K
PARTICLES.HistVar.Txx       =   PARTICLES.x*0;
PARTICLES.HistVar.Tyy       =   PARTICLES.x*0;
PARTICLES.HistVar.Txy       =   PARTICLES.x*0;


% Set phases
PARTICLES.phases            =   ones(size(PARTICLES.x));


Xc                          =   0;
Zc                          =   0;
ind                         =   find( (((PARTICLES.x-Xc).^2 + (PARTICLES.z-Zc).^2) < (Rinc).^2) );
PARTICLES.phases(ind)       =   2;


NUMERICS.InterpolationMethod.ParticlesToIntegrationPoints = 'constant';     % constant properties per element
NUMERICS.LinearSolver.UseFreeSurfaceStabilizationAlgorithm=logical(0);

NUMERICS.LinearSolver.PressureShapeFunction = 'Original_MILAMIN';



dt = 1e-3;

[PARTICLES, MESH, INTP_PROPS, NUMERICS] 	=   StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);

INTP_PROPS.E2nd=INTP_PROPS.Strainrate.E2nd;
INTP_PROPS.T2nd=INTP_PROPS.Stress.T2nd;

%% Update BC's for stokes
[BC]                                    =   SetBC(MESH, BC, INTP_PROPS);



%
INTP_PROPS.PHASE_PROP = round(INTP_PROPS.PHASE_PROP);




%% Compute Stokes solution
[MESH, INTP_PROPS, SUCCESS, minDetJ, NUMERICS]    =   Stokes2d_vep(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, CHAR, PARTICLES, dt);




[ MESH_DATA_average, MESH_DATA] = Extrapolate_IntpDataToNodes(MESH, INTP_PROPS.Stress.Txx, NUMERICS);



figure(1), clf
%plotclr(INTP_PROPS.X(:),INTP_PROPS.Z(:),(INTP_PROPS.Stress.Tyy(:)),'o')
plotclr(INTP_PROPS.X(:),INTP_PROPS.Z(:),(INTP_PROPS.Pressure(:)),'o')
axis([-2 2 -2 2])



figure(3), clf
PlotMesh(MESH,log10(INTP_PROPS.Mu_Eff'*CHAR.Viscosity)); shading interp
axis equal, axis tight, colorbar, title('log10(effective viscosity)')
axis([-2 2 -2 2])


figure(4), clf
PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time))); shading interp
axis equal, axis tight, colorbar, title('log10(e2nd [1/s])')
axis([-2 2 -2 2])

figure(2), clf
subplot(121)
PlotMesh(MESH,(INTP_PROPS.Pressure')); %shading interp
axis([-2 2 -2 2])
title('P at Integration points')


subplot(122)
PlotMesh(MESH,MESH.CompVar.Pressure);
axis([-2 2 -2 2])
title('P at nodes')
colorbar
drawnow


% plot P directly
figure(5), clf
X =     MESH.NODES(1,:);
Z =     MESH.NODES(2,:);
X =     X(MESH.ELEMS);
Z =     Z(MESH.ELEMS);
patch(X(1:3,:),Z(1:3,:),MESH.PRESSURE)




figure(6), clf
id          =   [1 6 2 4 3 5 1];    

% id = [1 2 3];
% 
% % plot P extrapolated from intp -> nodes
% % MESH_DATA1   =   reshape(MESH_DATA_average(1,MESH.ELEMS), 7, 2464);
% 
% MESH_DATA1 = MESH_DATA_average(MESH.ELEMS);
%  
% 
% patch(X(id,:),Z(id,:),MESH_DATA(id,:))
% % patch(X(1:3,:),Z(1:3,:),MESH_DATA(1:3,:))

MESH_DATA1 = MESH_DATA_average(MESH.ELEMS);


els = 1:length(X);
subplot(121)
patch(X(id,els), Z(id,els), MESH_DATA(id,els))

subplot(122)
patch(X(id,els), Z(id,els), MESH_DATA1(id,els))




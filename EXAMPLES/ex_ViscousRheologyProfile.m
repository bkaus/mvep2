% This tests the implementation of Creep rheologies in the code,
% by assuming a linear temperature profile.
% 
% The results are compared with the results from a MATLAB script related to 
% the textbook of Taras Gerya

% $Id$


% Add paths & subdirectories
AddDirectories;

clear

tic

%% Create mesh
opts.element_type.velocity   =   'Q2';
opts.element_type.pressure   =   'P-1';
mesh_input.x_min    =   -5e3;
mesh_input.x_max    =    5e3;
mesh_input.z_min    =   -100e3;
mesh_input.z_max    =   0;
opts.nx             =   11;
opts.nz             =   201;

T_top_Celcius       =   400;
T_bottom_Celcius    =   1200;

e_bg                =   1e-15;
% T_top_Celcius       =   1400;
% T_bottom_Celcius    =   1400;

% % Uncomment this to do particle-based averaging:
% NUMERICS.Nonlinear.Method = 'ParticleBased';
% NUMERICS.InterpolationMethod.ParticlesToIntegrationPoints = 'Arithmetic';

% NUMERICS.Nonlinear.IterationMethod          =       'NonlinearResidual';
NUMERICS.Nonlinear.NumberPicardIterations   =       10;
NUMERICS.Nonlinear.MinNumberIterations     = 5;
NUMERICS.Plasticity.MaximumYieldStress = 1e80;
%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values

% Properties
Phase                =   1;
MATERIAL_PROPS=[];
[MATERIAL_PROPS]   =   Add_DislocationCreep([], Phase, 'Dry Olivine - Ranalli 1995');   % to compare with solution below
% [MATERIAL_PROPS]   =   Add_ParameterizedDislocationCreep([], Phase, 'Dry Olivine - Ranalli 1995');   % to compare with solution below

% [MATERIAL_PROPS]     =   Add_DislocationCreep([], Phase, 'Wet Olivine - Ranalli 1995');   
% [MATERIAL_PROPS]     =   Add_DislocationCreep([], Phase,  'Maryland (strong) diabase - Mackwell et al.  1986');  
% [MATERIAL_PROPS]     =   Add_DislocationCreep([], Phase,  'Granite - Huismans et al (2001)');  
% [MATERIAL_PROPS]     =   Add_DislocationCreep([], Phase,  'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003');  
% [MATERIAL_PROPS]     =   Add_DislocationCreep([], Phase,  'Wet Olivine dislocation creep- Hirth and Kohlstedt 2003 (constant C_OH)');  

% [MATERIAL_PROPS]     =   Add_DislocationCreep([], Phase,  'Dry Olivine dislocation creep- Hirth and Kohlstedt 2003');  
% [MATERIAL_PROPS]     =   Add_DiffusionCreep([], Phase,  'Wet Olivine diffusion creep- Hirth and Kohlstedt 2003 constant C_OH');  
% [MATERIAL_PROPS]     =   Add_DiffusionCreep([], Phase,  'Dry Olivine diffusion creep- Hirth and Kohlstedt 2003');  
% [MATERIAL_PROPS]     =   Add_DiffusionCreep(MATERIAL_PROPS, Phase,  'Wet Olivine diffusion creep- Hirth and Kohlstedt 2003');  

% [MATERIAL_PROPS]     =   Add_PeierlsCreep(MATERIAL_PROPS, Phase,  'Goetze and Evans (1979)');  


% Type                                        =   'Constant';                     % type of viscosity law we can employ
% MATERIAL_PROPS(Phase).Viscosity.(Type).Mu 	=   1e20;                              % parameter


MATERIAL_PROPS(1).Viscosity.DislocationCreep.V  =   0;
MATERIAL_PROPS(1).Gravity.Value                 =   10;

% Add material properties that were not defined here, but which MILAMIN_VEP
% needs to know
MATERIAL_PROPS                                 =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required

%% Numerical parameters
NUMERICS                =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required


%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{3};
BC.Stokes.Left          =   Bound{3};
BC.Stokes.Right         =   Bound{3};
BC.Stokes.Top           =   Bound{4};
BC.Stokes.BG_strrate    =   e_bg;

% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   T_top_Celcius + 273;
BC.Energy.Value.Bottom=   T_bottom_Celcius+273;                    % T at bottom of mantle


%% Create initial particles distribution
n_markers               =   5e5;
[X, Z]                  =   meshgrid(   linspace(mesh_input.x_min,mesh_input.x_max,ceil(sqrt(n_markers))), ...
    linspace(mesh_input.z_min,mesh_input.z_max,ceil(sqrt(n_markers)))   );
PARTICLES.x             =   X(:)';
PARTICLES.z             =   Z(:)';
PARTICLES.phases        =   ones(size(PARTICLES.x),'uint32');
PARTICLES.HistVar.Txx   =   0*PARTICLES.z;                                  % temperature
PARTICLES.HistVar.Tyy   =   0*PARTICLES.z;                                % temperature
PARTICLES.HistVar.Txy   =   0*PARTICLES.z;                                % temperature

% Linear T profile
PARTICLES.HistVar.T     =   abs(Z(:)')*(T_bottom_Celcius-T_top_Celcius)/(mesh_input.z_max-mesh_input.z_min)+T_top_Celcius;                        % temperature
PARTICLES.HistVar.T     =   PARTICLES.HistVar.T +273;       % in K


%% Non-dimensionalize input parameters
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS, []);



%% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input);
dt                  =   1e-3;

%% Compute Stokes (& Energy solution if required)
[PARTICLES, MESH, INTP_PROPS, NUMERICS] 	=   StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt,NUMERICS, CHAR);
 


%% plot numerical solution
Z    =   MESH.NODES(2,:);
Z2d  =   Z(MESH.RegularGridNumber)*CHAR.Length/1e3;
Mu2d =   MESH.CompVar.Mu_Eff(MESH.RegularGridNumber)*CHAR.Viscosity;
T2d  =   MESH.TEMP(MESH.RegularGridNumber)*CHAR.Temperature-273;

Tau2d =   MESH.CompVar.T2nd(MESH.RegularGridNumber)*CHAR.Stress;

figure(2), clf
PlotMesh(MESH,log10(MESH.CompVar.E2nd)); colorbar, title('log10 strainrate')
axis equal


figure(1), clf
subplot(121)
plot(T2d(:,1), Z2d(:,1),'o')
ylabel('Depth [km]')
xlabel('log10(\eta)')
subplot(122)
plot(log10(Mu2d(:,1)/1), Z2d(:,1),'o')
ylabel('Depth [km]')
xlabel('log10(\eta)')







%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute solution of textbook of TARAS GERYA for a Dry Olivine rheology

% Flow law parameters - equ
AD      =   2.5e-17;    %1/Pa^n/s, 
n       =   3.5;
Ea      =   532000;     %J/mol 
R       =   8.3145;      %J/mol/K

% Define and display (in the Command Window) correction coefficient F2 
% for a strain rate based viscosity formulation
F2      =   1/2^((n-1)/n)/3^((n+1)/2/n)

%F2=F2*2

% Strain Rate
eps     =   e_bg;      %1/s


% Defining temperature profile
L       =   100e3;          % Length of the profile, m
ttop    =   400;            % T ot the top, oC
tbottom =   1200;           % T at the bottom, oC
ynum    =   101;            % Number of points in the profile
y       =   0:L/(ynum-1):L; % depth vector, m
T       =   ttop:(tbottom-ttop)/(ynum-1):tbottom; % Temperature vector, oC

% Compute viscosity profile 
eta     =   ones(ynum,1); % viscosity array
for i=1:1:ynum
    % Compute and check activation energy exponent
    eterm = Ea/n/R/(T(i)+273);
    if (eterm>100) 
        eterm=100; 
    end
    eterm = exp(eterm);
    
    % Compute viscosity
    eta(i)= F2/AD^(1/n)/eps^((n-1)/n)*eterm;
    
    
    tau_TARAS(i) = 2*eta(i)*eps;      % stress

end



% Defining new figure
figure(1)

% Plotting vertical  temperature profile
subplot(1,2,1);     % defining 1st plotting area in the 3x1 figure
hold on
plot(T,-y/1000,'k'); % making plot: 'k' = black color
box on;             % making box around the plot
title('Temperature profile');   % title of the plot
xlabel('T, ^oC'); % title of horizontal axis, ^o gives supercript o)
ylabel('Depth, km'); % title of vertical axis


% Plotting vertical log viscosity profile
subplot(1,2,2);     % defining 2nd plotting area in the 3x1 figure
hold on
plot(log10(eta/1),-y/1000,'k'); % making plot: 'k' = black color
box on;             % making box around the plot
title('log_{10} viscosity profile');   % title of the plot
xlabel('log_{10}(\eta), Pa s'); % title of horizontal axis, _{10} gives subcript 10, (\eta) gives Greek eta symbol
ylabel('Depth, km'); % title of vertical axis

legend('Numerics','Solution of Taras Gerya book for Dry Olivine','Location','SouthEast')


% Compare stresses


figure(2)
plot(log10(tau_TARAS/1e6),-y/1000,'k'); % making plot: 'k' = black color
hold on
plot(log10(Tau2d(:,1)/1e6), Z2d(:,1),'o')
ylabel('Depth [km]')
xlabel('log10(Stress [MPa])')




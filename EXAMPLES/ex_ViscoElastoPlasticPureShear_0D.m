% This shows how to run a simple 0D viscoelastioplastic compression test

% $Id$


% Add paths & subdirectories
AddDirectories;


clear

tic

%% Create mesh
opts.element_type.velocity   =   'Q1';
mesh_input.x_min    =   -250e3;
mesh_input.z_min    =   -250e3;
mesh_input.x_max    =   500e3;
mesh_input.z_max    =   0;
opts.nx             =   17;
opts.nz             =   17;

% % Uncomment this to do particle-based averaging:
% NUMERICS.Nonlinear.Method = 'ParticleBased';
% NUMERICS.InterpolationMethod.ParticlesToIntegrationPoints = 'Arithmetic';


%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values

% Properties
Phase                                       =   1;
MATERIAL_PROPS(Phase).Viscosity.Constant.Mu          =   1e22;                           % parameter
MATERIAL_PROPS(Phase).Elasticity.Constant.G          =   1e10;                           % Elastic shear module

MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.Cohesion       =   17e6;                       % Cohesion [Pa]
MATERIAL_PROPS(Phase).Plasticity.DruckerPrager.FrictionAngle  =   0;                          % Friction angle



MATERIAL_PROPS(1).Gravity.Value               =   0;
        
% Add material properties that were not defined here, but which MILAMIN_VEP
% needs to know
MATERIAL_PROPS                          =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required


%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{3};
BC.Stokes.Left          =   Bound{3};
BC.Stokes.Right         =   Bound{3};
BC.Stokes.Top           =   Bound{3};
BC.Stokes.BG_strrate    =   1e-15;

% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   500;
BC.Energy.Value.Bottom=   1300;                    % T at bottom of mantle

%% Numerical parameters
NUMERICS.Nonlinear.MinNumberIterations = 1;      
NUMERICS                =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required



%% Create Particles
n_markers               =   5e3;
[X, Z]                  =   meshgrid(   linspace(mesh_input.x_min,mesh_input.x_max,ceil(sqrt(n_markers))), ...
                                        linspace(mesh_input.z_min,mesh_input.z_max,ceil(sqrt(n_markers)))   );
PARTICLES.x             =   X(:)';
PARTICLES.z             =   Z(:)';
PARTICLES.phases        =   ones(size(PARTICLES.x),'uint32');
PARTICLES.HistVar.T     =   PARTICLES.z/(500e3);                        % temperature
PARTICLES.HistVar.Txx   =   PARTICLES.z*0 + 1e-6;                        % Txx
PARTICLES.HistVar.Tyy   =   PARTICLES.z*0 + 1e-6;                        % Txy
PARTICLES.HistVar.Txy   =   PARTICLES.z*0 + 1e-6;                        % Tyy


%% Non-dimensionalize input parameters
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,[]);



%% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input);

dt                  =   1e-4;

time               =    0;
for itime=1:50
    start_cpu = cputime;
    
    %% Compute Stokes (& Energy solution if required)
    [PARTICLES, MESH, INTP_PROPS, NUMERICS] 	=   StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt,NUMERICS, CHAR);
    

    %% Advect particles & MESH
    PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
    time            =   time+dt;
    MESH.NODES      =   MESH.NODES  + MESH.VEL*dt;
    
    
%     %% Compute new timestep 
%     dx              =   min(diff(unique(MESH.NODES(1,:))));
%     Vx              =   max(PARTICLES.Vx);
%     CFL             =   0.5;
%     dt              =   CFL*dx/Vx;
    
    Time_vec(itime) = time;
    Txx_vec(itime)  = mean(MESH.HistVar.Txx);
    Tyy_vec(itime)  = mean(MESH.HistVar.Tyy);
    Txy_vec(itime)  = mean(MESH.HistVar.Txy);
    
    
    %% Plot results
    if mod(itime,1)==0
       
        %figure(1), clf
        figure(1), hold on
        
        plot(Time_vec*CHAR.Time/CHAR.SecYear/1e6,Txx_vec*CHAR.Stress/1e6,'ro-');
        
        xlabel('Time [Myrs]')
        ylabel('Stress [MPa]')
        
        figure(2), clf
        PlotMesh(MESH);
        hold on
        plot(PARTICLES.x,PARTICLES.z,'r.')
        
        
        
    end
    
    end_cpu         = cputime;
    time_timestep    = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs'])
end



% 
% 
% % Scalability results
% 
% % Q1P0 elements
% Res_nx       	=   [33 65 129 257  513 769]
% DOF          	=    2*Res_nx.^2 + (Res_nx-1).^2;         % V + P dof
% 
% Time_timestep   =   [1 1.26 2.2 5.4 22 69.3] 




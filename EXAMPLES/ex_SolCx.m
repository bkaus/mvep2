% This shows how to run the SolCx benchmark introduced by May & Moresi in
% PEPI, 2008.

% Add paths & subdirectories
AddDirectories;

clear

tic

%% Create mesh
opts.element_type.velocity   = 'Q2' % 'T2' 'Q1'
mesh_input.x_min    = 0;
mesh_input.z_min    = 0;
mesh_input.x_max    = 1;
mesh_input.z_max    = 1;
opts.nx             = 33;      % in effect for quadrilateral element
opts.nz             = 33;
opts.max_tri_area   = .001; % in effect for triangular element


%% Create Particles
n_markers           =   1e6;
[X, Z]              =   meshgrid(linspace(0,1,ceil(sqrt(n_markers))));
PARTICLES.x         =   X(:)';
PARTICLES.z         =   Z(:)';
PARTICLES.phases    =   ones(size(PARTICLES.x),'uint32');
PARTICLES.HistVar   =   struct();


% set initial particles distribution
ind             =    find(PARTICLES.x>.5);
PARTICLES.phases(ind) = 2;


%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values

% Left phase
Phase                                           =   1;
Type                                            =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu       =   1;                      % parameter

Type                                            =   'SolCx_Benchmark';             % type of density law we can employ
MATERIAL_PROPS(Phase).Density.(Type)            =   struct();                      % parameter


% Right phase
Phase                                           =   2;
Type                                            =   'Constant';         	% type of viscosity law we can employ
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu       =   1e3;                    	% parameter

Type                                            =   'SolCx_Benchmark';         	% type of density law we employ
MATERIAL_PROPS(Phase).Density.(Type)            =   struct();                      % parameter


% Add material properties that were not defined here, but which MILAMIN_VEP
% needs to know
MATERIAL_PROPS                          =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required


%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{2};
BC.Stokes.Left          =   Bound{2};
BC.Stokes.Right         =   Bound{2};
BC.Stokes.Top           =   Bound{2};
BC.Stokes.BG_strrate    =   0;

% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{1};
BC.Energy.Bottom      =   BoundThermal{1};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};



%% numerical parameters
NUMERICS.Viscosity.UpperCutoff = 1e10;
NUMERICS.Viscosity.LowerCutoff = 1e-10;

NUMERICS            =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required


%% Non-dimensionalize input parameters
CHAR.Length         =   1;
CHAR.Viscosity      =   1;
CHAR.Time           =   1;
CHAR.Temperature    =  	1;
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS, CHAR);


%% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input);
% regular square mesh for plotting
h                   =   1/40;
[xq,yq]             =   meshgrid(mesh_input.x_min + h:h:mesh_input.x_max - h,mesh_input.z_min + h:h:mesh_input.z_max - h);
clearvars mesh_input opts

dt                  =   1;
time                =   0;
for itime=1:1 % single time step
    start_cpu = cputime;
    
    %% Compute Stokes (& Energy solution if required)
    [PARTICLES, MESH, INTP_PROPS, NUMERICS] 	 = StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    
    
    %% Advect particles [MESH is Eulerian in this simulation]
    PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
    time            =   time+dt;
    
    
    %% Compute new timestep
    dx              =   min(diff(unique(MESH.NODES(1,:))));
    Vx              =   max(MESH.VEL(1,:));
    CFL             =   0.5;
    dt              =   CFL*dx/Vx;
    
    % interpolate (scattered node-based) data to regular grid
    v1scatter       =   scatteredInterpolant(MESH.NODES',MESH.VEL(1,:)','linear');
    v1reg           =   v1scatter(xq,yq);
    v2scatter       =   scatteredInterpolant(MESH.NODES',MESH.VEL(2,:)','linear');
    v2reg           =   v2scatter(xq,yq);
    Pscatter        =   scatteredInterpolant(MESH.NODES(:,MESH.ELEMS(end,:))',MESH.PRESSURE(1,:)','linear');
    Preg            =   Pscatter(xq,yq);
    clearvars v1scatter v2scatter Pscatter
    
    %% Plot results
    figure(1), clf;
    hold on;
    quiver(xq,yq,v1reg,v2reg);
    axis equal tight;
    title('Velocity field');
    drawnow;
    
    figure(2), clf;
    pcolor(xq,yq,Preg);
    title('Pressure field'); colorbar;
    axis equal tight; shading interp;
    
    end_cpu         = cputime;
    time_timestep    = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time),' and has dt=',num2str(dt)])
end
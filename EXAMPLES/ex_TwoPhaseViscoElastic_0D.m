% This shows how to run a simple 0D viscoelastic compression test with the
% two-phase part of the code. For constant (low) porosity and material not
% weakened for this porosity the results should be roughly identical to the
% results from ex_ViscoElasticPureShear_0D.m

% $Id$


% Add paths & subdirectories
AddDirectories;

clear

tic

testShearPlasticity = true;

%% Create mesh
opts.element_type.velocity  = 'Q2';
opts.element_type.darcy     = 'Q1';
opts.element_type.pressure  = 'Q1';
mesh_input.x_min    =   -250e3;
mesh_input.z_min    =   -250e3;
mesh_input.x_max    =   500e3;
mesh_input.z_max    =   0;
opts.max_tri_area   =   1;
opts.nx             =   17;
opts.nz             =   17;



%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values

% Properties
Phase                                   =   1;
MATERIAL_PROPS(Phase).Viscosity.Constant.Mu      =   1e22;                          % parameter
MATERIAL_PROPS(Phase).Elasticity.Constant.G      =   1e10;                          % Elastic shear module
MATERIAL_PROPS(1).Gravity.Value               =   0;

if testShearPlasticity
    MATERIAL_PROPS(Phase).Plasticity.DruckerPragerGriffithMurrell.Cohesion       =   17e6;                       % Cohesion [Pa]
    MATERIAL_PROPS(Phase).Plasticity.DruckerPragerGriffithMurrell.FrictionAngle  =   0;                          % Friction angle
    MATERIAL_PROPS(Phase).Plasticity.DruckerPragerGriffithMurrell.sigma_T        =   1e100;                      % Tensile strength
end

MATERIAL_PROPS(1).TwoPhase.WeakenAboveCritical = false;
        
% Add material properties that were not defined here, but which MILAMIN_VEP
% needs to know
MATERIAL_PROPS                          =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required


%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{3};
BC.Stokes.Left          =   Bound{3};
BC.Stokes.Right         =   Bound{3};
BC.Stokes.Top           =   Bound{3};
BC.Stokes.BG_strrate    =   1e-15;

% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   500;
BC.Energy.Value.Bottom=   1300;                    % T at bottom of mantle

% Darcy (fluid Pressure)
BoundDarcy              = {'zero flux', 'Melt reservoir', 'dirichlet', 'periodic'};
BC.Darcy.Bottom         = BoundDarcy{1};
BC.Darcy.Left           = BoundDarcy{1};
BC.Darcy.Right          = BoundDarcy{1};
BC.Darcy.Top            = BoundDarcy{3};
BC.Darcy.Value.Top      = 0;

%% Numerical parameters
if testShearPlasticity
    NUMERICS.Nonlinear.MinNumberIterations  = 2;
    NUMERICS            =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required
else
    NUMERICS            =   SetDefaultNumericalParameters([]);
end


%% Create Particles
n_markers               =   5e3;
[X, Z]                  =   meshgrid(   linspace(mesh_input.x_min,mesh_input.x_max,ceil(sqrt(n_markers))), ...
                                        linspace(mesh_input.z_min,mesh_input.z_max,ceil(sqrt(n_markers)))   );
PARTICLES.x             =   X(:)';
PARTICLES.z             =   Z(:)';
PARTICLES.phases        =   ones(size(PARTICLES.x),'uint32');
PARTICLES.HistVar.T     =   PARTICLES.z/(500e3);                        % temperature
PARTICLES.HistVar.Txx   =   PARTICLES.z*0 + 1e-6;                        % Txx
PARTICLES.HistVar.Tyy   =   PARTICLES.z*0 + 1e-6;                        % Txy
PARTICLES.HistVar.Txy   =   PARTICLES.z*0 + 1e-6;                        % Tyy

PARTICLES.HistVar.PHI   =   1e-3*ones(size(PARTICLES.z));                        % MeltFraction
NUMERICS.TwoPhase       =   true;

%% Non-dimensionalize input parameters
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,[]);



%% Create mesh and prepare time-stepping
[MESH]      = CreateMesh(opts, mesh_input, []);

dt          = 2e-4;
no_steps    = 30;
time        = 0;

Time_vec    = zeros(1,no_steps);
Txx_vec     = zeros(1,no_steps);

for itime=1:no_steps
    start_cpu = cputime;
    
    %% Compute Stokes (& Energy solution if required)
    [PARTICLES, MESH, INTP_PROPS, NUMERICS] 	 = StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR );
    

    %% Advect particles & MESH
    PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
    time            =   time+dt;
    MESH.NODES      =   MESH.NODES  + MESH.VEL*dt;
    MESH.DARCY.NODES=   MESH.DARCY.NODES + MESH.VEL(:,MESH.DARCY.restr)*dt;
    
    % save time and stress for later plotting
    Time_vec(itime) = time;
    Txx_vec(itime)  = mean(MESH.HistVar.Txx);
    
    %% Plot results
    if mod(itime,30)==0
       
        figure(1), clf
        plot(Time_vec*CHAR.Time/CHAR.SecYear/1e6,Txx_vec*CHAR.Stress/1e6,'ro');
        hold on
        
        dt_anal   = Time_vec(end)*CHAR.Time/100;
        Time_anal = 0:dt_anal:100*dt_anal;
        t_m       = MATERIAL_PROPS(1).Viscosity.Constant.Mu/(MATERIAL_PROPS(1).Elasticity.Constant.G*CHAR.Stress);
        Txx_anal  = 2*MATERIAL_PROPS(1).Viscosity.Constant.Mu*(BC.Stokes.BG_strrate*1/CHAR.Time)*(1-exp(-Time_anal./t_m));
        
        if testShearPlasticity
            Txx_anal    = min(Txx_anal, MATERIAL_PROPS(1).Plasticity.DruckerPragerGriffithMurrell.Cohesion*CHAR.Stress);
        end
        
        plot(Time_anal/CHAR.SecYear/1e6,Txx_anal/1e6,'k-')
        
        xlabel('Time [Myrs]')
        ylabel('Stress [MPa]')
        
        legend('Numerics','Analytics','Location','southeast')
    end
    
    end_cpu         = cputime;
    time_timestep    = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs'])
end
function ex_ParticlesToIntegrationPoints()
% ex_ParticlesToIntegrationPoints gives an example of the usage of ParticlesToIntegrationPoints for
%   - triangular unstructured mesh (tri3 and tri7)
%   - recangular mesh
%
% It uses the routines
%
%   ComputeElementsAndLocalCoordinates_Particles
%   mtriangle
%   ParticlesToIntegrationPoints

% $Id$

% Add paths & subdirectories
AddDirectories;


%% Generate unstructured triangular mesh
% The example setup is a square domain with a finely-resolved circular
% inclusion in the center.

% Setup domain - square box
x_min        = 0;
x_max        = 1;
y_min        = 0;
y_max        = 1;
points   = [
    x_min y_min;
    x_max y_min;
    x_max y_max;
    x_min y_max]';
segments = [1 2; 2 3; 3 4; 4 1]';

% Add a circular inclusion in the center
no_pts_incl  = 150;
radius       = 0.1;
alpha        = 0.5;

theta        = linspace(0,2*pi,no_pts_incl);
theta(end)   = [];
xx           = cos(theta);
yy           = sin(theta);
center_x     = alpha*x_max+(1-alpha)*x_min;
center_y     = 0.5*(y_max+y_min);
INCLUSION    = [center_x + radius*xx; center_y + radius*yy];
no_pts       = size(INCLUSION,2);
pts_u        = 4 + no_pts;
pts_l        = 5;
INCLUSION_s  = [pts_l:pts_u;pts_l+1:pts_u+1];
INCLUSION_s(2,end) = pts_l;
points       = [points INCLUSION];
segments     = [segments INCLUSION_s];

% Set triangle input
tristr.points         = points;
tristr.segments       = uint32(segments);

NUMERICS              = SetDefaultNumericalParameters();

for loop=1:4
    tic
    % Set triangle options
    opts = [];
    if loop==1
        opts.element_type.velocity     = 'T1';
        nip                	=   3;
    elseif loop==2
        opts.element_type.velocity      = 'T2';
        nip                	=   12;
        
    elseif loop==3
        opts.element_type.velocity     = 'Q1';
        nip                	=   4;
    
    elseif loop==4
        opts.element_type.velocity     = 'Q2';
        nip                	=   9;
        
    end
    
    opts.gen_neighbors    = 1;
    opts.triangulate_poly = 1;
    opts.min_angle        = 30;
    opts.max_tri_area     = 0.01;
    
    
    %%
    % Generate and show the mesh:
    
    % Generate the mesh using triangle
    if loop==1 | loop==2
        [MESH]              =   CreateMesh(opts, tristr);
         ncorners           =   3;
   
    elseif loop==3 | loop==3
        mesh_input.x_min    =   0;
        mesh_input.z_min    =   0;
        mesh_input.x_max    =   1;
        mesh_input.z_max    =   1;
        opts.nx             =   5;
        opts.nz             =   11;
        
        [MESH]              =   CreateMesh(opts, mesh_input);
        ncorners            =   4;
        
    end
    
    % Show the mesh
    nel = length(MESH.ELEMS);
    
    
    %% Create Particles
    n_markers = 5e5;
    
    % grid of markers
    [X, Z]          =   meshgrid(linspace(0,1,ceil(sqrt(n_markers))));
    PARTICLES.x     =   X(:)';
    PARTICLES.z     =   Z(:)';
    PARTICLES.phases=   ones(size(PARTICLES.x),'uint32')
    
    
    %% Compute local elements of each of the particles
    [PARTICLES] 	=   ComputeElementsAndLocalCoordinates_Particles(MESH, PARTICLES, NUMERICS);
    
    
    
    %% Create some interesting phase relationship
    PARTICLES.phases(PARTICLES.x>0.7) = 2;
    PARTICLES.phases(PARTICLES.z>0.7) = 3;
    
    
    %% Define some history variables on the particles
    % we can add as many history variables as we want to the particles - they will automatically be
    % averaged to integration points (and have the same name)
    PARTICLES.HistVar.T         = PARTICLES.z*10+100;       % Temperature
    PARTICLES.HistVar.Strain    = PARTICLES.x.*2;           % Strain
    
    
    %% Define some computational variables (e.g., viscosity) 
    %  We need these for the computations as initial guess etc.
    PARTICLES.CompVar.Mu        = PARTICLES.z.^2*10+100;
    
    
    %% Compute phase propertions and history variables on integration points
    BC.Stokes.Left          =   'none';
    BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
    BC.Energy.Top           =   BoundThermal{1};
    BC.Energy.Bottom        =   BoundThermal{1};
    BC.Energy.Left          =   BoundThermal{1};
    BC.Energy.Right         =   BoundThermal{1};
    [INTP_PROPS, MESH]     =   ParticlesToIntegrationPoints(PARTICLES,MESH,BC,'distance',max(PARTICLES.phases),NUMERICS);

    tic
%     [INTP_PROPS, MESH]	=   ParticlesToIntegrationPoints(PARTICLES,MESH,'RegularMesh');
%    [INTP_PROPS, MESH]	=   ParticlesToIntegrationPoints(PARTICLES,MESH,'constant');
    
    toc
    
    
    
    % PLOTTING
    X   = reshape(MESH.NODES(1,MESH.ELEMS(1:ncorners,:)), ncorners, nel);
    Y   = reshape(MESH.NODES(2,MESH.ELEMS(1:ncorners,:)), ncorners, nel);
    C   = zeros(size(X));
    figure(loop); clf;
    subplot(221)
    h = patch(X, Y, C);
    axis square
    
    elem_number     =   1000;
    ind             =   find(PARTICLES.elem==elem_number);
    
    hold on
    plot(PARTICLES.x(ind),PARTICLES.z(ind),'b+')
    
    
    title(['mesh and particles in element ',num2str(elem_number),' with element-type ',opts.element_type.velocity ])
    
    
    
    subplot(222)
    X       =       reshape(MESH.NODES(1,MESH.ELEMS(1:ncorners,:)), ncorners, nel);
    Y       =       reshape(MESH.NODES(2,MESH.ELEMS(1:ncorners,:)), ncorners, nel);
    
    data    =       repmat(INTP_PROPS.TotNumPart, [1 ncorners])';
    h       =       patch(X, Y, data);
    axis square
    colorbar
    title('# of particles per element')
    
    
    
    if isfield(MESH,'HistVar')
        if isfield(MESH.HistVar,'T')
            subplot(223)
            X       =       reshape(MESH.NODES(1,MESH.ELEMS(1:ncorners,:)), ncorners, nel);
            Y       =       reshape(MESH.NODES(2,MESH.ELEMS(1:ncorners,:)), ncorners, nel);
            C       =       reshape(MESH.HistVar.T(1,MESH.ELEMS(1:ncorners,:)), ncorners, nel);
            h       =       patch(X, Y, C);
             
            axis square
            colorbar
            title('temperature at nodes')
        end
        
    end
    subplot(224)
    X       =       reshape(MESH.NODES(1,MESH.ELEMS(1:ncorners,:)), ncorners, nel);
    Y       =       reshape(MESH.NODES(2,MESH.ELEMS(1:ncorners,:)), ncorners, nel);
    
    data    =       repmat(INTP_PROPS.PHASE_PROP(:,2), [1 ncorners])';
    h       =       patch(X, Y, data);
             
    
    axis square
    colorbar
    title('phase proportions of phase 2 in every element')
    toc
    
end




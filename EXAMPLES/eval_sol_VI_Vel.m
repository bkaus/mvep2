%==============================================================================================================
%   Derived from dani's solution but also shows total stresses etc. 
%
% YL_P_MATRIX.M
%
% Pressure around circular inclusion
%
% 2002, Dani Schmid
%
% DISCLAIMER OF WARRANTY: 
% Since the Software is provided free of charge, the Software is provided on an AS IS basis,
% without warranty of any kind, including without limitation the warranties of merchantability,
% fitness for a particular purpose and non-infringement. The entire risk as to the quality and performance 
% of the Software is borne by you. Should the Software prove defective, 
% you assume the entire cost of any service and repair. 
%
% LIMITATION OF LIABILITY: 
% UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL THEORY, TORT, CONTRACT, OR OTHERWISE, 
% SHALL THE AUTHORS BE LIABLE TO YOU OR ANY OTHER PERSON FOR ANY INDIRECT, SPECIAL, INCIDENTAL, 
% OR CONSEQUENTIAL DAMAGES OF ANY CHARACTER INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF GOODWILL, 
% WORK STOPPAGE, COMPUTER FAILURE OR MALFUNCTION, OR ANY AND ALL OTHER COMMERCIAL DAMAGES OR LOSSES
%==============================================================================================================

function [Vx,Vz] = eval_sol_VI_Vel(X,Y,gr,er,mm,mc,rc)


if nargin == 0
    %FAR FIELD FLOW - VISCOSITIES - GEOMETRY
    gr	    = 0;        %
    er      = 1;
    mm      = 1;
    mc      = 1e6;
    rc	    = 0.1;
    
    [X,Y]       =   meshgrid(-1:.01:1);
end
%PRESSURE CALCULATION IN THE Z-PLANE

Z           =   X+1i*Y;
A           =   mm.*(mc-mm)./(mc+mm);

% Compute parameters for the matrix
P_mat               =  -2*real(-1./2.*1i.*mm.*gr+(gr.*1i+2.*er).*A.*rc.^2./Z.^2);
Vel_mat             =   1./2.*(-1./2.*1i.*mm.*gr.*Z-(gr.*1i+2.*er).*A.*rc.^2./Z-Z.*conj(-1./2.*1i.*mm.*gr+(gr.*1i+2.*er).*A.*rc.^2./Z.^2)-conj((gr.*1i-2.*er).*mm.*Z-(gr.*1i+2.*er).*A.*rc.^4./Z.^3))./mm;
Stress_mat          =   -2.*conj(Z).*(gr.*1i+2.*er).*A.*rc.^2./Z.^3+(gr.*1i-2.*er).*mm+3.*(gr.*1i+2.*er).*A.*rc.^4./Z.^4;


Sxz_mat                 =   imag(Stress_mat);
Szz_min_Sxx_over2_mat   =   real(Stress_mat);
Sxx_mat                 =   (Szz_min_Sxx_over2_mat+P_mat);
Szz_mat                 =   -2*P_mat-Sxx_mat;


Vx_mat      =   real(Vel_mat);
Vz_mat      =   imag(Vel_mat);


% Compute parameters for the clast
P_clast         =   zeros(size(Z)); 
Vel_clast       =   1./2.*(-1./2.*1i.*mc.*gr.*Z-Z.*conj(-1./2.*1i.*mc.*gr)-conj(2.*(gr.*1i-2.*er).*mc.*mm./(mc+mm).*Z))./mc;
Vx_clast        =   real(Vel_clast);
Vz_clast        =   imag(Vel_clast);
Stress_clast    =   2*(gr*1i-2.*er).*mc.*mm./(mc+mm)*ones(size(Z));

Sxz_clast               =   imag(Stress_clast);
Szz_min_Sxx_over2_clast =   real(Stress_clast);
Sxx_clast               =   (Szz_min_Sxx_over2_clast+P_clast);
Szz_clast               =   -2*P_clast-Sxx_clast;

%PRESSURE IS ONLY FOR THE OUTSIDE OF THE CLAST
P               =       zeros(size(Z));
Vx              =       zeros(size(Z));
Vz              =       zeros(size(Z));
Sxz             =       zeros(size(Z));
Sxx             =       zeros(size(Z));
Szz             =       zeros(size(Z));


P(abs(Z)>=rc)   =       P_mat(abs(Z)>=rc);
Vx(abs(Z)>=rc)  =       Vx_mat(abs(Z)>=rc);
Vz(abs(Z)>=rc)  =       Vz_mat(abs(Z)>=rc);
Sxz(abs(Z)>=rc) =       Sxz_mat(abs(Z)>=rc);
Sxx(abs(Z)>=rc) =       Sxx_mat(abs(Z)>=rc);
Szz(abs(Z)>=rc) =       Szz_mat(abs(Z)>=rc);


P(abs(Z)<  rc)  =       P_clast(abs(Z)< rc);
Vx(abs(Z)< rc)  =       Vx_clast(abs(Z)< rc);
Vz(abs(Z)< rc)  =       Vz_clast(abs(Z)< rc);
Sxz(abs(Z)< rc) =       Sxz_clast(abs(Z)< rc);
Szz(abs(Z)< rc) =       Szz_clast(abs(Z)< rc);
Sxx(abs(Z)< rc) =       Sxx_clast(abs(Z)< rc);


% derived parameters
Txx     =   Sxx+P;
Tzz     =   Szz+P;
Txz     =   Sxz;
Exx     =   Txx;    Exx(abs(Z)<  rc) = Exx(abs(Z)<  rc)/2/mc; Exx(abs(Z)>=rc) = Exx(abs(Z)>=rc)/2/mm;

sol.vx  = Vx;
sol.vz  = Vz;
sol.P   = P;

sol.Txx = Txx;
sol.Txz = Txz;
sol.Tzz = Tzz;


if nargin == 0
    %PLOTTING
    subplot(331)
    pcolor(X,Y,P)
    axis image;
    shading interp;
    colorbar('horiz');
    title('P');
    
    subplot(332)
    pcolor(X,Y,Sxx)
    axis image;
    shading interp;
    colorbar('horiz');
    title('\sigma_{xx}');
    
    subplot(333)
    pcolor(X,Y,Szz)
    axis image;
    shading interp;
    colorbar('horiz');
    title('\sigma_{zz}');
    
    subplot(334)
    pcolor(X,Y,Sxz)
    axis image;
    shading interp;
    colorbar('horiz');
    title('\sigma_{xz}');
    
    subplot(335)
    pcolor(X,Y,Vx)
    axis image;
    shading interp;
    colorbar('horiz');
    title('V_x');
    
    
    subplot(336)
    pcolor(X,Y,Vz)
    axis image;
    shading interp;
    colorbar('horiz');
    title('V_z');
    
    
    
    subplot(337)
    pcolor(X,Y,Txx)
    axis image;
    shading interp;
    colorbar('horiz');
    title('\tau_{xx}');
    
    
    subplot(338)
    pcolor(X,Y,Tzz)
    axis image;
    shading interp;
    colorbar('horiz');
    title('\tau_{zz}');
    
    
    subplot(339)
    pcolor(X,Y,Sxx-Szz )
    axis image;
    shading interp;
    colorbar('horiz');
    title('\sigma_{xx} - \sigma_{zz}');
    
end
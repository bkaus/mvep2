% This shows how to run a simple RT instability in ND units, using a fixed
% Eulerian mesh.
% The setup is that of the isoviscous van Keken [1997] benchmark &
% resulting geometries are quite similar
%
%

% $Id$

% Add paths & subdirectories
AddDirectories;


clear

tic

%% Create mesh
% opts.element_type   =   'quad4'
mesh_input.x_min    =   0;
mesh_input.z_min    =   0;
mesh_input.x_max    =   0.9142;
mesh_input.z_max    =   1;
opts.nx             =   65;
opts.nz             =   65;


%% Create Particles
n_markers           =   1e6;
[X, Z]              =   meshgrid(linspace(0,1,ceil(sqrt(n_markers))));
PARTICLES.x         =   X(:)';
PARTICLES.z         =   Z(:)';
PARTICLES.phases    =   ones(size(PARTICLES.x),'uint32');
PARTICLES.HistVar.T =   PARTICLES.z;                        % temperature


% set initial particles distribution
ind             =    find(PARTICLES.z<cos(PARTICLES.x/0.9142*pi)*0.02 + 0.2);
PARTICLES.phases(ind) = 2;


%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values

% Upper layer
Phase                                           =   1;
Type                                            =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu       =   1;                      % parameter


Type                                            =   'Constant';             % type of density law we can employ
MATERIAL_PROPS(Phase).Density.(Type).Rho        =   2;                      % parameter


% Lower layer
Phase                                           =   2;
Type                                            =   'Constant';         	% type of viscosity law we can employ
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu       =   1;                    	% parameter

Type                                            =   'Constant';         	% type of density law we employ
MATERIAL_PROPS(Phase).Density.(Type).Rho        =   1;                      % parameter


% Add material properties that were not defined here, but which MILAMIN_VEP
% needs to know
MATERIAL_PROPS                          =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required

MATERIAL_PROPS(1).Gravity.Value                 = 1;    % as we compute in ND units

%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{1};
BC.Stokes.Left          =   Bound{2};
BC.Stokes.Right         =   Bound{2};
BC.Stokes.Top           =   Bound{3};
BC.Stokes.BG_strrate    =   0;

% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   0;
BC.Energy.Value.Bottom=   1;                    % T at bottom of mantle




%% numerical parameters
NUMERICS.Viscosity.UpperCutoff = 1e10;
NUMERICS.Viscosity.LowerCutoff = 1e-10;

NUMERICS                =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required


%% Non-dimensionalize input parameters
CHAR.Length         =   1;
CHAR.Viscosity      =   1;
CHAR.Time           =   1;
CHAR.Temperature    =  	1;
 
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS, CHAR); 




%% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input);

dt                  =   1e-3;
dt=50;
time               =    0;
for itime=1:100
    start_cpu = cputime;
    
    %% Compute Stokes (& Energy solution if required)
    [PARTICLES, MESH, INTP_PROPS, NUMERICS] 	 = StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    

    %% Advect particles [MESH is Eulerian in this simulation]
    PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
    time            =   time+dt;
    
    
    
    %% Compute new timestep 
    dx              =   min(diff(unique(MESH.NODES(1,:))));
    Vx              =   max(MESH.VEL(1,:));
    CFL             =   0.5;
    dt              =   CFL*dx/Vx;
   % dt=50;
    
   % dt=1e100;
    
    %% Plot results
    if mod(itime,10)==0
       
        figure(1), clf
        ind = find(PARTICLES.phases==2);
        plot(PARTICLES.x(ind), PARTICLES.z(ind),'o')
        hold on
        
        quiver(MESH.NODES(1,:),MESH.NODES(2,:),MESH.VEL(1,:),MESH.VEL(2,:))
        axis equal, axis tight
        title(time)
        drawnow
        
        
        
        if isfield(MESH,'TEMP')
            figure(2), clf
            PlotMesh(MESH,MESH.TEMP), axis equal, axis tight, colorbar; shading flat
        end
        
        
    end
    
    end_cpu         = cputime;
    time_timestep    = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time),' and has dt=',num2str(dt)])
end



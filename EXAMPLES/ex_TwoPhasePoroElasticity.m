% This script runs a poro-elastic compression test with the two-phase part 
% of the code.

% $Id$

% Add paths & subdirectories
AddDirectories;
clear
tic

testTensilePlasticity   = true;

%% Create mesh
opts.element_type.velocity  = 'Q2';
opts.element_type.pressure  = 'Q1';
opts.element_type.darcy     = 'Q1';
mesh_input.x_min            = -1;
mesh_input.z_min            = mesh_input.x_min;
mesh_input.x_max            = -mesh_input.x_min;
mesh_input.z_max            = -mesh_input.z_min;
opts.nx                     = 5;
opts.nz                     = 5;

%% physical quantities, major setup parameters
Mu      = 1e21;
phi0    = 1e-2;
vdot    = 1e-15;
K0      = 1e10;     % 10 GPa
q       = -.5;      % powerlaw exponent for poro-elastic modulus (s.b.)
p       = -1;       % powerlaw exponent for compaction viscosity
r_zeta  =  1;

%% Set material properties for every phase
MATERIAL_PROPS(1).Gravity.Value                         = 0;
MATERIAL_PROPS(1).Viscosity.Constant.Mu                 = Mu;

% porosity-dependent poro-elastic modulus Kphi = K0 * phi^q
Type                                                    = 'Constant';
MATERIAL_PROPS(1).Elasticity.(Type).K0                  = K0;
MATERIAL_PROPS(1).Elasticity.(Type).q                   = q;

% compaction viscosity xi = (1-phi) * r * mu_eff * phi^p
Type                                                    = 'Constant';
MATERIAL_PROPS(1).Viscosity.(Type).alpha    = 0;
MATERIAL_PROPS(1).BulkViscosity.(Type).p_zeta   = p;
MATERIAL_PROPS(1).BulkViscosity.(Type).r_zeta   = r_zeta;

if testTensilePlasticity
    % plasticity due to tensile stress
    Type                                            = 'DruckerPragerGriffithMurrell';
    Cohesion                                        = 1e8;   % Cohesion (102 MPa)
    StrengthRed                                     = 2;        % Strength reduction parameter b/w 2 and 8
    % As 2nd invariant of stress is going to be 1e6, these values result in
    % a constant yield pressure P_y = T2nd - sigma_T = -5e7.
    MATERIAL_PROPS(1).Plasticity.(Type).Cohesion    = Cohesion;
    MATERIAL_PROPS(1).Plasticity.(Type).sigma_T     = Cohesion/StrengthRed;
    MATERIAL_PROPS(1).Plasticity.(Type).FrictionAngle   = 30;
end

MATERIAL_PROPS  = AppendMaterialProps(MATERIAL_PROPS,0);

%% Set boundary conditions
% Stokes
%                           1           2           3               
Bound                   = {'No Slip','Free Slip','Constant Strainrate', 'No Stress', 'Constant Bulk Strainrate'};
BC.Stokes.Bottom        = Bound{5};
BC.Stokes.Left          = Bound{5};
BC.Stokes.Right         = Bound{5};
BC.Stokes.Top           = Bound{5};
BC.Stokes.BG_strrate    = .5*vdot; % multiplied by 1/2 because it expands in both directions.

% Thermal boundary condition
%                           1               2           3
BoundThermal            = {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top           = BoundThermal{2};
BC.Energy.Bottom        = BoundThermal{2};
BC.Energy.Left          = BoundThermal{1};
BC.Energy.Right         = BoundThermal{1};
BC.Energy.Value.Top     = 500;
BC.Energy.Value.Bottom  = 1300;

% Darcy (fluid Pressure)
%                           1               2               3           4
BoundDarcy              = {'zero flux', 'Melt reservoir', 'dirichlet', 'periodic'};
BC.Darcy.Bottom         = BoundDarcy{1};
BC.Darcy.Left           = BoundDarcy{1};
BC.Darcy.Right          = BoundDarcy{1};
BC.Darcy.Top            = BoundDarcy{3};
BC.Darcy.Value.Top      = 0;

%% Numerical parameters
NUMERICS.TwoPhase                       = true;
% NUMERICS.LinearSolver.Method            = 'std';
NUMERICS.Nonlinear.MinNumberIterations  = 2;
NUMERICS                                = SetDefaultNumericalParameters(NUMERICS);

%% Create Particles
n_markers               =   5e3;
[X, Z]                  =   meshgrid(   linspace(mesh_input.x_min,mesh_input.x_max,ceil(sqrt(n_markers))), ...
                                        linspace(mesh_input.z_min,mesh_input.z_max,ceil(sqrt(n_markers)))   );
PARTICLES.x             =   X(:)';
PARTICLES.z             =   Z(:)';
PARTICLES.phases        =   ones(size(PARTICLES.x),'uint32');
PARTICLES.HistVar.T     =   PARTICLES.z/(500e3);            % temperature
PARTICLES.HistVar.Txx   =   PARTICLES.z*0 + 1e-6;           % Txx
PARTICLES.HistVar.Tyy   =   PARTICLES.z*0 + 1e-6;           % Txy
PARTICLES.HistVar.Txy   =   PARTICLES.z*0 + 1e-6;           % Tyy
PARTICLES.HistVar.PHI   =   phi0*ones(size(PARTICLES.z));	% MeltFraction
PARTICLES.HistVar.Pc    =   zeros(size(PARTICLES.z));       % Compaction Pr

%% Non-dimensionalize input parameters
CHAR.Length             = 1;
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS] = NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS, CHAR);

%% Create mesh and prepare time-stepping loop
[MESH]          = CreateMesh(opts, mesh_input, []);

start_time     	= 0;
time            = start_time;
dim_dt_yr       = 1e4; % dimensional time-step of 10 kyr
dt            	= dim_dt_yr*CHAR.SecYear/CHAR.Time;
time_steps   	= 20;

%% initialize variables to store results during time-stepping
Pc_num         	= zeros(1,time_steps+1);
PHI_num        	= zeros(1,time_steps+1);
PHI_num(1)      = phi0;
Zeta_num      	= zeros(1,time_steps+1);
Kphi_num       	= zeros(1,time_steps+1);
Kphi_num(1)     = (K0/CHAR.Stress)*phi0^q;
Zeta_num(1)     = (1-phi0) * r_zeta * (Mu/CHAR.Viscosity) * phi0^p;
Zeta_num(1)     = 1./(1./(Kphi_num(1).*dt) + 1./Zeta_num(1));
Mu_num          = zeros(1,time_steps+1);
Mu_num(1)       = (1-phi0) * Mu/CHAR.Viscosity;

%% Time-stepping loop
for itime=1:time_steps
    start_cpu = cputime;
    
    %% Compute Stokes (& Energy solution if required)
    [PARTICLES, MESH, INTP_PROPS, NUMERICS]   = StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR );
    
    %% Advect particles & MESH
    PARTICLES.x                     = PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z                     = PARTICLES.z + PARTICLES.Vz*dt;
    time                            = time + dt;
    MESH.NODES                      = MESH.NODES + MESH.VEL*dt;
    MESH.DARCY.NODES                = MESH.DARCY.NODES + MESH.VEL(:,MESH.RegularGridNumber(1:2:end,1:2:end))*dt;
    
    %% Save numerical results to plot with analytic solution
    if std(MESH.DARCY.Pc(1,:))/mean(MESH.DARCY.Pc(1,:)) < 1e-10
        Pc_num(itime+1)   = mean(MESH.DARCY.Pc(1,:));
        PHI_num(itime+1)  = mean(MESH.DARCY.PHI(:));
        Zeta_num(itime+1) = mean(INTP_PROPS.Zeta_Eff(:));
        Kphi_num(itime+1) = mean(INTP_PROPS.Kphi(:));
        Mu_num(itime+1)   = mean(INTP_PROPS.Mu_Eff(:));
    else
        error('Too large variations in compaction pressure field.');
    end
    
    end_cpu         = cputime;
    time_timestep   = end_cpu-start_cpu;
    
    if ~mod(itime,10)
        disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3,'%1.1f'),' kyrs.'])
    end
end

%% analytic solution
dt_yr_ana    	=   dim_dt_yr/10;
dt_sec        	=   dt_yr_ana*CHAR.SecYear;
tgrid_sec      	=   start_time:dt_sec:start_time+10*time_steps*dt_sec;

PHI_ana        	=   zeros(size(tgrid_sec));
PHI_ana(1)   	=   phi0;
for i=2:length(PHI_ana)
    PHI_ana(i)  = PHI_ana(i-1) + dt_sec * vdot * (1 - PHI_ana(i-1));
end
K_grid          = K0 .* PHI_ana .^ q;
zeta_grid       = (1-PHI_ana) .*  r_zeta .*  Mu .* PHI_ana .^ p;

Pc_ana          = - zeta_grid .* vdot .* (1 - exp(-K_grid.*tgrid_sec./zeta_grid));
zeta_grid       = 1./(1./(K_grid.*dt_sec.*10) + 1./zeta_grid); % apply elastic weakening when comparing to Zeta_eff computed in code

if testTensilePlasticity
    P_y         = (INTP_PROPS.Stress.T2nd(1) - MATERIAL_PROPS(1).Plasticity.DruckerPragerGriffithMurrell.sigma_T)*CHAR.Stress;
    Pc_ana      = max(Pc_ana, P_y);
end

figure(1); clf;
hold on;
plot(tgrid_sec*CHAR.SecYear/CHAR.Time/1e3,Pc_ana/1e6,'k-');
xlabel('Time [kyr]');
ylabel('Compaction Pressure [MPa]');

%% plot results
plot((start_time:dt:start_time+time_steps*dt)*CHAR.Time/CHAR.SecYear/1e3,Pc_num*CHAR.Stress/1e6,'bx');
% ylim([-120 0]);
hold off;

figure(2); clf;
hold on;
plot(tgrid_sec*CHAR.SecYear/CHAR.Time/1e3,PHI_ana,'k-');
plot((start_time:dt:start_time+time_steps*dt)*CHAR.Time/CHAR.SecYear/1e3,PHI_num,'bx');
xlabel('Time [kyr]');
ylabel('Porosity [1]');
title('Porosity');
hold off;

figure(3); clf;
hold on;
plot(tgrid_sec*CHAR.SecYear/CHAR.Time/1e3,log10(zeta_grid),'k-');
plot((start_time:dt:start_time+time_steps*dt)*CHAR.Time/CHAR.SecYear/1e3,log10(Zeta_num*CHAR.Viscosity),'bx');
xlabel('Time [kyr]');
ylabel('Zeta_Eff [Pa s]');
title('Compaction Viscosity');
hold off;

figure(4); clf;
hold on;
plot(tgrid_sec*CHAR.SecYear/CHAR.Time/1e3,log10(K_grid),'k-');
plot((start_time:dt:start_time+time_steps*dt)*CHAR.Time/CHAR.SecYear/1e3,log10(Kphi_num*CHAR.Stress),'bx');
xlabel('Time [kyr]');
ylabel('K_phi [Pa]');
title('Pore Modulus');
hold off;

figure(5); clf;
hold on;
plot(tgrid_sec*CHAR.SecYear/CHAR.Time/1e3,log10(Mu * (1-PHI_ana)),'k-');
plot((start_time:dt:start_time+time_steps*dt)*CHAR.Time/CHAR.SecYear/1e3,log10(Mu_num*CHAR.Viscosity),'bx');
xlabel('Time [kyr]');
ylabel('Mu_eff [Pa s]');
title('Shear Viscosity');
hold off;
% Example file to test two-phase solver.

% Add paths & subdirectories
AddDirectories; clearvars;

%% reference quantities
k_0         = 1e-7;     % [m^2] % reference permeability
n           = 3;        % [1]   % powerlaw exponent for permeability fluid fraction
m           = 2;        % [1]   % powerlaw exponent for permeability solid fraction
Mu0         = 1e23;     % Pa.s  % reference (shear) viscosity
alpha       = 27;       % [1]   % parameter of porosity-dependent viscosity eta = eta0*exp(-alpha*phi)
rho_s       = 3.0e3;    % kg/m^3% solid density
rho_f       = 2.5e3;    % kg/m^3% fluid density
g           = 9.81;     % m/s^2 % gravity
K0          = 5e8;      % [Pa]  % reference pore modulus 1e9 <= K0 <= 1e11
q           = -.5;      % [1]   % powerlaw exponent for pore modulus
cohesion    = 4e7;
friction    = 30;
shear_modulus=5e10;
strength_red= 4; % 2 <= R <= 8
r_zeta      = 1;
p_zeta      = -1;

% time stepping options
dt_yr       = 100;
dt_min      = 100;
dt_max      = 10000;
no_steps    = 20;

save_figs   = false;

%% Mesh parameters
opts.element_type.velocity   = 'T2';
switch opts.element_type.velocity
    case 'T2'
        opts.element_type.pressure   = 'T1';
        opts.element_type.darcy      = 'T1';
        opts.max_tri_area       = 6e-3; % non-dimensional size
        opts.min_angle          = 22;
        
        % define no. of refinement steps and reduction factor for area of refined triangles
        opts.AMR.NumberRefinementSteps  = 2;
        opts.refinement                 = 4;
    case 'Q2'
        opts.element_type.pressure   = 'Q1';
        opts.element_type.darcy      = 'Q1';
    otherwise
        error(['invalid velocity element input: ' opts.element_type.velocity]);
end

opts.nx                 = 241;
opts.nz                 = 161;
mesh_input.x_min        = -3e3;
mesh_input.x_max        =  3e3;
mesh_input.z_min        = -4e3;
mesh_input.z_max        = 0.e3;

%% Numerical parameters
NUMERICS.TwoPhase                       = true;
NUMERICS.ComputeLithostaticOverpressure = true;
NUMERICS.LinearSolver.DisplayInfo       = false;
NUMERICS.Nonlinear.MaxNumberIterations  = 10;
NUMERICS.Nonlinear.MinNumberIterations  = 2;
NUMERICS.Nonlinear.Tolerance            = 1e-4;
NUMERICS.Plasticity.MaximumYieldStress  = 1e10;
% NUMERICS.InterpolationMethod            = 'Constant';
% NUMERICS.LinearSolver.Method            = 'std';
NUMERICS.Viscosity.LowerCutoff          = 2e17; % in dimensional units [Pa.s]
NUMERICS.Viscosity.UpperCutoff          = 1e25;
NUMERICS                                = SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required

%% Set material properties
MATERIAL_PROPS(1).Gravity.Value                     = g;
MATERIAL_PROPS(1).Gravity.Angle                     = 90;

% Solid phase, constant density, fluid density
MATERIAL_PROPS(1).Density.Constant.Rho              = rho_s;
MATERIAL_PROPS(1).FluidDensity.Constant.Rho         = rho_f;

% Shear viscosity
MATERIAL_PROPS(1).Viscosity.Constant.Mu             = Mu0;

% Mekt weakening
MATERIAL_PROPS(1).MeltWeakening.Constant.alpha      = alpha;

% Bulk Viscosity
MATERIAL_PROPS(1).BulkViscosity.Constant.r_zeta     = r_zeta;
MATERIAL_PROPS(1).BulkViscosity.Constant.p_zeta     = p_zeta;

% Shear plasticity
Type                                                = 'DruckerPragerGriffithMurrell';
MATERIAL_PROPS(1).Plasticity.(Type).Cohesion        = cohesion; % 40 MPa
MATERIAL_PROPS(1).Plasticity.(Type).FrictionAngle   = friction; % 30 degrees
MATERIAL_PROPS(1).Plasticity.(Type).sigma_T         = cohesion/strength_red;

% (Shear) Elasticity
MATERIAL_PROPS(1).Elasticity.Constant.G             = shear_modulus;
MATERIAL_PROPS(1).Elasticity.Constant.K0            = K0;
MATERIAL_PROPS(1).Elasticity.Constant.q             = q;

MATERIAL_PROPS(1).FluidViscosity.Constant.Mu        = 1e2;

% Permeability
Type                                                = 'Keller2013';
MATERIAL_PROPS(1).Permeability.(Type).k_0           = k_0;
MATERIAL_PROPS(1).Permeability.(Type).n             = n;
MATERIAL_PROPS(1).Permeability.(Type).m             = m;


% Add required material properties that were not defined here
MATERIAL_PROPS                                      = AppendMaterialProps(MATERIAL_PROPS,0);

%% Set boundary conditions
% Stokes
Bound                   = {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        = Bound{3};
BC.Stokes.Left          = Bound{3};
BC.Stokes.Right         = Bound{3};
BC.Stokes.Top           = Bound{4};
BC.Stokes.BG_strrate    = 2e-15;

% Darcy (fluid Pressure)
PULSE_AT_BOTTOM     = 1;
PULSE_CENTERED      = 0;

BoundDarcy              = {'zero flux', 'Melt reservoir', 'Dirichlet'};
BC.Darcy.Left           = BoundDarcy{1};
BC.Darcy.Right          = BoundDarcy{1};
if PULSE_AT_BOTTOM
    BC.Darcy.Bottom     = BoundDarcy{1};
    BC.Darcy.Top        = BoundDarcy{1};
    BC.Darcy.Value.Top  = 0;
else
    BC.Darcy.Bottom     = BoundDarcy{1};
    BC.Darcy.Top        = BoundDarcy{1};
    BC.Darcy.Value.Top  = 0;
end

%% Create Particles
hx        =  1/(.5*(opts.nx-1)) / 5;
hy        =  1/(.5*(opts.nz-1)) / 5;
x         =  hx/2:hx:1;
y         =  hy/2:hy:1;

[Z,X]         =  meshgrid(y,x);

PARTICLES.x             = X(:)'.*(mesh_input.x_max-mesh_input.x_min)+mesh_input.x_min;
PARTICLES.z             = Z(:)'.*(mesh_input.z_max-mesh_input.z_min)+mesh_input.z_min;
PARTICLES.phases        = ones(size(PARTICLES.x),'uint32');

% particle class: {1: crystallized (solid), 2: silicate melt}
PARTICLES.class         = PARTICLES.phases;

% initialize history variables
PARTICLES.HistVar.PHI   = zeros(size(PARTICLES.x));            % melt fraction phi
PARTICLES.HistVar.vf_x  = zeros(1,length(PARTICLES.x));        % fluid velocity
PARTICLES.HistVar.vf_z  = zeros(1,length(PARTICLES.x));

%% Non-dimensionalize input parameters and create mesh
CHAR = struct();
CHAR.Porosity   = 1;
CHAR.Length     = 1e3;
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS] = NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS, CHAR);
[MESH]          = CreateMesh(opts, mesh_input);

if strcmp(MESH.element_type.velocity,'T2')
    aspect_ratio    = (mesh_input.x_max - mesh_input.x_min) / (mesh_input.z_max - mesh_input.z_min);
    domain_area     = (mesh_input.x_max - mesh_input.x_min) * (mesh_input.z_max - mesh_input.z_min);
    num_fine_el     = 8 * domain_area / mean(MESH.AREA(:));
    NUMERICS.InterpolationMethod.NxGrid = ceil(sqrt(num_fine_el*aspect_ratio)/4)*4;
    NUMERICS.InterpolationMethod.NzGrid = ceil(NUMERICS.InterpolationMethod.NxGrid / aspect_ratio / 4)*4;
end

%% Initialize porosity field
x_center            = .5*(min(MESH.NODES(1,:))+max(MESH.NODES(1,:)));
x_range             = max(MESH.NODES(1,:)) - min(MESH.NODES(1,:));
z_range             = max(MESH.NODES(2,:)) - min(MESH.NODES(2,:));
wv_length           = z_range/8; % z-direction
wv_width            = x_range/12;

% Gaussian pulse has maximum of 'phi_max'
phi_max             = .2;   % maximum porosity in pulse

if PULSE_AT_BOTTOM
    % Gaussian pulse at bottom center of domain
    z_bottom            = min(MESH.NODES(2,:));
    PARTICLES.HistVar.PHI   = phi_max * exp( -(PARTICLES.x - x_center).^2/wv_width^2  -(PARTICLES.z - z_bottom).^2/wv_length^2 );
else
    z_center        = .5*(min(MESH.NODES(2,:))+max(MESH.NODES(2,:)));
    z_range         = x_range;
    PARTICLES.HistVar.PHI   = phi_max * exp( -(PARTICLES.x - x_center).^2/wv_width^2  -(PARTICLES.z - z_center).^2/wv_length^2 );
end

BC.Darcy.phi_max    = phi_max;

%% Time-stepping loop
dt                  = dt_yr*CHAR.SecYear/CHAR.Time;
dt_max              = dt_max*CHAR.SecYear/CHAR.Time;
dt_min              = dt_min*CHAR.SecYear/CHAR.Time;

time                = 0;

if save_figs
    d = [opts.element_type.velocity opts.element_type.darcy opts.element_type.pressure '/'];
    mkdir(d);
end

for itime=1:no_steps % time-stepping
    start_cpu = cputime;
    
    %% create (refined) mesh
    if itime==1 || strcmp(opts.element_type.velocity,'Q2')
        MESH            = CreateMesh(opts, mesh_input, MESH);
    else
        refine              = zeros(size(PARTICLES.x));
        min_Pc              = min(PARTICLES.CompVar.Pc);
        max_Pc              = max(abs(PARTICLES.CompVar.Pc));
        if min_Pc < 0 % should always be true
            refine(PARTICLES.CompVar.Zeta_Eff < 5e1 * min(PARTICLES.CompVar.Zeta_Eff(:))) = 1;
        else
            error(['minimum compaction pressure >= 0 after time-step ' num2str(itime-1)]);
        end
        MESH            = CreateMesh_AMR(opts,mesh_input,MESH,PARTICLES,refine);
    end
    disp(['*** No. of elements: ' num2str(size(MESH.ELEMS,2))]);
    
    %% Compute Stokes-Darcy
    [PARTICLES, MESH, INTP_PROPS, NUMERICS] = StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    
    if mod(itime,1)==0
        %% Plot results
        x_min       = min(MESH.NODES(1,:));
        x_max       = max(MESH.NODES(1,:));
        z_min       = min(MESH.NODES(2,:));
        z_max       = max(MESH.NODES(2,:));
        
        h_x         = (x_max - x_min)/20; % coarser
        h_z         = (z_max - z_min)/20; % coarser
        [XregC,ZregC]=meshgrid(x_min + h_x:h_x:x_max - h_x,z_min + h_z:h_z:z_max - h_z);
        
        v1scatter   = scatteredInterpolant(MESH.NODES',MESH.VEL(1,:)');
        v2scatter   = scatteredInterpolant(MESH.NODES',MESH.VEL(2,:)');
        v1reg       = v1scatter(XregC,ZregC);
        v2reg       = v2scatter(XregC,ZregC);
        
        vf1scatter  = scatteredInterpolant(MESH.DARCY.NODES',MESH.DARCY.vf_x');
        vf2scatter  = scatteredInterpolant(MESH.DARCY.NODES',MESH.DARCY.vf_z');
        vf1reg      = vf1scatter(XregC,ZregC);
        vf2reg      = vf2scatter(XregC,ZregC);
        
        
        figure(1); clf; colormap(jet);
        subplot(211); title('phi'); % porosity
        PlotMesh(MESH.DARCY,MESH.DARCY.PHI,CHAR.Length/1e3);
        shading flat; axis equal tight; colorbar;
        
        subplot(212); title('div_vs'); % (solid) velocity divergence
        PlotMesh(MESH.DARCY,MESH.div_vs,CHAR.Length/1e3);
        shading flat; axis equal tight; colorbar;
        drawnow;
        if save_figs, saveas(gcf,[d 'fig1_' num2str(itime,'%02u')],'png'); end;
        
        
        figure(2); clf; colormap(jet);
        subplot(211); hold on; title('Pc, Vs');% compaction pressure, velocity field
        PlotMesh(MESH.DARCY,MESH.DARCY.Pc*CHAR.Stress,CHAR.Length/1e3);
        shading flat; axis equal tight; colorbar;
        quiver(XregC,ZregC,v1reg-BC.Stokes.BG_strrate*XregC,v2reg+BC.Stokes.BG_strrate*ZregC,'color','k');
        hold off;
        
        subplot(212); hold on; title('Pf, Vf'); % fluid pressure, fluid velocity field
        PlotMesh(MESH.DARCY,MESH.DARCY.DynamicPressure*CHAR.Stress,CHAR.Length/1e3);
        shading flat; axis equal tight; colorbar;
        quiver(XregC,ZregC,vf1reg-BC.Stokes.BG_strrate*XregC,vf2reg+BC.Stokes.BG_strrate*ZregC,'color','k');
        hold off;
        drawnow;
        if save_figs, saveas(gcf,[d 'fig2_' num2str(itime,'%02u')],'png'); end;
        
        
        figure(3); clf; colormap(jet); % velocity components
        subplot(221); title('Vs_x [cm/yr]');
        PlotMesh(MESH,MESH.VEL(1,:)*CHAR.Velocity*CHAR.cm_Year,CHAR.Length/1e3);
        shading flat; axis equal tight; colorbar;
        subplot(222); title('Vs_z [cm/yr]');
        PlotMesh(MESH,MESH.VEL(2,:)*CHAR.Velocity*CHAR.cm_Year,CHAR.Length/1e3);
        shading flat; axis equal tight; colorbar;
        
        subplot(223); title('Vf_x [cm/yr]');
        PlotMesh(MESH.DARCY,MESH.DARCY.vf_x*CHAR.Velocity*CHAR.cm_Year, CHAR.Length/1e3);
        shading flat; axis equal tight; colorbar;
        subplot(224); title('Vf_z [cm/yr]');
        PlotMesh(MESH.DARCY,MESH.DARCY.vf_z*CHAR.Velocity*CHAR.cm_Year,CHAR.Length/1e3);
        shading flat; axis equal tight; colorbar;
        drawnow;
        if save_figs, saveas(gcf,[d 'fig3_' num2str(itime,'%02u')],'png'); end;
        
        
        figure(4); clf; colormap(jet);
        subplot(221); title('log10(E2nd)');
        PlotMesh(MESH,log10(MESH.CompVar.E2nd/CHAR.Time),CHAR.Length/1e3);
        shading flat; axis equal tight; colorbar;
        subplot(222); title('T2nd');
        PlotMesh(MESH,(MESH.CompVar.T2nd*CHAR.Stress),CHAR.Length/1e3);
        shading flat; axis equal tight; colorbar;
        
        subplot(223); title('log10(Mu_Eff)');
        PlotMesh(MESH,log10(MESH.CompVar.Mu_Eff*CHAR.Viscosity),CHAR.Length/1e3);
        shading flat; axis equal tight; colorbar;
        subplot(224); title('log10(Zeta_Eff)');
        PlotMesh(MESH,log10(MESH.CompVar.Zeta_Eff*CHAR.Viscosity),CHAR.Length/1e3);
        shading flat; axis equal tight; colorbar;
        drawnow;
        if save_figs, saveas(gcf,[d 'fig4_' num2str(itime,'%02u')],'png'); end;
        
        
        figure(5); clf; colormap(jet);
        subplot(211); title('div_vs'); % (solid) velocity divergence
        PlotMesh(MESH.DARCY,MESH.div_vs,CHAR.Length/1e3);
        shading faceted; axis equal tight; colorbar;
        subplot(212); title('Strain');
        PlotMesh(MESH,MESH.HistVar.Strain,CHAR.Length/1e3);
        shading flat; axis equal tight; colorbar;
        drawnow;
        if save_figs, saveas(gcf,[d 'fig5_' num2str(itime,'%02u')],'png'); end;
    end
    
    %% Advect particles and MESH
    solid               = find(PARTICLES.class==1);
    fluid               = find(PARTICLES.class==2);
    PARTICLES.x(solid)  = PARTICLES.x(solid) + PARTICLES.Vx(solid)*dt;
    PARTICLES.z(solid)  = PARTICLES.z(solid) + PARTICLES.Vz(solid)*dt;
    
    MESH.NODES          = MESH.NODES          + MESH.VEL*dt;
    MESH.DARCY.NODES    = MESH.DARCY.NODES    + MESH.VEL(:,MESH.DARCY.restr)*dt;
    
    %% Compute new timestep
    time                = time+dt;
    CFL                 = 0.5;
    dt_courant          = CourantTimestep(MESH,CFL);
    
    %% Finalize timestep
    end_cpu             = cputime;
    time_timestep       = end_cpu-start_cpu;
    
    disp(' ');
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s, ' ...
        'reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e3),'kyrs ' ...
        'and has dt=',num2str(dt*CHAR.Time/CHAR.SecYear) 'yrs.'])
    
    dt                  = min([2*dt, dt_courant, dt_max]);
    dt                  = max(dt_min, dt);
    
    disp(['  Minimum compaction pressure = ' num2str(min(MESH.DARCY.Pc(:))*CHAR.Stress/1e6) ' MPa.']);
    disp(' ');
end
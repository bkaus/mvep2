% script to run SolCx for different resolutions and pressure shape
% functions
% also create the necessary plots

clearvars
close all

% EVEN MESH
%======================
nnodes_vec = round(10.^[1:0.25:3] + 1);

ErrorP_L1 = zeros(size(nnodes_vec));
ErrorP_L2 = zeros(size(nnodes_vec));
ErrorV_L1 = zeros(size(nnodes_vec));
ErrorV_L2 = zeros(size(nnodes_vec));

PressureShapeFunction = 'Local';

for inodes = 1:length(nnodes_vec)
    [Error] = SolCxBenchmark(nnodes_vec(inodes),PressureShapeFunction);
    ErrorP_L1(inodes) = Error.P_L1;
    ErrorP_L2(inodes) = Error.P_L2;
    ErrorV_L1(inodes) = Error.V_L1;
    ErrorV_L2(inodes) = Error.V_L2;
    disp([num2str(inodes),' of ',num2str(length(nnodes_vec))])
end


%% PLOT
figure(2),clf
subplot(121)
hold on
plot(1./nnodes_vec,ErrorP_L1,'-o')
plot(1./nnodes_vec,ErrorP_L2,'-o')
legend({'e_{p,1}';'e_{p,2}'})
% plot lines with given slopes
c1 = ErrorP_L1(end)/(1./nnodes_vec(end))^1 * 0.5;
c05 = ErrorP_L1(end)/(1./nnodes_vec(end))^0.5 * 0.5;
plot(1./nnodes_vec,(1./nnodes_vec).^1 * c1,'k--')
plot(1./nnodes_vec,(1./nnodes_vec).^0.5 * c05,'k-')
legend({'$e_{p,1}$';'$e_{p,2}$';'$\mathcal{O}(h^1)$';'$\mathcal{O}(h^{0.5})$'},'interpreter','latex','location','NorthWest')
xlabel('$h$','interpreter','latex')
ylabel('$e$','interpreter','latex')
set(gca,'xscale','log','yscale','log','box','on','LineWidth',2,'FontSize',16)
axis square, axis tight
xlim([1e-3 1e-1])
subplot(122)
hold on
plot(1./nnodes_vec,ErrorV_L1,'-o')
plot(1./nnodes_vec,ErrorV_L2,'-o')
c2 = ErrorV_L1(end)/(1./nnodes_vec(end))^2 * 0.5;
plot(1./nnodes_vec,(1./nnodes_vec).^2 * c2,'k-')
xlabel('$h$','interpreter','latex')
ylabel('$e$','interpreter','latex')
legend({'$e_{v,1}$';'$e_{v,2}$';'$\mathcal{O}(h^2)$'},'interpreter','latex','location','NorthWest')
set(gca,'xscale','log','yscale','log','box','on','LineWidth',2,'FontSize',16)
axis square, axis tight
xlim([1e-3 1e-1])
% This adds the directories, required to run MILAMIN_VEP2
%
% This will have to modiffy this, depending on the system you use.

if exist('maxNumCompThreads')==2
    if ismac
        % Set number of computational threads always to 1 with MILAMIN_VEP!
        %maxNumCompThreads(1);
    end
end


cd 
addpath(genpath('../../../MVEP2/mutils-0.4'));
%addpath(genpath('../../MVEP2/Mesh2d_v24')); % only use when using an
%alternative mesh generator
addpath(genpath('../../../MVEP2/SOURCE_CODE/'))
addpath(genpath('../../../MVEP2/PHASE_TRANSITIONS/'))
addpath(genpath('../../../MVEP2/SOURCE_CODE/MantleMeltFraction_Katz2003/'))
if isunix & ~ismac
    addpath(genpath('../../../MVEP2/SOURCE_CODE/kdtree_alg/'))                  % still necessary - we need to get rid of this in the future
elseif ismac
    addpath(genpath('../../../MVEP2/SOURCE_CODE/KDTREE/kdtree_alg_OSX/'))       % still necessary - we need to get rid of this in the future
elseif ispc
    addpath(genpath('../../../MVEP2/SOURCE_CODE/KDTREE/'))                      
else
    error('You will likely have to compile the KDTREE routine by hand on this machine, and link it correctly')
end    



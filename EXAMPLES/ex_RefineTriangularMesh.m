

% ex_RefineTriangularMesh
% Example file to create and plot a couple of iteratively refined meshes.
AddDirectories; clearvars;

%% Define factor by which element area should be refined per iteration step
refinement                      = 3;

%% Define preliminary mesh input parameters
mesh_input.x_min                = 2;
mesh_input.x_max                = 5;
mesh_input.z_min                = 4;
mesh_input.z_max                = 5;

opts.element_type.velocity      = 'T1';
opts.element_type.pressure      = 'P0'; % just to avoid triggering warning
opts.max_tri_area               = 0.25;

%% Create preliminary mesh
MESH                            = CreateMesh(opts, mesh_input);

%% Prepare refined mesh, 1st iteration
[~,pick_elem1]                  =   min(MESH.AREA);
MESH_1                          =   RefineMesh(MESH, opts, pick_elem1);


%% Prepare refined mesh, 2nd iteration
[~,pick_elem2]                  =   min(MESH_1.AREA);
MESH_2                          =   RefineMesh(MESH_1, opts, pick_elem2);

%% Prepare refined mesh, 3rd iteration
[~,pick_elem3]                  =   min(MESH_2.AREA);
MESH_3                          =   RefineMesh(MESH_2, opts, pick_elem3);

% % duplicate mesh_input and opts structs
% mesh_input1                     = mesh_input;
% opts1                           = opts;
% 
% % Find number of mesh elements
% no_elems1                       = size(MESH.ELEMS,2);
% 
% % Pick element(s) to be refined, e.g., the one with smallest area
% [~,pick_elem1]                  = min(MESH.AREA);
% 
% % Set flag 'r' (read/refine prior mesh) to be passed to triangle routine
% % Note: The number or order of elements, nodes or edges is (in general) not
% % going to coincide with the one from the previous mesh.
% opts1.other_options             = 'r';
% 
% % (Optionally) define minimum angle (defaults to 15 degrees)
% opts1.min_angle                 = 20;
% 
% % Zeros or negative values will switch off triangle area constraints,
% % refine such that refined triangle has only quarter of previous area.
% % Note: opts.max_tri_area decides about flag that is used when calling
% % triangle routine. You should distinguish the following two cases:
% %   (1) max_tri_area scalar: zero or negative value implies globally no 
% %           triangle area constraint, positive value is enforced globally,
% %           i.e., for each triangle.
% %   (2) max_tri_area vector: sets flag -a for triangle routine such that
% %           the area constraints are going to be read from the field
% %           mesh_input.trianglearea
% %contains one value per triangle of previous
% %           mesh, zeros or negative values imply no constraints, positive
% %           value(s) are enforced for the respective triangles.
% opts1.max_tri_area              = zeros(1,no_elems1);
% 
% % Element to be refined should have 1/(refinement) of previous area.
% % Note: Adjacent elements might also be refined.
% opts1.max_tri_area(pick_elem1)  = MESH.AREA(pick_elem1)/refinement;
% 
% % mesh_input.trianglearea contains one value per triangle of previous mesh,
% % zeros or negative values imply no constraints, positive value(s) are 
% % enforced for the respective triangles.
% mesh_input1.trianglearea        = opts1.max_tri_area;


%% Create refined mesh, 1st iteration
% MESH_1                          = CreateMesh(opts1, mesh_input1, MESH);

% %% Prepare and create refined mesh, 2nd iteration
% mesh_input2                     = mesh_input;
% opts2                           = opts;
% 
% no_elems2                       = size(MESH_1.ELEMS,2);
% [~,pick_elem2]                  = min(MESH_1.AREA);
% opts2.other_options             = 'r';
% opts2.min_angle                 = 20;
% opts2.max_tri_area              = zeros(1,no_elems2);
% opts2.max_tri_area(pick_elem2)  = MESH_1.AREA(pick_elem2)/refinement;
% mesh_input2.trianglearea        = opts2.max_tri_area;
% 
% MESH_2                          = CreateMesh(opts2, mesh_input2, MESH_1);

% %% Prepare and create refined mesh, 3rd iteration
% mesh_input3                     = mesh_input2;
% opts3                           = opts2;
% 
% no_elems3                       = size(MESH_2.ELEMS,2);
% [~,pick_elem3]                  = min(MESH_2.AREA);
% opts3.other_options             = 'r';
% opts3.min_angle                 = 20;
% opts3.max_tri_area              = zeros(1,no_elems3);
% opts3.max_tri_area(pick_elem3)  = MESH_2.AREA(pick_elem3)/refinement;
% mesh_input3.trianglearea        = opts3.max_tri_area;
% 
% MESH_3                          = CreateMesh(opts3, mesh_input3, MESH_2);

%% Plot meshes
% elements to be refined in next mesh are indicated by light red color
figure(1); subplot(411);
PlotMesh(MESH); 
axis equal tight;
hold on;
patch(MESH.NODES(1,MESH.ELEMS(1:3,pick_elem1)),MESH.NODES(2,MESH.ELEMS(1:3,pick_elem1)),[1 .5 .5]);
hold off;

subplot(412);
PlotMesh(MESH_1);
axis equal tight;
hold on;
patch(MESH_1.NODES(1,MESH_1.ELEMS(1:3,pick_elem2)),MESH_1.NODES(2,MESH_1.ELEMS(1:3,pick_elem2)),[1 .5 .5]);
hold off;

subplot(413);
PlotMesh(MESH_2);
axis equal tight;
hold on;
patch(MESH_2.NODES(1,MESH_2.ELEMS(1:3,pick_elem3)),MESH_2.NODES(2,MESH_2.ELEMS(1:3,pick_elem3)),[1 .5 .5]);
hold off;

subplot(414);
PlotMesh(MESH_3);
axis equal tight;

clear ans;
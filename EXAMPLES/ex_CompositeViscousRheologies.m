% This tests the composite rheology implemention.
%

% $Id$


% Add paths & subdirectories
AddDirectories;

clear

tic

%% Create mesh
mesh_input.x_min    =   -250e3;
mesh_input.z_min    =   -250e3;
mesh_input.x_max    =   500e3;
mesh_input.z_max    =   0;
opts.nx             =   11;
opts.nz             =   11;


% 
% % Uncomment this to do particle-based averaging:
% NUMERICS.Nonlinear.Method = 'ParticleBased';
% NUMERICS.InterpolationMethod.ParticlesToIntegrationPoints = 'Arithmetic';

%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values

% Properties
Phase                                          =   1;

% % Powerlaw creep
% Type                                           =   'Powerlaw';
% MATERIAL_PROPS(Phase).Viscosity.(Type).Mu0     =   1e22;                    % prefactor
% MATERIAL_PROPS(Phase).Viscosity.(Type).n       =   4;                       % powerlaw exponent
% MATERIAL_PROPS(Phase).Viscosity.(Type).e0      =   1e-15;                   % reference strainrate

% % % Add constant viscosity
% Type                                           =   'Constant';
% MATERIAL_PROPS(Phase).Viscosity.(Type).Mu      =   1e22;                    % viscosity


[MATERIAL_PROPS]                                =   Add_DislocationCreep([], Phase, 'Wet Olivine - Ranalli 1995');



MATERIAL_PROPS(1).Gravity.Value                 =   0;

% Add material properties that were not defined here, but which MILAMIN_VEP
% needs to know
MATERIAL_PROPS                                 =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required

%% Numerical parameters
NUMERICS.Nonlinear.MaxNumberIterations          =   10;
%NUMERICS.MinNumberNonlinearIterations          =   3;
NUMERICS                                    =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required



%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{3};
BC.Stokes.Left          =   Bound{3};
BC.Stokes.Right         =   Bound{3};
BC.Stokes.Top           =   Bound{3};
BC.Stokes.BG_strrate    =   1e-15;

% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   500;
BC.Energy.Value.Bottom=   1300;                    % T at bottom of mantle


%% Create Particles
n_markers               =   5e3;
[X, Z]                  =   meshgrid(   linspace(mesh_input.x_min,mesh_input.x_max,ceil(sqrt(n_markers))), ...
    linspace(mesh_input.z_min,mesh_input.z_max,ceil(sqrt(n_markers)))   );
PARTICLES.x             =   X(:)';
PARTICLES.z             =   Z(:)';
PARTICLES.phases        =   ones(size(PARTICLES.x),'uint32');
PARTICLES.HistVar.T     =   ones(size(PARTICLES.x));                        % temperature
PARTICLES.HistVar.Txx   =   0*PARTICLES.z;                                  % temperature
PARTICLES.HistVar.Tyy     =   0*PARTICLES.z;                                % temperature
PARTICLES.HistVar.Txy     =   0*PARTICLES.z;                                % temperature


%% Non-dimensionalize input parameters
CHAR.Length         =   100e3;
CHAR.Viscosity      =   1e19;
CHAR.Time           =   1e15;
CHAR.Temperature    =  	873;

[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS, CHAR);



%%


%% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input);
dt                  =   1e-3;

str_rate_vec        =   10.^[-18:-12];      % strainrate
T_Celcius_Vec       =   [200:100:1500];     % temperature


time               =    0;


for jStep=1:length(T_Celcius_Vec)
    for iStep=1:length(str_rate_vec)
        start_cpu = cputime;
        
        
        T                       =      (T_Celcius_Vec(jStep)+273)/CHAR.Temperature;     % in ND units
        PARTICLES.HistVar.T 	=       ones(size(PARTICLES.x))*T;
        
        BC.Stokes.BG_strrate  	=       str_rate_vec(iStep)/(1/CHAR.Time);                % in ND units
        BC.Energy.Value.Top     =       T;
        BC.Energy.Value.Bottom  =       T;
        
        %% Compute Stokes (& Energy solution if required)
        [PARTICLES, MESH, INTP_PROPS, NUMERICS] 	=   StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt,NUMERICS, CHAR);
        
        
        Stress_2D(jStep, iStep)         =   mean(INTP_PROPS.Stress.T2nd(:)*CHAR.Stress);
        Mu_2D(jStep, iStep)             =   mean(INTP_PROPS.Mu_Eff(:)*CHAR.Viscosity);
        
        
        end_cpu         = cputime;
        time_timestep    = end_cpu-start_cpu;
        
        disp(['Step ',num2str(iStep), ' took ',num2str(time_timestep),'s '])
    end
end



figure(1), clf
plot(log10(str_rate_vec), log10(Mu_2D),'o-')
xlabel('log10(eII)')
ylabel('log10(\eta)')







% Example file to test StokesDarcy_2D.m

% $Id$

% Add paths & subdirectories
AddDirectories;

clear;

% Depth D = sqrt(K_D * (1-phi)*zeta), KD = kappa/eta_f (permeability over
% fluid viscosity), default kappa=5e-18, eta_f=1e2, phi=1e-3, zeta=1e20.

%% reference quantities
% Non-dimensional setup
phi_e                   =   0;                % regularization parameter for power-laws
n                       =   3;                % powerlaw exponent for permeability fluid fraction
m_zeta                  =   1;                % powerlaw exponent for compaction viscosity
eta0                    =   1e20;             % viscosity
zeta_0                  =   1e20;             % compaction viscosity
eta_f                   =   100;              % fluid viscosity
rho_s                   =   3.0e3;            % kg/m^3    % solid density
rho_f                   =   2.5e3;            % kg/m^3    % fluid density
g                       =   10;             % m/s^2     % gravity
pmb_cutoff              =   1e-11;            % lower permeability cut-off
kappa_0                 =   5e-9;             % reference permeability

% kappa = kappa_0 * phi.^n  yields kappa = 5e-18 for phi=1e-3, n=3
phi_0                   =   1e-3;

%% Mesh parameters
opts.element_type       =   'quad9';
opts.element_type_darcy =   'quad4';
mesh_input.x_min        =   0;
mesh_input.x_max        =   100;
K_D                     =   kappa_0 * phi_0 ^ n / eta_f;
delta_0                 =   sqrt( (zeta_0 + 4*eta0/3) * K_D);
mesh_input.z_min        =   0;
mesh_input.z_max        =   400*delta_0;
opts.nx                 =   9;
opts.nz                 =   4801;


%% Set material properties for every phase
MATERIAL_PROPS(1).Gravity.Value  = g;

% Solid phase
Phase                                                           =   1;
MATERIAL_PROPS(Phase).Viscosity.Constant.Mu                     =   eta0;
MATERIAL_PROPS(Phase).Density.Constant.Rho                      =   rho_s;
MATERIAL_PROPS(Phase).TwoPhase.CompactionViscosity.Constant.zeta_0  =   1e20;

Type                                                            =   'Powerlaw';       % permeability
MATERIAL_PROPS(Phase).TwoPhase.Permeability.(Type).kappa_0      =   kappa_0;
MATERIAL_PROPS(Phase).TwoPhase.Permeability.(Type).n            =   n;
MATERIAL_PROPS(Phase).TwoPhase.Permeability.(Type).phi_e        =   phi_e;

MATERIAL_PROPS(Phase).TwoPhase.FluidDensity.Constant.rho_0      =   rho_f;
MATERIAL_PROPS(Phase).TwoPhase.FluidViscosity.Constant.eta_f    =   eta_f;

% Flag whether effective viscosity should be lowered to fluid viscosity
% when melt fraction is above some critical value, i.e. 5%
MATERIAL_PROPS(Phase).TwoPhase.WeakenAboveCritical              =   0; % should be 1 by default in future (or completely removed)

% Fluid phase
Phase                                                           =   2;
MATERIAL_PROPS(Phase).Viscosity.Constant.Mu                     =   eta0;
MATERIAL_PROPS(Phase).Density.Constant.Rho                      =   rho_f;
MATERIAL_PROPS(Phase).TwoPhase.CompactionViscosity.Constant.zeta_0  =   zeta_0;

Type                                                            =   'Powerlaw';       % permeability
MATERIAL_PROPS(Phase).TwoPhase.Permeability.(Type).kappa_0      =   kappa_0;
MATERIAL_PROPS(Phase).TwoPhase.Permeability.(Type).n            =   n;
MATERIAL_PROPS(Phase).TwoPhase.Permeability.(Type).phi_e        =   phi_e;

MATERIAL_PROPS(Phase).TwoPhase.FluidDensity.Constant.rho_0      =   rho_f;
MATERIAL_PROPS(Phase).TwoPhase.FluidViscosity.Constant.eta_f    =   eta_f;


% Add material properties that were not defined yet, but we need to know
% (employ standard values in that case)
MATERIAL_PROPS                                                  =   AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required


%% Numerical parameters
NUMERICS.TwoPhase                   = true;
NUMERICS.Viscosity.LowerCutoff      = 1e17;
NUMERICS.Viscosity.UpperCutoff      = 1e23;
NUMERICS.InterpolationMethod.ParticlesToIntegrationPoints='RegularMesh';
NUMERICS.Nonlinear.Tolerance = 1e-6;

% NUMERICS.LinearSolver.StaticPresCondensation     = false;
NUMERICS                            = SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required

%% Set boundary conditions

% Stokes
Bound                   = {'No Slip','Free Slip','Constant Strainrate', 'No Stress', 'periodic'};
BC.Stokes.Bottom        = Bound{1};
BC.Stokes.Left          = Bound{2};
BC.Stokes.Right         = Bound{2};
BC.Stokes.Top           = Bound{1};
% BC.Stokes.BG_strrate    = 1e-15;

% Darcy (fluid Pressure)
BoundDarcy              = {'zero flux', 'Melt reservoir', 'dirichlet', 'periodic'};
BC.Darcy.Bottom         = BoundDarcy{1};
BC.Darcy.Left           = BoundDarcy{1};
BC.Darcy.Right          = BoundDarcy{1};
BC.Darcy.Top            = BoundDarcy{3};
BC.Darcy.Value.Bottom   = 0;
BC.Darcy.Value.Top      = 0;

% BC.Energy               = struct(); % necessary for NonDimensionalizeParameters not to crash

%% Create Particles
numz                        =   opts.nz*3;
numx                        =   opts.nx*4;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (mesh_input.z_max-mesh_input.z_min)/numz;
[PARTICLES.x,PARTICLES.z]   =   meshgrid(mesh_input.x_min+1e-3:dx:mesh_input.x_max-1e-3,mesh_input.z_min+1e-3:dz:mesh_input.z_max-1e-3);
PARTICLES.phases            =   ones(size(PARTICLES.x),'uint32');
% PARTICLES.x                 =   [PARTICLES.x, PARTICLES.x];
% PARTICLES.z                 =   [PARTICLES.z, PARTICLES.z];
% PARTICLES.phases            =   [PARTICLES.phases, 2*ones(size(PARTICLES.phases), 'uint32')];

% particle class: {1: crystallized (solid), 2: silicate melt}
%   - REMARK: We should probably change this such that it is a bit more
%   readable, maybe by creating fields an additional field called PARTICLES.FLUID, which has x,z and phases information.
PARTICLES.class                 =   PARTICLES.phases;
PARTICLES.HistVar.PHI  =   zeros(size(PARTICLES.x));            % melt fraction phi

%% Non-dimensionalize input parameters
CHAR.Viscosity  = 1e20;
CHAR.Length     = sqrt(kappa_0 * (zeta_0 + 4*eta0/3) / eta_f);
CHAR.Porosity   = 1;
CHAR.Time       = phi_0 * (rho_s-rho_f) * g * sqrt(eta_f * (zeta_0 + 4*eta0/3) / kappa_0 ); % [Barcilon, 1989]
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS, CHAR);

% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input);

BC.Darcy.bc_nodes   =   find(MESH.DARCY.Point_id==1);
BC.Darcy.bc_phi     =   phi_0 * ones(size(BC.Darcy.bc_nodes));

% Initialize porosity field
load SolWaveSolutionHR                      % Load analytical solution that was used in the Keller et al. (2013) paper
[Z_anal,in] = unique(GRID.EL.Coord(:,2));
Phi_anal    = PROP.Phi(in);

PARTICLES.HistVar.PHI  = interp1(Z_anal/CHAR.Length,Phi_anal,PARTICLES.z);
PARTICLES.HistVar.PHI(isnan(PARTICLES.HistVar.PHI)) = 1e-3;

PARTICLES.HistVar.PHI = PARTICLES.HistVar.PHI*1/CHAR.Porosity;

%% plot initial porosity distribution
figure(1); clf;
plot(PARTICLES.z*CHAR.Length/1e3,PARTICLES.HistVar.PHI*CHAR.Porosity,'bo');
xlabel('z [km]');
title('Initial porosity profile');


%% Time-stepping loop
dt                  = 3e4*CHAR.SecYear/CHAR.Time; % 30k years
time                = 0;
max_time            = 40; % 1.2 Myr
for itime=1:max_time % time-stepping
    start_cpu = cputime;
    
    %% Compute Stokes-Darcy equations
    [PARTICLES, MESH, INTP_PROPS, NUMERICS] = StokesDarcy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    
    %% Advect particles [MESH is Eulerian in this simulation]
    PARTICLES.x     = PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     = PARTICLES.z + PARTICLES.Vz*dt;
    time            = time+dt;
    
    %% Compute new timestep
    dx              = min(diff(unique(MESH.NODES(1,:))));
    Vx              = max(abs(MESH.VEL(:)));
    CFL             = 0.5;
    % dt              = CFL*dx/Vx;
    
    %% Plot results
    if itime == 1
        figure(2); clf;
        subplot(2,1,1);
        plot(MESH.DARCY.NODES(2,MESH.DARCY.RegularGridNumber(:,3))*CHAR.Length/1e3,MESH.DARCY.PHI0(MESH.DARCY.RegularGridNumber(:,3))*CHAR.Porosity,'k-');
        title('Initial Porosity [1], t = 0');
        xlabel('z [km]');
        axis([0 1.4 0 0.01]); set(gca,'ytick',[.001 .01],'xtick',0:.3:1.2);
        
        pc_plot = MESH.CompVar.Pressure(MESH.RegularGridNumber(:,3))*CHAR.Stress;
        pc_plot([1:end/10, ceil(.9*end:end)]) = 0;
        subplot(2,1,2);
        plot(MESH.NODES(2,MESH.RegularGridNumber(:,3))*CHAR.Length/1e3,pc_plot,'k-');
        title('Compaction pressure [Pa]');
        xlabel('z [km]');
        axis([0 1.4 -1e5 1e5]); set(gca,'ytick',[-1e5 0 1e5],'xtick',0:.3:1.2);
        drawnow;
        
        % savepdf2(2,'SolWave_00',[3 2]);
    end
    
    if mod(itime,10)==0 && itime>1
        phi_plot= MESH.DARCY.PHI(MESH.DARCY.RegularGridNumber(:,3))*CHAR.Porosity;
        phi_plot([1:end/10, ceil(.9*end:end)]) = phi_0;
        pc_plot = MESH.CompVar.Pressure(MESH.RegularGridNumber(:,3))*CHAR.Stress;
        pc_plot([1:end/10, ceil(.9*end:end)]) = 0;
        
        figure(2);
        subplot(2,1,1); hold on;
        plot(MESH.DARCY.NODES(2,MESH.DARCY.RegularGridNumber(:,3))*CHAR.Length/1e3,phi_plot,'-');
        title(['Porosity [1] every 10 time-steps, dt = ', num2str(dt*CHAR.Time/CHAR.SecYear/1e3,'%u'),' kyrs, T = ' num2str(time*CHAR.Time/CHAR.SecYear/1e6,'%1.2f') ' Myrs']);
        axis([0 1.4 0 0.01]); set(gca,'ytick',[.001 .01],'xtick',0:.3:1.2);
        hold off;
        
        subplot(2,1,2); hold on;
        plot(MESH.NODES(2,MESH.RegularGridNumber(:,3))*CHAR.Length/1e3,pc_plot,'-');
        axis([0 1.4 -1e5 1e5]); set(gca,'ytick',[-1e5 0 1e5],'xtick',0:.3:1.2);
        hold off;
        
        % savepdf2(2,['SolWave_' num2str(itime,'%02u')],[3 2]);
    end
    
    %% Timing and console output
    end_cpu         = cputime;
    time_timestep   = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs and has dt=',num2str(dt)])
    disp(' ')
    
end
% ex_ParameterizedMeltFraction give an example of usage of ParameterizedMeltFraction_HydrousMantleMelting_Katz2003

% More details described in:
%   A new parameterization of hydrous mantle melting
%   Katz, Richard F.; Spiegelman, Marc; Langmuir, Charles H. 
%   Geochem. Geophys. Geosyst.  Vol. 4, No. 9, 1073  DOI 10.1029/2002GC000433 
%   09 September 2003 
% $Id:ex_ParameterizedMeltFraction.m 5190 2015-03-09 ljeff $
% 
% 
% Reproduce some results of Katz et al.,2003 
clear;

AddDirectories;
%% reproduce Figure 4
% isobaric melting for different water content
Temperature     =   900:10:1400;% in [oC]
Water           =   [0 0.02 0.05 0.1 0.3]./1e2;% water content wt%
Pressure        =   1.0.*ones(size(Temperature));% in [GPa]
Fcpx            =   0.17.*ones(size(Temperature)); % cpx weight fraction

color=[0 0 0; 0 0 1;0 1 0 ;0 1 1;1 0 0] ;
figure(4),clf
for i=1:length(Water)
    X     =   Water(i).*ones(size(Temperature));
    [F,cf]   =   ParameterizedMeltFraction_HydrousMantleMelting_Katz2003(Pressure,Temperature,X,Fcpx);
    
    plot(Temperature,F,'color',color(i,:),'linewidth',2);title('Isobaric (1GPa) hydrous melting');axis([900 1400 0 0.4]);
    xlabel('Temperature [oC]');ylabel('F,melt fraction')
    grid on;hold on
    
end
hold off
legend('0 wt%','0.02wt%','0.05wt%','0.1wt%','0.3wt%','Location','Best');




%% reproduce of Figure 5a
% isobaric melting for different temperature
Water           =   [0:0.005:0.25]./1e2;% water content
Temperature     =   [1200 1250 1300 1350];%in [Oc]
Pressure        =   1.5.*ones(size(Water));% in [GPa]
Fcpx            =   0.17.*ones(size(Water));
figure(5),clf
subplot(121)
for i=1:length(Temperature)
    T       =   Temperature(i).*ones(size(Water));
    [F]     =   ParameterizedMeltFraction_HydrousMantleMelting_Katz2003(Pressure,T,Water,Fcpx);
    plot(Water.*1e2,F,'color',color(i,:),'linewidth',2);title('Isobaric (1.5GPa) hydrous melting');axis([0 0.25 0 0.25]);
    xlabel('Bulk water,wt%');ylabel('F,melt fraction')
    grid on;hold on
end
hold off
legend('1200^\circC','1250^\circC','1300^\circC','1350^\circC','Location','Best');


%% reproduce of Figure 5b
% isobaric melting for different temperature
Water           =   [0:0.005:0.25]./1e2;% water content
Temperature     =   [1200 1250 1300 1350];%in [Oc]
Pressure        =   1.5.*ones(size(Water));% in [GPa]
Fcpx            =   0.17.*ones(size(Water));

subplot(122)
for i=1:length(Temperature)
    T       =   Temperature(i).*ones(size(Water));
    [F,cf]     =   ParameterizedMeltFraction_HydrousMantleMelting_Katz2003(Pressure,T,Water,Fcpx);
    plot(cf.*100,F,'color',color(i,:),'linewidth',2);title('Isobaric (1.5GPa) hydrous melting');axis([0 5 0 0.2]);
    xlabel('Water in the melt,wt%');ylabel('F,melt fraction')
    grid on;hold on
end
hold off
legend('1200^\circC','1250^\circC','1300^\circC','1350^\circC','Location','Best');


%% reproduce of Figure 6
% anhydrous melting,modal cpx=15% compared to experiments
Temperature     =   1000:10:1900; % in [oC]
Pressure        =   [0 1 1.5 3];% in [GPa]
Fcpx            =   0.15.*ones(size(Temperature));

figure(6),clf
% x,y domain
xl = [1000 1600;1150 1500;1200 1700;1400 1900];
yl = [0 0.7;0 0.7;0 0.8;0 1];
for i=1:length(Pressure)
    P       =   Pressure(i).*ones(size(Temperature));
    X       =   0.*ones(size(Temperature));
    [F]     =   ParameterizedMeltFraction_HydrousMantleMelting_Katz2003(P,Temperature,X,Fcpx);
    subplot(2,2,i)
    plot(Temperature,F,'color',color(i,:),'linewidth',2);title(['Isobaric (',num2str(Pressure(i)),'GPa) anhydrous melting']);
    axis([xl(i,:) yl(i,:)]);xlabel('Temperature [oC]');ylabel('F,melt fraction')
    grid on;hold on
end
hold off
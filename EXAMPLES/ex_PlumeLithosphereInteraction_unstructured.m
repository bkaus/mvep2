% Models plume-lithosphere interaction, similar to a benchmark study by
% Crameri et al. (2012), GJI.

% $Id: ex_ViscousRheologyProfile.m 4901 2013-10-26 19:33:44Z lkausb $


% Add paths & subdirectories
AddDirectories;

clear

tic

if ismac
    CreatePlots         =   logical(1);
else
    CreatePlots         =   logical(0);
end

factor              =   2;
SecYear             =   3600*24*365.25;
NUMERICS.Restart    =   logical(1);
NUMERICS.dt_max     =   250e3*SecYear;
NUMERICS.time_end   =   1 + 0*30;
UpperBC             =   'Free Slip';
% UpperBC             =   'No Stress';


%% Create mesh
opts.element_type   =   'quad4'
mesh_input.x_min    =   -3400e3;
mesh_input.x_max    =    3400e3;
mesh_input.z_min    =   -700e3;
mesh_input.z_max    =   0;
opts.nx             =   501*factor;
opts.nz             =   201*factor;
NUMERICS.Nonlinear.Tolerance=1;    % linear viscous so we can switch off nonlinearities
NUMERICS.Plasticity.MaximumYieldStress = 1e20;
NUMERICS.LinearSolver.PressureShapeFunction = 'Original_MILAMIN';

ThermalAgeLeft_Myrs     =   30;                 % Thermal age of lithosphere on the left in Myrs, assuming half-space cooling.
ThermalAgeRight_Myrs    =   50;                 % Thermal age of lithosphere to the right in Myrs

dt                      =   100*SecYear;       % initial dt
ThicknessLithosphere    =   100e3;          	% Thickness crust in km
RadiusPlume             =   50e3;
DepthPlume              =   300e3;
ViscosityLithosphere    =   1e25;       % 21-22-23-24-25-26
ViscosityMantle         =   1e20;


%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values


%------------------------------
% Mantle
Phase                   =   1;
Type                                            =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu       =   ViscosityMantle;                             % parameter

Type              	=   'Constant';
MATERIAL_PROPS(Phase).Density.(Type).Rho        =   3300;


%
%----------------------------------

%------------------------------
% Lithosphere
Phase            	=   2;
Type                                            =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu       =   ViscosityLithosphere;                             % parameter

Type              	=   'Constant';
MATERIAL_PROPS(Phase).Density.(Type).Rho        =   3300;

% Type              	=   'DruckerPrager';
% MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion        =   1000e6;
% MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle   =   0;

%
%----------------------------------

%------------------------------
% Plume
Phase            	=   3;
Type                                            =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu       =   ViscosityMantle;                             % parameter

Type              	=   'Constant';
MATERIAL_PROPS(Phase).Density.(Type).Rho        =   3200;

%
%----------------------------------

% Gravitational acceleration
MATERIAL_PROPS(1).Gravity.Value                 =   10;

% Add material properties that were not defined here, but which MILAMIN_VEP needs to know
MATERIAL_PROPS                                 =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required


%% Numerical parameters
NUMERICS.Viscosity.LowerCutoff                  =   1e17;
NUMERICS.Viscosity.UpperCutoff                  =   1e28;
NUMERICS                                        =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required



%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{2};
BC.Stokes.Left          =   Bound{2};
BC.Stokes.Right         =   Bound{2};
BC.Stokes.Top           =   UpperBC;
BC.Stokes.BG_strrate    =   0;

% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   20 + 273;
BC.Energy.Value.Bottom=   1300+273;                    % T at bottom of mantle


%% Create initial Particles

factor                      =   1;
numz                        =   400*factor;
numx                        =   ceil((mesh_input.x_max-mesh_input.x_min)/(mesh_input.z_max-mesh_input.z_min))*numz;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (mesh_input.z_max-mesh_input.z_min)/numz;
[PARTICLES.x,PARTICLES.z]   =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,mesh_input.z_min:dz:mesh_input.z_max);
PARTICLES.x                 =   PARTICLES.x(:);
PARTICLES.z                 =   PARTICLES.z(:);
PARTICLES.x                 =   PARTICLES.x + 0*[rand(size(PARTICLES.x))-0.5]*dx;
PARTICLES.z                 =   PARTICLES.z + 0*[rand(size(PARTICLES.x))-0.5]*dz;

% Set initial temperature
PARTICLES.HistVar.T         =   PARTICLES.x*0;       % in K
PARTICLES.HistVar.Txx       =   PARTICLES.x*0;
PARTICLES.HistVar.Tyy       =   PARTICLES.x*0;
PARTICLES.HistVar.Txy       =   PARTICLES.x*0;


% % add refined particles
% factor = 1;
% numz                        =   600*factor;
% % numx=400;
% numx                        =   ceil((2000e3)/(300e3))*numz;
% dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
% dz                          =   (mesh_input.z_max--300e3)/numz;
% [PARTICLES1.x,PARTICLES1.z] =   meshgrid(-1000e3:dx:1000e3,-300e3:dz:mesh_input.z_max);
% PARTICLES1.x            	=   PARTICLES1.x(:);
% PARTICLES1.z            	=   PARTICLES1.z(:);
% PARTICLES1.x              	=   PARTICLES1.x + 0*[rand(size(PARTICLES1.x))-0.5]*dx;
% PARTICLES1.z            	=   PARTICLES1.z + 0*[rand(size(PARTICLES1.x))-0.5]*dz;
%
% PARTICLES.x                 =   [PARTICLES.x; PARTICLES1.x ];
% PARTICLES.z                 =   [PARTICLES.z; PARTICLES1.z ];





% % Generate a z-grid with low res. @ bottom and higher @ lithosphere
% refine_x                =   [0 .7    1];
% refine_z                =   [1 .1   .1];
% z                       =   GenerateGrid_1D(refine_x , refine_z, opts.nz);
% mesh_input.z_vec        =   z;

%
% refine_x                =   [0 .2  .8 1];
% refine_z                =   [1 .9  .9 1];
% x                       =   GenerateGrid_1D(refine_x , refine_z, opts.nx);

% x = -1:(2/(opts.nx-1)):1;
% x=x.^3;
% mesh_input.x_vec        =   x;









%% Non-dimensionalize input parameters
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,[]);


dt                      =   dt/CHAR.Time;
time                    =    0;

%% Load breakpoint file if required
Load_BreakpointFile

% % Generate a z-grid with low res. @ bottom and higher @ lithosphere
% mesh_input.z_vec        =   GenerateGrid_1D([0 .5    1] ,  [1 .15  .15], opts.nz);
%
% % refine x-grid around plume
% mesh_input.x_vec        =   GenerateGrid_1D([0 .2  .8 1] , [1 .1  .1 1], opts.nx);


%% Plot initial particles
if 1==0 & CreatePlots
    Style	= {'*r', '+g', '*y', '.b', '*k', '.m', 'd'};
    %     if max(Point_id)>1
    %         for i=1:max(Point_id)
    %             Ind	= find(Point_id==i);
    %             a = mod(i,size(Style))+1;
    %             plot(GCOORD(1,Ind), GCOORD(2,Ind), Style{a(2)});
    %         end
    %     end
    
    % Create mesh
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);
    
    figure(1), clf, hold on
    PlotMesh(MESH,[],CHAR.Length/1e3,'r.-'),
    
    for i=1:max(PARTICLES.phases)
        ind = find(PARTICLES.phases==i );
        plot(PARTICLES.x(ind)*CHAR.Length/1e3,PARTICLES.z(ind)*CHAR.Length/1e3,Style{i})
    end
    
end





% Create contours to build unstructured mesh
opts.element_type   =   'tri7'
opts.max_tri_area   =   100;
opts.triangulate_poly = 1;
opts.min_angle        = 33.5;



Topo_x = mesh_input.x_min:(mesh_input.x_max-mesh_input.x_min)/100:mesh_input.x_max;
Topo_z = 0*Topo_x;


% Outer box of the computational domain
points              = [ mesh_input.x_min mesh_input.z_min;
    mesh_input.x_max mesh_input.z_min;
    Topo_x(end:-1:1)' Topo_z(:)]';
n                   = length(points)
segments            = [[1:n]; [2:n, 1]; ];
segmentmarkers      = [ 1; 2; 3*ones(length(Topo_x)-1,1); 4]';




% Lithosphere
points1             = [ Topo_x,                    ;
    Topo_z-ThicknessLithosphere/CHAR.Length];
n1                 	= length(points1);
segments1        	= [[1:n1-1]; [2:n1]; ]+length(segments);
segmentmarkers1   	= [ 5*ones(length(segments1),1) ]';

points              = [points,          points1];
segmentmarkers      = [segmentmarkers,  segmentmarkers1];
segments            = [segments,        segments1];


% the mtriangle mesh generator doesn't seem to allow to put local area constraints (probably a mtriangle bug), so we fake it here

% add additional segments for lithosphere every 5 km
depth = ThicknessLithosphere;

for i=1:300
    depth = depth-2e3;
    
    if depth<1e3
        break
    end
    
    points1             = [ Topo_x,                  ;
        Topo_z-depth/CHAR.Length];
    n1                 	= length(points1);
    segments1        	= [[1:n1-1]; [2:n1]; ]+length(points);
    segmentmarkers1   	= [ 5*ones(length(segments1),1) ]';
    
    points              = [points,          points1];
    segmentmarkers      = [segmentmarkers,  segmentmarkers1];
    segments            = [segments,        segments1];
    
    
    
    
end


% Plume
t                   = 0:.1:2*pi;
points1             = [RadiusPlume/CHAR.Length*cos(t); -sin(t)*RadiusPlume/CHAR.Length- DepthPlume/CHAR.Length];
n1                   = length(points);
segments1           = [[1:n1]; [2:n1, 1]; ]+length(points);
segmentmarkers1     = ones(1,n1)*6;


points          = [points,          points1];
segmentmarkers  = [segmentmarkers,  segmentmarkers1];
segments        = [segments,        segments1];



% % Set points with regional atributes
% mesh_input.regions(:,1) = [mesh_input.x_min+0.1 mesh_input.z_min+0.1 10 1e2]';                           % mantle
% mesh_input.regions(:,2) = [mesh_input.x_min+0.1 -ThicknessLithosphere/CHAR.Length+1e-3 2 10 ]';     % lithos


% Set triangle input
mesh_input.points         = points;
mesh_input.segments       = uint32(segments);
mesh_input.segmentmarkers = uint32(segmentmarkers);




%% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input, MESH);



%% ADD PARTICLES such that every triangle has at least one particle
ind                     =   (MESH.ELEMS(7,:));
PARTICLES1.x            =   MESH.NODES(1,ind)';
PARTICLES1.z            =   MESH.NODES(2,ind)';
PARTICLES.x             =   [PARTICLES.x; PARTICLES1.x ];
PARTICLES.z             =   [PARTICLES.z; PARTICLES1.z ];





% Set initial temperature
PARTICLES.HistVar.T         =   PARTICLES.x*0;       % in K
PARTICLES.HistVar.Txx       =   PARTICLES.x*0;
PARTICLES.HistVar.Tyy       =   PARTICLES.x*0;
PARTICLES.HistVar.Txy       =   PARTICLES.x*0;


% Set phases
PARTICLES.phases            =   ones(size(PARTICLES.x));

ind                         =   find(PARTICLES.z>-ThicknessLithosphere/CHAR.Length);
PARTICLES.phases(ind)       =   2;

Xc                          =   0;
Zc                          =   -DepthPlume/CHAR.Length;
ind                         =   find( (((PARTICLES.x-Xc).^2 + (PARTICLES.z-Zc).^2) < (RadiusPlume/CHAR.Length).^2) );
PARTICLES.phases(ind)       =   3;


NUMERICS.InterpolationMethod.ParticlesToIntegrationPoints = 'constant';     % constant properties per element
NUMERICS.LinearSolver.UseFreeSurfaceStabilizationAlgorithm=logical(0);



[PARTICLES, MESH, INTP_PROPS, NUMERICS] 	=   StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);

INTP_PROPS.E2nd=INTP_PROPS.Strainrate.E2nd;
INTP_PROPS.T2nd=INTP_PROPS.Stress.T2nd;
%% Update BC's for stokes
[BC]                                    =   SetBC(MESH, BC, INTP_PROPS);




% figure(1), clf
% PlotMesh(MESH,[], CHAR.Length/1e3), axis equal
% axis([-100 100 -100 10])


% error(' ')


for itime=NUMERICS.time_start:NUMERICS.time_end
    start_cpu = cputime;
    
    
    %% Compute Stokes solution
    [MESH, INTP_PROPS, SUCCESS, minDetJ, NUMERICS]    =   Stokes2d_vep(MESH, INTP_PROPS, BC, MATERIAL_PROPS, NUMERICS, CHAR, PARTICLES, dt);
    
    % Update stresses
    INTP_PROPS                  =   UpdateStressesStrainsAfterTimestep(INTP_PROPS, dt);
    
    
    
    
    
    
    %% Plot results
    if mod(itime,100)==0 & CreatePlots
        
        % Temperature
        figure(2), clf
        PlotMesh(MESH,INTP_PROPS.Rho'*CHAR.Density, CHAR.Length/1e3), axis equal, axis tight, colorbar; shading flat
        axis equal, axis tight, colorbar, title('Density')
        axis([-1000 1000 -400 10])
        
        figure(3), clf
        PlotMesh(MESH,log10(INTP_PROPS.Mu_Eff'*CHAR.Viscosity), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('log10(effective viscosity)')
        axis([-1000 1000 -400 10])
        
        figure(4), clf
        [ MESH_DATA_average, MESH_DATA] = Extrapolate_IntpDataToNodes(MESH, INTP_PROPS.Strainrate.Exx); % extrapolate from intp -> nodes (in a discontinous manner)
        
        PlotMesh(MESH,log10(MESH_DATA*(1/CHAR.Time)), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('log10(e2nd [1/s])')
        axis([-1000 1000 -400 10])
        
        hold on
        ind=find(PARTICLES.phases==3 & ~isnan(PARTICLES.elem) & abs(PARTICLES.x*CHAR.Length<150e3) & (PARTICLES.z*CHAR.Length<-200e3));
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'w.')
        
        
        
        figure(5), clf
        %         subplot('position',[0.1 0.8 0.8 0.15])
        
        
        VEL_x       =   MESH.VEL(1,:);
        VEL_z       =   MESH.VEL(2,:);
        
        X           =   MESH.NODES(1,:);Z=MESH.NODES(2,:);
        ind_topo    =   find(MESH.Point_id==3);
        
        Top_x       =   X(ind_topo)*CHAR.Length;
        [Top_x,id]  =   sort(Top_x);
        ind_topo    =   ind_topo(id);
        Top_z       =   Z(ind_topo)*CHAR.Length;
        
        Top_Vx      =   VEL_x(ind_topo)*CHAR.Velocity;
        Top_Vz      =   VEL_z(ind_topo)*CHAR.Velocity;
        
        [ TXX_DATA, dummy] = Extrapolate_IntpDataToNodes(MESH, INTP_PROPS.Stress.Txx*CHAR.Stress); % extrapolate from intp -> nodes (in a discontinous manner)
        [ T2nd_DATA, dummy] = Extrapolate_IntpDataToNodes(MESH, INTP_PROPS.Stress.T2nd*CHAR.Stress); % extrapolate from intp -> nodes (in a discontinous manner)
        
        Top_Txx  = TXX_DATA(ind_topo);
        Top_T2nd = T2nd_DATA(ind_topo);
        
        
        
        figure(41), clf
        %         plot(Top_x/1e3, Top_z)
        %
        %         axis tight
        %         xlabel('Width [km]')
        %         ylabel('Height [m]')
        %         switch UpperBC
        %             case 'Free Slip'
        %                 axis([-1000 1000 -10 10])
        %             case 'No Stress'
        %                 %                 axis([-1000 1000 min(Z2d(end,:)) max(Z2d(end,:))])
        %         end
        
        %
        %         %
        %
        %
        %
        %         subplot('position',[0.1 0.0 0.8 0.75])
        %         PlotMesh(MESH,(MESH.CompVar.T2nd*(CHAR.Stress/1e6)), CHAR.Length/1e3), shading interp
        
        [ MESH_DATA_average, MESH_DATA] = Extrapolate_IntpDataToNodes(MESH, INTP_PROPS.Stress.T2nd); % extrapolate from intp -> nodes (in a discontinous manner)
        
        PlotMesh(MESH,(MESH_DATA*(CHAR.Stress/1e6)), CHAR.Length/1e3), shading interp
        
        
        axis equal, axis tight, h=colorbar('horiz'), axis tight
        xlabel(h,'Stress [MPa]')
        
        
        %         hold on
        %         ind=find(PARTICLES.phases==3 & ~isnan(PARTICLES.elem) & abs(PARTICLES.x*CHAR.Length<100e3) & (PARTICLES.z*CHAR.Length<-200e3));
        %         plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'w.')
        %
        
        %         hold on
        %         ind = find(PARTICLES.phases==3); % plume
        %         plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'wo')
        h=        title(['\tau_{2nd} [MPa]; Viscosity Lithosphere=',num2str(MATERIAL_PROPS(2).Viscosity.Constant.Mu*CHAR.Viscosity,'%1.1e'),'; Thickness Lithosphere=',num2str(ThicknessLithosphere/1e3),' km'  ])
        axis([-1000 1000 -400 0])
        set(gca,'FontSize',12)
        set(h,'FontSize',12)
        
        
        
        
        
        %         figure(7), clf
        %         PlotMesh(MESH,(MESH.CompVar.Rho*(CHAR.Density)), CHAR.Length/1e3), shading interp
        %         axis equal, axis tight, colorbar, title('Density [kg/m^3]')
        %
        
        %         figure(8), clf
        %         X=MESH.NODES(1,:);Z=MESH.NODES(2,:);
        %         X2d = X(MESH.RegularGridNumber)*CHAR.Length;
        %         Z2d = Z(MESH.RegularGridNumber)*CHAR.Length;
        %         plot(X2d(end,:)/1e3,Z2d(end,:))
        %         xlabel('Width [km]')
        %         ylabel('Topography [m]')
        %         drawnow
        %
        %         Z   = MESH.NODES(2,:);
        %         Z2d = Z(MESH.RegularGridNumber)*CHAR.Length/1e3;
        %         T2nd = MESH.CompVar.T2nd(MESH.RegularGridNumber)*CHAR.Stress;
        %         Tyy_2d = MESH.HistVar.Tyy(MESH.RegularGridNumber)*CHAR.Stress;
        %         Txx_2d = MESH.HistVar.Txx(MESH.RegularGridNumber)*CHAR.Stress;
        %
        
        figure(3), drawnow
        
        %         figure(9)
        %         plot(Time_vec, MaxTopoTime,'b-')
        %         xlabel('Time [Myrs]')
        %         ylabel('Topography [m]')
        
        
        
        % Predict horizontal stresses from tthin plate theory
        figure(10), clf
        
        dx  =   Top_x(2)-Top_x(1);
        Exx =   ThicknessLithosphere/2*diff(Top_Vz,2)/dx^2;        % in-plate strain rate
        
        Sxx  =  -4*ViscosityLithosphere*Exx;
        Txx  =  -2*ViscosityLithosphere*Exx;
        
        
        dVz1_dx     = diff(Top_Vz,1)/dx;                        % dVz/dx
        dVz1_dx_1   = (dVz1_dx(2:end)+dVz1_dx(1:end-1))/2;      % Smooth
        
        dVz2_dx     = diff(dVz1_dx_1,1)/dx;                     % dVz/dx^2
        dVz2_dx_1   = (dVz2_dx(2:end)+dVz2_dx(1:end-1))/2;      % Smooth
        
        dVz3_dx     = diff(dVz2_dx_1,1)/dx;                     % dVz/dx^3
        dVz3_dx_1   = (dVz3_dx(2:end)+dVz3_dx(1:end-1))/2;      % Smooth
        
        dVz4_dx     = diff(dVz3_dx_1,1)/dx;                     % dVz/dx^4
        dVz4_dx_1   = (dVz4_dx(2:end)+dVz4_dx(1:end-1))/2;      % Smooth
        
        
        q   = ThicknessLithosphere^3*ViscosityLithosphere/3*diff(Top_Vz,4)/dx^4;
        
        plot(Top_x(2:end-1)/1e3,Txx/1e6,Top_x(2:end-1)/1e3,Top_Txx(2:end-1)/1e6,'-');
        legend('Predicted Txx','Measured Txx')
        
        xlabel('Width [km]')
        ylabel('Txx at surface (MPa)')
        axis tight
        
        
        
        %
        %
        %         % Compare with line-load theory
        %         D               =   1/27*ViscosityLithosphere*ThicknessLithosphere^3;
        %         alpha           =   [D/(3200*10) ]^(1/4);
        %
        %         alpha=8e5;
        %
        %
        %         Vz_norm = exp(-abs(Top_x)/alpha).*(cos(abs(Top_x)/alpha) + sin(abs(Top_x)/alpha) );
        %
        %         q = ThicknessLithosphere^3*ViscosityLithosphere/3*diff(Top_Vz,4)/dx^4 + 3300*10*Top_z(3:end-2);
        %
        figure(11), clf
        %         plot(Top_x,Top_Vz/max(Top_Vz))
        
        
        subplot(411)
        plot(Top_x(3:end-2)/1e3,q)
        ylabel('q')
        
        
        subplot(412)
        plot(Top_x(3:end-2)/1e3, Top_z(3:end-2))
        ylabel('Amplitude [m]')
        
        
        subplot(413)
        plot(Top_x(3:end-2)/1e3, Top_Vz(3:end-2))
        ylabel('Vz [m/s]')
        
        
        subplot(414)
        plot(Top_x(2:end-1)/1e3, Txx/1e6, Top_x/1e3,Top_Txx/1e6)
        ylabel('Txx [MPa]')
        
        
        
    end
    
    
    
    end_cpu         = cputime;
    time_timestep    = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs; dt=',num2str(dt*CHAR.Time/CHAR.SecYear/1e3),' kyrs'])
    disp(' ')
    
    
    
    
    % Save analysis data for plume-lithos runs
    VEL_x   =   MESH.VEL(1,:);
    VEL_z   =   MESH.VEL(2,:);
    X       =   MESH.NODES(1,:);Z=MESH.NODES(2,:);
    
    Z   = MESH.NODES(2,:);
    ind_topo    =   find(MESH.Point_id==3);
    
    Top_x = X(ind_topo)*CHAR.Length;
    [Top_x,id] = sort(Top_x);
    ind_topo = ind_topo(id);
    
    
    Top_z = Z(ind_topo)*CHAR.Length;
    
    Top_Vx = VEL_x(ind_topo)*CHAR.Velocity;
    Top_Vz = VEL_z(ind_topo)*CHAR.Velocity;
    
    
    [ TXX_DATA, dummy] = Extrapolate_IntpDataToNodes(MESH, INTP_PROPS.Stress.Txx*CHAR.Stress); % extrapolate from intp -> nodes (in a discontinous manner)
    [ T2nd_DATA, dummy] = Extrapolate_IntpDataToNodes(MESH, INTP_PROPS.Stress.T2nd*CHAR.Stress); % extrapolate from intp -> nodes (in a discontinous manner)
    
    Top_Txx  = TXX_DATA(ind_topo);
    Top_T2nd = T2nd_DATA(ind_topo);
    
    
    
    
    ANALA.x     =   Top_x;
    ANALA.z     =   Top_z;
    ANALA.Vx    =   Top_Vx;
    ANALA.Vz    =   Top_Vz;
    ANALA.eta   =   ViscosityLithosphere;
    ANALA.eta_m =   ViscosityMantle;
    ANALA.drho  =   100;
    ANALA.R     =   RadiusPlume;
    ANALA.d     =   DepthPlume;
    ANALA.dt    =   dt;
    ANALA.SecYear = SecYear;
    ANALA.h     =   ThicknessLithosphere;
    ANALA.T2nd  =   Top_T2nd;              % Second invariant of stress
    ANALA.Txx   =   Top_Txx;            % Txx
    ANALA.Max_T2nd = max(T2nd_DATA(:));
    
    
    
    if itime==1
        ANALA0 = ANALA;
    end
    
    
    
    
    
    MaxTopoTime(itime)          =    max(MESH.NODES(2,:))*CHAR.Length;          % in m
    Time_vec(itime)             =   time*CHAR.Time/CHAR.SecYear/1e6;            % in Myrs
    
    max(MESH.NODES(2,:))*CHAR.Length
    
    
    
    
    if  MaxTopoTime(itime)>=100
        break;
    end
    
    
    switch UpperBC
        case 'No Stress'
            % Compute new timestep to ensure constat vertical motion at surface
            dt = 5/max(Top_Vz);
            dt = dt/CHAR.Time;
            
            
            %% Advect particles and mesh - mesh is recreated every timestep
            PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
            PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
            MESH.NODES      =   MESH.NODES  + MESH.VEL*dt;
            
            time            =   time+dt;
            
    end
    
end


switch  UpperBC
    case 'Free Slip'
        fname = ['Eta',num2str(log10(ViscosityLithosphere)),'_EtaM',num2str(log10(ViscosityMantle)),'_ThicknessLithos',num2str(ThicknessLithosphere/1e3),'_SmallBox_FreeSlip']
        
    case 'No Stress'
        fname = ['Eta',num2str(log10(ViscosityLithosphere)),'_EtaM',num2str(log10(ViscosityMantle)),'_ThicknessLithos',num2str(ThicknessLithosphere/1e3),'_SmallBox']
    otherwise
        error('unknown BC')
end



eval(['save ',fname,'  ANALA0  ANALA itime time'])


curdir=pwd;
cd /Users/kausb/WORK/WORK_IN_PROGRESS/MARCEL_Convection/PaperViscoElasticConvection_Trunk/PlumeLithosphereModels
eval(['save ',fname,'  ANALA0  ANALA itime time'])
cd(curdir)


max_Stress_MPa = round(max(Top_T2nd/1e6))
max_Stress_Txx = (max(Top_Txx/1e6))
Max_T2nd  = ANALA.Max_T2nd/1e6

% Standard setup after 100 m = 252.8855 (10 timesteps) vs 262.6866 MPa (20
% timesteps), so difference is a few %

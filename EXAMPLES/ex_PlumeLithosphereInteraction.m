% Models plume-lithosphere interaction, similar to a benchmark study by
% Crameri et al. (2012), GJI.

% $Id: ex_ViscousRheologyProfile.m 4901 2013-10-26 19:33:44Z lkausb $


% Add paths & subdirectories
AddDirectories;

clear

tic

if ismac
    CreatePlots         =   logical(1);
else
    CreatePlots         =   logical(0);
end

factor              =   1;
SecYear             =   3600*24*365.25;
NUMERICS.Restart    =   logical(1);
NUMERICS.dt_max     =   250e3*SecYear;
NUMERICS.time_end   =   10;
UpperBC             =   'Free Slip';
UpperBC             =   'No Stress';


%% Create mesh
opts.element_type   =   'quad4'
mesh_input.x_min    =   -3400e3;
mesh_input.x_max    =    3400e3;
mesh_input.z_min    =   -700e3;
mesh_input.z_max    =   0;
opts.nx             =   201*factor;
opts.nz             =   101*factor;
NUMERICS.Nonlinear.Tolerance=1;    % linear viscous so we can switch off nonlinearities
NUMERICS.Plasticity.MaximumYieldStress = 1e20;

ThermalAgeLeft_Myrs     =   30;                 % Thermal age of lithosphere on the left in Myrs, assuming half-space cooling.
ThermalAgeRight_Myrs    =   50;                 % Thermal age of lithosphere to the right in Myrs

dt                      =   100*SecYear;       % initial dt
ThicknessLithosphere    =   50e3;          	% Thickness crust in km
RadiusPlume             =   50e3;
DepthPlume              =   300e3;
ViscosityLithosphere    =   1e25;
ViscosityMantle         =   1e19;


%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values


%------------------------------
% Mantle
Phase                   =   1;
Type                                            =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu       =   ViscosityMantle;                             % parameter

Type              	=   'Constant';
MATERIAL_PROPS(Phase).Density.(Type).Rho        =   3300;


%
%----------------------------------

%------------------------------
% Lithosphere
Phase            	=   2;
Type                                            =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu       =   ViscosityLithosphere;                             % parameter

Type              	=   'Constant';
MATERIAL_PROPS(Phase).Density.(Type).Rho        =   3300;

% Type              	=   'DruckerPrager';
% MATERIAL_PROPS(Phase).Plasticity.(Type).Cohesion        =   1000e6;
% MATERIAL_PROPS(Phase).Plasticity.(Type).FrictionAngle   =   0;

%
%----------------------------------

%------------------------------
% Plume
Phase            	=   3;
Type                                            =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu       =   ViscosityMantle;                             % parameter

Type              	=   'Constant';
MATERIAL_PROPS(Phase).Density.(Type).Rho        =   3200;

%
%----------------------------------

% Gravitational acceleration
MATERIAL_PROPS(1).Gravity.Value                 =   10;

% Add material properties that were not defined here, but which MILAMIN_VEP needs to know
MATERIAL_PROPS                                 =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required


%% Numerical parameters
NUMERICS.Viscosity.LowerCutoff                  =   1e17;
NUMERICS.Viscosity.UpperCutoff                  =   1e28;
NUMERICS                                        =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required



%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{2};
BC.Stokes.Left          =   Bound{2};
BC.Stokes.Right         =   Bound{2};
BC.Stokes.Top           =   UpperBC;
BC.Stokes.BG_strrate    =   0;

% Thermal boundary condition
%                                        1              2           3
BoundThermal                    =   {'Zero flux', 'Isothermal','GaussianShapedPerturbation'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   20 + 273;
BC.Energy.Value.Bottom=   1300+273;                    % T at bottom of mantle


%% Create initial Particles

factor = 2;
numz                        =   400*factor;
numx                        =   ceil((mesh_input.x_max-mesh_input.x_min)/(mesh_input.z_max-mesh_input.z_min))*numz;
dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
dz                          =   (mesh_input.z_max-mesh_input.z_min)/numz;
[PARTICLES.x,PARTICLES.z]   =   meshgrid(mesh_input.x_min:dx:mesh_input.x_max,mesh_input.z_min:dz:mesh_input.z_max);
PARTICLES.x                 =   PARTICLES.x(:);
PARTICLES.z                 =   PARTICLES.z(:);
PARTICLES.x                 =   PARTICLES.x + 0*[rand(size(PARTICLES.x))-0.5]*dx;
PARTICLES.z                 =   PARTICLES.z + 0*[rand(size(PARTICLES.x))-0.5]*dz;

% % add refined particles
% factor = 1;
% numz                        =   600*factor;
% % numx=400;
% numx                        =   ceil((2000e3)/(300e3))*numz;
% dx                          =   (mesh_input.x_max-mesh_input.x_min)/numx;
% dz                          =   (mesh_input.z_max--300e3)/numz;
% [PARTICLES1.x,PARTICLES1.z] =   meshgrid(-1000e3:dx:1000e3,-300e3:dz:mesh_input.z_max);
% PARTICLES1.x            	=   PARTICLES1.x(:);
% PARTICLES1.z            	=   PARTICLES1.z(:);
% PARTICLES1.x              	=   PARTICLES1.x + 0*[rand(size(PARTICLES1.x))-0.5]*dx;
% PARTICLES1.z            	=   PARTICLES1.z + 0*[rand(size(PARTICLES1.x))-0.5]*dz;
% 
% PARTICLES.x                 =   [PARTICLES.x; PARTICLES1.x ];
% PARTICLES.z                 =   [PARTICLES.z; PARTICLES1.z ];


% Set initial temperature
PARTICLES.HistVar.T         =   PARTICLES.x*0 +273;       % in K
PARTICLES.HistVar.Txx       =   PARTICLES.x*0;
PARTICLES.HistVar.Tyy       =   PARTICLES.x*0;
PARTICLES.HistVar.Txy       =   PARTICLES.x*0;


% Set phases
PARTICLES.phases            =   ones(size(PARTICLES.x));

ind                         =   find(PARTICLES.z>-ThicknessLithosphere);
PARTICLES.phases(ind)       =   2;

Xc                          =   0;
Zc                          =   -DepthPlume;
ind                         =   find( (((PARTICLES.x-Xc).^2 + (PARTICLES.z-Zc).^2) < RadiusPlume.^2) );
PARTICLES.phases(ind)       =   3;

%
% % Plume
% ind = find(PARTICLES.z<=(-ThicknessCrust*1e3) & PARTICLES.z>(-(ThicknessCrust + ThicknessSHB)*1e3) ) ;
% PARTICLES.phases(ind) = 2;




% % Generate a z-grid with low res. @ bottom and higher @ lithosphere
% refine_x                =   [0 .7    1];
% refine_z                =   [1 .1   .1];
% z                       =   GenerateGrid_1D(refine_x , refine_z, opts.nz);
% mesh_input.z_vec        =   z;

%
% refine_x                =   [0 .2  .8 1];
% refine_z                =   [1 .9  .9 1];
% x                       =   GenerateGrid_1D(refine_x , refine_z, opts.nx);

% x = -1:(2/(opts.nx-1)):1;
% x=x.^3;
% mesh_input.x_vec        =   x;



%% Non-dimensionalize input parameters
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES, NUMERICS,[]);


dt                      =   dt/CHAR.Time;
time                    =    0;

%% Load breakpoint file if required
Load_BreakpointFile

% % Generate a z-grid with low res. @ bottom and higher @ lithosphere
% mesh_input.z_vec        =   GenerateGrid_1D([0 .5    1] ,  [1 .15  .15], opts.nz);
%
% % refine x-grid around plume
% mesh_input.x_vec        =   GenerateGrid_1D([0 .2  .8 1] , [1 .1  .1 1], opts.nx);


%% Plot initial particles
if 1==0 & CreatePlots
    Style	= {'*r', '+g', '*y', '.b', '*k', '.m', 'd'};
    %     if max(Point_id)>1
    %         for i=1:max(Point_id)
    %             Ind	= find(Point_id==i);
    %             a = mod(i,size(Style))+1;
    %             plot(GCOORD(1,Ind), GCOORD(2,Ind), Style{a(2)});
    %         end
    %     end
    
    % Create mesh
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);
    
    figure(1), clf, hold on
    PlotMesh(MESH,[],CHAR.Length/1e3,'r.-'),
    
    for i=1:max(PARTICLES.phases)
        ind = find(PARTICLES.phases==i );
        plot(PARTICLES.x(ind)*CHAR.Length/1e3,PARTICLES.z(ind)*CHAR.Length/1e3,Style{i})
    end
    
end

%% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input, MESH);

% Adapt mesh to Lithosphere
X                   =   MESH.NODES(1,:);
Z                   =   MESH.NODES(2,:);

Z2d                 =   Z(MESH.RegularGridNumber);
[d,id]              =   min(abs(Z2d(:,1)+ThicknessLithosphere/CHAR.Length));
Z2d(id,:)           =   -ThicknessLithosphere/CHAR.Length;
MESH.NODES(2,:)     =   Z2d(MESH.RegularGridNumber(:));



for itime=NUMERICS.time_start:NUMERICS.time_end
    start_cpu = cputime;
    
    
    %% Compute Stokes & Energy solution
    [PARTICLES, MESH, INTP_PROPS, NUMERICS] 	=   StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    
    
    
    %% Plot results
    if mod(itime,1)==0 & CreatePlots
        
        % Particles and velocities
        %         figure(1), clf, hold on
        %         PlotMesh(MESH,[],CHAR.Length/1e3,'r.-'),
        %         ind = find(PARTICLES.phases==1); % pos. buoyant  crust
        %         plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'ro')
        %         ind = find(PARTICLES.phases==2);% neg. buoyant  crust
        %         plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'go')
        %         ind = find(PARTICLES.phases==3);% mantle
        %         plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'bo')
        %         ind = find(PARTICLES.phases==4);% mantle lithosphere
        %         plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'mo')
        %         ind = find(PARTICLES.phases==5);% mantle, partially molten
        %         plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'y.')
        %
        %         %         quiver(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(1,:),MESH.VEL(2,:))
        %         axis equal, axis tight
        %         title(['Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),'Myrs'])
        %         drawnow
        %
        %
        % Temperature
        figure(2), clf
        PlotMesh(MESH,MESH.CompVar.Rho*CHAR.Density, CHAR.Length/1e3), axis equal, axis tight, colorbar; shading flat
        axis equal, axis tight, colorbar, title('Density')
        axis([-1000 1000 -400 10])
        
        figure(3), clf
        PlotMesh(MESH,log10(MESH.CompVar.Mu_Eff*CHAR.Viscosity), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('log10(effective viscosity)')
        axis([-1000 1000 -400 10])
        
        figure(4), clf
        PlotMesh(MESH,log10(MESH.CompVar.E2nd*(1/CHAR.Time)), CHAR.Length/1e3), shading interp
        axis equal, axis tight, colorbar, title('log10(e2nd [1/s])')
        axis([-1000 1000 -400 10])
        
        figure(5), clf
        subplot('position',[0.1 0.8 0.8 0.15])
        
        
        VEL_x = MESH.VEL(1,:);
        VEL_z = MESH.VEL(2,:);
        
        X=MESH.NODES(1,:);Z=MESH.NODES(2,:);
        X2d     = X(MESH.RegularGridNumber)*CHAR.Length;
        Z2d     = Z(MESH.RegularGridNumber)*CHAR.Length;
        
        Vx2d    = VEL_x(MESH.RegularGridNumber)*CHAR.Velocity;
        Vz2d    = VEL_z(MESH.RegularGridNumber)*CHAR.Velocity;
        
        Sxx_2d  =   MESH.HistVar.Txx(MESH.RegularGridNumber)*CHAR.Stress;       % Horizontal stress
        
        
        Top_x = X2d(end,:);
        Top_z = Z2d(end,:);
        
        Top_Vx = Vx2d(end,:);
        Top_Vz = Vz2d(end,:);
        
        
        plot(Top_x/1e3, Top_z)
        
        axis tight
        xlabel('Width [km]')
        ylabel('Height [m]')
        switch UpperBC
            case 'Free Slip'
                axis([-1000 1000 -10 10])
            case 'No Stress'
                %                 axis([-1000 1000 min(Z2d(end,:)) max(Z2d(end,:))])
        end
        
        
        %
        
        
        
        subplot('position',[0.1 0.0 0.8 0.75])
        PlotMesh(MESH,(MESH.CompVar.T2nd*(CHAR.Stress/1e6)), CHAR.Length/1e3), shading interp
        axis equal, axis tight, h=colorbar('horiz'), axis tight
        xlabel(h,'Stress [MPa]')
        
        
        hold on
        ind = find(PARTICLES.phases==3); % plume
        plot(PARTICLES.x(ind)*CHAR.Length/1e3, PARTICLES.z(ind)*CHAR.Length/1e3,'wo')
        title(['\tau_{2nd} [MPa]; Viscosity Lithosphere=',num2str(MATERIAL_PROPS(2).Viscosity.Constant.Mu*CHAR.Viscosity,'%1.1e'),'; Thickness Lithosphere=',num2str(ThicknessLithosphere/1e3),' km'  ])
        axis([-1000 1000 -400 0])
        
        
        
        
        
        %         figure(7), clf
        %         PlotMesh(MESH,(MESH.CompVar.Rho*(CHAR.Density)), CHAR.Length/1e3), shading interp
        %         axis equal, axis tight, colorbar, title('Density [kg/m^3]')
        %
        
        figure(8), clf
        X=MESH.NODES(1,:);Z=MESH.NODES(2,:);
        X2d = X(MESH.RegularGridNumber)*CHAR.Length;
        Z2d = Z(MESH.RegularGridNumber)*CHAR.Length;
        plot(X2d(end,:)/1e3,Z2d(end,:))
        xlabel('Width [km]')
        ylabel('Topography [m]')
        drawnow
        
        Z   = MESH.NODES(2,:);
        Z2d = Z(MESH.RegularGridNumber)*CHAR.Length/1e3;
        T2nd = MESH.CompVar.T2nd(MESH.RegularGridNumber)*CHAR.Stress;
        Tyy_2d = MESH.HistVar.Tyy(MESH.RegularGridNumber)*CHAR.Stress;
        Txx_2d = MESH.HistVar.Txx(MESH.RegularGridNumber)*CHAR.Stress;
        
        
        figure(3), drawnow
        
        %         figure(9)
        %         plot(Time_vec, MaxTopoTime,'b-')
        %         xlabel('Time [Myrs]')
        %         ylabel('Topography [m]')
        
        
        
        % Predict horizontal stresses from tthin plate theory
        figure(10), clf
        
        dx  =   Top_x(2)-Top_x(1);
        Exx =   ThicknessLithosphere/2*diff(Top_Vz,2)/dx^2;        % in-plate strain rate
        
        Sxx  =  -4*ViscosityLithosphere*Exx;
        Txx  =  -2*ViscosityLithosphere*Exx;
        
        
        
        q = ThicknessLithosphere^3*ViscosityLithosphere/3*diff(Top_Vz,4)/dx^4;
        
        plot(Top_x(2:end-1)/1e3,Txx/1e6,Top_x(2:end-1)/1e3,Sxx_2d(end,2:end-1)/1e6,'o',Top_x(2:end-1)/1e3,T2nd(end,2:end-1)/1e6,'r+');
        legend('Predicted Txx','Measured Txx')
        
        xlabel('Width [km]')
        ylabel('Txx at surface (MPa)')
        
        
        
        
        % Compare with line-load theory
        D               =   1/27*ViscosityLithosphere*ThicknessLithosphere^3;
        alpha           =   [D/(3200*10) ]^(1/4);
        
        alpha=8e5;
        
        
        Vz_norm = exp(-abs(Top_x)/alpha).*(cos(abs(Top_x)/alpha) + sin(abs(Top_x)/alpha) );
        
        q = ThicknessLithosphere^3*ViscosityLithosphere/3*diff(Top_Vz,4)/dx^4 + 3300*10*Top_z(3:end-2);
        
        figure(11), clf
        %         plot(Top_x,Top_Vz/max(Top_Vz))
        
        
        subplot(411)
        
        q_anal = (100*10*RadiusPlume^2/DepthPlume*2)*exp(-Top_x(3:end-2).^2/100e3^2) ;
        
        
        q_max = max(q)
        
        q_anal = q_max*exp(-Top_x(3:end-2).^2/100e3^2) ;
        
        plot(Top_x(3:end-2),q,Top_x(3:end-2),q_anal, Top_x(3:end-2),3300*10*Top_z(3:end-2))
        ylabel('q')
        
        
        subplot(412)
        plot(Top_x(3:end-2), Top_z(3:end-2))
        ylabel('Amplitude [m]')
        
        
        subplot(413)
        plot(Top_x(3:end-2), Top_Vz(3:end-2))
        ylabel('Vz [m/s]')
        
        
        subplot(414)
        plot(Top_x(2:end-1), Txx/1e6)
        ylabel('Txx [MPa]')
        
        
        
    end
    
    % Create breakpoint file if required
    Save_BreakpointFile;
    
    % Save filename if required
    if mod(itime,20)==0
        SaveFile(itime, PARTICLES, NUMERICS,MESH,INTP_PROPS,CHAR,BC, time);
    end
    
    
    end_cpu         = cputime;
    time_timestep    = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs; dt=',num2str(dt*CHAR.Time/CHAR.SecYear/1e3),' kyrs'])
    disp(' ')
    
    
    
    
    % Save analysis data for plume-lithos runs
    VEL_x   =   MESH.VEL(1,:);
    VEL_z   =   MESH.VEL(2,:);
    X       =   MESH.NODES(1,:);Z=MESH.NODES(2,:);
    X2d     =   X(MESH.RegularGridNumber)*CHAR.Length;
    Z2d     =   Z(MESH.RegularGridNumber)*CHAR.Length;
    
    Vx2d    =   VEL_x(MESH.RegularGridNumber)*CHAR.Velocity;
    Vz2d    =   VEL_z(MESH.RegularGridNumber)*CHAR.Velocity;
    
    Sxx_2d  =   MESH.HistVar.Txx(MESH.RegularGridNumber)*CHAR.Stress;       % Horizontal stress
    
    Z   = MESH.NODES(2,:);
    Z2d = Z(MESH.RegularGridNumber)*CHAR.Length/1e3;
    T2nd = MESH.CompVar.T2nd(MESH.RegularGridNumber)*CHAR.Stress;
    Tyy_2d = MESH.HistVar.Tyy(MESH.RegularGridNumber)*CHAR.Stress;
    Txx_2d = MESH.HistVar.Txx(MESH.RegularGridNumber)*CHAR.Stress;
    
    
    Top_x = X2d(end,:);
    Top_z = Z2d(end,:);
    
    Top_Vx = Vx2d(end,:);
    Top_Vz = Vz2d(end,:);
    
    
    ANALA.x     =   Top_x;
    ANALA.z     =   Top_z;
    ANALA.Vx    =   Top_Vx;
    ANALA.Vz    =   Top_Vz;
    ANALA.eta   =   ViscosityLithosphere;
    ANALA.eta_m =   ViscosityMantle;
    ANALA.drho  =   100;
    ANALA.R     =   RadiusPlume;
    ANALA.d     =   DepthPlume;
    ANALA.dt    =   dt;
    ANALA.SecYear = SecYear;
    ANALA.h     =   ThicknessLithosphere;
    ANALA.T2nd  =   T2nd(end,2:end-1);              % Second invariant of stress
    ANALA.Txx   =   Txx_2d(end,2:end-1);            % Txx
    
    
    if itime==1
        ANALA0 = ANALA
    end
    
    
    %% Advect particles and mesh - mesh is recreated every timestep
    PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*dt;
    PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*dt;
    MESH.NODES      =   MESH.NODES  + MESH.VEL*dt;
    
    time            =   time+dt;
    
    
    MaxTopoTime(itime)          =    max(MESH.NODES(2,:))*CHAR.Length;          % in m
    Time_vec(itime)             =   time*CHAR.Time/CHAR.SecYear/1e6;            % in Myrs
    
    max(MESH.NODES(2,:))*CHAR.Length
    
    
    
    
    if  MaxTopoTime(itime)>50
       break; 
    end
    
    
    
    %% Compute new timestep
    %     dt                          =   dt*1.25;
    %     dx_min                      =   min(MESH.dx_min, MESH.dz_min);
    %     CourantFactor               =   5;
    %     MaximumSurfaceMotionMeters  =   25;    % maximum change in surface topography per timestep
    %     dt_courant                  =   CourantFactor*dx_min/max(abs(MESH.VEL(:)));
    %     dt                          =   min([dt NUMERICS.dt_max dt_courant]);                                  % ensure it is not larger than a given maximum
    %
end


fname = ['Eta',num2str(log10(ViscosityLithosphere)),'_EtaM',num2str(log10(ViscosityMantle)),'_ThicknessLithos',num2str(ThicknessLithosphere/1e3)]


eval(['save ',fname,'  ANALA0  ANALA itime'])



max_Stress_MPa = round(max(T2nd(end,2:end-1)/1e6))
max_Stress_Txx = (max(Txx(end,2:end-1)/1e6))



% fntsize = 20;
%
% figure(222), clf
% % Systematic results of max. Stress:
% %
% % % eta_l = 1e22, rardiusPlume=50km, PlumeDepth=300km
% Eta_Lith                =   1e22;
% Thickness_Lith_km(1,:) 	=   [15     25      50      100     150];
% Max_T2nd_MPa(1,:)      	=   [302    316     271     174     121];
%
%
% Eta_Lith                =   1e23;
% Thickness_Lith_km(2,:)  =   [15     25      50      100     150];
% Max_T2nd_MPa(2,:)       =   [1180   1029    672     343     222];
%
% Eta_Lith                 =   1e24;
% Thickness_Lith_km(3,:)   =   [15     25      50      100     150];
% Max_T2nd_MPa(3,:)        =   [3244   2440    1329    656     394];
%
% Eta_Lith                 =   1e25;
% Thickness_Lith_km(4,:)   =   [15     25      50      100     150];
% Max_T2nd_MPa(4,:)        =   [6816   4794    2548    935     468];
%
% semilogy(Thickness_Lith_km',Max_T2nd_MPa')
%
% set(gca,'XTick',[15 25 50 100 150])
% axis tight
% set(gca,'YTickLabel',[200 500 1000 2000 3000 4000 5000],'YTick',[200 500 1000 2000 3000 4000 5000])
%
% set(gca,'FontSize',fntsize)
%
% xlabel('Lithosphere Thickness [km]','FontSize',fntsize)
% ylabel('Maximum stress (T2nd) [MPa]','FontSize',fntsize)
% legend('\eta = 10^{22} Pas','\eta = 10^{23} Pas','\eta = 10^{24} Pas','\eta = 10^{25} Pas')
%
%
%

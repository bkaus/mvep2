???%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This is how you compile Mvep2 on Mogon

% $Id: compile_Mvep2_Mogon.txt 5421 0000-01-01 00:02:35 cnorris $

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
INFO: This is an instruction on how to compile and execute mvep2 on Mogon. Mogon is a machine that does not provide matlab interactively or as a standalone application.
This means if one wants to execute his simulation on Mogon the matlab script needs to be compiled as standalone every time the job gets submitted.
Before that mvep2 needs to be 'installed' on Mogon which is explained below.
IMPORTANT: Steps 1) - 7) only need to be done onces, while 8) - 13) must be checked and updated for every simulation.



1) Get the mutils and the SOURCE_CODE as well as your input file into a directory in mogon.



2) load module matlab by 'module load software/matlab/R2013a'



3) Go to mutils-0.*/SuiteSparse and change line 42 in suitesparse_install to:

	http://faculty.cse.tamu.edu/davis/SuiteSparse/SuiteSparse-4.2.1.tar.gz

	to get the correct download URL.



4) go back to mutils-0.* (cd ..) and start matlab (type in 'matlab -singleCompThread -nodisplay -nosplash')



5) type in 'clean' and then 'install' .. wait until installation is done.

	Optimally it should give you something like that:







mutils available

 

 

--------------------------------------------------------------------

Paths required by mutils have been added to your MATLAB environment.

You need to either save the path from menu File->Set Path,

or add the following lines to your code whenever you want to use

mutils:

 

    addpath('/gpfs/fs1/home/cnorris/test/mutils-0.4')

    addpath('/gpfs/fs1/home/cnorris/test/mutils-0.4/triangle')

    addpath('/gpfs/fs1/home/cnorris/test/mutils-0.4/SuiteSparse')

    addpath('/gpfs/fs1/home/cnorris/test/mutils-0.4/mutils')

    addpath('/gpfs/fs1/home/cnorris/test/mutils-0.4/mutils/quadtree')

    addpath('/gpfs/fs1/home/cnorris/test/mutils-0.4/mutils/libutils')

    addpath('/gpfs/fs1/home/cnorris/test/mutils-0.4/mutils/libmatlab')

    addpath('/gpfs/fs1/home/cnorris/test/mutils-0.4/mutils/interp')

    addpath('/gpfs/fs1/home/cnorris/test/mutils-0.4/mutils/reorder')

    addpath('/gpfs/fs1/home/cnorris/test/mutils-0.4/mutils/sparse')







6) Close matlab.



7) go to SOURCE_CODE/KDTREE/kdtree_alg and edit the Makefile:

	a) change line 5 (MATDIR = /usr/local/MATLAB/R2014b) to the path to your matlab executable this can usually be find by going to a terminal and typing 'which matlab'. Should be something like 

			/cluster/Lic_Apps/Matlab/R2013a/ (usually */bin/matlab must be removed from the path obtained by 'which'), HINT: follow the path until the is a subdirectory called 'extern'.

	b) type 'make' optimally you should get something like this:







g++ -Wall -O3 -fomit-frame-pointer -mtune=opteron -msse -msse2 -fPIC  -Isrc -I/usr/local/MATLAB/R2014b/extern/include -c src/kdtree_create.cpp -o kdtree_create.o

g++ -Wall -O3 -fomit-frame-pointer -mtune=opteron -msse -msse2 -fPIC  -Isrc -I/usr/local/MATLAB/R2014b/extern/include -c src/kdtree.cpp -o kdtree.o

ln -s @kdtree kdtree

g++ -Wall -O3 -fomit-frame-pointer -mtune=opteron -msse -msse2 -fPIC  -L/usr/local/MATLAB/R2014b/bin/glnxa64 -lmat -lmex -lmx -lm -shared kdtree_create.o kdtree.o -o kdtree/kdtree.mexa64

rm kdtree

g++ -Wall -O3 -fomit-frame-pointer -mtune=opteron -msse -msse2 -fPIC  -Isrc -I/usr/local/MATLAB/R2014b/extern/include -c src/kdtree_closestpoint.cpp -o kdtree_closestpoint.o

ln -s @kdtree kdtree

g++ -Wall -O3 -fomit-frame-pointer -mtune=opteron -msse -msse2 -fPIC  -L/usr/local/MATLAB/R2014b/bin/glnxa64 -lmat -lmex -lmx -lm -shared kdtree_closestpoint.o kdtree.o -o kdtree/		kdtree_closestpoint.mexa64 rm kdtree

g++ -Wall -O3 -fomit-frame-pointer -mtune=opteron -msse -msse2 -fPIC  -Isrc -I/usr/local/MATLAB/R2014b/extern/include -c src/kdtree_range.cpp -o kdtree_range.o

ln -s @kdtree kdtree

g++ -Wall -O3 -fomit-frame-pointer -mtune=opteron -msse -msse2 -fPIC  -L/usr/local/MATLAB/R2014b/bin/glnxa64 -lmat -lmex -lmx -lm -shared kdtree_range.o kdtree.o -o kdtree/kdtree_range.mexa64

rm kdtree

rm kdtree_range.o kdtree_closestpoint.o









8) Please, add all pathes required to all used functions and scripts in your input script (Usually: 'AddDirectories.m')
IMPORTANT HERE: put an if condition around all the pathes (or the AddDirectories call):
if (~isdeployed)
	addpath('path')
end



9) edit your submit script to compile your code on a node (can also do this on the login node)

	example script (needs to be edited, especially the -a <PathToMutils> & -a <PathToSOURCECODE> variable; as well as the FILENAME variable):







#!/bin/bash



# Store working directory to be safe

SAVEDPWD=`pwd`

FILENAME=ex_FallingBlock_Dimensional



# We define a bash function to do the cleaning when the signal is caught

cleanup(){

    cp /jobdir/${LSB_JOBID}/*$FILENAME.sh ${SAVEDPWD}/

    cp /jobdir/${LSB_JOBID}/$FILENAME ${SAVEDPWD}/

    exit 0

}



# Register the cleanup function when SIGUSR2 is sent, ten minutes before the job gets killed

trap 'cleanup' SIGUSR2



# Copy input file

cp ${SAVEDPWD}/$FILENAME.m /jobdir/${LSB_JOBID}/



# Go to jobdir and start the program

cd /jobdir/${LSB_JOBID}






mcc -m -R -singleCompThread $FILENAME.m -a /home/cnorris/test/mutils-0.4 -a /home/cnorris/test/SOURCE_CODE
 



# Call the cleanup function when everything went fine

cleanup







10) If 9) was succesful you should have a new file called run_<FILENAME>.sh. This script is now used to execute your matlab script. To do so you need to include the path to the matlab MCRroot directory on mogon
(usually: /cluster/Lic_Apps/Matlab/R2013a) into your submit script from 11)



11) edit your actual submit script to the node with the compiled code:

	example file:







#!/bin/bash



# enable your environment, which will use .bashrc configuration in your home directory

#BSUB -L /bin/bash



# the name of your job showing on the queue system

#SUB -J mogon_test



# the following BSUB line specify the queue that you will use,

#BSUB -q short



# Execution time

#BSUB -W 00:05



# Memory reservation

#BSUB -app Reserve3G



# the system output and error message output, %J will show as your jobID

#BSUB -o %J.out BSUB -e %J.err



# the CPU number that you will collect

#BSUB -n 1

# Give a close-by node splitting
#BSUB -R "affinity[core(2,same=numa):cpubind=numa:membind=localonly]"



#enter your working directory

cd /home/cnorris/matlab_mogon_test/mvep_test/mvep2



# Finally, Start the program

./run_ex_FallingBlock_Dimensional.sh /cluster/Lic_Apps/Matlab/R2013a









12) IMPORTANT: Make sure that the submission files are executables (To check: 'ls -l' -> -rwxr--r-- 1 cnorris geodeform   684 11. Apr 18:14 submit_matlab_mogon.sh)

   To change the rights use 'chmod u+x submit_matlab_mogon.sh'





13) Enjoy!!







